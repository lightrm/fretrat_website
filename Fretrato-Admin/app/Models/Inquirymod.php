<?php namespace App\Models;

class Usermod extends BaseModel
{
	// protected $DBGroup = 'default';
	protected $table      = 'inquiry';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'email', 'about', 'contact_no', 'message'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function getInquiry($id = null, $paging = false, $additionalWhere = []) {
        $this->select("inquiry.*, properties.*,
            inquiry.id AS inquiry_id,
            inquiry.name AS inquiry_name,
            inquiry.about AS inquiry_about,
            properties.name AS property_name,
            properties.img_session AS property_imgSession,
            properties.about AS property_about,
            (SELECT name FROM main_category WHERE main_category.id = properties.property_type LIMIT 1) as mainCatName, 
            (SELECT name FROM sub_category WHERE sub_category.id = properties.subcategory LIMIT 1) as subCatName, 
            (SELECT id FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_id, 
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail, 
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_dir"
        );
        $this->join('properties', 'properties.id = property_id', 'left');
        $this->where('inquiry.deleted_at', NULL);

        if( !empty($additionalWhere) ){
            $this->where($additionalWhere);
        }

        if( !empty($id) ) {
            $this->where('inquiry.id', $id);
        } else {

            if( $search = \App\Libraries\Universallib::getGet("search") ){
                $search = mb_strtolower($search);
                $this->where("( 
                    CASE
                        WHEN ( IFNULL( (SELECT SUM(id) FROM inquiry WHERE id = CAST('{$search}' AS UNSIGNED)), 0) != 0 ) THEN inquiry.id LIKE CAST('{$search}' AS UNSIGNED)
                        ELSE ( 
                            lower(properties.name)              LIKE '%{$search}%' OR 
                            lower(properties.description)       LIKE '%{$search}%' OR
                            lower(properties.street)            LIKE '%{$search}%' OR
                            lower(properties.city)              LIKE '%{$search}%' OR
                            lower(properties.barangay)          LIKE '%{$search}%' OR
                            lower(properties.province)          LIKE '%{$search}%' OR
                            lower(properties.country)           LIKE '%{$search}%' OR
                            lower(properties.location)          LIKE '%{$search}%' OR
                            lower(properties.users.name)        LIKE '%{$search}%' OR
                            lower(properties.username)          LIKE '%{$search}%'
                        )
                    END
                )");
            }

            $this->orderBy("inquiry.created_at", "DESC");

            if( !$paging ) {
                if( $page = \App\Libraries\Universallib::getGet("page") ){
                    $CountPerPage = 9;
                    $this->limit($CountPerPage, (($page-1) * $CountPerPage));
                }
            }
        }

        return $paging? $this->countAllResults():(!empty($id)? $this->get()->getRow(1, 'array'):$this->get()->getResult('array'));
    }


    public function getNumberofInquiry() {
        $get = $this->db->query("SELECT
            (
                SELECT COUNT(inquiry.id) FROM inquiry LEFT JOIN properties ON properties.id = property_id WHERE properties.offer_type = 'Rental' AND inquiry.deleted_at IS NULL
            ) AS Rental_COUNT, 
            (
                SELECT COUNT(inquiry.id) FROM inquiry LEFT JOIN properties ON properties.id = property_id WHERE properties.offer_type = 'Resale' AND inquiry.deleted_at IS NULL
            ) AS Resale_COUNT");
        
        return $get->getRow(1, 'array');
    }

}