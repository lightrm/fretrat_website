<?php namespace App\Models;

// use CodeIginter\Database\ConnectionInterface;
use CodeIgniter\Model;

class BaseModel extends Model
{
	protected $db;
    
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

	protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

	public function __construct()
	{
		$this->db = db_connect('default');
	}

	public function get_LastQuery($stringed = true) {
		// ->getCompiledSelect()
		return $stringed?$this->db->getLastQuery()->getQuery():$this->db->getLastQuery();
	}

}