<?php namespace App\Models;

class Usermod extends BaseModel
{
	// protected $DBGroup = 'default';
	protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'username', 'email', 'password', 'contact_no', 'about', 'address', 'role', 'gender', 'toogleInfo', 'active', 'img_session', 'order_ranking', 'confirmation_code', 'facebook_link', 'facebook_toogle', 'twitter_link', 'twitter_toogle', 'linkedin_link', 'linkedin_toogle', 'telegram_link', 'telegram_toogle', 'viber_link', 'viber_toogle', 'whatsup_link', 'whatsup_toogle'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function request_login($username, $password) {
        $result = array("error" => true, "msg" => "Something went wrong!");

        $this->select("*, 
            (SELECT filename FROM photos WHERE session = img_session ORDER BY created_at DESC LIMIT 1) as user_avatar, 
            (SELECT directory FROM photos WHERE session = img_session ORDER BY created_at DESC LIMIT 1) as user_dir,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $this->where('(lower(email) = "'.mb_strtolower($username).'" OR lower(username) = "'.mb_strtolower($username).'")');
        $this->where('role <= ', 2);
        $result['user'] = $this->get()->getRow(1, 'array');
        if( !empty($result['user']) ) {

            if( password_verify($password, $result['user']['password']) ) {
                $result['error'] = false;
            } else {
                $result['msg'] = "Password does not match in our system.";
            }

        } else {
            $result['msg'] = "Username / Email does not exist.";
        }


        return $result;
    }

    public function getUsers_with_img($id = null, $paging = false) {
        $this->select("*, 
            (SELECT filename FROM photos WHERE session = img_session ORDER BY created_at DESC LIMIT 1) as user_avatar, 
            (SELECT directory FROM photos WHERE session = img_session ORDER BY created_at DESC LIMIT 1) as user_dir,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $this->where('deleted_at', NULL);
        if( !empty($id) ) {
            $this->where('id', $id);
        } else {
            if( $role = \App\Libraries\Universallib::getGet("role") ){
                $this->where('(SELECT name FROM roles WHERE roles.id = role)', mb_strtolower($role));
            }

            if( $search = \App\Libraries\Universallib::getGet("search") ){
                $search = mb_strtolower($search);
                $this->where("( 
                    CASE
                        WHEN ( IFNULL( (SELECT SUM(id) FROM users WHERE id = CAST('{$search}' AS UNSIGNED)), 0) != 0 ) THEN id LIKE CAST('{$search}' AS UNSIGNED)
                        ELSE ( 
                            lower(name)         LIKE '%{$search}%' OR 
                            lower(email)        LIKE '%{$search}%' OR
                            lower(contact_no)   LIKE '%{$search}%' OR
                            lower(about)        LIKE '%{$search}%' OR
                            lower(address)      LIKE '%{$search}%'
                        )
                    END
                )");
            }

            $this->orderBy("created_at", "DESC");

            if( !$paging ) {
                if( $page = \App\Libraries\Universallib::getGet("page") ){
                    $CountPerPage = 9;
                    $this->limit($CountPerPage, (($page-1) * $CountPerPage));
                }
            }
        }
        
        return $paging? $this->countAllResults():(!empty($id)? $this->get()->getRow(1, 'array'):$this->get()->getResult('array'));
    }

    public function getRoles()
    {
        return $this->db->table("roles")->get()->getResult('array');
    }

	public function addRole($data)
    {
        $data[$this->createdField] = date("y-m-d h-i-s");
        $data[$this->updatedField] = date("y-m-d h-i-s");
        return  $this->db->table("roles")->insert($data);
    }

    public function editRole($id, $data)
    {
        $data[$this->updatedField] = date("y-m-d h-i-s");
        return  $this->db->table("roles")->where('id', $id)->update($data);
    }

    public function deleteRole($id)
    {
        return  $this->db->table("roles")->where('id', $id)->delete();
    }
}