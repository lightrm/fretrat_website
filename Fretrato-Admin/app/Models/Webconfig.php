<?php namespace App\Models;

class Webconfig extends BaseModel
{
	// protected $DBGroup = 'default';
	protected $table      = 'website_detail';
    protected $primaryKey = 'type';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['type', 'value', 'toogle', 'source'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;



}