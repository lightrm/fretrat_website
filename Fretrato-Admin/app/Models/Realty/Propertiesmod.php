<?php namespace App\Models\Realty;

use \App\Models\BaseModel;

class Propertiesmod extends BaseModel
{
	// protected $DBGroup = 'default';
	protected $table      = 'properties';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'name', 'about', 'description', 'street', 'city', 'barangay', 'province', 'country', 'location', 'bedroom', 'bathroom', 'floor_area', 'land_area', 'floor_level', 'furnish_type', 'condition', 'offer_type', 'property_type', 'dateFinished', 'stayMonth', 'stayYear', 'owner_name', 'owner_contact', 'owner_email', 'agent_lastupdate',  'parking', 'subcategory', 'price', 'featured_image_id', 'toogleFeature', 'endFeature', 'property_vid_url', 'is_direct', 'is_owner', 'published', 'taken', 'contract_start', 'contract_end', 'status', 'meta_keywords', 'slug', 'img_session', 'deleted_at'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;


    public function getProperty_with_thumb($id = null, $paging = false,  $deleted = false, $additionalWhere = []) {
        $this->select("properties.*, users.*,
            properties.id AS propery_id,
            properties.name AS propery_name,
            properties.img_session AS propery_imgSession,
            properties.about AS propery_about,
            users.id AS userr_id,
            users.name AS user_name,
            users.img_session AS userr_imgSession,
            (SELECT name FROM main_category WHERE main_category.id = properties.property_type LIMIT 1) as mainCatName, 
            (SELECT name FROM sub_category WHERE sub_category.id = properties.subcategory LIMIT 1) as subCatName, 
            (SELECT filename FROM photos WHERE session = users.img_session ORDER BY created_at DESC LIMIT 1) as user_avatar, 
            (SELECT directory FROM photos WHERE session = users.img_session ORDER BY created_at DESC LIMIT 1) as user_dir,
            (SELECT name FROM roles WHERE roles.id = (SELECT role FROM users WHERE users.id = user_id LIMIT 1)) as user_Role,
            (SELECT id FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_id, 
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail, 
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_dir,
            (SELECT id FROM photos WHERE session = properties.img_session AND directory LIKE '%Slider%' ORDER BY created_at DESC LIMIT 1) as slider_id, 
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Slider%' ORDER BY created_at DESC LIMIT 1) as slider, 
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Slider%' ORDER BY created_at DESC LIMIT 1) as slider_dir,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $this->join('users', 'users.id = user_id', 'left');
        

        if( !empty($additionalWhere) ){
            $this->where($additionalWhere);
        }

        if( !$deleted )
            $this->where('properties.deleted_at', null);
        else 
            $this->where('properties.deleted_at IS NOT', null);

        if( !empty($id) ) {
            $this->where('properties.id', $id);
        } else {
            if( $type = \App\Libraries\Universallib::getGet("type") ){
                $this->where('offer_type', mb_strtolower($type));
            }

            if( $type = \App\Libraries\Universallib::getGet("user") ){
                $this->where('user_id', intval(mb_strtolower($type)));
            }

            if( $search = \App\Libraries\Universallib::getGet("search") ){
                $search = mb_strtolower($search);
                $this->where("( 
                    CASE
                        WHEN ( IFNULL( (SELECT SUM(id) FROM properties WHERE id = CAST('{$search}' AS UNSIGNED)), 0) != 0 ) THEN properties.id LIKE CAST('{$search}' AS UNSIGNED)
                        ELSE ( 
                            lower(properties.name)  LIKE '%{$search}%' OR 
                            lower(description)     LIKE '%{$search}%' OR
                            lower(street)          LIKE '%{$search}%' OR
                            lower(city)            LIKE '%{$search}%' OR
                            lower(barangay)        LIKE '%{$search}%' OR
                            lower(province)        LIKE '%{$search}%' OR
                            lower(country)         LIKE '%{$search}%' OR
                            lower(location)        LIKE '%{$search}%' OR
                            lower(users.name)      LIKE '%{$search}%' OR
                            lower(username)        LIKE '%{$search}%'
                        )
                    END
                )");
            }

            $this->orderBy("properties.created_at", "DESC");

            if( !$paging ) {
                if( $page = \App\Libraries\Universallib::getGet("page") ){
                    $CountPerPage = 9;
                    $this->limit($CountPerPage, (($page-1) * $CountPerPage));
                }
            }
        }

        return $paging? $this->countAllResults():(!empty($id)? $this->get()->getRow(1, 'array'):$this->get()->getResult('array'));
    }

    public function getPropertyApproval($paging = false, $additionalWhere = []) {
        $builder = $this->db->table("property_approval");
        $builder->select("property_approval.*,
            property_approval.id AS approval_id,
            properties.name AS propery_name,
            properties.img_session AS propery_imgSession,
            users.name AS user_name,
            users.img_session AS userr_imgSession,
            (SELECT filename FROM photos WHERE session = users.img_session ORDER BY created_at DESC LIMIT 1) as user_avatar, 
            (SELECT directory FROM photos WHERE session = users.img_session ORDER BY created_at DESC LIMIT 1) as user_dir,
            (SELECT name FROM roles WHERE roles.id = (SELECT role FROM users WHERE users.id = property_approval.user_id LIMIT 1)) as user_Role,
            (SELECT id FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_id, 
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail, 
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_dir,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $builder->join('properties', 'properties.id = property_id', 'left');
        $builder->join('users', 'users.id = property_approval.user_id', 'left');

        $builder->where("action", 0);
        
        $builder->orderBy("property_approval.id", "DESC");
        if( !$paging ) {
            if( $page = \App\Libraries\Universallib::getGet("page") ){
                $CountPerPage = 9;
                $builder->limit($CountPerPage, (($page-1) * $CountPerPage));
            }
        }

        return $paging? $builder->countAllResults():(!empty($id)? $builder->get()->getRow(1, 'array'):$builder->get()->getResult('array'));
    }

    public function change_status_approval($id, $action)
    {

        $data = $action? ["action" => 1, "action_made" => 1,"action_made_str" => "Approve"]:["action" => 1, "action_made" => 0,"action_made_str" => "Disapprove"];
        $builder = $this->db->table("property_approval");

        $builder->update($data, ["id" => $id]);
    }


    // Property Category
    public function get_category ( $paging = false, $pager = false, $additional = [])
    {
        $builder = $this->db->table("main_category");

        if( isset($additional['order_by']) ) {
            foreach ($additional['order_by'] as $key => $value)
                $builder->orderBy($key, $value);

        } else 
            $builder->orderBy("id", "DESC");

        if( !$paging ) {
            if( $pager && $page = \App\Libraries\Universallib::getGet("page") ){
                $CountPerPage = 10;
                $builder->limit($CountPerPage, (($page-1) * $CountPerPage));
            }
        }


        return $paging? $builder->countAllResults():$builder->get()->getResult('array');
    }

    public function add_category ($data)
    {
        $builder = $this->db->table("main_category");

        return $builder->insert($data);
    }

    public function edit_category ($id, $data)
    {
        $builder = $this->db->table("main_category");

        return $builder->update($data, ['id' => $id]);
    }

    public function delete_category ($id)
    {
        $builder = $this->db->table("main_category");

        return $builder->where("id", $id)->delete();
    }



    public function get_sub_category ( $paging = false, $pager = false)
    {
        $builder = $this->db->table("sub_category");

        $builder->orderBy("id", "DESC");

        if( !$paging ) {
            if( $pager && $page = \App\Libraries\Universallib::getGet("page") ){
                $CountPerPage = 10;
                $builder->limit($CountPerPage, (($page-1) * $CountPerPage));
            }
        }


        return $paging? $builder->countAllResults():$builder->get()->getResult('array');
    }

    public function add_sub_category ($data)
    {
        $builder = $this->db->table("sub_category");

        return $builder->insert($data);
    }

    public function edit_sub_category ($id, $data)
    {
        $builder = $this->db->table("sub_category");

        return $builder->update($data, ['id' => $id]);
    }

    public function delete_sub_category ($id)
    {
        $builder = $this->db->table("sub_category");

        return $builder->where("id", $id)->delete();
    }



    public function get_link_category ( $paging = false, $pager = false )
    {
        $builder = $this->db->table("main_sub_categories msCat");
        $builder->select("*,
            (SELECT name FROM main_category WHERE main_category.id = msCat.main_categ_id) AS parentName, 
            (SELECT name FROM sub_category WHERE sub_category.id = msCat.sub_categ_id) AS subName");

        $builder->orderBy("parentName", "ASC");
        $builder->orderBy("subName", "ASC");

        if( !$paging ) {
            if( $pager && $page = \App\Libraries\Universallib::getGet("page") ){
                $CountPerPage = 10;
                $builder->limit($CountPerPage, (($page-1) * $CountPerPage));
            }
        }


        return $paging? $builder->countAllResults():$builder->get()->getResult('array');
    }

    public function add_link_category ($data)
    {
        $builder = $this->db->table("main_sub_categories");

        return $builder->insert($data);
    }

    public function edit_link_category ($id, $data)
    {
        $builder = $this->db->table("main_sub_categories");

        return $builder->update($data, ['id' => $id]);
    }

    public function delete_link_category ($id)
    {
        $builder = $this->db->table("main_sub_categories");

        return $builder->where("id", $id)->delete();
    }
    // End Property Category

}