<?php namespace App\Models\Realty;

use \App\Models\BaseModel;

class Amenitiesmod extends BaseModel
{
	// protected $DBGroup = 'default';
	protected $table      = 'amenities';
    protected $primaryKey = 'type';

    protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['amenity_name', 'amenity_description'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;


    function load_amenity($table = "amenity_properties", $paging = false)
    {
        $builder = $this->db->table($table);

        $builder->orderBy("created_at", "DESC");

        if( $search = \App\Libraries\Universallib::getGet("search") ) {
            $search = mb_strtolower($search);
            $builder->like("lower(amenity_name)", $search);
        }

        if(  !\App\Libraries\Universallib::getGet("no_pager") ) {
            if( !$paging ) {
                if( $page = \App\Libraries\Universallib::getGet("page") ){
                    $CountPerPage = 15;
                    $builder->limit($CountPerPage, (($page-1) * $CountPerPage));
                }
            }
        }

        return $paging? $builder->countAllResults():$builder->get()->getResult('array');
    } 

    function addAmenity($data, $table = "amenity_properties")
    {
        if( !isset($data['created_at']) ) {
            $data['created_at'] = date("Y-m-d H:i:s");
        }

        return $this->db->table($table)->insert($data);
    }

    function EditAmenity($id, $data, $table = "amenity_properties")
    {
        if( !isset($data['updated_at']) ) {
            $data['updated_at'] = date("Y-m-d H:i:s");
        }

        return $this->db->table($table)->where("id", $id)->update($data);
    }

    function deleteAmenity($id, $table = "amenity_properties")
    {
        return $this->db->table($table)->delete(['id' => $id]);
    }

    function loadAmenity_link($id, $table = "amenity_property")
    {
        $builder = $this->db->table($table);
        ($table == "amenity_property")? $builder->where("property_id", $id):$builder->where("hotel_id", $id);
        return $builder->get()->getResult('array');
    }

    function addAmenity_link($data, $table = "amenity_property")
    {
        return (!isset($data['property_id']) && !isset($data['hotel_id']))?$this->db->table($table)->insertBatch($data):$this->db->table($table)->insert($data);
    }

    function deleteAmenity_link($id, $table = "amenity_property")
    {
        return ($table == "amenity_property")?$this->db->table($table)->where("property_id", $id)->delete():$this->db->table($table)->where("hotel_id", $id)->delete();
    }



}