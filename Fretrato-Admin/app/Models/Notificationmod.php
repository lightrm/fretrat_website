<?php namespace App\Models;

class Notificationmod extends BaseModel
{
	protected $table      = 'notification';
    protected $primaryKey = 'id';

    // protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['icon', 'user_id', 'title', 'description', 'url', 'unread'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function count_unread($id)
    {
        $this->where("user_id", $id);
        $this->where("unread", 1);
        return $this->countAllResults();
    }

}