<?php namespace App\Models;

class Blogsmod extends BaseModel
{
	// protected $DBGroup = 'default';
	protected $table      = 'blogs';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['user_id', 'type', 'title', 'body', 'img_session', 'slug', 'img_session', 'video_url', 'published'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function getBlog_with_thumb($id = null, $paging = false, $additionalWhere = []) {
        $this->select("blogs.*, users.*,
            blogs.id AS blog_id,
            blogs.title AS blog_title,
            blogs.img_session AS blog_imgSession,
            users.id AS userr_id,
            users.name AS user_name,
            users.img_session AS userr_imgSession,
            (SELECT filename FROM photos WHERE session = users.img_session ORDER BY created_at DESC LIMIT 1) as user_avatar, 
            (SELECT directory FROM photos WHERE session = users.img_session ORDER BY created_at DESC LIMIT 1) as user_dir,
            (SELECT name FROM roles WHERE roles.id = (SELECT role FROM users WHERE users.id = user_id LIMIT 1)) as user_Role,
            (SELECT id FROM photos WHERE session = blogs.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_id, 
            (SELECT filename FROM photos WHERE session = blogs.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail, 
            (SELECT directory FROM photos WHERE session = blogs.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) as thumbnail_dir,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $this->join('users', 'users.id = user_id', 'right');

        if( !empty($additionalWhere) ){
            $this->where($additionalWhere);
        }

        if( !empty($id) ) {
            $this->where('blogs.id', $id);
        } else {
            if( $type = \App\Libraries\Universallib::getGet("type") ){
                $this->where('type', mb_strtolower($type));
            }

            if( $type = \App\Libraries\Universallib::getGet("user") ){
                $this->where('user_id', intval(mb_strtolower($type)));
            }

            if( $search = \App\Libraries\Universallib::getGet("search") ){
                $search = mb_strtolower($search);
                $this->where("( 
                    CASE
                        WHEN ( IFNULL( (SELECT SUM(id) FROM blogs WHERE id = CAST('{$search}' AS UNSIGNED)), 0) != 0 ) THEN blogs.id LIKE CAST('{$search}' AS UNSIGNED)
                        ELSE ( 
                            lower(blogs.title)      LIKE '%{$search}%' OR 
                            lower(blogs.body)       LIKE '%{$search}%' OR
                            lower(users.name)       LIKE '%{$search}%' OR
                            lower(username)         LIKE '%{$search}%'
                        )
                    END
                )");
            }

            $this->orderBy("blogs.created_at", "DESC");

            if( !$paging ) {
                if( $page = \App\Libraries\Universallib::getGet("page") ){
                    $CountPerPage = 9;
                    $this->limit($CountPerPage, (($page-1) * $CountPerPage));
                }
            }
        }

        return $paging? $this->countAllResults():(!empty($id)? $this->get()->getRow(1, 'array'):$this->get()->getResult('array'));
    }


    // Category Link
    public function loadCategory_link($id, $withData = false, $type="blog_id")
    {
        $builder = $this->db->table("blog_category_concat");

        if( $withData )
        {
            $builder->join('blogs', 'blogs.id = blog_id', 'right');
            $builder->join('blog_categories', 'blog_categories.id = category_id', 'right');
        }

        $builder->where($type, $id);
        return $builder->get()->getResult('array');
    }

    public function addCategory_link($data)
    {
        return $this->db->table("blog_category_concat")->insertBatch($data);
    }

    public function deleteCategory_link($id, $type="blog_id")
    {
        return $this->db->table("blog_category_concat")->where($type, $id)->delete();
    }




    // Category
    public function load_category($paging = false)
    {
        $builder = $this->db->table("blog_categories");

        $builder->orderBy("created_at", "DESC");

        if( $search = \App\Libraries\Universallib::getGet("search") ){
            $search = mb_strtolower($search);
            $builder->like("lower(amenity_name)", $search);
        }

        if( $type = \App\Libraries\Universallib::getGet("type") ){
            $type = mb_strtolower($type);
            $builder->where("type", $type);
        }

        if(  !\App\Libraries\Universallib::getGet("no_pager") ) {
            if( !$paging ) {
                if( $page = \App\Libraries\Universallib::getGet("page") ){
                    $CountPerPage = 15;
                    $builder->limit($CountPerPage, (($page-1) * $CountPerPage));
                }
            }
        }

        return $paging? $builder->countAllResults():$builder->get()->getResult('array');
    } 

    public function add_category($data)
    {
        if( !isset($data['created_at']) ) {
            $data['created_at'] = date("Y-m-d H:i:s");
        }

        return $this->db->table("blog_categories")->insert($data);
    }

    public function edit_category($id, $data)
    {
        if( !isset($data['updated_at']) ) {
            $data['updated_at'] = date("Y-m-d H:i:s");
        }

        return $this->db->table("blog_categories")->where("id", $id)->update($data);
    }

    public function delete_category($id)
    {
        return $this->db->table("blog_categories")->delete(['id' => $id]);
    }

}