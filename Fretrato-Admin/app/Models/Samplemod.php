<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

class Samplemod
{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function addRole()
    {
        // $this->db;
        $builder = $this->db->table("roles");
        return $builder->get()->getResult();
    }
}