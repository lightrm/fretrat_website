<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class MainFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        //Check if User is logged in

    	//remove Return if has Login System
		if( !session()->has("User") ) return redirect()->to("/login");	
        if( !session()->has(\App\Libraries\Roleslib::$role_sessName) ) \App\Libraries\Roleslib::RefreshRoles();


        if( $notif_id = \App\Libraries\Universallib::getGet("notif_id") ) {
            $notifactionmod = model('\App\Models\Notificationmod');
            $notifactionmod->update($notif_id, ['unread' => 0]);
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here


        echo $this->request->getGet("notif_id");
        exit;

        // if( $this->request->getGet("notif_id") )
    }
}