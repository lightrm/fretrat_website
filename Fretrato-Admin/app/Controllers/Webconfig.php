<?php namespace App\Controllers;

class webconfig extends BaseController
{

	private $webconfDir = "Web_configuration/";
	private $jsWCDir = "dist/js/webconfig/";
	private $reactWCDir = "reactjs/webconfig/";
	private $prefixUrl = "webconfig/";
	private $SocialMediaArr;
	private $webconmod;
	private $prop_cat;

	public function __construct() {
		$this->SocialMediaArr = array(
			"Facebook" => [
				"color" => "blue",
				"icon" 	=> "fab fa-facebook-square"
			],
			"Instagram" => [
				"color" => "orange",
				"icon" 	=> "fab fa-instagram-square"
			],
			"Twitter" => [
				"color" => "blue",
				"icon" 	=> "fab fa-twitter"
			],
			"LinkedIn" => [
				"color" => "blue",
				"icon" 	=> "fab fa-linkedin"
			],
			"Google Plus" => [
				"color" => "red",
				"icon" 	=> "fab fa-google-plus-g"
			],
			"Viber" => [
				"color" => "purple",
				"icon" 	=> "fab fa-viber"
			],
			"Wechat" => [
				"color" => "gray",
				"icon" 	=> "fab fa-weixin"
			],
			"Whatsup" => [
				"color" => "green",
				"icon" 	=> "fab fa-whatsapp-square"
			],
			"Telegram" => [
				"color" => "blue",
				"icon" 	=> "fab fa-telegram"
			],

		);

		$this->webconmod = model('\App\Models\Webconfig');
		$this->propertymod = model('\App\Models\Realty\Propertiesmod');
	}

	public function index()
	{
		$this->setup_page();

		return parent::basic_page("BlankPage");
	}

	public function home()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - Home";
		$this->headerData["breadCrumps"]['Home'] = base_url("webconfig/home");


		array_push($this->footerData['ftjs'], "/dist/js/diyplugin/editable_content.js");

		return parent::basic_page($this->webconfDir."Home");
	}

	public function contact_us()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - Contact Us";
		$this->headerData["breadCrumps"]['Contact Us'] = base_url("webconfig/contact_us");


		array_push(
			$this->footerData['ftjs'], 
			array(
				"dir" 	=> $this->reactWCDir."contact.js",
				"attr"	=> array(
					"type"	=> "text/babel"
				)
			)
		);

		return parent::basic_page($this->webconfDir."Contact");
	}

	public function about_us()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - About Us";
		$this->headerData["breadCrumps"]['About Us'] = base_url("webconfig/about_us");

		$webconfigDet = $this->webconmod->asArray()->find("About_Us_img_session");


		$imagemod = model("\App\Models\Imagemod");
		$fileDets = $imagemod->select("id, directory, filename")->where('session', $webconfigDet['value'])->asArray()->first();

		$bodyData['imageDets'] = $fileDets;

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");

		return parent::basic_page($this->webconfDir."About", $bodyData);
	}

	public function privacy_policies()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - Privacy Policies";
		$this->headerData["breadCrumps"]['Privacy Policies'] = base_url("webconfig/privacy_policies");

		$webconfigDet = $this->webconmod->asArray()->find("Privacy_Policy_img_session");


		$imagemod = model("\App\Models\Imagemod");
		$fileDets = $imagemod->select("id, directory, filename")->where('session', $webconfigDet['value'])->asArray()->first();

		$bodyData['imageDets'] = $fileDets;

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");

		return parent::basic_page($this->webconfDir."Privacy_policy", $bodyData);
	}

	public function terms_and_condition()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - Terms and Condition";
		$this->headerData["breadCrumps"]['Terms and Condition'] = base_url("webconfig/terms_and_condition");

		$webconfigDet = $this->webconmod->asArray()->find("Terms_and_Condition_img_session");


		$imagemod = model("\App\Models\Imagemod");
		$fileDets = $imagemod->select("id, directory, filename")->where('session', $webconfigDet['value'])->asArray()->first();

		$bodyData['imageDets'] = $fileDets;

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");

		return parent::basic_page($this->webconfDir."Terms_and_condition", $bodyData);
	}

	public function social_media()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - Social Media";
		$this->headerData["breadCrumps"]['Social Media'] = base_url("webconfig/social_media");
		

		foreach ($this->SocialMediaArr as $Name => $option) {
			$name = str_replace(" ", "", strtolower($Name));

			$optData = $this->webconmod->asArray()->find("{$name}_link");

			if( !empty($optData) ) {
				$this->SocialMediaArr[$Name]['value'] = $optData['value'];
				$this->SocialMediaArr[$Name]['toogle'] = $optData['toogle'];
			} else {
				$this->SocialMediaArr[$Name]['value'] = "";
				$this->SocialMediaArr[$Name]['toogle'] = false;
				$this->webconmod->insert(["type" => "{$name}_link"]);
			}
			

		}

		$bodyData['SocialMediaArr'] = $this->SocialMediaArr;


		return parent::basic_page($this->webconfDir."Socialmedia", $bodyData);
	}

	public function miscellaneous()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - Miscellaneous";
		$this->headerData["breadCrumps"]['Social Media'] = base_url("webconfig/miscellaneous");
		
		array_push($this->headerData['hdcss'], "plugins/datatables-bs4/css/dataTables.bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/jsgrid/jsgrid-theme.min.css");
		array_push($this->footerData['ftjs'], "plugins/datatables/jquery.dataTables.min.js");
		array_push($this->footerData['ftjs'], "plugins/datatables-bs4/js/dataTables.bootstrap4.min.js");
		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");


		$bodyData = [];

		return parent::basic_page($this->webconfDir."Miscellaneous", $bodyData);
	}

	public function meta()
	{
		$this->setup_page();
		$this->headerData["headerTitle"] = "Web Configuration - Miscellaneous";
		$this->headerData["breadCrumps"]['Social Media'] = base_url("webconfig/miscellaneous");
		
		array_push($this->headerData['hdcss'], "plugins/datatables-bs4/css/dataTables.bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/jsgrid/jsgrid-theme.min.css");
		array_push($this->footerData['ftjs'], "plugins/datatables/jquery.dataTables.min.js");
		array_push($this->footerData['ftjs'], "plugins/datatables-bs4/js/dataTables.bootstrap4.min.js");
		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");


		$bodyData = [];

		return parent::basic_page($this->webconfDir."Meta", $bodyData);
	}

	public function request(string $type_request)
	{

		/*if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}*/


		$turner = array();
		switch ($type_request) {
			case 'load_content':

				switch ($this->request->getGet("type")) {
					case 'home':



						foreach ($this->request->getPost("content") as $html_id) {
							$key = $html_id . "_html";
							$content = $this->webconmod->asArray()->find($key);
							if(!empty($content))
								$turner[$html_id] = $content['value'];
							else {
								$turner[$html_id] = null;
								$this->webconmod->insert(["type" => $key, "source" => "web_config"]);
							}
						}


						break;
					case 'contact':

						$content = $this->webconmod->asArray()->find("contact_us");
						
						if(!empty($content))
							$turner['content'] = $content['value'];
						else {
							$turner['content'] = null;
							$this->webconmod->insert(["type" => "contact_us", "source" => "web_config"]);
						}

						$address = $this->webconmod->asArray()->find("address_html");
						if(!empty($address))
							$turner['address'] = $address['value'];
						else {
							$turner['address'] = null;
							$this->webconmod->insert(["type" => "address_html", "source" => "web_config"]);
						}

						$email = $this->webconmod->asArray()->find("email_html");
						if(!empty($email))
							$turner['email'] = $email['value'];
						else {
							$turner['email'] = null;
							$this->webconmod->insert(["type" => "email_html", "source" => "web_config"]);
						}

						$contact = $this->webconmod->asArray()->find("contact_html");
						if(!empty($contact))
							$turner['contact'] = $contact['value'];
						else {
							$turner['contact'] = null;
							$this->webconmod->insert(["type" => "contact_html", "source" => "web_config"]);
						}

						$landline = $this->webconmod->asArray()->find("landline_html");
						if(!empty($landline))
							$turner['landline'] = $landline['value'];
						else {
							$turner['landline'] = null;
							$this->webconmod->insert(["type" => "landline_html", "source" => "web_config"]);
						}

						break;
					case 'about':

						$content = $this->webconmod->asArray()->find("about_us")['value'];

						$turner['content'] = $content;
						break;
					case 'privacy_policies':
						$content = $this->webconmod->asArray()->find("privacy_policies")['value'];

						$turner['content'] = $content;
						break;
					case 'terms_and_condition':
						$content = $this->webconmod->asArray()->find("terms_and_condition")['value'];

						$turner['content'] = $content;
						break;
					case 'home':
						$turner['content'] = $content;
						break;
					case 'socialmedia':
						$turner['content'] = $content;
						break;
					case 'miscellaneous':
						if( strtolower($this->request->getGet("subtype")) === "category" ){
							
							$sample_msh = array();
							foreach ($this->propertymod->get_category(false, true) as $row) {
								array_push($sample_msh, array(
									"SessionID" 	=> $row['id'],
									"Name" 			=> $row['name']
								));
							}


							$total_record = $this->propertymod->get_category(true);
							$turner['categoryList'] = $sample_msh;
							$turner['totalRecord'] = number_format($total_record);

							if ( !$this->request->getGet("no_pager") ) {

								$pager = \Config\Services::pager();
								$pager->setPath('/webconfig/miscellaneous');
								$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 10, $total_record, 'mini_pagination');

							}

						} elseif ( strtolower($this->request->getGet("subtype")) === "subcategory" ) {

							$sample_msh = array();
							foreach ($this->propertymod->get_sub_category(false, true) as $row) {
								array_push($sample_msh, array(
									"SessionID" 	=> $row['id'],
									"Name" 			=> $row['name']
								));
							}

							$total_record = $this->propertymod->get_sub_category(true);
							$turner['categoryList'] = $sample_msh;
							$turner['totalRecord'] = number_format($total_record);

							if ( !$this->request->getGet("no_pager") ) {

								$pager = \Config\Services::pager();
								$pager->setPath('/webconfig/miscellaneous');
								$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 10, $total_record, 'mini_pagination');

							}

						} elseif( strtolower($this->request->getGet("subtype")) === "linked" ) {
							$sample_msh = array();
							foreach ($this->propertymod->get_link_category(false, true) as $row) {
								array_push($sample_msh, array(
									"SessionID" 	=> $row['id'],
									"parentID" 		=> $row['main_categ_id'],
									"parentName" 	=> $row['parentName'],
									"childID" 		=> $row['sub_categ_id'],
									"childName" 	=> $row['subName']
								));
							}


							$total_record = $this->propertymod->get_link_category(true);
							$turner['categoryList'] = $sample_msh;
							$turner['totalRecord'] = number_format($total_record);

							if ( !$this->request->getGet("no_pager") ) {

								$pager = \Config\Services::pager();
								$pager->setPath('/webconfig/miscellaneous');
								$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 10, $total_record, 'mini_pagination');

							}
						} elseif( strtolower($this->request->getGet("subtype")) === "all_categ" ) {
							$sample_msh = array();
							foreach ($this->propertymod->get_category(false, false, ['order_by' => ['name' => 'ASC']]) as $row) {
								array_push($sample_msh, array(
									"SessionID" 	=> $row['id'],
									"Name" 			=> $row['name'],
								));
							}

							$turner['category'] = $sample_msh;

							$sample_msh = array();
							foreach ($this->propertymod->get_sub_category() as $row) {
								array_push($sample_msh, array(
									"SessionID" 	=> $row['id'],
									"Name" 			=> $row['name'],
								));
							}

							$turner['subcategory'] = $sample_msh;

							$sample_msh = array();
							foreach ($this->propertymod->get_link_category() as $row) {
								array_push($sample_msh, array(
									"SessionID" 	=> $row['id'],
									"parentID" 		=> $row['main_categ_id'],
									"parentName" 	=> $row['parentName'],
									"childID" 		=> $row['sub_categ_id'],
									"childName" 	=> $row['subName']
								));
							}

							$turner['linked'] = $sample_msh;
						}

						
						break;
					case 'meta':

						$title = $this->webconmod->asArray()->find("meta_title");
						
						if(!empty($title))
							$turner['title'] = $title['value'];
						else {
							$turner['title'] = null;
							$this->webconmod->insert(["type" => "meta_title", "source" => "web_config"]);
						}

						$description = $this->webconmod->asArray()->find("meta_description");
						if(!empty($description))
							$turner['description'] = $description['value'];
						else {
							$turner['description'] = null;
							$this->webconmod->insert(["type" => "meta_description", "source" => "web_config"]);
						}

						break;
					
					default: exit(json_encode("Invalid Request")); break;
				}

				

				

				break;
			case 'save_content':

				switch ($this->request->getGet("type")) {
					case 'home':
						$this->webconmod->save(['type' => ($this->request->getPost("content_type") . "_html"), 'value' => $this->request->getPost("content_val")]);
						break;
					case 'contact':

						if( $this->request->getPost("Content-about") ) {
							$this->webconmod->save(["type" => "contact_us", "value" => $this->request->getPost("Content-about")]);
						} elseif( $this->request->getPost("Content-address") ) {
							$this->webconmod->save(["type" => "address_html", "value" => $this->request->getPost("Content-address")]);
						} elseif( $this->request->getPost("Content-email") ) {
							$this->webconmod->save(["type" => "email_html", "value" => $this->request->getPost("Content-email")]);
						} elseif( $this->request->getPost("Content-contact") ) {
							$this->webconmod->save(["type" => "contact_html", "value" => $this->request->getPost("Content-contact")]);
						} elseif( $this->request->getPost("Content-landline") ) {
							$this->webconmod->save(["type" => "landline_html", "value" => $this->request->getPost("Content-landline")]);
						}

						break;
					case 'about':

						$this->webconmod->save(['type' => "about_us", 'value' => $this->request->getPost("Contents")]);

						break;
					case 'privacy_policies':
						
						$this->webconmod->save(['type' => "privacy_policies", 'value' => $this->request->getPost("Contents")]);

						break;
					case 'terms_and_condition':

						$this->webconmod->save(['type' => "terms_and_condition", 'value' => $this->request->getPost("Contents")]);
						
						break;
					case 'home':

						break;
					case 'socialmedia':


						foreach ($this->SocialMediaArr as $name => $option) {
							$name = str_replace(" ", "", strtolower($name));

							$asd = $this->webconmod->save(array(
								"type" => "{$name}_link",
								"value" => $this->request->getPost("{$name}-link"),
								"toogle" => (int) json_decode($this->request->getPost("{$name}-toogle"))
							));

						}

						$turner['msg'] = "Success";
						break;
					case 'img_upload':

						$image_type = $this->request->getGet('sub_type');
						$imagefile = $this->request->getFile('imgThumb');
						$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
							$imagefile, 
							$image_type, 
							[
								'file_mark' 	=> "Thumbnail",
								'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original']
							]):null;


						if( empty( $this->webconmod->asArray()->find("{$image_type}_img_session") ) )
							$this->webconmod->insert(array(
								"type" => "{$image_type}_img_session",
								"value" => $img_data,
								"toogle" => 1,
								"source" => 'webconfig',
							));


						$this->webconmod->save([ "type" => "{$image_type}_img_session", "value" => $img_data ]);

						$imagemod = model("\App\Models\Imagemod");
						$fileDets = $imagemod->select("id, directory, filename")->where('session', $img_data)->asArray()->first();
						$imgSrc = \App\Libraries\Imagelib::getImageURL("original-{$fileDets['filename']}", $fileDets['directory']);
						$imgHTML = "<img src={$imgSrc} class='file-preview-image kv-preview-data' alt={$fileDets['filename']} style='width: auto; height: auto; max-width: 100%; max-height: 100%;'>";

						$turner = [
			                'chunkIndex' => $this->request->getPost("chunkIndex"),         // the chunk index processed
			                'initialPreview' => $imgHTML, // the thumbnail preview data (e.g. image)
			                'initialPreviewConfig' => [
			                    [
			                        'caption' 	=> $fileDets['filename'], // caption
			                        'url' 		=> base_url("webconfig/request/delete_s_image?type={$image_type}&id={$fileDets['id']}")
			                    ]
			                ],
			                'append' => true
			            ];

						break;
					case 'meta':
						$this->webconmod->save(['type' => "meta_title", 'toogle' => 1, 'value' => $this->request->getPost("title")]);
						$this->webconmod->save(['type' => "meta_description", 'toogle' => 1, 'value' => $this->request->getPost("description")]);
						break;
					
					default: exit(json_encode("Invalid Request")); break;
				}

				$turner['msg'] = "Success";
				break;

			case 'delete_s_image':
				$image_type = $this->request->getGet("type");

				$imagemod = model('\App\Models\Imagemod');
				$imagemod->delete($this->request->getGet("id"));


				$this->webconmod->save([ "type" => "{$image_type}_img_session", "value" => NULL ]);
				break;
			case 'toogle_info':

				$turner['msg'] = "Success";
				break;
			case 'miscellaneous':
				switch ($this->request->getGet("type")) {
					case 'add':
						if($this->request->getGet("subtype") == "category"){
							$this->propertymod->add_category(array(
								"name" => $this->request->getPost("categoryName")
							));
						} elseif ($this->request->getGet("subtype") == "subcategory") {
							$this->propertymod->add_sub_category(array(
								"name" => $this->request->getPost("categoryName")
							));
						} elseif($this->request->getGet("subtype") == "linked") {
							$this->propertymod->add_link_category(array(
								"main_categ_id" => $this->request->getPost("categoryID"),
								"sub_categ_id" => $this->request->getPost("subcategoryID"),
							));
						}
						break;
					case 'edit':
						if($this->request->getGet("subtype") == "category"){
							$this->propertymod->edit_category($this->request->getPost("categoryID"), array(
								"name" => $this->request->getPost("categoryName")
							));
						} elseif ($this->request->getGet("subtype") == "subcategory") {
							$this->propertymod->edit_sub_category($this->request->getPost("categoryID"), array(
								"name" => $this->request->getPost("categoryName")
							));
						} elseif($this->request->getGet("subtype") == "linked") {
							$this->propertymod->edit_link_category($this->request->getPost("categoryID"), array(
								"main_categ_id" => $this->request->getPost("mcategoryID"),
								"sub_categ_id" => $this->request->getPost("subcategoryID"),
							));
						}
						break;
					case 'delete':
						if($this->request->getGet("subtype") == "category"){
							$this->propertymod->delete_category($this->request->getGet("categoryID"));
						} elseif ($this->request->getGet("subtype") == "subcategory") {
							$this->propertymod->delete_sub_category($this->request->getGet("categoryID"));
						} elseif($this->request->getGet("subtype") == "linked") {
							$this->propertymod->delete_link_category($this->request->getGet("categoryID"));
						}
						
						break;
				} 
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Web Configuration";
		$this->headerData["pageTitle"] = "Web Configuration";
		$this->headerData["breadCrumps"] = array(
			"Webconfig" => "#"
		);

		array_push($this->headerData['hdcss'], "plugins/summernote/summernote-bs4.css");

		array_push($this->footerData['ftjs'], "plugins/summernote/summernote-bs4.min.js");

		$this->headerData["spageURL"] = $this->prefixUrl; 
	}

	//--------------------------------------------------------------------

}
