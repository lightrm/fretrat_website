<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];
	protected $universallib;
	protected $request;
	public $pluginDir = "assets/";
	public $inlcudesDir = "Includes/";
	public $headerData = array();
	public $footerData = array();

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
		$this->request = \Config\Services::request();
		$this->universallib = new \App\Libraries\Universallib();

		//Add Libraries to Pass on our view for basic page
		$this->librarytoView = array(
			// '(Varable)' => (Class)
			// Example: 'Foo' => FooClass

			'universallib' => $this->universallib,
		);

		$sourcesVersion = 2;
		

		// Default Paramameters
		$this->headerData["pluginDir"] = $this->pluginDir;
		$this->headerData["headerTitle"] = "";
		$this->headerData["pageTitle"] = "Title";
		$this->headerData["breadCrumps"] = array();
		$this->headerData['hdjs'] = array();
		$this->headerData['hdcss'] = array();
		$this->footerData['ftjs'] = array();
		$this->footerData['srcVer'] = $sourcesVersion;
		$this->headerData['srcVer'] = $sourcesVersion;
	}

	public function basic_page($page, $bodyData = array())
	{
		$bodyData = $this->AddLibraries($bodyData);

		$this->insertAdditionalData();

		$pageV = view($this->inlcudesDir.'Header', $this->headerData);
		$pageV .= view($this->inlcudesDir.'Sidenav', $this->headerData);
		$pageV .= view($page, $bodyData);
		$pageV .= view($this->inlcudesDir.'Footer', $this->footerData);
		
		return $pageV;

	}

	public function header_back($link)
	{
		$this->headerData['back_header'] = $link;
	}

	private function AddLibraries($data) {

		foreach ($this->librarytoView as $nameIndex => $class) {
			$this->headerData[$nameIndex] = $class;
			$data[$nameIndex] = $class;
			$this->footerData[$nameIndex] = $class;
		}

		return $data;
	}

	private function insertAdditionalData() {

		// \App\Libraries\Roleslib::RefreshRoles();
	}

	public function seo_friendly_url($string) {
		$string = str_replace(array('[\', \']'), '', $string);
		$string = preg_replace('/\[.*\]/U', '', $string);
		$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
		$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
		return strtolower(trim($string, '-'));
	}

}
