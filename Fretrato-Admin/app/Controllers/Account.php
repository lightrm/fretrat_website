<?php namespace App\Controllers;

class Account extends BaseController
{
	private $usermod;

	public function __construct()
	{
		$this->usermod = model('\App\Models\Usermod');
	}

	public function index()
	{

		if( $this->request->getMethod(true) == "POST" ) {

			$session_id = session()->get("User")['id'];
			$validator = \Config\Services::validation();
			$rule = $validator->getRuleGroup('user_form');
			if( empty($this->request->getPost('txtpassword')) ) {
				unset($rule['txtpassword']);
				unset($rule['txtpassword2']);
			}
			
			$rule['txtusername']['rules'] = "required|is_unique[users.username, id, {$session_id}]";
			$rule['txtemail']['rules'] = "required|valid_email|is_unique[users.email, id, {$session_id}]";

			if( $this->validate($rule) ) {

				// image Uploading
				$imagefile = $this->request->getFile("imgThumb");
				$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
					$imagefile, 
					"Users", 
					[
						'sessionid' => session()->get("User")['img_session'],
						'option' 	=> ['avatar_small', 'avatar_medium', 'avatar_large', 'original'],
						'crop'		=> ['if_width' => 215, 'if_height' => 215]
					]
				):session()->get("User")['img_session'];
				
				if( is_array($img_data) && isset($img_data['Error']) ) {
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> "Something went wrong in uploading the Images."
					));
					goto return_w_inp;
				}

				$update_data = array(
					'name'			=> $this->request->getPost('txtname'),
					'username'		=> $this->request->getPost('txtusername'),
					'email'			=> $this->request->getPost('txtemail'),
					'contact_no'	=> $this->request->getPost('txtcontact'),
					'about'			=> $this->request->getPost('txtabout'),
					'gender' 		=> $this->request->getPost('radgender'),
					'address'		=> $this->request->getPost('txtaddress'),
					'img_session'	=> $img_data
				);

				if(!empty($this->request->getPost('txtpassword'))) {
					$update_data['password'] = password_hash ($this->request->getPost('txtpassword'), PASSWORD_DEFAULT);
				}

				$this->usermod->update($session_id, $update_data);

				session()->set("User", $this->usermod->getUsers_with_img($session_id));

				return redirect()->to('/account');
			} else {

				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));


				return_w_inp:
				return redirect()->back()->withInput();
			}

		}
		$this->setup_page();


		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");

		return parent::basic_page("Myaccount");
	}

	public function login()
	{
		if( session()->has("User") ) return redirect()->to("/");

		$this->headerData['headerTitle'] = "Login";
		$data = array_merge($this->headerData, $this->footerData);


		if( $this->request->getMethod(true) == "POST" ) {

			$rule = array(
				"txtuser" 		=> ['label' => "Email/Username", 'rules' => 'required'],
				"txtpassword" 	=> ['label' => "Password", 'rules' => 'required']
			);

			if( $this->validate($rule) ) {

				$result = $this->usermod->request_login($this->request->getPost("txtuser"), $this->request->getPost("txtpassword"));
				if( !$result['error'] ) {
					// Successful login
					session()->set("User", $result['user']);
					\App\Libraries\Roleslib::RefreshRoles();
					
				} else {
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> $result['msg']
					));
					goto return_w_inp;
				}

			} else {
				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));
				goto return_w_inp;
			}

			return redirect()->to("/");


			return_w_inp:
			return redirect()->back()->withInput();
		}
		
		return view("Login", $data);
	}

	public function logout()
	{
		$this->headerData['headerTitle'] = "Login";
		$data = array_merge($this->headerData, $this->footerData);

		session()->remove("User");
		session()->remove(\App\Libraries\Roleslib::$role_sessName);
		return redirect()->to("login");
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "My account";
		$this->headerData["pageTitle"] = "My Account";
		$this->headerData["breadCrumps"] = array(
			"My Account" => base_url("account")
		);
	}

	

	//--------------------------------------------------------------------

}
