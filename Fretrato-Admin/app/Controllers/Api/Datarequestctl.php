<?php namespace App\Controllers\Api;

use CodeIgniter\Controller;

class Datarequestctl extends Controller
{

	public function index()
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request Form!"));
		}

		$turner = "Invlid Request!";

		exit(json_encode($turner));
	}

	public function ajax($type_request = null)
	{

		if(is_null($type_request)){
			exit(json_encode("Invalid Request!"));
		}

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request Form!"));
		}


		$turner = array();
		switch ($type_request) {
			case 'web_configure':

				$turner = array(
					"web_type"			=> ENVIRONMENT,
					"allow_debug"		=> (ENVIRONMENT != "production"),
					"web_url"			=> base_url()."/",
					"acpptblFilter"		=> array("search", "type", "role"),
				);

				break;
			case 'header_notif':
				if( !session()->has('User') )
					exit(json_encode("error"));

				$notifactionmod = model('\App\Models\Notificationmod');

				if( $this->request->getGet("read-all") )
					$notifactionmod->where('user_id', session()->get("User")['id'])->update(null, ['unread' => 0]);
				

				$notificationList = array();
				foreach ($notifactionmod->where("unread", 1)->where("user_id", session()->get("User")['id'])->orderBy("created_at", "DESC")->asArray()->findAll(0, 10) as $notif) {
					array_push($notificationList, array(
						"notif_id" 	=> $notif['id'],
						"unread"	=> (booL) $notif['unread'],
						"title" 	=> $notif['title'],
						"time" 		=> date("h:i", strtotime($notif['created_at'])),
						"url" 		=> $notif['url'],
						"icon"		=> $notif['icon']
					));
				}
				$turner = array(
					"unread" => $notifactionmod->count_unread(session()->get("User")['id']),
					"notifs" => $notificationList
				);
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	//--------------------------------------------------------------------

}
