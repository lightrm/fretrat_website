<?php namespace App\Controllers;

class Inquiry extends BaseController
{
	private $userInq = "Inquiry/";
	private $inquirymod;

	public function __construct()
	{
		$this->inquirymod = model('\App\Models\Inquirymod');
	}

	public function index()
	{
		$this->setup_page();

		return parent::basic_page("home");
	}

	public function message($id)
	{
		$this->setup_page();


		$bodyD['inqDets'] = $this->inquirymod->getInquiry($id);

		if( !empty($bodyD['inqDets']['property_id']) ) {
			$pagetoLoad = 'property';

		} else {

			$pagetoLoad = 'contact';
		}


		$this->headerData["breadCrumps"]['Property'] = "#";



		$this->header_back(base_url());

		return parent::basic_page($this->userInq.$pagetoLoad, $bodyD);
	}


	public function request(string $type_request)
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}


		$turner = array();
		switch ($type_request) {

			case 'load_inqury':

				$sample_msh = array();
				foreach ($this->propertymod->getPropertyApproval() as $row) {
					array_push($sample_msh, array(
						"SessionID" => $row['approval_id'],
						"InquirerName" => $row['name'],
						"Property" => array(
							"SessionID" => $row['property_id'],
							"Name" => $row['propery_name'],
							"Image" => \App\Libraries\Imagelib::getImageURL(
								"thumbnail_small-".$row['thumbnail'], 
								$row['thumbnail_dir'],
								"photo1.png"//"property_noimage.jpg"
							),
						)
					));
				}

				$turner['dataList'] = $sample_msh;
				$total_record = $this->propertymod->getPropertyApproval(true);
				$turner['totalRecord'] = number_format($total_record);

				if ( !$this->request->getGet("no_pager") ) {

					$pager = \Config\Services::pager();
					$pager->setPath('/blogs/Category');
					$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 9, $total_record, 'mini_pagination');

				}


				break;

			case 'delete_inquiry':
				$this->blogmod->delete_category($this->request->getGet("categoryID"));
				$turner['msg'] = "Success";
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Inquiry";
		$this->headerData["pageTitle"] = "Inquiry";
		$this->headerData["breadCrumps"] = array(
			"Dashboard" => base_url()."/",
			"Inquiry" => "#"
		);


	}

	//--------------------------------------------------------------------

}
