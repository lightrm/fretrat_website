<?php namespace App\Controllers\Blogs;
use App\Controllers\BaseController;

class Category extends BaseController
{
	private $ctgryDir = "Blogs/Category/";
	private $prefixUrl = "blogs/";
	private $mainUrl = "category/";
	private $blogmod;

	public function __construct()
	{
		$this->blogmod = model('\App\Models\Blogsmod');
	}

	public function index()
	{
		$this->setup_page();

		array_push($this->headerData['hdcss'], "plugins/datatables-bs4/css/dataTables.bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/jsgrid/jsgrid-theme.min.css");
		array_push($this->footerData['ftjs'], "plugins/datatables/jquery.dataTables.min.js");
		array_push($this->footerData['ftjs'], "plugins/datatables-bs4/js/dataTables.bootstrap4.min.js");
		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");

		return parent::basic_page($this->ctgryDir."category");
	}

	public function request(string $type_request)
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}


		$turner = array();
		switch ($type_request) {
			case 'load_category':

				$sample_msh = array();
				foreach ($this->blogmod->load_category() as $row) {
					array_push($sample_msh, array(
						"SessionID" 	=> $row['id'],
						"Type"			=> $row['type'],
						"Name" 			=> $row['category_name'],
						"Description" 	=> $row['category_description']
					));
				}

				$turner['categlist'] = $sample_msh;
				$total_record = $this->blogmod->load_category(true);
				$turner['totalRecord'] = number_format($total_record);

				if ( !$this->request->getGet("no_pager") ) {

					$pager = \Config\Services::pager();
					$pager->setPath('/blogs/Category');
					$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 15, $total_record, 'mini_pagination');

				}

				break;
			case 'add_category':
				$this->blogmod->add_category(
					[
						'user_id' 				=> session()->get("User")['id'], 
						'type' 					=> $this->request->getPost("categoryType"), 
						'category_name' 		=> $this->request->getPost("categoryName"), 
						'category_description' 	=> $this->request->getPost("categoryDesc")
					]
				);
				$turner['msg'] = "Success";
				break;
			case 'edit_category':
				$this->blogmod->edit_category(
					$this->request->getPost("categoryID"), 
					[
						'type' 					=> $this->request->getPost("categoryType"), 
						'category_name' 		=> $this->request->getPost("categoryName"), 
						'category_description' 	=> $this->request->getPost("categoryDesc")
					]
				);
				$turner['msg'] = "Success";
				break;
			case 'delete_category':
				foreach ($this->blogmod->loadCategory_link($this->request->getGet("categoryID"), false, "category_id") as $row)
					$this->blogmod->delete($row['blog_id']);

				$this->blogmod->deleteCategory_link($this->request->getGet("categoryID"), "category_id");
				$this->blogmod->delete_category($this->request->getGet("categoryID"));
				$turner['msg'] = "Success";
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Blogs";
		$this->headerData["pageTitle"] = "Category";
		$this->headerData["breadCrumps"] = array(
			"Realty" => "#",
			"List" => base_url($this->prefixUrl.$this->mainUrl)
		);

		$this->headerData["spageURL"] = $this->prefixUrl.$this->mainUrl; 
	}

	//--------------------------------------------------------------------

}
