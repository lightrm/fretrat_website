<?php namespace App\Controllers\Blogs;
use App\Controllers\BaseController;


class Plan_your_best_life_now extends BaseController
{

	private $blogDir = "Blogs/Plan_your_best_life_now/";
	private $blogType = "plan_your_best_life_now";
	private $prefixUrl = "blogs/";
	private $mainUrl = "plan_your_best_life_now/";
	private $usermod;

	public function __construct()
	{
		$this->blogmod = model('\App\Models\Blogsmod');
	}

	public function index()
	{
		$this->setup_page();

		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");
	
		return parent::basic_page($this->blogDir."list");
	}

	public function add() 
	{
		$this->setup_page();
		$this->headerData["breadCrumps"]['Add'] = "#";


		if( $this->request->getMethod(true) == "POST" ) {
			$validator = \Config\Services::validation();

			if( $this->validate($validator->getRuleGroup('blogs_form')) ) {

				// image Uploading
				$imagefile = $this->request->getFile("imgThumb");
				$galleryFile = $this->request->getFiles()['imgGallery'];
				$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
					$imagefile, 
					"Blogs", 
					[
						'file_mark' 	=> "Thumbnail",
						'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original']
					]):null;

				foreach ($galleryFile as $imageInfo) {
					$result = $imageInfo->isValid()? \App\Libraries\Imagelib::upload_image(
						$imageInfo, 
						"Blogs", 
						[
							'sessionid' 	=> $img_data,
							'file_mark' 	=> "Gallery",
							'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original']
						]
					):$img_data;

					if( $result !== $img_data ) $img_data = $result;
					if( is_array($img_data) && isset($img_data['Error']) ) { goto error_image_upload; break; }
				}

				
				if( is_array($img_data) && isset($img_data['Error']) ) {
					error_image_upload:
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> "Something went wrong in uploading the Images."
					));
					goto return_w_inp;
				}
				
				$this->blogmod->insert(array(
					'user_id'			=> session()->get("User")['id'],
					'type'				=> $this->blogType,
					'title'				=> $this->request->getPost('txttitle'),
					'body'				=> $this->request->getPost('txtbody'),
					'slug'				=> $this->seo_friendly_url( $this->request->getPost('txttitle') ),
					'published' 		=> (int)(bool) $this->request->getPost('ckpublish'),
					'video_url'			=> $this->request->getPost("txtvideo"),
					'img_session'		=> $img_data
				));
				$propertyID = $this->blogmod->getInsertID();



				// Set Category
				$category_link = array();
				foreach ((array) $this->request->getPost("slctcategory") as $categoryID) {
					array_push($category_link, array(
						'blog_id' => $propertyID,
						'category_id' => $categoryID,
					));
				}

				if(!empty($category_link))
					$this->blogmod->addCategory_link($category_link);


				// Send Notification to other Admins
				$notifactionmod = model('\App\Models\Notificationmod');
				$users = model('\App\Models\Usermod');

				foreach ($users->where("id !=", session()->get("User")['id'])->where("role <", 3)->asArray()->findAll() as $user) {
					$notifactionmod->insert(array(
						"user_id" => $user['id'],
						"title" => "New Blog!",
						"description" => "New Blog created by " . session()->get("User")['name'] ,
						"url" => base_url("blogs/real_estate_and_travel?type=".$this->request->getPost('rdpropertytype')."&search={$propertyID}")
					));
				}


				return redirect()->to('/blogs/plan_your_best_life_now?'.(current_url(true)->getQuery()));
			} else {

				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));


				return_w_inp:
				return redirect()->back()->withInput();
			}

		}

		
		array_push($this->headerData["hdcss"], "plugins/select2/css/select2.min.css");
		array_push($this->headerData["hdcss"], "plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/summernote/summernote-bs4.css");
		array_push($this->footerData["ftjs"], "dist/js/blogs/plan_your_best_life_now.js");
		array_push($this->footerData["ftjs"], "plugins/select2/js/select2.min.js");
		array_push($this->footerData['ftjs'], "plugins/summernote/summernote-bs4.min.js");

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");



		$this->header_back(base_url('blogs/real_estate_and_travel?'.current_url(true)->getQuery()));

		return parent::basic_page($this->blogDir."add");
	}

	public function edit($session_id) 
	{
		$this->setup_page();
		$this->headerData["breadCrumps"]['Edit'] = "#";

		$imagemod = model('\App\Models\Imagemod');

		$bodyData['blogDets'] = $this->blogmod->getBlog_with_thumb($session_id);
		$bodyData['blog_category'] = $this->blogmod->loadCategory_link($session_id);
		$bodyData['blog_gallery'] = $imagemod->like("directory", "%Gallery%")->where('session', $bodyData['blogDets']['blog_imgSession'])->asArray()->find();

		if( $this->request->getMethod(true) == "POST" ) {
			$validator = \Config\Services::validation();
			$rule = $validator->getRuleGroup('blogs_form');
			
			$rule['txttitle']['rules'] = "required|is_unique[blogs.title, id, {$session_id}]";

			if( $this->validate($rule) ) {

				// image Uploading
				$imagefile = $this->request->getFile("imgThumb");
				$galleryFile = $this->request->getFiles()['imgGallery'];
				$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
					$imagefile, 
					"Blogs", 
					[
						'sessionid' 	=> $bodyData['blogDets']['blog_imgSession'],
						'file_mark' 	=> "Thumbnail",
						'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original']
					]):$bodyData['blogDets']['blog_imgSession'];

				foreach ($galleryFile as $imageInfo) {
					$result = $imageInfo->isValid()? \App\Libraries\Imagelib::upload_image(
						$imageInfo, 
						"Blogs", 
						[
							'sessionid' 	=> $img_data,
							'file_mark' 	=> "Gallery",
							'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original']
						]
					):$img_data;

					if( $result !== $img_data ) $img_data = $result;
					if( is_array($img_data) && isset($img_data['Error']) ) { goto error_image_upload; break; }
				}
				
				if( is_array($img_data) && isset($img_data['Error']) ) {
					error_image_upload:
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> "Something went wrong in uploading the Images."
					));
					goto return_w_inp;
				}
				
				$this->blogmod->update($bodyData['blogDets']['blog_id'], array(
					'title'				=> $this->request->getPost('txttitle'),
					'body'				=> $this->request->getPost('txtbody'),
					'slug'				=> $this->seo_friendly_url( $this->request->getPost('txttitle') ),
					'published' 		=> (int)(bool) $this->request->getPost('ckpublish'),
					'video_url'			=> $this->request->getPost("txtvideo"),
					'img_session'		=> $img_data
				));


				// Set Category
				$this->blogmod->deleteCategory_link($bodyData['blogDets']['blog_id']);
				$category_link = array();
				foreach ((array) $this->request->getPost("slctcategory") as $categoryID) {
					array_push($category_link, array(
						'blog_id' => $bodyData['blogDets']['blog_id'],
						'category_id' => $categoryID,
					));
				}

				if(!empty($category_link))
					$this->blogmod->addCategory_link($category_link);

				return redirect()->to('/blogs/plan_your_best_life_now?'.(current_url(true)->getQuery()));
			} else {

				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));


				return_w_inp:
				return redirect()->back()->withInput();
			}

		}


		array_push($this->headerData["hdcss"], "plugins/select2/css/select2.min.css");
		array_push($this->headerData["hdcss"], "plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/summernote/summernote-bs4.css");
		array_push($this->footerData["ftjs"], "dist/js/blogs/plan_your_best_life_now.js");
		array_push($this->footerData["ftjs"], "plugins/select2/js/select2.min.js");
		array_push($this->footerData['ftjs'], "plugins/summernote/summernote-bs4.min.js");

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");



		$this->header_back(base_url('blogs/real_estate_and_travel?'.current_url(true)->getQuery()));

		return parent::basic_page($this->blogDir."edit", $bodyData);
	}

	public function request(string $type_request)
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}


		$turner = array();
		switch ($type_request) {
			case 'load_blogs':

				$sample_msh = array();
				foreach ($this->blogmod->getBlog_with_thumb(null, false) as $blogs) {
					array_push($sample_msh, array(
						"SessionID" => $blogs['blog_id'],
						"Title" => $blogs['blog_title'],
						"Category" => $this->blogmod->loadCategory_link($blogs['blog_id'], true),
						"Image" => \App\Libraries\Imagelib::getImageURL(
							"thumbnail_medium-".$blogs['thumbnail'], 
							$blogs['thumbnail_dir'],
							"photo1.png"//"property_noimage.jpg"
						),
						"Agent"	=> array(
							"SessionID" => $blogs['userr_id'],
							"Type" => $blogs['role_name'],
							"Name" => $blogs['user_name'],
							"Address" => $blogs['role_name'],
							"Image" => \App\Libraries\Imagelib::getImageURL(
								"avatar_small-".$blogs['user_avatar'], 
								$blogs['user_dir'], 
								($blogs['gender'] == 'male'? 'blank-user-male.png':'blank-user-female.png')
							),
						),
					));
				};

				$turner['userlist'] = $sample_msh;
				$total_record = $this->blogmod->getBlog_with_thumb(null, true);
				$turner['totalRecord'] = number_format($total_record);

				$pager = \Config\Services::pager();
				$pager->setPath('/blogs/real_estate_and_travel');
				$page_temlate = !$this->request->getGet("user")? 'ajax_pagination':'mini_pagination';
				$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 9, $total_record, $page_temlate);
				break;
			case 'delete_blogs':
				$this->blogmod->deleteCategory_link($this->request->getGet("id"));
				$this->blogmod->delete($this->request->getGet("id"));
				$turner['msg'] = "Success";
				break;
			case 'delete_imgGallery':

				$imagemod = model('\App\Models\Imagemod');
				$imagemod->delete($this->request->getGet("id"));

				$turner['msg'] = "Success";
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}



	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Blogs";
		$this->headerData["pageTitle"] = "Real Estate Blogs";
		$this->headerData["breadCrumps"] = array(
			"Blogs" => "#",
			"List" 	=> base_url("blogs/real_estate_and_travel")
		);

		$this->headerData["spageURL"] = $this->prefixUrl.$this->mainUrl; 
	}

	//--------------------------------------------------------------------

}
