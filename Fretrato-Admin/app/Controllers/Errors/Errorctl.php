<?php namespace App\Controllers\Errors;

use App\Controllers\BaseController;

class Errorctl extends BaseController
{
	private $errorDir = "errors/";

	public function index()
	{	

		$this->setup_page();
	
		echo parent::basic_page($this->errorDir."404");
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "404 NOT FOUND";
		$this->headerData["pageTitle"] = "Error";
		$this->headerData["breadCrumps"] = array(
			"404" => "#"
		);
	}

	//--------------------------------------------------------------------

}
