<?php namespace App\Controllers;


class Users extends BaseController
{

	private $userDir = "User/";
	private $SocialMediaArr;
	private $usermod;

	public function __construct()
	{
		$this->SocialMediaArr = array(
			"Facebook" => [
				"color" => "blue",
				"icon" 	=> "fab fa-facebook-square"
			],
			"Twitter" => [
				"color" => "blue",
				"icon" 	=> "fab fa-twitter"
			],
			"LinkedIn" => [
				"color" => "blue",
				"icon" 	=> "fab fa-linkedin"
			],
			"Telegram" => [
				"color" => "blue",
				"icon" 	=> "fab fa-telegram"
			],
			"Viber" => [
				"color" => "purple",
				"icon" 	=> "fab fa-viber"
			],
			"Whatsup" => [
				"color" => "green",
				"icon" 	=> "fab fa-whatsapp-square"
			],

		);

		$this->usermod = model('\App\Models\Usermod');
	}

	public function index()
	{
		$this->setup_page();

		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");
	
		return parent::basic_page($this->userDir."list");
	}

	public function roles()
	{
		$this->setup_page();
		$this->headerData["breadCrumps"]['Roles'] = "#";

		if( $this->request->getMethod(true) == "POST" ) {
			try
			{
				switch ($this->request->getPost("hdntype")) {
					case 'add':
					    $success = $this->usermod->addRole(array(
							"name" 			=> $this->request->getPost("txtrolename"), 
							"description" 	=> $this->request->getPost("txtroledescription")
						));

						if($success) {
							\App\Libraries\Roleslib::RefreshRoles();

							session()->setFlashdata("user_alert", array(
								"type" 	=> "success",
								"msg"	=> "Role Successfully Added!"
							));
						} else {
							session()->setFlashdata("user_alert", array(
								"type" 	=> "danger",
								"msg"	=> "Error Adding data!"
							));
						}
						break;

					case 'edit':
					    $success = $this->usermod->editRole($this->request->getPost("hdnid"), array(
							"name" 			=> $this->request->getPost("txtrolename"),
							"description" 	=> $this->request->getPost("txtroledescription")
						));

						if($success) {
							\App\Libraries\Roleslib::RefreshRoles();

							session()->setFlashdata("user_alert", array(
								"type" 	=> "success",
								"msg"	=> "Role Successfully Edited!"
							));
						} else {
							session()->setFlashdata("user_alert", array(
								"type" 	=> "danger",
								"msg"	=> "Error Editing data!"
							));
						}
						break;

					case 'delete':
					    
						$errorMsg = "Error Deleting data!";
						$success = false;
						if( (int) $this->request->getPost("hdnid") > 2 ) 
					    	$success = $this->usermod->deleteRole($this->request->getPost("hdnid"));
					    else
					    	$errorMsg = ($this->request->getPost("hdnid") == 1?"Admin":"Employee")." role cannot be deleted";

						if($success) {
							\App\Libraries\Roleslib::RefreshRoles();

							session()->setFlashdata("user_alert", array(
								"type" 	=> "success",
								"msg"	=> "Role Successfully Deleted!"
							));
						} else {
							session()->setFlashdata("user_alert", array(
								"type" 	=> "danger",
								"msg"	=> $errorMsg 
							));
						}
						break;
					
					default:
						session()->setFlashdata("user_alert", array(
							"type" 	=> "danger",
							"msg"	=> "Invalid Request!"
						));
						break;
				}

				return redirect()->to('/users/roles');
			} catch (\Exception $e) {
			    die($e->getMessage());
			}
		}

		$bodyD['roles'] = $this->usermod->getRoles();

		return parent::basic_page($this->userDir."roles", $bodyD);
	}

	public function add() 
	{
		if(empty(\App\Libraries\Roleslib::getRoles())) {
			\App\Libraries\Roleslib::RefreshRoles();
			return redirect()->to('/users/roles');
		}

		$validator = \Config\Services::validation();

		if( $this->request->getMethod(true) == "POST" ) {

			if( $this->validate($validator->getRuleGroup('user_form')) ) {

				// image Uploading
				$imagefile = $this->request->getFile("imgThumb");
				$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
					$imagefile, 
					"Users", 
					[
						'option' => ['avatar_small', 'avatar_medium', 'avatar_large', 'original'],
						// 'crop'		=> 'auto'
					]):null;
				
				if( is_array($img_data) && isset($img_data['Error']) ) {
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> "Something went wrong in uploading the Images."
					));
					goto return_w_inp;
				}


				$newData = array(
					'name'			=> $this->request->getPost('txtname'),
					'username'		=> $this->request->getPost('txtusername'),
					'email'			=> $this->request->getPost('txtemail'),
					'password'		=> password_hash ($this->request->getPost('txtpassword'), PASSWORD_DEFAULT),
					'contact_no'	=> $this->request->getPost('txtcontact'),
					'about'			=> $this->request->getPost('txtabout'),
					'gender' 		=> $this->request->getPost('radgender'),
					'address'		=> $this->request->getPost('txtaddress'),
					'role'			=> $this->request->getPost('slcrole'),
					'toogleInfo' 	=> (int)(bool) $this->request->getPost('ckcontact_info'),
					'order_ranking' => $this->request->getPost('txtranking'),
					'active' 		=> 1,
					'img_session'	=> $img_data
				);
				foreach ($this->SocialMediaArr as $Name => $option) {
					$name = str_replace(" ", "", strtolower($Name));

					$newData[$name.'_link'] = $this->request->getPost($name.'-link');
					$newData[$name.'_toogle'] = (int)(bool)$this->request->getPost($name.'-toogle');

				}

				$this->usermod->insert($newData);

				return redirect()->to('/users?'.(current_url(true)->getQuery()));
			} else {

				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));


				return_w_inp:
				return redirect()->back()->withInput();
			}

		}

		$this->setup_page();
		$this->headerData["breadCrumps"]['Add'] = "#";

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");
		$bodyD['SocialMediaArr'] = $this->SocialMediaArr;


		$this->header_back(base_url('users?'.current_url(true)->getQuery()));

		return parent::basic_page($this->userDir."add", $bodyD);
	}

	public function edit($session_id) 
	{
		$this->setup_page();
		$this->headerData["breadCrumps"]['Edit'] = "#";
		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");


		$bodyD['user_detail'] = $this->usermod->getUsers_with_img($session_id);
		if( $this->request->getMethod(true) == "POST" ) {
			$validator = \Config\Services::validation();
			$rule = $validator->getRuleGroup('user_form');
			if( empty($this->request->getPost('txtpassword')) ) {
				unset($rule['txtpassword']);
				unset($rule['txtpassword2']);
			}
			
			$rule['txtusername']['rules'] = "required|is_unique[users.username, id, {$session_id}]";
			$rule['txtemail']['rules'] = "required|valid_email|is_unique[users.email, id, {$session_id}]";

			if( $this->validate($rule) ) {

				// image Uploading
				$imagefile = $this->request->getFile("imgThumb");
				$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
					$imagefile, 
					"Users", 
					[
						'sessionid' => $bodyD['user_detail']['img_session'],
						'option' 	=> ['avatar_small', 'avatar_medium', 'avatar_large', 'original'],
						// 'crop'		=> 'auto'
					]
				):$bodyD['user_detail']['img_session'];
				
				if( is_array($img_data) && isset($img_data['Error']) ) {
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> "Something went wrong in uploading the Images."
					));
					goto return_w_inp;
				}

				$update_data = array(
					'name'			=> $this->request->getPost('txtname'),
					'username'		=> $this->request->getPost('txtusername'),
					'email'			=> $this->request->getPost('txtemail'),
					'contact_no'	=> $this->request->getPost('txtcontact'),
					'about'			=> $this->request->getPost('txtabout'),
					'gender' 		=> $this->request->getPost('radgender'),
					'address'		=> $this->request->getPost('txtaddress'),
					'role'			=> $this->request->getPost('slcrole'),
					'toogleInfo' 	=> (int)(bool) $this->request->getPost('ckcontact_info'),
					'order_ranking' => $this->request->getPost('txtranking'),
					'img_session'	=> $img_data
				);

				if(!empty($this->request->getPost('txtpassword'))) {
					$update_data['password'] = password_hash ($this->request->getPost('txtpassword'), PASSWORD_DEFAULT);
				}
				foreach ($this->SocialMediaArr as $Name => $option) {
					$name = str_replace(" ", "", strtolower($Name));

					$update_data[$name.'_link'] = $this->request->getPost($name.'-link');
					$update_data[$name.'_toogle'] = (int)(bool)$this->request->getPost($name.'-toogle');

				}

				$this->usermod->update($session_id, $update_data);

				if($bodyD['user_detail']['id'] == session()->get("User")['id']) {
					session()->set("User", $this->usermod->getUsers_with_img($session_id));
				}

				return redirect()->to('/users?'.(current_url(true)->getQuery()));
			} else {

				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));


				return_w_inp:
				return redirect()->back()->withInput();
			}

		}

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");

		foreach ($this->SocialMediaArr as $Name => $option) {
			$name = str_replace(" ", "", strtolower($Name));

			$optData = $bodyD['user_detail'];
			$this->SocialMediaArr[$Name]['value'] = $optData[$name.'_link'];
			$this->SocialMediaArr[$Name]['toogle'] = $optData[$name.'_toogle'];

		}
		$bodyD['SocialMediaArr'] = $this->SocialMediaArr;


		
		$this->header_back(base_url('users?'.current_url(true)->getQuery()));

		return parent::basic_page($this->userDir."edit", $bodyD);
	}

	public function request(string $type_request)
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}


		$turner = array();
		switch ($type_request) {
			case 'load_users':

				$Array_mesh = array();

				//dd($this->usermod->getUsers_with_img());
				foreach ($this->usermod->getUsers_with_img() as $user) {
					array_push($Array_mesh, array(
						"SessionID" => $user['id'],
						"Role" => $user['role_name'],
						"Name" => $user['name'],
						"Username" => $user['username'],
						"Email" => $user['email'],
						"Address" => $user['address'],
						"About" => $user['about'],
						"Contact" => $user['contact_no'],
						"Image" => \App\Libraries\Imagelib::getImageURL(
							"avatar_medium-".$user['user_avatar'], 
							$user['user_dir'], 
							($user['gender'] == 'male'? 'blank-user-male.png':'blank-user-female.png')
						),
						"Tinfo" => (bool) $user['toogleInfo']
					));
				}

				$turner['userlist'] = $Array_mesh;
				$total_record = $this->usermod->getUsers_with_img(null, true);
				$turner['totalRecord'] = number_format($total_record);

				$pager = \Config\Services::pager();
				$pager->setPath('/users');
				$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 9, $total_record, 'ajax_pagination');

				break;
			case 'delete_user':
				$userRole = $this->usermod->asArray()->select("role")->find($this->request->getGet('user'))['role'];

				$turner['success'] = false;
				$turner['msg'] = "Admin cannot be deleted! <br> Contact your developer.";
				if( $userRole > 1 ) {
					$this->usermod->delete($this->request->getGet('user'));
					$turner['success'] = true;
				}
				break;
			case 'toogle_info':

				$this->usermod->update($this->request->getGet('user'), ['toogleInfo' => $this->request->getGet('toogle')]);

				$turner['msg'] = "Success";
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Users";
		$this->headerData["pageTitle"] = "Users";
		$this->headerData["breadCrumps"] = array(
			"List" => base_url("users")
		);
	}

	//--------------------------------------------------------------------

}
