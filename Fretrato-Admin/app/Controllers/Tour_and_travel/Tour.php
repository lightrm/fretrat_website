<?php namespace App\Controllers\Tour_and_travel;
use App\Controllers\BaseController;

class Tour extends BaseController
{
	public function index()
	{
		$this->setup_page();

		return parent::basic_page("Blankpage");
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Tour & Travel";
		$this->headerData["pageTitle"] = "Promo";
		$this->headerData["breadCrumps"] = array(
			"Tour & Travel" => "#",
			"Tour" => base_url()."/"
		);
	}

	//--------------------------------------------------------------------

}
