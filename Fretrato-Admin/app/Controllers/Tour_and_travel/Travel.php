<?php namespace App\Controllers\Tour_and_travel;
use App\Controllers\BaseController;

class Travel extends BaseController
{
	public function index()
	{
		$this->setup_page();

		return parent::basic_page("Blankpage");
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Tour & Travel";
		$this->headerData["pageTitle"] = "Travel";
		$this->headerData["breadCrumps"] = array(
			"Tour & Travel" => "#",
			"Travel" => base_url()."/"
		);
	}

	//--------------------------------------------------------------------

}
