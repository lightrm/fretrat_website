<?php namespace App\Controllers\Tour_and_travel;
use App\Controllers\BaseController;

class Hotel extends BaseController
{
	public function index()
	{
		$this->setup_page();

		return parent::basic_page("Blankpage");
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Tour & Travel";
		$this->headerData["pageTitle"] = "Hotel";
		$this->headerData["breadCrumps"] = array(
			"Tour & Travel" => "#",
			"Hotel" => base_url()."/"
		);
	}

	//--------------------------------------------------------------------

}
