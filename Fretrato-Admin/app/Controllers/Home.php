<?php namespace App\Controllers;

class Home extends BaseController
{
	private $propertymod;
	private $inquirymod;

	public function __construct()
	{
		$this->propertymod = model('\App\Models\Realty\Propertiesmod');
		$this->inquirymod = model('\App\Models\Inquirymod');
	}

	public function index()
	{
		$this->setup_page();

		$inquiryNum = $this->inquirymod->getNumberofInquiry();

		$bodyD['tour_inquiry'] = 0;
		$bodyD['hotel_inquiry'] = 0;
		$bodyD['rental_inquiry'] = $inquiryNum['Rental_COUNT'];
		$bodyD['resale_inquiry'] = $inquiryNum['Resale_COUNT'];

		return parent::basic_page("Home", $bodyD);
	}


	public function request(string $type_request)
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}


		$turner = array();
		switch ($type_request) {

			case 'load_inqury':

				$sample_msh = array();
				foreach ($this->inquirymod->getInquiry() as $row) {
					$inquiryRtn = array(
						"SessionID" => $row['inquiry_id'],
						"Property" => array(
							"SessionID" => null,
							"Name" => $row['inquiry_about']
						),
						"Inquirer"	=> array(
							"Name" => $row['inquiry_name'],
							"Email" => $row['email'],
							"Contact" => $row['contact_no'],
						)
					);
					if( !empty($row['property_id']) ) {
						$inquiryRtn['Property'] = array(
							"SessionID" => $row['property_id'],
							"Name" => $row['property_name'],
							"Image" => \App\Libraries\Imagelib::getImageURL(
								"thumbnail_small-".$row['thumbnail'], 
								$row['thumbnail_dir'],
								"photo1.png"//"property_noimage.jpg"
							)
						);
					}

					array_push($sample_msh, $inquiryRtn);
				}

				$turner['dataList'] = $sample_msh;
				$total_record = $this->inquirymod->getInquiry(null, true);
				$turner['totalRecord'] = number_format($total_record);

				if ( !$this->request->getGet("no_pager") ) {

					$pager = \Config\Services::pager();
					$pager->setPath('/blogs/Category');
					$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 9, $total_record, 'mini_pagination');

				}

				break;

			case 'delete_inquiry':
				$this->inquirymod->delete( $this->request->getGet("ID") );
				$turner['msg'] = "Success";
				break;

			case 'load_approval':

				$sample_msh = array();
				foreach ($this->propertymod->getPropertyApproval() as $row) {
					array_push($sample_msh, array(
						"SessionID" => $row['approval_id'],
						"Property" => array(
							"SessionID" => $row['property_id'],
							"Name" => $row['propery_name'],
							"Image" => \App\Libraries\Imagelib::getImageURL(
								"thumbnail_small-".$row['thumbnail'], 
								$row['thumbnail_dir'],
								"photo1.png"//"property_noimage.jpg"
							),
						),
						"Agent"	=> array(
							"SessionID" => $row['user_id'],
							"Name" => $row['user_name'],
							"Role" => $row['role_name']
						),
					));
				}

				$turner['dataList'] = $sample_msh;
				$total_record = $this->propertymod->getPropertyApproval(true);
				$turner['totalRecord'] = number_format($total_record);

				if ( !$this->request->getGet("no_pager") ) {

					$pager = \Config\Services::pager();
					$pager->setPath('/blogs/Category');
					$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 9, $total_record, 'mini_pagination');

				}

				break;
			case 'change_approval':

				if( $this->request->getGet("approval") === "true" )
					$this->propertymod->save(['id' => $this->request->getGet("property_id"), "published" => 1]);

				$this->propertymod->change_status_approval($this->request->getGet("id"), ($this->request->getGet("approval") === "true"));
				$turner['msg'] = "Success";
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Dashboard";
		$this->headerData["pageTitle"] = "Dashboard";
		$this->headerData["breadCrumps"] = array(
			"Dashboard" => base_url()."/"
		);


		array_push($this->headerData['hdcss'], "plugins/daterangepicker/daterangepicker.css");
		array_push($this->headerData['hdcss'], "plugins/jqvmap/jqvmap.min.css");


		array_push($this->footerData['ftjs'], "plugins/sparklines/sparkline.js");
		array_push($this->footerData['ftjs'], "plugins/daterangepicker/daterangepicker.js");
		array_push($this->footerData['ftjs'], "plugins/jqvmap/jquery.vmap.min.js");
		array_push($this->footerData['ftjs'], "plugins/jqvmap/maps/jquery.vmap.usa.js");
		array_push($this->footerData['ftjs'], "plugins/jquery-knob/jquery.knob.min.js");
		array_push($this->footerData['ftjs'], "plugins/chart.js/Chart.min.js");
		array_push($this->footerData['ftjs'], "plugins/summernote/summernote-bs4.min.js");
		// array_push($this->footerData['ftjs'], "dist/js/pages/dashboard3.js");
		array_push($this->footerData['ftjs'], "dist/js/demo.js");
		array_push($this->footerData['ftjs'], "dist/js/dashboard/realty.js");
		array_push($this->footerData['ftjs'], "dist/js/dashboard/travel.js");
		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");

		
		//array_push($this->footerData['ftjs'], "dist/js/adminlte.js");


	}

	//--------------------------------------------------------------------

}
