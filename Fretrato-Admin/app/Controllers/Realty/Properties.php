<?php namespace App\Controllers\Realty;
use App\Controllers\BaseController;

class Properties extends BaseController
{
	private $propertiesDir = "Realty/Properties/";
	private $prefixUrl = "realty/";
	private $mainUrl = "properties/";
	private $propertymod;

	public function __construct()
	{
		$this->propertymod = model('\App\Models\Realty\Propertiesmod');
	}

	public function index()
	{
		$this->setup_page();

		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");

		return parent::basic_page($this->propertiesDir."list");
	}

	public function add() 
	{
		$this->setup_page();
		$this->headerData["breadCrumps"]['Add'] = "#";


		// Set Amenities
		$amenitymod = model('\App\Models\Realty\Amenitiesmod');


		if( (bool) $this->request->getGet('reference') ){
			$bodyData['propRef'] = $this->propertymod->getProperty_with_thumb($this->request->getGet('reference'));
			$bodyData['propAmty'] = $amenitymod->loadAmenity_link($this->request->getGet('reference'));
		}


		if( $this->request->getMethod(true) == "POST" ) {
			$validator = \Config\Services::validation();

			if( $this->validate($validator->getRuleGroup('propety_form')) ) {

				// image Uploading
				$imagefile = $this->request->getFile("imgThumb");
				$imageSlider = $this->request->getFile("imgSlider");
				$galleryFile = $this->request->getFiles()['imgGallery'];
				$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
					$imagefile, 
					"Realty", 
					[
						'file_mark' 	=> "Thumbnail",
						'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
						'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
					]):null;
				if( $imageSlider->isValid() ) {
					$result = \App\Libraries\Imagelib::upload_image(
						$imageSlider, 
						"Realty", 
						[
							'sessionid' 	=> $img_data,
							'file_mark' 	=> "Slider",
							'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
							'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
						]
					);

					if( $result !== $img_data ) $img_data = $result;
				}

				foreach ($galleryFile as $imageInfo) {
					$result = $imageInfo->isValid()? \App\Libraries\Imagelib::upload_image(
						$imageInfo,
						"Realty",
						[
							'sessionid' 	=> $img_data,
							'file_mark' 	=> "Gallery",
							'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
							'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
						]
					):$img_data;

					if( $result !== $img_data ) $img_data = $result;
					if( is_array($img_data) && isset($img_data['Error']) ) { goto error_image_upload; break; }
				}

				
				if( is_array($img_data) && isset($img_data['Error']) ) {
					error_image_upload:
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> "Something went wrong in uploading the Images."
					));
					goto return_w_inp;
				}

				$location = '';
				foreach (['txtstreet', 'txtbarangay', 'txtcity', 'txtprovince', 'txtcountry'] as $address) {
					if( $this->request->getPost($address) !== "" && $this->request->getPost($address) !== null ) {
						$location .= rtrim($this->request->getPost($address), " ");

						if( $address !== 'txtstreet' )
							$location .= ", ";
						else
							$location .= " ";
					}
				}
				$location = rtrim($location, ", ");

				
				$this->propertymod->insert(array(
					'user_id'			=> session()->get("User")['id'],
					'name'				=> $this->request->getPost('txtname'),
					'about'				=> $this->request->getPost('txtabout'),
					'description'		=> $this->request->getPost('txtdescription'),
					'street'			=> $this->request->getPost('txtstreet'),
					'city'				=> $this->request->getPost('txtcity'),
					'barangay'			=> $this->request->getPost('txtbarangay'),
					'province'			=> $this->request->getPost('txtprovince'),
					'country' 			=> $this->request->getPost('txtcountry'),
					'furnish_type' 		=> $this->request->getPost('condition_slc'),
					'condition' 		=> (string) empty($this->request->getPost('housecondition_slc'))? $this->request->getPost('landcondition_slc'): $this->request->getPost('housecondition_slc'),
					'parking' 			=> (int) $this->request->getPost('parking_txt'),
					'bathroom'			=> (double) $this->request->getPost('bathroom_txt'),
					'bedroom'			=> (int) $this->request->getPost('bedroom_txt'),
					'floor_level'		=> $this->request->getPost('floorlevel_txt'),
					'floor_area'		=> $this->request->getPost('floorarea_txt'),
					'land_area'			=> $this->request->getPost('landarea_txt'),
					'offer_type'		=> $this->request->getPost('rdpropertytype'),
					'property_type'		=> $this->request->getPost('slcpropertytype'),
					'subcategory' 		=> $this->request->getPost('slcsubctgry'),
					'price'				=> $this->request->getPost('txtprice'),
					'meta_keywords'		=> implode(", ", (array) $this->request->getPost('slctSEOkw')),
					'slug'				=> $this->seo_friendly_url( $this->request->getPost('txtname') ),
					'property_vid_url' 	=> $this->request->getPost('txtvideo'),
					'status' 			=> "approved",
					'location'			=> $location,
					'dateFinished'		=> date("Y-m-d", strtotime($this->request->getPost('datefinish_txt'))),
					'stayMonth'			=> (int) $this->request->getPost('stay-month_txt'),
					'stayYear'			=> (int) $this->request->getPost('stay-year_txt'),

					// Agent Option
					'owner_name'		=> $this->request->getPost('txtownerName'),
					'owner_contact'		=> $this->request->getPost('txtownercontact'),
					'owner_email'		=> $this->request->getPost('txtownerEmail'),
					'agent_lastupdate'	=> date("Y-m-d H:i:s", strtotime($this->request->getPost('txtLastupdate'))),

					// Contract
					'contract_start'	=> date("Y-m-d", strtotime($this->request->getPost('txtcntrctstrt'))),
					'contract_end'		=> date("Y-m-d", strtotime($this->request->getPost('txtcntrctend'))),

					'toogleFeature' 	=> (int)(bool) $this->request->getPost('ckfeature'),
					'endFeature' 		=> ((bool) $this->request->getPost('ckfeature'))? date("Y-m-d H:i:s", strtotime("+ 1 week")):null,
					'is_direct' 		=> (int)(bool) $this->request->getPost('ckdirect'),
					'published' 		=> (int)(bool) $this->request->getPost('ckpublish'),
					'taken' 			=> (int)(bool) $this->request->getPost('cktaken'),
					'img_session'		=> $img_data
				));
				$propertyID = $this->propertymod->getInsertID();



				// Set Amenities
				$amenitymod = model('\App\Models\Realty\Amenitiesmod');

				$amenity_property = array();
				foreach ((array) $this->request->getPost("slctamenities") as $amenityId) {
					array_push($amenity_property, array(
						'property_id' => $propertyID,
						'amenity_property_id' => $amenityId,
					));
				}


				if(!empty($amenity_property))
					$amenitymod->addAmenity_link($amenity_property);


				// Send Notification to other Admins
				$notifactionmod = model('\App\Models\Notificationmod');
				$users = model('\App\Models\Usermod');

				foreach ($users->where("id !=", session()->get("User")['id'])->where("role <", 3)->asArray()->findAll() as $user) {
					$notifactionmod->insert(array(
						"user_id" => $user['id'],
						"title" => "New Property!",
						"description" => "New property created by " . session()->get("User")['name'] ,
						"url" => base_url("realty/properties?type=".$this->request->getPost('rdpropertytype')."&search={$propertyID}")
					));
				}





				return redirect()->to('/realty/properties?'.(current_url(true)->getQuery()));
			} else {

				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));


				return_w_inp:
				return redirect()->back()->withInput();
			}

		}

		
		array_push($this->headerData["hdcss"], "plugins/select2/css/select2.min.css");
		array_push($this->headerData["hdcss"], "plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/summernote/summernote-bs4.css");
		array_push($this->headerData['hdcss'], "plugins/daterangepicker/daterangepicker.css");
		array_push($this->footerData["ftjs"], "dist/js/properties/properties.js");
		array_push($this->footerData["ftjs"], "plugins/select2/js/select2.min.js");
		array_push($this->footerData['ftjs'], "plugins/summernote/summernote-bs4.min.js");
		array_push($this->footerData['ftjs'], "plugins/daterangepicker/daterangepicker.js");


		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");

		$bodyData['mainCategory'] = $this->propertymod->get_category();
		foreach ($bodyData['mainCategory'] as $row) 
			$bodyData['subCategory'][$row['id']] = [];
		foreach ($this->propertymod->get_link_category() as $row) {
			if ( empty( $bodyData['subCategory'][$row['main_categ_id']] ) )
				$bodyData['subCategory'][$row['main_categ_id']] = [];
			array_push($bodyData['subCategory'][$row['main_categ_id']], $row);
		}

		$this->header_back(base_url('realty/properties?'.current_url(true)->getQuery()));

		return parent::basic_page($this->propertiesDir."add", $bodyData);
	}

	public function edit($session_id) 
	{
		$this->setup_page();
		$this->headerData["breadCrumps"]['Edit'] = "#";

		$amenitymod = model('\App\Models\Realty\Amenitiesmod');
		$imagemod = model('\App\Models\Imagemod');

		$bodyData['propertyDets'] = $this->propertymod->getProperty_with_thumb($session_id);
		$bodyData['propery_amty'] = $amenitymod->loadAmenity_link($session_id);
		$bodyData['propery_gallery'] = $imagemod->like("directory", "%Gallery%")->where('session', $bodyData['propertyDets']['propery_imgSession'])->orderBy('sequence', 'asc')->asArray()->find();

		if( $this->request->getMethod(true) == "POST" ) {
			$validator = \Config\Services::validation();
			$rule = $validator->getRuleGroup('propety_form');
			
			$rule['txtname']['rules'] = "required|is_unique[properties.name, id, {$session_id}]";

			if( $this->validate($rule) ) {

				// image Uploading
				$imagefile = $this->request->getFile("imgThumb");
				$imageSlider = $this->request->getFile("imgSlider");
				$galleryFile = $this->request->getFiles()['imgGallery'];
				
				$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
					$imagefile, 
					"Realty", 
					[
						'sessionid' 	=> $bodyData['propertyDets']['propery_imgSession'],
						'file_mark' 	=> "Thumbnail",
						'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
						'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
					]):$bodyData['propertyDets']['propery_imgSession'];

				if( $imageSlider->isValid() ) {
					$result = \App\Libraries\Imagelib::upload_image(
						$imageSlider, 
						"Realty", 
						[
							'sessionid' 	=> $img_data,
							'file_mark' 	=> "Slider",
							'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
							'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
						]
					);

					if( $result !== $img_data ) $img_data = $result;
				}

				foreach ($galleryFile as $imageInfo) {
					$result = $imageInfo->isValid()? \App\Libraries\Imagelib::upload_image(
						$imageInfo, 
						"Realty", 
						[
							'sessionid' 	=> $img_data,
							'file_mark' 	=> "Gallery",
							'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
							'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
						]
					):$img_data;

					if( $result !== $img_data ) $img_data = $result;
					if( is_array($img_data) && isset($img_data['Error']) ) { goto error_image_upload; break; }
				}

				
				if( is_array($img_data) && isset($img_data['Error']) ) {
					error_image_upload:
					session()->setFlashdata("user_alert", array(
						"type" 	=> "danger",
						"msg"	=> "Something went wrong in uploading the Images."
					));
					goto return_w_inp;
				}

				$location = '';
				foreach (['txtstreet', 'txtbarangay', 'txtcity', 'txtprovince', 'txtcountry'] as $address) {
					if( $this->request->getPost($address) !== "" && $this->request->getPost($address) !== null ) {
						$location .= rtrim($this->request->getPost($address), " ");

						if( $address !== 'txtstreet' )
							$location .= ", ";
						else
							$location .= " ";
					}
				}
				$location = rtrim($location, ", ");

				$updateData = array(
					'name'				=> $this->request->getPost('txtname'),
					'about'				=> $this->request->getPost('txtabout'),
					'description'		=> $this->request->getPost('txtdescription'),
					'street'			=> $this->request->getPost('txtstreet'),
					'city'				=> $this->request->getPost('txtcity'),
					'barangay'			=> $this->request->getPost('txtbarangay'),
					'province'			=> $this->request->getPost('txtprovince'),
					'country' 			=> $this->request->getPost('txtcountry'),
					'furnish_type' 		=> $this->request->getPost('condition_slc'),
					'condition' 		=> (string) empty($this->request->getPost('housecondition_slc'))? $this->request->getPost('landcondition_slc'): $this->request->getPost('housecondition_slc'),
					'parking' 			=> (int) $this->request->getPost('parking_txt'),
					'bathroom'			=> (double) $this->request->getPost('bathroom_txt'),
					'bedroom'			=> (int) $this->request->getPost('bedroom_txt'),
					'floor_level'		=> (double) $this->request->getPost('floorlevel_txt'),
					'floor_area'		=> (double) $this->request->getPost('floorarea_txt'),
					'land_area'			=> (double) $this->request->getPost('landarea_txt'),
					'offer_type'		=> $this->request->getPost('rdpropertytype'),
					'property_type'		=> $this->request->getPost('slcpropertytype'),
					'subcategory' 		=> $this->request->getPost('slcsubctgry'),
					'price'				=> $this->request->getPost('txtprice'),
					'meta_keywords'		=> implode(", ", (array) $this->request->getPost('slctSEOkw')),
					'slug'				=> $this->seo_friendly_url( $this->request->getPost('txtname') ),
					'property_vid_url' 	=> $this->request->getPost('txtvideo'),
					'status' 			=> "approved",
					'location'			=> $location,
					'dateFinished'		=> date("Y-m-d H:i:s", strtotime($this->request->getPost('datefinish_txt'))),
					'stayMonth'			=> (int) $this->request->getPost('stay-month_txt'),
					'stayYear'			=> (int) $this->request->getPost('stay-year_txt'),

					// Agent Option
					'owner_name'		=> $this->request->getPost('txtownerName'),
					'owner_contact'		=> $this->request->getPost('txtownercontact'),
					'owner_email'		=> $this->request->getPost('txtownerEmail'),
					'agent_lastupdate'	=> date("Y-m-d", strtotime($this->request->getPost('txtLastupdate'))),

					// Contract
					'contract_start'	=> date("Y-m-d", strtotime($this->request->getPost('txtcntrctstrt'))),
					'contract_end'		=> date("Y-m-d", strtotime($this->request->getPost('txtcntrctend'))),

					'toogleFeature' 	=> (int)(bool) $this->request->getPost('ckfeature'),
					'endFeature' 		=> ((bool) $this->request->getPost('ckfeature'))? date("Y-m-d H:i:s", strtotime("+ 1 week")):null,
					'is_direct' 		=> (int)(bool) $this->request->getPost('ckdirect'),
					'published' 		=> (int)(bool) $this->request->getPost('ckpublish'),
					'taken' 			=> (int)(bool) $this->request->getPost('cktaken'),
					'img_session'		=> $img_data
				);

				if( !$bodyData['propertyDets']['toogleFeature'] && $updateData['toogleFeature'] )
					$updateData['endFeature'] = date("Y-m-d H:i:s", strtotime("+ 1 week"));
				
				$this->propertymod->update($bodyData['propertyDets']['propery_id'], $updateData);


				$amenitymod->deleteAmenity_link($bodyData['propertyDets']['propery_id']);
				$amenity_property = array();
				foreach ((array) $this->request->getPost("slctamenities") as $amenityId) {
					array_push($amenity_property, array(
						'property_id' => $bodyData['propertyDets']['propery_id'],
						'amenity_property_id' => $amenityId,
					));
				}

				if(!empty($amenity_property))
					$amenitymod->addAmenity_link($amenity_property);

				return redirect()->to('/realty/properties?'.(current_url(true)->getQuery()));
			} else {

				session()->setFlashdata("user_alert", array(
					"type" 	=> "warning",
					"msg"	=> $validator->listErrors()
				));


				return_w_inp:
				return redirect()->back()->withInput();
			}

		}


		
		array_push($this->headerData["hdcss"], "plugins/select2/css/select2.min.css");
		array_push($this->headerData["hdcss"], "plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/summernote/summernote-bs4.css");
		array_push($this->headerData['hdcss'], "plugins/daterangepicker/daterangepicker.css");
		array_push($this->footerData["ftjs"], "dist/js/properties/properties.js");
		array_push($this->footerData["ftjs"], "plugins/select2/js/select2.min.js");
		array_push($this->footerData['ftjs'], "plugins/summernote/summernote-bs4.min.js");
		array_push($this->footerData['ftjs'], "plugins/daterangepicker/daterangepicker.js");

		//Uploading Img
		array_push($this->headerData["hdcss"], "plugins/file-input/css/fileinput.min.css");
		array_push($this->footerData["ftjs"], "plugins/file-input/js/fileinput.min.js");
		array_push($this->footerData["ftjs"], "plugins/file-input/themes/fas/theme.js");
		

		$bodyData['mainCategory'] = $this->propertymod->get_category();
		foreach ($bodyData['mainCategory'] as $row) 
			$bodyData['subCategory'][$row['id']] = [];
		foreach ($this->propertymod->get_link_category() as $row) {
			if ( empty( $bodyData['subCategory'][$row['main_categ_id']] ) )
				$bodyData['subCategory'][$row['main_categ_id']] = [];
			array_push($bodyData['subCategory'][$row['main_categ_id']], $row);
		}


		$this->header_back(base_url('realty/properties?'.current_url(true)->getQuery()));

		return parent::basic_page($this->propertiesDir."edit", $bodyData);
	}

	public function recycle_bin()
	{
		$this->setup_page();

		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");

		return parent::basic_page($this->propertiesDir."recycle_bin");
	}

	public function request(string $type_request)
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}


		$turner = array();
		switch ($type_request) {
			case 'load_properties':

				$sample_msh = array();
				foreach ($this->propertymod->getProperty_with_thumb(null, false) as $properties) {
					array_push($sample_msh, array(
						"SessionID" => $properties['propery_id'],
						"Name" => $properties['propery_name'],
						"Street" => $properties['street'],
						"Type" => $properties['offer_type'],
						"Barangay" => $properties['barangay'],
						"City" => $properties['city'],
						"Country" => $properties['country'],
						"Province" => $properties['province'],
						"Bedroom" => number_format($properties['bedroom']),
						"Squaremeter" => number_format($properties['floor_area'], 2),
						"Parking" => $properties['parking'],
						"Price" => number_format($properties['price'], 2),
						"Image" => \App\Libraries\Imagelib::getImageURL(
							"thumbnail_medium-".$properties['thumbnail'], 
							$properties['thumbnail_dir'],
							"photo1.png"//"property_noimage.jpg"
						),
						"Tfeature" => (bool) $properties['toogleFeature'],
						"Tpublish" => (bool) $properties['published'],
						"Agent"	=> array(
							"SessionID" => $properties['userr_id'],
							"Type" => $properties['role_name'],
							"Name" => $properties['user_name'],
							"Address" => $properties['role_name'],
							"Image" => \App\Libraries\Imagelib::getImageURL(
								"avatar_medium-".$properties['user_avatar'], 
								$properties['user_dir'], 
								($properties['gender'] == 'male'? 'blank-user-male.png':'blank-user-female.png')
							),
						),
					));
				};

				$turner['userlist'] = $sample_msh;
				$total_record = $this->propertymod->getProperty_with_thumb(null, true);
				$turner['totalRecord'] = number_format($total_record);

				$pager = \Config\Services::pager();
				$pager->setPath('/realty/properties');
				$page_temlate = !$this->request->getGet("user")? 'ajax_pagination':'mini_pagination';
				$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 9, $total_record, $page_temlate);

				break;

			case 'load_recycle_bin':

				$sample_msh = array();
				foreach ($this->propertymod->getProperty_with_thumb(null, false, true) as $properties) {
					array_push($sample_msh, array(
						"SessionID" => $properties['propery_id'],
						"Name" => $properties['propery_name'],
						"Street" => $properties['street'],
						"Type" => $properties['offer_type'],
						"Barangay" => $properties['barangay'],
						"City" => $properties['city'],
						"Country" => $properties['country'],
						"Province" => $properties['province'],
						"Bedroom" => number_format($properties['bedroom']),
						"Squaremeter" => number_format($properties['size'], 2),
						"Parking" => $properties['parking'],
						"Price" => number_format($properties['price'], 2),
						"Image" => \App\Libraries\Imagelib::getImageURL(
							"thumbnail_medium-".$properties['thumbnail'], 
							$properties['thumbnail_dir'],
							"photo1.png"//"property_noimage.jpg"
						),
						"Tfeature" => (bool) $properties['toogleFeature'],
						"Tpublish" => (bool) $properties['published'],
						"Agent"	=> array(
							"SessionID" => $properties['userr_id'],
							"Type" => $properties['role_name'],
							"Name" => $properties['user_name'],
							"Address" => $properties['role_name'],
							"Image" => \App\Libraries\Imagelib::getImageURL(
								"avatar_medium-".$properties['user_avatar'], 
								$properties['user_dir'], 
								($properties['gender'] == 'male'? 'blank-user-male.png':'blank-user-female.png')
							),
						),
					));
				};

				$turner['userlist'] = $sample_msh;
				$total_record = $this->propertymod->getProperty_with_thumb(null, true, true);
				$turner['totalRecord'] = number_format($total_record);

				$pager = \Config\Services::pager();
				$pager->setPath('/realty/properties');
				$page_temlate = !$this->request->getGet("user")? 'ajax_pagination':'mini_pagination';
				$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 9, $total_record, $page_temlate);

				break;
			case 'restore_property':
				$this->propertymod->update($this->request->getGet("id"), ['deleted_at' => NULL]);
				$turner['msg'] = "Success";
				break;

			case 'delete_property':
				$permaDelete = (bool) $this->request->getGet("permanent");
				$this->propertymod->delete($this->request->getGet("id"), $permaDelete);
				$turner['msg'] = "Success";
				break;
			case 'delete_imgGallery':

				$imagemod = model('\App\Models\Imagemod');
				$imagemod->delete($this->request->getGet("id"));

				$turner['msg'] = "Success";
				break;
			case 'sort_images':

				$imagemod = model('\App\Models\Imagemod');
				$session_id = $this->request->getGet("session_id");
				$imgList = $this->request->getPost("images");
				
				foreach( $imgList as $key => $img_name ) {
					$imagemod->where('session', $session_id)->where('filename', $img_name)->set(['sequence' => ($key + 1 )])->update();
				}
				
				$turner['msg'] = "Success";
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	public function sampleWeb()
	{
		$beller = view("sampleWeb", $this->headerData);
		$beller .= view("Includes/footer", $this->footerData);
		return $beller;
	}

	public function symbolConvert() 
	{
		$all_properties = $this->propertymod->asArray()->findAll();
		foreach ($all_properties as $property) {
			$this->propertymod->update($property['id'], [ 'slug' => $this->seo_friendly_url( $property['name'] ) ]);
			echo "\"{$property['name']}\" Done <br>";
		}
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Properties";
		$this->headerData["pageTitle"] = "Properties";
		$this->headerData["breadCrumps"] = array(
			"Realty" => "#",
			"List" => base_url($this->prefixUrl.$this->mainUrl)
		);

		$this->headerData["spageURL"] = $this->prefixUrl.$this->mainUrl; 

	}

	//--------------------------------------------------------------------

}
