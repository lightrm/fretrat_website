<?php namespace App\Controllers\Realty;
use App\Controllers\BaseController;

class Amenities extends BaseController
{
	private $amtyDir = "Realty/Amenities/";
	private $prefixUrl = "realty/";
	private $mainUrl = "amenities/";
	private $amtymod;

	public function __construct()
	{
		$this->amtymod = model('\App\Models\Realty\Amenitiesmod');
	}

	public function index()
	{
		$this->setup_page();

		array_push($this->headerData['hdcss'], "plugins/datatables-bs4/css/dataTables.bootstrap4.min.css");
		array_push($this->headerData['hdcss'], "plugins/jsgrid/jsgrid-theme.min.css");
		array_push($this->footerData['ftjs'], "plugins/datatables/jquery.dataTables.min.js");
		array_push($this->footerData['ftjs'], "plugins/datatables-bs4/js/dataTables.bootstrap4.min.js");
		array_push($this->footerData["ftjs"], "dist/js/ajax_pagination.js");
		array_push($this->footerData["ftjs"], "dist/js/search_ajax.js");

		return parent::basic_page($this->amtyDir."amenities");
	}

	public function request(string $type_request)
	{

		if( !$this->request->isAjax() ) {
			exit(json_encode("Invalid Request"));
		}

		$turner = array();
		switch ($type_request) {
			case 'load_amenties':

				$sample_msh = array();
				foreach ($this->amtymod->load_amenity() as $row) {
					array_push($sample_msh, array(
						"SessionID" => $row['id'],
						"Name" => $row['amenity_name']
					));
				}

				$turner['amtylist'] = $sample_msh;
				$total_record = $this->amtymod->load_amenity("amenity_properties", true);
				$turner['totalRecord'] = number_format($total_record);

				if ( !$this->request->getGet("no_pager") ) {

					$pager = \Config\Services::pager();
					$pager->setPath('/realty/amenities');
					$turner['pagination'] = $pager->makeLinks($this->request->getGet("page"), 15, $total_record, 'ajax_pagination');

				}

				break;
			case 'add_amenity':
				$this->amtymod->addAmenity(['amenity_name' => $this->request->getPost("AmtyName")]);
				$turner['msg'] = "Success";
				break;
			case 'edit_amenity':
				$this->amtymod->EditAmenity($this->request->getPost("AmtyID"), ['amenity_name' => $this->request->getPost("AmtyName")]);
				$turner['msg'] = "Success";
				break;
			case 'delete_amenity':
				$this->amtymod->deleteAmenity($this->request->getGet("amtyid"));
				$turner['msg'] = "Success";
				break;
			default:
				# code...
				break;
		}

		exit(json_encode($turner));
	}

	private function setup_page()
	{
		$this->headerData["headerTitle"] = "Amenities";
		$this->headerData["pageTitle"] = "Amenities";
		$this->headerData["breadCrumps"] = array(
			"Realty" => "#",
			"List" => base_url($this->prefixUrl.$this->mainUrl)
		);

		$this->headerData["spageURL"] = $this->prefixUrl.$this->mainUrl; 
	}

	//--------------------------------------------------------------------

}
