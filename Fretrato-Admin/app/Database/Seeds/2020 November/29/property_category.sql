-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 05, 2020 at 04:35 AM
-- Server version: 5.7.26
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fretrato_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `property_category`
--

DROP TABLE IF EXISTS `property_category`;
CREATE TABLE IF NOT EXISTS `property_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `property_category`
--

INSERT INTO `property_category` (`id`, `parentID`, `name`) VALUES
(1, NULL, 'Commercial'),
(28, 6, 'Loft'),
(4, NULL, 'Land'),
(5, NULL, 'House and Lot'),
(6, NULL, 'Appartment'),
(27, 6, 'Penhouse'),
(31, 6, 'Condominium'),
(30, 6, 'Duplex Apartment'),
(29, 6, 'Attic Appartment'),
(26, 6, 'Special Property'),
(25, 6, 'Other'),
(32, 6, 'Room'),
(33, 1, 'Retail'),
(34, 1, 'Offices'),
(35, 1, 'Building'),
(36, 1, 'Warehouse'),
(37, 1, 'Serviced Office'),
(38, 5, 'Town House'),
(39, 5, 'Beach House'),
(40, 4, 'Beach Land'),
(41, 4, 'Commercial Lot'),
(42, 4, 'Memorial'),
(43, 4, 'Residential'),
(44, 4, 'Agricultural');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
