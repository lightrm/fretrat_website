<?php namespace App\Libraries;

class Roleslib {

    public static $role_sessName = "User_Role";

    function  __construct(){

    }

    public static function RefreshRoles() {

        $userMod = new \App\Models\Usermod; 

        session()->set(self::$role_sessName, $userMod->getRoles());

        return self::getRoles();
    }

    public static function getRoles() {
        return session()->has(self::$role_sessName)?session()->get(self::$role_sessName):[];
    }

}