<?= $this->include("Includes/Header_content") ?>

<section class="content">

	<div class="card card-lightblue">
		<div class="card-header">
			<h3 class="card-title">Links</h3>
		</div>
		<div class="card-body p-5">
			<div class="row">

				<?php $counter = 1; foreach ($SocialMediaArr as $name => $option): ?>
				<div class="col-md-12">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text text-<?= $option['color'] ?>"><i class="<?= $option['icon'] ?>"></i></span>
							</div>
							<input type="text" class="form-control link-inp" data-name="<?= str_replace(" ", "", strtolower($name)) ?>-link" placeholder="<?= $name ?> Link" value="<?= $option['value'] ?>">
							<div class="input-group-append">
								<span class="input-group-text text-blue">
									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input link-ck" id="customSwitch<?= $counter ?>" data-name="<?= str_replace(" ", "", strtolower($name)) ?>-toogle" <?= $option['toogle']?"checked":"" ?>>
										<label class="custom-control-label" for="customSwitch<?= $counter ?>">Display Icon</label>
	                   				</div>
	                   			</span>
							</div>
		                </div>
					</div>
				</div>
				<!-- /.col-md-12 -->
				<?php $counter++;endforeach ?>

				<div class="col-md-12">
					<span class="float-right">
						<button type="button" class="btn btn-success" id="save-btn">
							Save

							<i id="spn-save" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
						</button>
					</span>
				</div>
			</div>
		</div>
		
	</div>

</section>

<script type="text/javascript">
	
	$("#save-btn").click(function() {
    	$("#spn-save").show();
		var formData = new FormData();
		$(".link-inp").each(function() {
			formData.append($(this).data("name"), $(this).val());
		});

		$(".link-ck").each(function() {
			formData.append($(this).data("name"), $(this).is(":checked"));
		});
		

		quick_request(function(data) { 

		    $("#spn-save").hide();
		    Toast.fire({
		      icon: 'success',
		      title: 'Successfully Saved!'
		    });

	    }, web_url + 'webconfig/request/save_content?type=socialmedia', formData);
	});

</script>