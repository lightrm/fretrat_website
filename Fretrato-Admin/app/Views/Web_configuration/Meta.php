<?= $this->include("Includes/Header_content") ?>
<?php

?>

<section class="content">

  <div class="row">

    <div class="col-md-12">
      <div class="card card-lightblue">
        <div class="card-header">
          <h3 class="card-title">
            Meta
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body pad overlay-wrapper">

        	<div class="overlay" id="list-overlay">
            	<i class="fas fa-3x fa-sync-alt fa-spin"></i>
          	</div>
        	<!-- /.overlay -->
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" id="title-txt">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" id="description-txt" rows="10"></textarea>
            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button class="btn btn-info float-right" id="btn-save">Save</button>
        </div>

      </div>
    </div>
    <!-- /.col-->
  </div>
  <!-- ./row -->

</section>

<script>
  $(function () {

    quick_request(setup_config, '<?= base_url("webconfig/request/load_content?type=meta") ?>');

    $("#btn-save").click(function() {
    	$("#list-overlay").show();
		var formData = new FormData();
        formData.append("title", $("#title-txt").val());
        formData.append("description", $("#description-txt").val());

		quick_request(function(data) { 
		    $("#list-overlay").hide();

		    Toast.fire({
		      icon: 'success',
		      title: 'Successfully Saved!'
		    });

	    }, '<?= base_url("webconfig/request/save_content?type=meta") ?>', formData);
    });

    function setup_config(data)
    {
    	$("#list-overlay").hide();
        $("#title-txt").val( data.title )
        $("#description-txt").val( data.description )
    	
    }

  })
</script>