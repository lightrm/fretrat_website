<?= $this->include("Includes/Header_content") ?>
<?php

?>

<style type="text/css">
	.parag-content {
		cursor: pointer;
	}

	.parag-content:hover {
		text-decoration: underline;;
	}
</style>

<section class="content">

  <div class="row">
    <div class="col-md-12">
      <div class="card card-lightblue">
        <div class="card-header">
          <h3 class="card-title">
            Contact Us
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body pad overlay-wrapper">

        	<div class="overlay" id="list-overlay">
            	<i class="fas fa-3x fa-sync-alt fa-spin"></i>
          	</div>
        	<!-- /.overlay -->

        	<div class="row">
	            		
            	<div class="col-md-12 mb-2">
            		<div class="m-auto text-center" style="max-width: 90%">
            			<h1 class="text-gray-dark font-weight-bold">Contact Us</h1>

	                    <div class="div-container" data-id="about">
	                    	<div class="edit-content text-left" style="display: none">
								<div class="d-flex justify-content-end mb-2">
									<button class="btn btn-success btn-save-edit" data-id="about">Save</button>
									&nbsp;
									<div id="btn-cancel">
										<button class="btn btn-default btn-cncl-edit" data-id="about">Cancel</button>
									</div>
								</div>
								<textarea class="textarea" id="content-about-editor"></textarea>
							</div>
			  				<div class="div-content">
								<div id="content-about"></div>
							</div>
	                    </div>

            		</div>
            	</div>

            	<div class="col-12">

            		<div class="row m-auto text-center" style="max-width: 90%">
            			
	            		<div class="col-12 col-lg-3 card card-body" style="min-height: 30vh">
	            			<h4>Office Address</h4>


	            			<div class="div-container" data-id="address">
		                    	<div class="edit-content text-left" style="display: none">
									<div class="d-flex justify-content-end mb-2">
										<button class="btn btn-success btn-save-edit" data-id="address">Save</button>
										&nbsp;
										<div id="btn-cancel">
											<button class="btn btn-default btn-cncl-edit" data-id="address">Cancel</button>
										</div>
									</div>
									<textarea class="textarea" id="content-address-editor"></textarea>
								</div>
				  				<div class="div-content">
									<div id="content-address"></div>
								</div>
		                    </div>
	            		</div>

	            		<div class="col-12 col-lg-3 card card-body" style="min-height: 30vh">
	            			<h4>Email Address</h4>

	            			<div class="div-container" data-id="email">
		                    	<div class="edit-content text-left" style="display: none">
									<div class="d-flex justify-content-end mb-2">
										<button class="btn btn-success btn-save-edit" data-id="email">Save</button>
										&nbsp;
										<div id="btn-cancel">
											<button class="btn btn-default btn-cncl-edit" data-id="email">Cancel</button>
										</div>
									</div>
									<textarea class="textarea" id="content-email-editor"></textarea>
								</div>
				  				<div class="div-content">
									<div id="content-email"></div>
								</div>
		                    </div>
	            		</div>

	            		<div class="col-12 col-lg-3 card card-body" style="min-height: 30vh">
	            			<h4>Mobile Number</h4>

	            			<div class="div-container" data-id="contact">
		                    	<div class="edit-content text-left" style="display: none">
									<div class="d-flex justify-content-end mb-2">
										<button class="btn btn-success btn-save-edit" data-id="contact">Save</button>
										&nbsp;
										<div id="btn-cancel">
											<button class="btn btn-default btn-cncl-edit" data-id="contact">Cancel</button>
										</div>
									</div>
									<textarea class="textarea" id="content-contact-editor"></textarea>
								</div>
				  				<div class="div-content">
									<div id="content-contact"></div>
								</div>
		                    </div>
	            		</div>

	            		<div class="col-12 col-lg-3 card card-body" style="min-height: 30vh">
	            			<h4>Landline</h4>

	            			<div class="div-container" data-id="landline">
		                    	<div class="edit-content text-left" style="display: none">
									<div class="d-flex justify-content-end mb-2">
										<button class="btn btn-success btn-save-edit" data-id="landline">Save</button>
										&nbsp;
										<div id="btn-cancel">
											<button class="btn btn-default btn-cncl-edit" data-id="landline">Cancel</button>
										</div>
									</div>
									<textarea class="textarea" id="content-landline-editor"></textarea>
								</div>
				  				<div class="div-content">
									<div id="content-landline"></div>
								</div>
		                    </div>
	            		</div>


            		</div>
            		
            	</div>

            	

            	<div class="col-12">
            		<div class="row m-auto" style="max-width: 90%">

			            <div class="col-lg-8">
			            	<div class="card card-body bg-gray-light" style="min-height: 45vh">
			            		<h5>Contact Form</h5>
			            	</div>
			            </div>
			            <div class="col-lg-4">
			            	<div class="card card-body bg-gray-light" style="min-height: 45vh">
			            		<h5>Opening Hours</h5>
			            	</div>
			            </div>
            			
            		</div>
            	</div>

	        </div>
        	<!-- /.row -->

        </div>
        <!-- /.card-body -->

      </div>
    </div>
    <!-- /.col-->
  </div>
  <!-- ./row -->

</section>

<script type="text/babel">
  $(function () {
    // Summernote
    $('.textarea').summernote({
    	placeholder: 'Edit Content Here',
    	height: 100,
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
    	toolbar: summernoteToolBar,
    	callbacks: {
	        onPaste: function (e) {
      			$('#summernote').summernote('removeFormat')
	        }
	    }
    });
    $('.note-editable p').each( function() {
    	$(this).css('margin-bottom', 0)
    })

    $(".btn-cncl-edit").click(function() {
    	change_setup($(this).data("id"), "display");
    });

    $(".btn-save-edit").click(function() {
    	var uniqueID = $(this).data("id");
    	var tragetDiv = $(".div-container[data-id='"+uniqueID+"']");
    	$("#list-overlay").show();
		var formData = new FormData();
		formData.append("Content-"+uniqueID, $("#content-"+uniqueID+"-editor").val());

		quick_request(function(data) { 
    		/*$("#content-"+uniqueID, tragetDiv).html($("#content-"+uniqueID+"-editor").val() + ' <i class="text-info fas fa-edit"></i>');
		    change_setup(uniqueID, "display");*/

		    if(uniqueID == "about") {
		    	aboutDOM.componentDidMount();
		    } else{
				window[uniqueID+'DOM'].componentDidMount();
		    }
		    $("#list-overlay").hide();

		    Toast.fire({
		      icon: 'success',
		      title: 'Successfully Saved!'
		    });

	    }, web_url + 'webconfig/request/save_content?type=contact', formData);
    });

  })

	function change_setup(id, type, withEdit = true)
	{
		trace(id + ", " + type + ", " + withEdit);

    	var tragetDiv = ".div-container[data-id='"+id+"']";
		if(type == "edit") {
	    	$(".edit-content", tragetDiv).show();
			$(".div-content", tragetDiv).hide();
		} else {
	    	$(".edit-content", tragetDiv).hide();
			$(".div-content", tragetDiv).show();
		}

		if(withEdit) {
			$(".btn-cncl-edit", tragetDiv).show();
		} else {
			$(".btn-cncl-edit", tragetDiv).hide();
		}
	}
</script>