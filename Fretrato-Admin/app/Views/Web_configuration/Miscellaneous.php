<?= $this->include("Includes/Header_content") ?>
<style type="text/css">
	.td-category, .td-subcategory{
		cursor: pointer;
	}
</style>

<section class="content">

	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Preview</h3>
		</div>
		<div class="card-body row">
			
			<div class="col-4 offset-4 bg-gray-light rounded">
				<h2>Find Your Destination</h2>	
				<p class="text-gray-dark">Discover the world</p>	
				<div class="form-group">
					<input type="text" placeholder="Search" class="form-control" disabled>
				</div>	
				<div class="form-group">
					<select class="form-control" disabled>
						<option>Property Status</option>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" id="preview-category-select">
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" id="preview-subcategory-select">
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" disabled>
						<option>Location</option>
					</select>
				</div>
				<div class="form-group">
					<button class="btn btn-block btn-info">SEARCH</button>
				</div>
			</div>
			
		</div>
		
	</div>

	

	<div class="row">
		<div class="col-6">

			<div class="card card-lightblue">
				<div class="card-header">
					<h3 class="card-title">Category</h3>
				</div>
				<div class="card-body">
					<div class="content">
						<div class="form-group">
							<label>Category Name</label>
							<input type="text" class="form-control category-form" name="category_name" placeholder="ie. Office">
						</div>
						<div class="form-group text-right">
							<button class="btn btn-primary" id="add-category">Add</button>
						</div>

						<div class="form-group">
							<label>SubCategory Name</label>
							<input type="text" class="form-control subcategory-form" name="category_name" placeholder="ie. Office">
						</div>
						<div class="form-group text-right">
							<button class="btn btn-primary" id="add-subcategory">Add</button>
						</div>
					</div>
					<table></table>
				</div>
				
			</div>
			
		</div>

		<div class="col-6">
			
			<div class="card card-lightblue">
				<div class="card-header">
					<h3 class="card-title">Link Category</h3>
				</div>
				<div class="card-body">
					<div class="content">
						<div class="form-group">
							<label>Category</label>
							<select class="form-control category-select link-form" name="category_id"></select>
						</div>
						<div class="form-group">
							<label>Sub Category</label>
							<select class="form-control subcategory-select link-form" name="subcategory_id"></select>
						</div>
						<div class="form-group">
							<button class="btn btn-primary float-right" id="add-link">Link</button>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="col-3">
			<div class="card card-lightblue">
		      	<div class="card-header">
        			<h4 class="card-title"> Category </h4>
		      	</div>
			      <!-- /.card-header -->
		      	<div class="card-body overlay-wrapper">

			      	<div class="overlay" id="list-category-overlay">
			          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
			        </div>

			        <table id="category-table" class="table table-bordered table-hover">
			          	<thead>
			          		<tr>
					            <th>Name</th>
					            <th width="2%">Action</th>
				          	</tr>
			          	</thead>
			          <tbody></tbody>
			        </table>
		      	</div>
		      	<!-- /.card-body -->

		      
			    <div class="card-footer">
			      <div class="pagination-mini" data-load="load_data" data-id="category" data-page="1"></div>
			    </div>
		    </div>
		    <!-- /.card -->
		</div>

		<div class="col-3">
			<div class="card card-lightblue">
		      	<div class="card-header">
        			<h4 class="card-title"> Subcatogery </h4>
		      	</div>
			      <!-- /.card-header -->
		      	<div class="card-body overlay-wrapper">

			      	<div class="overlay" id="list-subcategory-overlay">
			          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
			        </div>

			        <table id="subcategory-table" class="table table-bordered table-hover">
			          	<thead>
			          		<tr>
					            <th>Name</th>
					            <th width="2%">Action</th>
				          	</tr>
			          	</thead>
			          <tbody></tbody>
			        </table>
		      	</div>
		      	<!-- /.card-body -->

		      
			    <div class="card-footer">
			      <div class="pagination-mini" data-load="load_data" data-id="subcategory" data-page="1"></div>
			    </div>
		    </div>
		    <!-- /.card -->
		</div>

		<div class="col-6">
			<div class="card card-lightblue">
		      	<div class="card-header">
        			<h4 class="card-title"> Linked Category </h4>
		      	</div>
			      <!-- /.card-header -->
		      	<div class="card-body overlay-wrapper">

			      	<div class="overlay" id="list-linked-overlay">
			          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
			        </div>

			        <table id="linked-table" class="table table-bordered table-hover">
			          	<thead>
			          		<tr>
					            <th>Category</th>
					            <th>Subcategory</th>
					            <th width="2%">Action</th>
				          	</tr>
			          	</thead>
			          <tbody></tbody>
			        </table>
		      	</div>
		      	<!-- /.card-body -->

		      
			    <div class="card-footer">
			      <div class="pagination-mini" data-load="load_data" data-id="linked" data-page="1"></div>
			    </div>
		    </div>
		    <!-- /.card -->
		</div>
	</div>

	
</section>



<div class="modal fade" id="modal-edit-category">
	<input class="hd-category-id" type="hidden">
	<input id="hd-edt-category-lnk" type="hidden" value="<?= base_url($spageURL."request/miscellaneous?type=edit&subtype=category") ?>">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
			<div class="form-group row">
				<label class="col-4">Category Name</label>
				<div class="col-8">
					<input type="text" class="form-control" id="txt-category-name-edit">
				</div>
			</div>
	        <div class="modal-footer text-right">
				<button type="button" class="btn btn-primary float-right" id="btn-edit-category">
	        		&nbsp;
					Save Changes
	        		&nbsp;
					<i class="sm fas fa-sync-alt fa-spin spn-category-edit" style="display: none;"></i>
	      		</button>
	        </div>
    	</div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-edit-subcategory">
	<input id="hd-subcategory-id" type="hidden">
	<input id="hd-edt-subcategory-lnk" type="hidden" value="<?= base_url($spageURL."request/miscellaneous?type=edit&subtype=subcategory") ?>">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Subcategory</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
			<div class="form-group row">
				<label class="col-4">Sub Category Name</label>
				<div class="col-8">
					<input type="text" class="form-control" id="txt-subcategory-name-edit">
				</div>
			</div>
	        <div class="modal-footer text-right">
				<button type="button" class="btn btn-primary float-right" id="btn-edit-subcategory">
	        		&nbsp;
					Save Changes
	        		&nbsp;
					<i class="sm fas fa-sync-alt fa-spin spn-subcategory-edit" style="display: none;"></i>
	      		</button>
	        </div>
    	</div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-edit-linked">
	<input id="hd-linked-id" type="hidden">
	<input id="hd-edt-linked-lnk" type="hidden" value="<?= base_url($spageURL."request/miscellaneous?type=edit&subtype=linked") ?>">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Link</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
			<div class="form-group row">
				<label class="col-4">Category</label>
				<div class="col-8">
					<select class="form-control category-select" id="txt-linked-category-edit"></select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-4">Sub Category</label>
				<div class="col-8">
					<select class="form-control subcategory-select" id="txt-linked-subcategory-edit"></select>
				</div>
			</div>
	        <div class="modal-footer text-right">
				<button type="button" class="btn btn-primary float-right" id="btn-edit-linked">
	        		&nbsp;
					Save Changes
	        		&nbsp;
					<i class="sm fas fa-sync-alt fa-spin spn-linked-edit" style="display: none;"></i>
	      		</button>
	        </div>
    	</div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-delete-category">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <input class="hd-category-id" type="hidden">
	    <input class="hd-category-type" type="hidden">
	    <input id="hd-dlt-lnk" type="hidden" value="<?= base_url($spageURL."request/miscellaneous?type=delete") ?>">
	    <div class="modal-header">
	      <h4 class="modal-title">Delete Category</h4>
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	    </div>
	    <div class="modal-body">
	      <p>Are you sure you want to delete this data?</p>
	    </div>
	    <div class="modal-footer justify-content-between">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      <button id="btn-delete-category" type="button" class="btn btn-danger">
	        &nbsp;
	        Delete 
	        &nbsp;
	        <i class="sm fas fa-sync-alt fa-spin spn-category-edit" style="display: none;"></i>
	      </button>
	    </div>
	  </div>
	  <!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
	var subCategory = {};
	$(document).ready(function() {
		reload_categories();
		load_data("category");
		load_data("subcategory");
		load_data("linked");


		$("#preview-category-select").change(categoryChange);



	});

	function reload_categories() 
	{
		subCategory = {};
		quick_request(function(data) { 
			$(".category-select").children().remove();
			$("#preview-category-select").children().remove();
			$("#preview-category-select").append("<option>Category</option>");
			for( var categoryDet of data.category ) {
				$(".category-select").append('<option value="'+ categoryDet.SessionID +'">'+ categoryDet.Name +'</option>');
				$("#preview-category-select").append('<option value="'+ categoryDet.SessionID +'">'+ categoryDet.Name +'</option>');
				subCategory[categoryDet.SessionID] = [];
			}

			$(".subcategory-select").children().remove();
			$("#preview-subcategory-select").children().remove();
			$("#preview-subcategory-select").append("<option>Sub Category</option>");
			for( var subcategoryDet of data.subcategory ) 
				$(".subcategory-select").append('<option value="'+ subcategoryDet.SessionID +'">'+ subcategoryDet.Name +'</option>');

			for( var row of data.linked ) {
				if( typeof subCategory[row.parentID] !== 'undefined' )
					subCategory[row.parentID].push(row.childName);
				else
					trace(row);
			}
			

		}, web_url  + 'webconfig/request/load_content?type=miscellaneous&subtype=all_categ');

		
	}

	function categoryChange()
	{	
		if ( typeof subCategory[$("#preview-category-select").val()] === 'undefined')
			return;

		var items = subCategory[$("#preview-category-select").val()];

		// Reset Preview Subcategory
		$("#preview-subcategory-select").children().remove();
		$("#preview-subcategory-select").append("<option>Sub Category</option>");

		console.log( items );
		for(var subname of items)
			$("#preview-subcategory-select").append("<option>"+subname+"</option>");

	}

	$("body").on("click", ".category-delete", function() {
	    trace("Deleting: " + $(this).data("session-id"));
	    $(".hd-category-id").val($(this).data("session-id"));
	    $(".hd-category-type").val($(this).data("session-type"));

	    $("#modal-delete-category").modal("show");
  	});

	$("#btn-delete-category").click(function() {

		$(".spn-category-edit").show();

		quick_request(function(data) { 
			load_data("category");
			load_data("subcategory");
			load_data("linked");
			reload_categories();
		  $(".spn-category-edit").hide();
		  $("#modal-delete-category").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Data successfully deleted'
		  });
		}, $("#hd-dlt-lnk").val() + "&categoryID=" + $(".hd-category-id").val() + "&subtype=" + $(".hd-category-type").val() );

	});

	function load_data(id, offset)
	{
		$("#list-" + id +"-overlay").show();
		quick_request(function(data) { 
			load_minipagination(data.pagination, id);
			setup_data(data.categoryList, id);
			$("#list-"+ id +"-overlay").hide();
		}, web_url + 'webconfig/request/load_content?page=' + $(".pagination-mini[data-id='"+ id +"']").data("page") + "&type=miscellaneous&subtype=" + id);

	}

	function setup_data(list, id)
	{
		remove_alldata(id);

		trace("loading Data");
		var i = 1;
		for(var ccategory in list)
		{
			var categoryDets = list[ccategory];			
			$("#"+id+"-table tbody").append(window['initialize_' + id](categoryDets));
			i++;
		}

		if( id == "linked" )
			categoryChange();

	}


	function initialize_category(categoryDets)
	{
		var NewRow = $("#clone-tr").clone();

		NewRow.removeAttr("id");
		NewRow.find(".category-name").html(categoryDets.Name);

		NewRow.find(".td-category").each(function() {
			$(this).data("session-id", categoryDets.SessionID);
			$(this).data("category-name", categoryDets.Name);
		});

		NewRow.find(".category-delete").data("session-id", categoryDets.SessionID).data("session-type", "category");

		return NewRow;
	}


	function initialize_subcategory(categoryDets)
	{
		var NewRow = $("#clone-tr-subcategory").clone();

		NewRow.removeAttr("id");
		NewRow.find(".category-name").html(categoryDets.Name);

		NewRow.find(".td-subcategory").each(function() {
			$(this).data("session-id", categoryDets.SessionID);
			$(this).data("category-name", categoryDets.Name);
		});

		NewRow.find(".category-delete").data("session-id", categoryDets.SessionID).data("session-type", "subcategory");


		return NewRow;
	}

	function initialize_linked(categoryDets)
	{
		var NewRow = $("#clone-tr-linked").clone();

		NewRow.removeAttr("id");
		NewRow.find(".category-parent").html(categoryDets.parentName);
		NewRow.find(".category-child").html(categoryDets.childName);

		NewRow.find(".td-linked").each(function() {
			$(this).data("session-id", categoryDets.SessionID);
			$(this).data("parentID", categoryDets.parentID);
			$(this).data("childID", categoryDets.childID);
		});

		NewRow.find(".category-delete").data("session-id", categoryDets.SessionID).data("session-type", "linked");

		return NewRow;
	}

	function remove_alldata(id)
	{
		trace("Removing Data");
		$("#"+id+"-table tbody").children().remove();
	}



	$(".content").on("click", ".td-category", function() {
		$(".hd-category-id").val($(this).data("session-id"));

		$("#txt-category-name-edit").val($(this).data("category-name"));
		$("#modal-edit-category").modal("show");
	});

	$(".content").on("click", ".td-subcategory", function() {
		$("#hd-subcategory-id").val($(this).data("session-id"));

		$("#txt-subcategory-name-edit").val($(this).data("category-name"));
		$("#modal-edit-subcategory").modal("show");
	});

	$(".content").on("click", ".td-linked", function() {
		$("#hd-linked-id").val($(this).data("session-id"));

		$("#txt-linked-category-edit").val($(this).data("parentID"));
		$("#txt-linked-subcategory-edit").val($(this).data("childID"));
		$("#modal-edit-linked").modal("show");
	});

	

	$("#btn-edit-category").click(function() {
		$(".spn-category-edit").show();
		var formData = new FormData();

		formData.append("categoryName", $("#txt-category-name-edit").val());
		formData.append("categoryID", $(".hd-category-id").val());

		quick_request(function(data) { 
			load_data("category");
			load_data("linked");
			reload_categories();
		  $(".spn-category-edit").hide();
		  $("#modal-edit-category").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Category successfully edited'
		  });
		}, $("#hd-edt-category-lnk").val(), formData);

	});


	$("#btn-edit-subcategory").click(function() {
		$(".spn-subcategory-edit").show();
		var formData = new FormData();

		formData.append("categoryParent", $("#txt-subcategory-type-edit").val());
		formData.append("categoryName", $("#txt-subcategory-name-edit").val());
		formData.append("categoryID", $("#hd-subcategory-id").val());

		quick_request(function(data) { 
			load_data("subcategory");
			load_data("linked");
			reload_categories();
		  $(".spn-subcategory-edit").hide();
		  $("#modal-edit-subcategory").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Subcategory successfully edited'
		  });
		}, $("#hd-edt-subcategory-lnk").val(), formData);
	});

	$("#btn-edit-linked").click(function() {
		$(".spn-linked-edit").show();
		var formData = new FormData();

		formData.append("mcategoryID", $("#txt-linked-category-edit").val());
		formData.append("subcategoryID", $("#txt-linked-subcategory-edit").val());
		formData.append("categoryID", $("#hd-linked-id").val());

		quick_request(function(data) { 
			load_data("linked");
			reload_categories();
		  $(".spn-linked-edit").hide();
		  $("#modal-edit-linked").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Link successfully edited'
		  });
		}, $("#hd-edt-linked-lnk").val(), formData);
	});

	$("#add-category").click(function() {
		var formData = new FormData();

		formData.append("categoryName", $(".category-form[name='category_name']").val());

		quick_request(function(data) { 
			$(".category-form[name='category_name']").val("");
			
			load_data("category");
			load_data("linekd");
			reload_categories();
			  Toast.fire({
			    icon: 'success',
			    title: 'Category successfully added'
			  });
		}, web_url + 'webconfig/request/miscellaneous?type=add&subtype=category', formData);
	});

	$("#add-subcategory").click(function() {
		var formData = new FormData();

		formData.append("categoryName", $(".subcategory-form[name='category_name']").val());

		quick_request(function(data) { 
			$(".subcategory-form[name='category_name']").val("");
			
			load_data("subcategory");
			load_data("linekd");
			reload_categories();
			  Toast.fire({
			    icon: 'success',
			    title: 'Subcategory successfully added'
			  });
		}, web_url + 'webconfig/request/miscellaneous?type=add&subtype=subcategory', formData);
	});

	$("#add-link").click(function() {
		var formData = new FormData();

		formData.append("categoryID", $(".link-form[name='category_id']").val());
		formData.append("subcategoryID", $(".link-form[name='subcategory_id']").val());

		quick_request(function(data) {
			
			load_data("linked");
			reload_categories();
			  Toast.fire({
			    icon: 'success',
			    title: 'Subcategory successfully added'
			  });
		}, web_url + 'webconfig/request/miscellaneous?type=add&subtype=linked', formData);
	});
	
</script>

<div style="display: none;">
	<table>
		<tr id="clone-tr">
			<td class="td-category category-name"></td>
			<td>
				<button type="button" class="btn btn-sm bg-red category-delete" data-session-id="">
					<i class="fas fa-trash"></i>
				</button>
			</td>
		</tr>

		<tr id="clone-tr-subcategory">
			<td class="td-subcategory category-name"></td>
			<td>
				<button type="button" class="btn btn-sm bg-red category-delete" data-session-id="">
					<i class="fas fa-trash"></i>
				</button>
			</td>
		</tr>

		<tr id="clone-tr-linked">
			<td class="td-linked category-parent"></td>
			<td class="td-linked category-child"></td>
			<td>
				<button type="button" class="btn btn-sm bg-red category-delete" data-session-id="">
					<i class="fas fa-trash"></i>
				</button>
			</td>
		</tr>
	</table>
</div>