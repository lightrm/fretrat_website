<?= $this->include("Includes/Header_content") ?>

<style type="text/css">
	.div-content {
		cursor: pointer;
	}

	.div-content:hover {
		text-decoration: underline;
	}
</style>

<section class="content">

	<div class="card card-lightblue">

		<div class="card-header">
			<h1 class="card-title">Home</h1>
		</div>

		<div class="card-body">
			<div class="row">
				<div class="col-12 card bg-gray-light">
					<div class="card-body">
						<h3 class="text-center font-weight-bold">What Are you Looking For?</h3>

						<div class="div-container text-center" data-id="WAYLF"></div>

						<div class="row m-auto" style="max-width: 80%">
							<div class="col-3 card card-body">
								<h5 class="text-center">Condominium</h5>
								<div class="div-container text-center" data-id="WAYLF-AC"></div>
							</div>

							<div class="col-3 card card-body">
								<h5 class="text-center">House and Lot</h5>
								<div class="div-container text-center" data-id="WAYLF-H"></div>
							</div>
							
							<div class="col-3 card card-body">
								<h5 class="text-center">Comercial/Office</h5>
								<div class="div-container text-center" data-id="WAYLF-C"></div>
							</div>

							<div class="col-3 card card-body">
								<h5 class="text-center">Support 24/7</h5>
								<div class="div-container text-center" data-id="WAYLF-S247"></div>
							</div>

						</div>
					</div>
				</div>

				<div class="col-12 card">
					<div class="card-body">
						<h3 class="text-center font-weight-bold">Most Popular Places</h3>
						<div class="div-container text-center mb-2" data-id="MPP"></div>

						<div class="row m-auto" style="max-width: 80%">
							<div class="col-4 row">
								<div class="col-12 card card-body bg-gray-light" style="min-height: 20vh;">

								</div>

								<div class="col-12 card card-body bg-gray-light" style="min-height: 20vh;">

								</div>
							</div>
							<div class="col-4 row">
								<div class="col-12 card card-body bg-gray-light" style="min-height: 20vh;">

								</div>

								<div class="col-12 card card-body bg-gray-light" style="min-height: 20vh;">

								</div>
							</div>

							<!-- <div class="col-4 card card-body bg-gray-light mr-2 ml-2">

							</div> -->
							<div class="col-4 row">
								<div class="col-12 card card-body bg-gray-light" style="min-height: 20vh;">

								</div>

								<div class="col-12 card card-body bg-gray-light" style="min-height: 20vh;">

								</div>
							</div>

						</div>
					</div>
				</div>


				<div class="col-12 card bg-gray-light">
					<div class="card-body">
						<h5 class="font-weight-bold">Need Assistance</h5>

						<div class="div-container" data-id="LBHE"></div>
					</div>
				</div>

				<div class="col-12 card">
					<div class="card-body">
						<h3 class="text-center font-weight-bold">Meet Team Fretrato</h3>
						<div class="div-container text-center mb-2" data-id="MOA"></div>

						<div class="row m-auto" style="max-width: 80%">
							<div class="col-3 card card-body bg-gray-light" style="min-height: 35vh;"></div>
							<div class="col-3 card card-body bg-gray-light" style="min-height: 35vh;"></div>
							<div class="col-3 card card-body bg-gray-light" style="min-height: 35vh;"></div>
							<div class="col-3 card card-body bg-gray-light" style="min-height: 35vh;"></div>

						</div>
					</div>
				</div>

				

			</div>
		</div>
		
	</div>

</section>