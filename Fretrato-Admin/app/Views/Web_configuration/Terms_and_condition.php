<?= $this->include("Includes/Header_content") ?>
<?php

?>

<section class="content">

  <div class="row">

    <div class="col-md-12">
      <div class="card card-lightblue">
        <div class="card-header">
          <h3 class="card-title">
            Image
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body pad">

            <div class="form-group">
                <label class="text-gray-dark">Thumbnail</label>
                <div class="col-3 m-auto">
                    <input name=imgThumb class="file-upload" type="file" data-max-file-count="1" data-theme="fas" accept="image/jpeg, image/jpg, image/png">
                </div>      
            </div>

        </div>
        <!-- /.card-body -->

      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-lightblue">
        <div class="card-header">
          <h3 class="card-title">
            Terms and Condition
          </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body pad overlay-wrapper">

        	<div class="overlay" id="list-overlay">
            	<i class="fas fa-3x fa-sync-alt fa-spin"></i>
          	</div>
        	<!-- /.overlay -->

			<div class="content" id="edit-content" style="display: none;">
				<div class="d-flex justify-content-end mb-2">
					<button class="btn btn-success" id="btn-save-edit">Save</button>
					&nbsp;
					<div id="btn-cancel">
						<button class="btn btn-default" id="btn-cncl-edit">Cancel</button>
					</div>
				</div>
				<textarea class="textarea" id="content-editor"></textarea>
			</div>
        	<!-- /.content -->

			<div class="content" id="div-content">
				<div class="d-flex justify-content-end">
					<button class="btn btn-info btn-sm" id="btn-edit"><i class="fas fa-edit"></i> Edit</button>
				</div>
				<div id="content-about"></div>
			</div>
        	<!-- /.content -->

        </div>
        <!-- /.card-body -->

      </div>
    </div>
    <!-- /.col-->
  </div>
  <!-- ./row -->

</section>

<script>
  $(function () {
    // Summernote
    $('.textarea').summernote({
    	placeholder: 'Edit Content Here',
    	height: 1000,
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
        toolbar: summernoteToolBar,
        callbacks: {
            onPaste: function (e) {
                $('#summernote').summernote('removeFormat')
            }
        }
    });
    $('.note-editable p').each( function() {
        $(this).css('margin-bottom', 0)
    })

    $(".file-upload").fileinput({
        showCaption: false,
        showRemove: false,
        showUpload: false,
        uploadUrl: web_url+ "webconfig/request/save_content?type=img_upload&sub_type=Terms_and_Condition",
        initialPreview: [
        <?php if( !empty($imageDets) ) : ?>
            '<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".$imageDets['filename'], $imageDets['directory']) ?>" class="file-preview-image kv-preview-data" alt="<?= $imageDets['filename'] ?>">'
        <?php endif ?>],
        initialPreviewConfig: [
        <?php if( !empty($imageDets) ): ?>
        {
            caption: '<?= $imageDets['directory'] ?>',
            url: web_url + 'webconfig/request/delete_s_image?type=Terms_and_Condition&id=<?= !empty($imageDets)?$imageDets['id']:'' ?>'
        }
        <?php endif ?>]
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        // console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
        Toast.fire({
            icon: 'success',
            title: `File ${previewId} Uploaded!`
        });
    }).on('fileuploaderror', function(event, data, msg) {
        // console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
        Toast.fire({
            icon: 'error',
            title: `File ${data.fileId} Failed to upload!`
        });
    }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
        Toast.fire({
            icon: 'success',
            title: `Batch Upload Complete`
        });
    });


    quick_request(setup_config, '<?= base_url("webconfig/request/load_content?type=terms_and_condition") ?>');

    $("#btn-edit").click(function() {
    	change_setup("edit");
    });

    $("#btn-cncl-edit").click(function() {
    	change_setup("display");
    });

    $("#btn-save-edit").click(function() {
    	$("#list-overlay").show();
		var formData = new FormData();
		formData.append("Contents", $("#content-editor").val());

		quick_request(function(data) { 
    		$("#content-about").html($("#content-editor").val());
		    change_setup("display");

		    $("#list-overlay").hide();

		    Toast.fire({
		      icon: 'success',
		      title: 'Successfully Saved!'
		    });

	    }, '<?= base_url("webconfig/request/save_content?type=terms_and_condition") ?>', formData);
    });

    function setup_config(data)
    {
    	$("#list-overlay").hide();
    	if(data.content == undefined || data.content == null || data.content == "") {
    		change_setup("edit", false);
    	} else {
    		$('#content-editor').summernote('code', data.content);
    		$("#content-about").html(data.content);
    	}
    	
    }

    function change_setup(type, withEdit = true)
    {
    	if(type == "edit") {
	    	$("#edit-content").show();
			$("#div-content").hide();
		} else {
	    	$("#edit-content").hide();
			$("#div-content").show();
		}

		if(withEdit) {
			$("#btn-cancel").show();
		} else {
			$("#btn-cancel").hide();
		}
    }

  })
</script>