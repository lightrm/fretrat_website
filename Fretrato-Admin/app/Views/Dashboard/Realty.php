<div class="row">
  <?php /*
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Area <span class="font-weight-normal text-md">with most</span> Inquiry</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="location-chart" height="200"></canvas>
        </div>

      </div>
    </div>
    <!-- /.card -->


    <!-- Additional Card Here -->


  </div>
  <!-- /.col-md-12 -->
  */ ?>

  
  <div class="col-lg-6">
    <?php /*
    <div class="card">
      <?php
        $sampleChart = rand(0, 1);
      ?>
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Inquiries</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg"><?= rand(1, 50) ?></span>
            <span>Inquiries Over Time</span>
          </p>
          <p class="ml-auto d-flex flex-column text-right">
            <span class="<?= ($sampleChart)? "text-success":"text-danger" ?>">
              <?php if($sampleChart) { ?> 
                <i class="fas fa-arrow-up"></i> 
              <?php } else { ?> 
                <i class="fas fa-arrow-down"></i>
              <?php } ?>

              <?= rand(1, 12) . "%" ?>
            </span>
            <span class="text-muted">Since Last Year</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="visitors-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span class="mr-2">
            <i class="fas fa-square text-primary"></i> This Year
          </span>

          <span>
            <i class="fas fa-square text-gray"></i> Last Year
          </span>
        </div>
      </div>
    </div>
    <!-- /.card -->
    */ ?>

    <div class="card">
      <div class="card-header border-0">
        <h3 class="card-title">Properties Approval</h3>
        <div class="card-tools">
        </div>
      </div>
      <div class="card-body overlay-wrapper table-responsive p-0">

        <div class="overlay" id="list-PropAprvl-overlay">
          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
        </div>

        <table id="PropAprvl-table" class="table table-striped table-valign-middle">
          <thead>
          <tr>
            <th>Property</th>
            <th>Requestor</th>
            <th width="1%">Action</th>
          </tr>
          </thead>
          <tbody>

          </tbody>
        </table>

      </div>
      <div class="card-footer">
        <div class="pagination-mini" data-load="load_data" data-id="PropAprvl" data-page="1"></div>
      </div>
    </div>
    <!-- /.card -->

    <!-- Additional Card Here -->

  </div>
  <!-- /.col-md-6 -->


  <div class="col-lg-6">

    <?php
      $leadInq = "Pre-Selling"; /*
    ?>
    <div class="card">
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Most Inquiries</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg"><?= $leadInq ?></span>
            <span>Most Inquries</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="sales-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span class="mr-2">
            <i class="fas fa-square text-primary"></i> This Month
          </span>

          <span>
            <i class="fas fa-square text-gray"></i> Last Month
          </span>
        </div>
      </div>
    </div>
    <!-- /.card -->
    */ ?>

    <div class="card">
      <div class="card-header border-0">
        <h3 class="card-title">Properties Inquiries</h3>
        <div class="card-tools">
        </div>
      </div>
      <div class="card-body overlay-wrapper table-responsive p-0">

        <div class="overlay" id="list-PropInq-overlay">
          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
        </div>

        <table id="PropInq-table" class="table table-striped table-valign-middle">
          <thead>
          <tr>
            <th>Property/About</th>
            <th>Inquirer</th>
            <th width="10%">Action</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <div class="card-footer">
        <div class="pagination-mini" data-load="load_data" data-id="PropInq" data-page="1"></div>
      </div>
    </div>
    <!-- /.card -->


    <!-- Additional Card Here -->


  </div>
  <!-- /.col-md-6 -->
</div>
<!-- /.row -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <input class="hd-inquiry-id" type="hidden">
      <input id="hd-dlt-lnk" type="hidden">
      <div class="modal-header">
        <h4 class="modal-title">Delete Inquiry</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this data?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button id="btn-delete" type="button" class="btn btn-danger">
          &nbsp;
          Delete 
          &nbsp;
          <i id="spn-amty" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">

$(document).ready(function() {
  load_data("PropAprvl");
  load_data("PropInq");




  $(document).on("click", ".approval-btn", function() {
    quick_request(
      function() {

        load_data("PropAprvl");
        Toast.fire({
          icon: 'success',
          title: 'Action Executed!'
        });
      },
      web_url + "home/request/change_approval?id=" + $(this).data("session-id") + "&property_id=" + $(this).data("property-id") + "&approval=" + $(this).data("approval")
    );
  });

  $("#btn-delete").click(function() {

    $("#spn-amty").show();

    quick_request(function(data) { 
      load_data("PropInq");
      $("#spn-amty").hide();
      $("#modal-default").modal("hide");
      Toast.fire({
        icon: 'success',
        title: 'Inquiry Deleted!'
      });
    }, web_url + "home/request/delete_inquiry?ID=" + $(".hd-inquiry-id").val());

  });


  $(document).on("click", ".delete-btn", function() {

    $(".hd-inquiry-id").val($(this).data("session-id"))
    $("#modal-default").modal("show")

  });








});
function render_realty() {

  trace("Realty");
  <?php /*

  salesChart.reset();
  visitorsChart.reset();
  locationChart.reset();

  salesChart.data.datasets[0].data = [<?= 
    join(", ", array(5, 34, 2)); 
  ?>];
  salesChart.data.datasets[1].data = [<?= 
    join(", ", array(3, 15, 1));
  ?>];

  salesChart.update();


  visitorsChart.data.datasets[0].data = [<?= 
    join(", ", array(10, 20, 40, 20, 2, 50, 13)); 
  ?>];
  visitorsChart.data.datasets[1].data = [<?= 
    join(", ", array(0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 5)); 
  ?>];

  visitorsChart.update();


  locationChart.data.labels = [<?= 
    join(", ", array("'Manila'", "'Makati'", "'Bulacan'", "'Cavite'", "'Pasig'", "'Quezon'", "'Parañaque'")); 
  ?>];

  locationChart.data.datasets[0].data = [<?= 
    join(", ", array(5, 34, 2, 24, 10, 16, 2)); 
  ?>];

  locationChart.update();

  */ ?>
};







function load_data(id, offset)
{
  var load_url = (id == "PropAprvl")?'load_approval':'load_inqury';

  $("#list-" + id +"-overlay").show();
  quick_request(function(data) { 
    load_minipagination(data.pagination, id);
    setup_data(data.dataList, id);
    $("#list-"+ id +"-overlay").hide();
  }, web_url + 'home/request/'+ load_url +'?page=' + $(".pagination-mini[data-id='"+ id +"']").data("page"));

}

function setup_data(list, id)
{
  remove_alldata(id);

  trace("loading Data");
  if(id == "PropAprvl")
    load_approvalDetails(list);
  else
    load_inquiryDetails(list);

}

function load_approvalDetails(list)
{
  var i = 1;
  for(var ccategory in list)
  {
    var categoryDets = list[ccategory];
    var NewRow = $("#clone-tr-PropAprvl").clone();

    NewRow.removeAttr("id");
    NewRow.find(".target-name").attr("href", web_url + "realty/properties?search=" + categoryDets.Property['SessionID']);
    NewRow.find(".td-image").attr("src", categoryDets.Property['Image']);
    NewRow.find(".td-name").html(categoryDets.Property['Name']);
    NewRow.find(".target-user").attr("href", web_url + "users?search=" + categoryDets.Agent['SessionID']);
    NewRow.find(".td-user").html(categoryDets.Agent['Name']);


    NewRow.find(".approval-btn").data("session-id", categoryDets.SessionID);
    NewRow.find(".approval-btn").data("property-id", categoryDets.Property['SessionID']);

    $("#PropAprvl-table tbody").append(NewRow);

    i++;
  }
}

function load_inquiryDetails(list)
{
  var i = 1;
  for(var ccategory in list)
  {
    var categoryDets = list[ccategory];
    var NewRow = $("#clone-tr-PropInq").clone(), urlLink = 'contact';

    NewRow.removeAttr("id");
    NewRow.find(".td-user").html(categoryDets.Inquirer['Name']);
    NewRow.find(".td-property").html(categoryDets.Property['Name']);

    if( categoryDets.Property['SessionID'] !== null ) {
      NewRow.find(".td-property").text("");
      var imageThumb = $('<img class="img-size-50 mr-2 td-image">'),
          targetLink = $('<a target="_blank"></a>');
      imageThumb.attr("src", categoryDets.Property['Image']);
      targetLink.attr("href", web_url + "realty/properties?search=" + categoryDets.Property['SessionID'])
      targetLink.append(imageThumb)
      targetLink.append(categoryDets.Property['Name'])
      urlLink = 'property'
      NewRow.find(".td-property").prepend(targetLink);
    }

    NewRow.find(".delete-btn").data("session-id", categoryDets.SessionID);
    NewRow.find(".view-btn").attr("href", web_url + "inquiry/message/" + categoryDets.SessionID);

    $("#PropInq-table tbody").append(NewRow);

    i++;
  }
}

function remove_alldata(id)
{
  trace("Removing Data");
  $("#"+id+"-table tbody").children().remove();
}

</script>

<div style="display: none;">
  <table>
    <tr id="clone-tr-PropAprvl">
      <td>
        <a class="target-name" target="_blank">
          <img class="img-size-50 mr-2 td-image">
          <span class="td-name"></span>
        </a>
      </td>
      <td>
        <a class="target-user" target="_blank">
          <span class="td-user"></span>
        </a>
      </td>
      <td>
        <a href="javascript:void(0)" class="text-muted approval-btn" data-approval="true" title="Approve">
          <i class="fas fa-thumbs-up"></i>
        </a>
        &nbsp;
        <a href="javascript:void(0)" class="text-danger approval-btn" data-approval="false" title="Disapprove">
          <i class="fas fa-thumbs-down"></i>
        </a>
      </td>
    </tr>
  </table>
</div>  

<div style="display: none;">
  <table>
    <tr id="clone-tr-PropInq">
      <td class="td-property"></td>
      <td class="td-user"></td>
      <td>
        <a class="text-muted view-btn" target="_blank" title="View">
          <i class="fas fa-eye"></i>
        </a>
        &nbsp;
        <a href="javascript:void(0)" class="text-danger delete-btn" title="Delete">
          <i class="fas fa-trash"></i>
        </a>
      </td>
    </tr>
  </table>
</div>