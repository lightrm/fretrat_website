<div class="row"><?php /*
  <div class="col-lg-12">
    
    <div class="card">
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Country <span class="font-weight-normal text-md">with most</span> Booking</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="country-chart" height="200"></canvas>
        </div>

      </div>
    </div>
    <!-- /.card -->
    


    <!-- Additional Card Here -->


  </div>
  <!-- /.col-md-12 -->*/ ?>

  
  <div class="col-lg-6">
    <?php /*
    <div class="card">
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Travel Sales</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Travel report</span>
            <span>Sales Chart</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="travel-sales-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span class="mr-2">
            <i class="fas fa-square text-primary"></i> This Month
          </span>

          <span>
            <i class="fas fa-square text-gray"></i> Last Month
          </span>
        </div>
      </div>
    </div>
    <!-- /.card -->

    <div class="card">
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Tour Sales</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Tour report</span>
            <span>Sales Chart</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="tour-sales-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span class="mr-2">
            <i class="fas fa-square text-primary"></i> This Month
          </span>

          <span>
            <i class="fas fa-square text-gray"></i> Last Month
          </span>
        </div>
      </div>
    </div>
    <!-- /.card -->

    <div class="card">
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Hotel Sales</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Hotel report</span>
            <span>Sales Chart</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="hotel-sales-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span class="mr-2">
            <i class="fas fa-square text-primary"></i> This Month
          </span>

          <span>
            <i class="fas fa-square text-gray"></i> Last Month
          </span>
        </div>
      </div>
    </div>
    <!-- /.card -->

    */ ?>

    <div class="card">
      <div class="card-header border-0">
        <h3 class="card-title">Tour Inquiries</h3>
        <div class="card-tools">
        </div>
      </div>
      <div class="card-body table-responsive p-0">
        <table class="table table-striped table-valign-middle">
          <thead>
          <tr>
            <th>Tour</th>
            <th>Inquirer</th>
            <th width="10%">Action</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach (array(1, 2, 3) as $key => $value): ?>
          <tr>
            <td>
              <img src="<?= base_url($pluginDir) ?>/dist/img/default-150x150.png" alt="<?= "Image-for-"."(Tour Name)" ?>" class="img-circle img-size-32 mr-2">
              <?= "Tour Name" ?>
            </td>
            <td><?= "User ". rand(1, 50) ?></td>
            <td>
              <a href="#" class="text-muted" title="View">
                <i class="fas fa-eye"></i>
              </a>
              &nbsp;
              <a href="#" class="text-danger" title="Delete">
                <i class="fas fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.card -->

    <!-- Additional Card Here -->

  </div>
  <!-- /.col-md-6 -->


  <div class="col-lg-6">
    <?php /*
    <div class="card">
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Travel Inquiries</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Countries</span>
            <span>Inquiries of top 5 Over Time</span>
          </p>
          <p class="ml-auto d-flex flex-column text-right">
            <span>&nbsp;</span>
            <span class="text-muted">For the last 30 days</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="travel-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span>&nbsp;</span>
        </div>
      </div>
    </div>
    <!-- /.card -->

    <div class="card">
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Tour Inquiries</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Countries</span>
            <span>Inquiries of top 5 Over Time</span>
          </p>
          <p class="ml-auto d-flex flex-column text-right">
            <span>&nbsp;</span>
            <span class="text-muted">For the last 30 days</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="tour-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span>&nbsp;</span>
        </div>
      </div>
    </div>
    <!-- /.card -->

    <div class="card">
      <div class="card-header border-0">
        <div class="d-flex justify-content-between">
          <h3 class="card-title">Hotel Inquiries</h3>
        </div>
      </div>
      <div class="card-body">
        <div class="d-flex">
          <p class="d-flex flex-column">
            <span class="text-bold text-lg">Countries</span>
            <span>Inquiries of top 5 Over Time</span>
          </p>
          <p class="ml-auto d-flex flex-column text-right">
            <span>&nbsp;</span>
            <span class="text-muted">For the last 30 days</span>
          </p>
        </div>
        <!-- /.d-flex -->

        <div class="position-relative mb-4">
          <canvas id="hotel-chart" height="200"></canvas>
        </div>

        <div class="d-flex flex-row justify-content-end">
          <span>&nbsp;</span>
        </div>
      </div>
    </div>
    <!-- /.card -->
   */ ?>
    <div class="card">
      <div class="card-header border-0">
        <h3 class="card-title">Hotel Inquiries</h3>
        <div class="card-tools">
        </div>
      </div>
      <div class="card-body table-responsive p-0">
        <table class="table table-striped table-valign-middle">
          <thead>
          <tr>
            <th>Hotel</th>
            <th>Inquirer</th>
            <th width="10%">Action</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach (array(1, 2, 3) as $key => $value): ?>
          <tr>
            <td>
              <img src="<?= base_url($pluginDir) ?>/dist/img/default-150x150.png" alt="<?= "Image-for-"."(Hotel Name)" ?>" class="img-circle img-size-32 mr-2">
              <?= "Hotel Name" ?>
            </td>
            <td><?= "User ". rand(1, 50) ?></td>
            <td>
              <a href="#" class="text-muted" title="View">
                <i class="fas fa-eye"></i>
              </a>
              &nbsp;
              <a href="#" class="text-danger" title="Delete">
                <i class="fas fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.card -->


    <!-- Additional Card Here -->


  </div>
  <!-- /.col-md-6 -->
</div>
<!-- /.row -->

<script type="text/javascript">
function render_travel() {

  trace("Travel");

  <?php /*

  //Travel
  {
    travelSalesChart.data.datasets[0].data = [<?= 
      join(", ", array(rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000))); 
    ?>];
    tourSalesChart.data.datasets[1].data = [<?= 
      join(", ", array(0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 5)); 
    ?>];

    travelSalesChart.update();


    travelChart.data.labels = [<?= 
      join(", ", array("'Philippines'", "'Hong Kong'", "'India'", "'Vietnam'")); 
    ?>];
    travelChart.data.datasets[0].data = [<?= 
      join(", ", array(rand(0, 100), rand(0, 100), rand(0, 100), rand(0, 100))); 
    ?>];
    travelChart.update();
  }

  //Tour
  {
    tourSalesChart.data.datasets[0].data = [<?= 
      join(", ", array(rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000))); 
    ?>];
    tourSalesChart.data.datasets[1].data = [<?= 
      join(", ", array(0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 5)); 
    ?>];

    tourSalesChart.update();


    tourChart.data.labels = [<?= 
      join(", ", array("'Philippines'", "'Hong Kong'", "'India'", "'Vietnam'")); 
    ?>];
    tourChart.data.datasets[0].data = [<?= 
      join(", ", array(rand(0, 100), rand(0, 100), rand(0, 100), rand(0, 100))); 
    ?>];
    tourChart.update();
  }


  //Hotels
  {

    hotelSalesChart.data.datasets[0].data = [<?= 
      join(", ", array(rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000), rand(100, 10000))); 
    ?>];
    hotelSalesChart.data.datasets[1].data = [<?= 
      join(", ", array(0, 0, 0, 0, 0, 0, 0, 0 , 0, 0, 0, 5)); 
    ?>];

    hotelSalesChart.update();


    hotelChart.data.labels = [<?= 
      join(", ", array("'Philippines'", "'Hong Kong'", "'India'", "'Vietnam'")); 
    ?>];
    hotelChart.data.datasets[0].data = [<?= 
      join(", ", array(rand(0, 100), rand(0, 100), rand(0, 100), rand(0, 100))); 
    ?>];
    hotelChart.update();

  }

  */ ?>
};
</script>