<?= $this->include("Includes/Header_content") ?>

<div class="content">


	<div class="card card-lightblue">
		<div class="card-header">
			<h1 class="card-title">Contact Us</h1>

			<div class="card-tools">
				<button class="btn btn-tool delete-inquiry">
					<i class="fa fa-trash"></i>
				</button>
			</div>
		</div>

		<div class="card-body">
			

			<div class="w-75 m-auto">

				<img src="http://fretrato.com.ph/assets/fretrato/Images/fretrato-logo-name-2.png" class="img-fluid" style="height: 100px">
				<hr class="text-lightblue" style="border: 2px solid;">
				<h1 class="text-lightblue">
					<?= $inqDets['inquiry_name'] ?>
				</h1>
				<p class="font-italic text-gray"><?= $inqDets['inquiry_about'] ?> </p>

				<hr>
				
				<p class="text-gray">Contact #: <span class="font-weight-bold"><?= $inqDets['contact_no'] ?></span></p>
				<p class="text-gray">Email: <span class="font-weight-bold"><?= $inqDets['email'] ?></span></p>

				<hr>

				<p class="text-justify text-gray">
					<span class="font-weight-bold">Message: </span>
					<br>
					<?= $inqDets['message'] ?>
				</p>

				<!-- <p class="text-right">
					Inquirer, <br>
					<?= $inqDets['name'] ?>
				</p>

				<p>
					To whom it may concern,	
				</p>

				<p class="text-justify">
					&emsp;&emsp;&emsp;&emsp;<?= $inqDets['message'] ?>
				</p>

				<p class="text-right">
					Inquirer, <br>
					<?= $inqDets['name'] ?>
				</p> -->

			</div>
		</div>

	</div>


</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <input class="hd-inquiry-id" type="hidden" value="<?= $inqDets['inquiry_id'] ?>">
      <input id="hd-dlt-lnk" type="hidden">
      <div class="modal-header">
        <h4 class="modal-title">Delete Inquiry</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this data?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button id="btn-delete" type="button" class="btn btn-danger">
          &nbsp;
          Delete 
          &nbsp;
          <i id="spn-amty" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script type="text/javascript">

	$(document).on("click", ".delete-inquiry", function() {
		$("#modal-default").modal("show");
	});


	$("#btn-delete").click(function() {

		quick_request(function(data) { 
	      $("#spn-amty").hide();
	      Toast.fire({
	        icon: 'success',
	        title: 'Inquiry Deleted!'
	      });
	      window.location = '<?= base_url() ?>';
	    }, web_url + "home/request/delete_inquiry?ID=" + $(".hd-inquiry-id").val());

	});

	
</script>