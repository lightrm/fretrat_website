<?= $this->include("Includes/Header_content") ?>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?= number_format($tour_inquiry) ?></h3>

            <p>Tour Inquiries</p>
          </div>
          <div class="icon">
            <i class="fas fa-plane-departure"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?= number_format($hotel_inquiry) ?></h3>

            <p>Hotel Inquiries</p>
          </div>
          <div class="icon">
            <i class="nav-icon fas fa-building"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?= number_format($rental_inquiry) ?></h3>

            <p>Rental Properties</p>
          </div>
          <div class="icon">
            <i class="fas fa-home"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?= number_format($resale_inquiry) ?></h3>

            <p>Resale Properties</p>
          </div>
          <div class="icon">
            <i class="fas fa-dollar-sign"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">

      <div class="col-lg-12">
        <div class="header-dashboard">
          <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#realty-tab">Realty</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#travel-tab">Travel</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="col-lg-12">
        <div class="tab-content mt-2">
          <!-- "active" Required if panel has chart -->

          <div class="tab-pane active" id="realty-tab">
            <?= $this->include("Dashboard/Realty") ?>
          </div>
          <!-- /#realty-tab -->

          <div class="tab-pane active" id="travel-tab">
            <?= $this->include("Dashboard/Travel") ?>
          </div>
          <!-- /#travel-tab -->


        </div>
        <!-- /.tab-content -->
      </div>

      
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->

</section>
<!-- /.content -->


<script type="text/javascript">
  //$.cookie("active-dashboard-panel", 1, { expires : 10 });

  var activePanel = Cookies.get("active-panel");

  $(".header-dashboard .nav-link").click(function(){

    // Save Cookie for last panel
    Cookies.set("active-panel", $(this).attr("href"), {
       expires : 1,
       path: "/"
    });

    //window["render_"+($(this).attr("href").replace("-tab", "")).replace("#", "")]();

  });


  $(function() {
    render_realty();
    render_travel();

    // Insert Default if no cookie
    {
      if(activePanel == undefined || activePanel == null)
      {
        $($(".header-dashboard .nav-link")[0]).click();
        activePanel = Cookies.get("active-panel");
      }
    }

    //Activate Panel
    {

      $(".tab-pane").each(function(){ $(this).removeClass("active") });
      $(".header-dashboard .nav-link").each(function(){ $(this).removeClass("active") });
      $(".header-dashboard .nav-link[href='"+ activePanel +"']").click();
      $(".header-dashboard .nav-link[href='"+ activePanel +"']").addClass("active");
      $(activePanel).addClass("active");

    }
  })
</script>


<div style="display: none;">
  <table>
    <tr id="clone-tr-PropInq">
      <td class="td-name"></td>
      <td class="td-user"></td>
      <td>
        <a href="#" class="text-muted" title="View">
          <i class="fas fa-eye"></i>
        </a>
        &nbsp;
        <a href="javascript:void(0)" class="text-danger" title="Delete">
          <i class="fas fa-trash"></i>
        </a>
      </td>
    </tr>
  </table>
</div>