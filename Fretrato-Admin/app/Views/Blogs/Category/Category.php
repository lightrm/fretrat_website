<?= $this->include("Includes/Header_content") ?>
<style type="text/css">
	.td-amentity{
		cursor: pointer;
	}
</style>

<div class="content">

	<div class="mb-2">
		<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-lg"><i class="fa fa-plus"></i> Add Category</button>
	</div>

	<div class="row">
		<div class="col">
			<div class="card card-lightblue">
		      <div class="card-header">
		      		<h4 class="card-title"> Real Estate Blogs  </h4>
		      </div>
		      <!-- /.card-header -->
		      <div class="card-body overlay-wrapper">

		      	<div class="overlay" id="list-PYBLN-overlay">
		          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
		        </div>

		        <table id="PYBLN-table" class="table table-bordered table-hover">
		          <thead>
			          <tr>
			            <th>Name</th>
			            <th width="60%">Description</th>
			            <th width="2%">Action</th>
			          </tr>
		          </thead>
		          <tbody>
		          </tbody>
		        </table>
		      </div>
		      <!-- /.card-body -->

		      
			    <div class="card-footer">
			      <div class="pagination-mini" data-load="load_data" data-id="PYBLN" data-page="1"></div>
			    </div>
		    </div>
		    <!-- /.card -->
		</div>
		<div class="col">
			<div class="card card-lightblue">
		      <div class="card-header">
        			<h4 class="card-title"> Travel and Tours Blogs</h4>
		      </div>
		      <!-- /.card-header -->
		      <div class="card-body overlay-wrapper">

		      	<div class="overlay" id="list-REAT-overlay">
		          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
		        </div>

		        <table id="REAT-table" class="table table-bordered table-hover">
		          <thead>
			          <tr>
			            <th>Name</th>
			            <th width="60%">Description</th>
			            <th width="2%">Action</th>
			          </tr>
		          </thead>
		          <tbody>
		          </tbody>
		        </table>
		      </div>
		      <!-- /.card-body -->

		      
			    <div class="card-footer">
			      <div class="pagination-mini" data-load="load_data" data-id="REAT" data-page="1"></div>
			    </div>
		    </div>
		    <!-- /.card -->
		</div>
	</div>

</div>





<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <input class="hd-category-id" type="hidden">
	    <input id="hd-dlt-lnk" type="hidden" value="<?= base_url($spageURL."request/delete_category") ?>">
	    <div class="modal-header">
	      <h4 class="modal-title">Delete Category</h4>
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	    </div>
	    <div class="modal-body">
	    	<h5>Deleting this data will <span class="font-weight-bold text-red">FORCE DELETE</span> connected blogs</h5>
	    	<p>Are you sure you want to continue?</p>
	    </div>
	    <div class="modal-footer justify-content-between">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      <button id="btn-delete-amty" type="button" class="btn btn-danger">
	        &nbsp;
	        Delete 
	        &nbsp;
	        <i id="spn-amty" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
	      </button>
	    </div>
	  </div>
	  <!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">New Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
			<div class="form-group row">
				<label class="col-4">Category Type</label>
				<div class="col-8">
					<select class="form-control" id="txt-category-type-new">
						<option value="plan_your_best_life_now">Real Estate Blogs</option>
						<option value="real_estate_and_travel">Travel and Tours Blogs</option>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-4">Category Name</label>
				<div class="col-8">
					<input type="text" class="form-control" id="txt-category-name-new">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-4">Category Description</label>
				<div class="col-8">
					<textarea class="form-control" id="txt-category-desc-new" rows="6"></textarea>
				</div>
			</div>
			<div class="modal-footer text-right">
				<button type="button" class="btn btn-primary" id="btn-add-category">
	        		&nbsp;
					Add
	        		&nbsp;
					<i id="spn-add" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
				</button>
			</div>
    	</div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-edit-amty">
	<input class="hd-category-id" type="hidden">
	<input id="hd-edt-lnk" type="hidden" value="<?= base_url($spageURL."request/edit_category") ?>">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Amenity</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
			<div class="form-group row">
				<label class="col-4">Category Type</label>
				<div class="col-8">
					<select class="form-control" id="txt-category-type-edit">
						<option value="plan_your_best_life_now">Real Estate Blogs</option>
						<option value="real_estate_and_travel">Travel and Tours Blogs</option>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-4">Category Name</label>
				<div class="col-8">
					<input type="text" class="form-control" id="txt-category-name-edit">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-4">Category Description</label>
				<div class="col-8">
					<textarea class="form-control" id="txt-category-desc-edit" rows="6"></textarea>
				</div>
			</div>
	        <div class="modal-footer text-right">
				<button type="button" class="btn btn-primary float-right" id="btn-edit-category">
	        		&nbsp;
					Save Changes
	        		&nbsp;
					<i id="spn-edit" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
	      		</button>
	        </div>
    	</div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function() {
		load_data("REAT");
		load_data("PYBLN");

	});

	$("body").on("click", ".category-delete", function() {
	    trace("Deleting: " + $(this).data("session-id"));
	    $(".hd-category-id").val($(this).data("session-id"));

	    $("#modal-default").modal("show");
	  });

	$("#btn-delete-amty").click(function() {

		$("#spn-amty").show();

		quick_request(function(data) { 
			load_data("REAT");
			load_data("PYBLN");
		  $("#spn-amty").hide();
		  $("#modal-default").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Category successfully deleted'
		  });
		}, $("#hd-dlt-lnk").val() + "?categoryID=" + $(".hd-category-id").val());

	});

	function load_data(id, offset)
	{
		var type = (id == "REAT")?"real_estate_and_travel":"plan_your_best_life_now";
		$("#list-" + id +"-overlay").show();
		quick_request(function(data) { 
			load_minipagination(data.pagination, id);
			setup_data(data.categlist, id);
			$("#list-"+ id +"-overlay").hide();
		}, '<?= base_url($spageURL.'/request/load_category') ?>?page=' + $(".pagination-mini[data-id='"+ id +"']").data("page") + "&type=" + type);

	}

	function setup_data(list, id)
	{
		remove_alldata(id);

		trace("loading Data");
		var i = 1;
		for(var ccategory in list)
		{
			var categoryDets = list[ccategory];
			var NewRow = $("#clone-tr").clone();

			NewRow.removeAttr("id");
			NewRow.find(".category-name").html(categoryDets.Name);
			NewRow.find(".category-desc").html(categoryDets.Description);
			NewRow.find(".category-name").data("session-id", categoryDets.SessionID);
			NewRow.find(".category-desc").data("session-id", categoryDets.SessionID);

			NewRow.find(".td-amentity").each(function() {
				$(this).data("category-type", categoryDets.Type);
				$(this).data("category-name", categoryDets.Name);
				$(this).data("category-desc", categoryDets.Description);
			});

			NewRow.find(".category-delete").data("session-id", categoryDets.SessionID);

			$("#"+id+"-table tbody").append(NewRow);

			i++;
		}

	}

	function remove_alldata(id)
	{
		trace("Removing Data");
		$("#"+id+"-table tbody").children().remove();
	}



	$(".content").on("click", ".td-amentity", function() {
		$(".hd-category-id").val($(this).data("session-id"));

		$("#txt-category-type-edit").val($(this).data("category-type"));
		$("#txt-category-name-edit").val($(this).data("category-name"));
		$("#txt-category-desc-edit").val($(this).data("category-desc"));
		$("#modal-edit-amty").modal("show");
	});

	$("#btn-edit-category").click(function() {
		$("#spn-edit").show();
		var formData = new FormData();

		formData.append("categoryType", $("#txt-category-type-edit").val());
		formData.append("categoryName", $("#txt-category-name-edit").val());
		formData.append("categoryDesc", $("#txt-category-desc-edit").val());
		formData.append("categoryID", $(".hd-category-id").val());

		quick_request(function(data) { 
			load_data("REAT");
			load_data("PYBLN");
		  $("#spn-edit").hide();
		  $("#modal-edit-amty").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Amenity successfully edited'
		  });
		}, $("#hd-edt-lnk").val(), formData);
	});

	$("#btn-add-category").click(function() {
		$("#spn-add").show();
		var formData = new FormData();

		formData.append("categoryType", $("#txt-category-type-new").val());
		formData.append("categoryName", $("#txt-category-name-new").val());
		formData.append("categoryDesc", $("#txt-category-desc-new").val());

		quick_request(function(data) { 
			$("#txt-category-name-new").val("");
			$("#txt-category-desc-new").val("");
			load_data("REAT");
			load_data("PYBLN");
			  $("#spn-add").hide();
			  $("#modal-lg").modal("hide");
			  Toast.fire({
			    icon: 'success',
			    title: 'Category successfully added'
			  });
			}, '<?= base_url($spageURL.'/request/add_category') ?>', formData);
	});

</script>

<div style="display: none;">
	<table>
		<tr id="clone-tr">
			<td class="td-amentity category-name"></td>
			<td class="td-amentity category-desc"></td>
			<td>
				<button type="button" class="btn btn-sm bg-red category-delete" data-session-id="">
					<i class="fas fa-trash"></i>
				</button>
			</td>
		</tr>
	</table>
</div>