<?= $this->include("Includes/Header_content") ?>

<style type="text/css">
  .card .card-widget {

  }
</style>

<section class="content">

  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12 mb-2">
        <a href="<?= base_url($spageURL."add?".current_url(true)->getQuery()) ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Post Blog</a>
      </div>

      <div class="col-md-12">
        <div class="card card-solid">

          <div class="card-header">
            <section class="navbar navbar-light bg-transparent">
              <div class="content float-left d-flex">
                <span class="text-gray-dark mr-2">Total Record: <span id="total-record"></span></span>
                <div class="filter-div d-flex" data-load="load_blogs"></div>
              </div>
              
              <div class="content float-right">
                <!-- SEARCH FORM -->
                <div class="input-group input-group-sm">
                  <input type="hidden" id="target-search" value="load_blogs">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" id="txtsearch">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="button" id="btnsearch">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <div class="card-body pb-0">
            <div class="row d-flex align-items-stretch overlay-wrapper" id="blog_list">

              <div class="overlay" id="list-overlay">
                <i class="fas fa-3x fa-sync-alt fa-spin"></i>
              </div>

            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <div class="pagination-ajax" data-load="load_blogs"></div>
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->

      </div>
      <!-- /.col-md-12 -->

    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->


</section>



<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <input id="hd-user-id" type="hidden">
        <input id="hd-dlt-lnk" type="hidden" value="<?= base_url($spageURL."request/delete_blogs") ?>">
        <div class="modal-header">
          <h4 class="modal-title">Delete Blog</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this data?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button id="btn-delete-user" type="button" class="btn btn-danger">
            &nbsp;
            Delete 
            &nbsp;
            <i id="spn-user" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
          </button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

    
<script type="text/javascript">
  
  $("body").on("click", ".blog-delete", function() {
    trace("Deleting: " + $(this).data("session-id"));
    $("#hd-user-id").val($(this).data("session-id"));

    $("#modal-default").modal("show");
  });

  $("#btn-delete-user").click(function() {

    $("#spn-user").show();

    quick_request(function(data) { 
      load_blogs();
      $("#spn-user").hide();
      $("#modal-default").modal("hide");
      Toast.fire({
        icon: 'success',
        title: 'User successfully deleted'
      });
    }, $("#hd-dlt-lnk").val() + "?id=" + $("#hd-user-id").val());

  });

  $(document).ready(function() {

    load_blogs();

  });

  function load_blogs(offset)
  {
    setup_pagination(offset);

    $("#list-overlay").show();

    quick_request(function(data) { 
      load_pagination(data.pagination);
      load_filter();
      $("#total-record").html(data.totalRecord);
      setup_blogs(data.userlist);
      $("#list-overlay").hide();
    }, '<?= base_url($spageURL.'request/load_blogs') ?>?' + URLParser().getQuery() + "&type=plan_your_best_life_now");

  }

  function setup_blogs(list)
  {
    remove_allblogs();

    trace("loading Properties");

    var query = "?" + URLParser().getQuery();
    var i = 1;
    for(var cUser in list)
    {
      var PropertyDets = list[cUser];
      var NewUser = $("#copy_blog").clone();

      NewUser.removeAttr("id");
      NewUser.find(".blog-title").html(PropertyDets.Title);
      NewUser.find(".blog-edit-info").attr("href", NewUser.find(".blog-edit-info").attr("href") + "/" +PropertyDets.SessionID + query);
      NewUser.find(".blog-delete").data("session-id", PropertyDets.SessionID);
      NewUser.find(".blog-img").attr("src", PropertyDets.Image);


        NewUser.find(".blog-category").append("Category: ");
      for( var cCategory in PropertyDets.Category )
      {
        var category = PropertyDets.Category[cCategory];
        var badge = $('<span class="badge badge-primary mr-1"></span>');

        badge.html( category.category_name );

        NewUser.find(".blog-category").append(badge);
      } 
      
      



      // Agent      
      NewUser.find(".blog-author").html("Author: " + PropertyDets.Agent.Name);
      NewUser.find(".blog-author").attr("href", web_url + 'users?role='+ PropertyDets.Agent.Type.toLowerCase() + '&search=' + PropertyDets.Agent.SessionID);

      $("#blog_list").append(NewUser);
      
      i++;
    }



  }

  function remove_allblogs()
  {
    trace("Removing Properties");
    $("#blog_list").children().remove();
  }

</script>


<div style="display: none;">
  <div class="col-12" id="copy_blog">
    <div class="attachment-block clearfix">
      <div class="float-left">
        <img class="attachment-img blog-img" alt="Attachment Image">

        <div class="attachment-pushed">
          <h4 class="attachment-heading blog-title text-lightblue">Title</h4>

          <div class="attachment-text blog-category"></div>
          <div class="attachment-text blog-author">Author: </div>
          <!-- /.attachment-text -->
        </div>
        <!-- /.attachment-pushed -->                    
      </div>

      <div class="float-right margin-auto">
        <a class="btn btn-primary blog-edit-info btn-sm" href="<?= base_url("blogs/plan_your_best_life_now/edit") ?>"><i class="fas fa-edit"></i></a>
        <button class="btn btn-danger btn-sm blog-delete"><i class="fas fa-trash"></i></button>
      </div>

    </div>
  </div>
</div>