<?= $this->include("Includes/Header_content") ?>

<div class="content">
	<div class="callout callout-warning">
      <p>Fields with (<span class="text-red">*</span>) is required!</p>
    </div>
    <?= $this->include("Includes/Alert") ?>
	<form action="<?= base_url("blogs/real_estate_and_travel/Edit/".$blogDets['blog_id']."?".(current_url(true)->getQuery())) ?>" enctype="multipart/form-data" method="POST">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Title <span class="text-red">*</span></label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text">
												<div class="custom-control custom-checkbox">
						                        	<input class="custom-control-input" type="checkbox" name="ckpublish" id="publish-inp" <?= $blogDets['published']?"checked":"" ?>>
						                        	<label for="publish-inp" class="custom-control-label font-weight-normal">Publish</label>
						                        </div>
											</span>
										</div>
										<input type="type" name="txttitle" class="form-control" value="<?= old('txttitle')?old('txttitle'):$blogDets['blog_title'] ?>" required>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Category <span class="text-red">*</span></label>
									<span class="float-right"><i class="fas fa-sync-alt" id="btn-add-refresh" style="cursor: pointer;"></i></span>
									<div class="overlay-wrapper">
										<div class="overlay" id="list-overlay">
											<i class="fas fa-3x fa-sync-alt fa-spin"></i>
							            </div>
							            <select class="form-control select2" name="slctcategory[]" id="slct-ctgry" data-placeholder="Select Category" multiple required></select>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Body</label>
									<div>
										<textarea class="form-control textarea" name="txtbody"><?= old('txtbody')?old('txtbody'):$blogDets['body'] ?></textarea>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Embed Video</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text">
												<input type="checkbox" id="ck-video-url" <?= $blogDets['video_url'] != ""?"checked":"" ?>>
											</span>
										</div>
										<input type="text" class="form-control" id="inp-video-url" name="txtvideo" value="<?= $blogDets['video_url'] ?>">
									</div>
								</div>
							</div>




						</div>
						<!-- \.row -->
					</div>
					<!-- \.card-body -->
				</div>
				<!-- \.card -->
			</div>
			<!-- \.col-6 -->

			<div class="col-12">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">Images</h6>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse">
								<i class="fas fa-minus"></i>
							</button>
		                </div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label class="text-gray-dark">Thumbnail</label>
									<div class="col-6 m-auto">
										<input name=imgThumb class="file-upload" type="file" data-max-file-count="1" data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>		
								</div>

								
							</div>
							<div class="col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label class="text-gray-dark">Gallery</label>
									<div>
										<input name="imgGallery[]" class="gallery-upload" type="file" multiple data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>
								</div>
							</div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-12 -->

		</div>
		<!-- /.row -->

		<div class="text-right mb-2">
			<button class="btn btn-success">Save</button>
			<a class="btn btn-default" href="<?= base_url('blogs/real_estate_and_travel?'.current_url(true)->getQuery()) ?>">Cancel</a>
		</div>
	</form>

</div>

<script type="text/javascript">
	var prev_amenity = [<?= !empty($blog_category)?"'".implode("', '", array_map(function($row) { return $row['category_id'];  }, $blog_category))."'":'' ?>];
	$(function() {
		$('.textarea').summernote({
	    	placeholder: 'About the property',
	    	height: 300,
        	fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
	    	toolbar: summernoteToolBar,
	    	callbacks: {
		        onPaste: function (e) {
        			$('#summernote').summernote('removeFormat')
		        }
		    }
	    });
	    $('.note-editable p').each( function() {
	    	$(this).css('margin-bottom', 0)
	    })

		$(".file-upload").fileinput({
			showCaption: false,
			showRemove: false,
			showUpload: false,
			initialPreview: [
			<?php if( !empty($blogDets['blog_imgSession']) ) : ?>
				'<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".$blogDets['thumbnail'], $blogDets['thumbnail_dir']) ?>" class="file-preview-image kv-preview-data" alt="<?= $blogDets['thumbnail'] ?>">'
			<?php endif ?>],
			initialPreviewConfig: [
			<?php if( !empty($blogDets['blog_imgSession']) ): ?>
			{
				caption: '<?= $blogDets['thumbnail'] ?>',
				url: web_url + 'blogs/real_estate_and_travel/request/delete_imgGallery?id=<?= $blogDets['thumbnail_id'] ?>'
			}
			<?php endif ?>]
		});

		$(".gallery-upload").fileinput({
			showCaption: false,
			showRemove: false,
			showDrag: false,
			showUpload: false,
			initialPreview: [
			<?php foreach ($blog_gallery as $image): ?>
				'<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".$image['filename'], $image['directory']) ?>" class="file-preview-image kv-preview-data" alt="<?= $image['filename'] ?>">',
			<?php endforeach ?>],
			initialPreviewConfig: [
			<?php foreach ($blog_gallery as $image): ?>
			{
				caption: '<?= $image['filename'] ?>',
				url: web_url + 'blogs/real_estate_and_travel/request/delete_imgGallery?id=<?= $image['id'] ?>'
			},
			<?php endforeach ?>],
		});
	});
</script>