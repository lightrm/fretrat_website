<?= $this->include("Includes/Header_content") ?>
<div class="content mb-2">
	<div class="callout callout-info">
      <p>Fields with (<span class="text-red">*</span>) is required!</p>
    </div>
    <?= $this->include("Includes/Alert") ?>
    <form action="<?= base_url("account") ?>" enctype="multipart/form-data" method="POST">
		<div class="row">
			<div class="col-12">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">Account Details</h6>
					</div>
					<div class="card-body">
						<div class="row">

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Avatar</label>
									<div class="col-6 m-auto">
										<input name=imgThumb class="file-upload" type="file" data-max-file-count="1" data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>	
								</div>

								<div class="form-group">
									<label class="text-gray-dark">Gender</label>
									<div class="d-flex justify-content-around">
										<div class="custom-control custom-radio">
											<input class="custom-control-input" type="radio" id="male-gen" name="radgender" value="male" <?= session()->get("User")['gender'] == "male"?"checked":'' ?>>
											<label for="male-gen" class="custom-control-label font-weight-normal">Male</label>
										</div>

										<div class="custom-control custom-radio">
											<input class="custom-control-input" type="radio" id="female-gen" name="radgender" value="female" <?= session()->get("User")['gender'] == "female"?"checked":'' ?>>
											<label for="female-gen" class="custom-control-label font-weight-normal">Female</label>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="text-gray-dark">Address</label>
									<div><textarea name="txtaddress" class="form-control" placeholder="Address"><?= old('txtaddress')?old('txtaddress'):session()->get("User")['address'] ?></textarea></div>
								</div>

							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Full Name <span class="text-red">*</span></label>
									<div><input type="type" name="txtname" class="form-control" placeholder="Name" value="<?= old('txtname')?old('txtname'):session()->get("User")['name'] ?>" required></div>
								</div>

								<div class="form-group">
									<label class="text-gray-dark">Username <span class="text-red">*</span></label>
									<div><input type="type" name="txtusername" class="form-control" placeholder="Username" value="<?= old('txtusername')?old('txtusername'):session()->get("User")['username'] ?>" required></div>
								</div>

								<div class="form-group">
									<label class="text-gray-dark">Email <span class="text-red">*</span></label>
									<div><input type="email" name="txtemail" class="form-control" placeholder="Email" value="<?= old('txtemail')?old('txtemail'):session()->get("User")['email'] ?>" required/></div>
								</div>

								<div class="border border-info rounded p-1">
									<span class="text-gray-dark"><label class="text-info">Note:</label> No need to this fill up if you're not going to change the your password</span>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="text-gray-dark">Password</label>
												<div><input type="Password" name="txtpassword" class="form-control" placeholder="Password"></div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="text-gray-dark">Confirm Password</label>
												<div><input type="Password" name="txtpassword2" class="form-control" placeholder="Confirm"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="text-gray-dark">Contact</label>
									<div><input type="text" name="txtcontact" class="form-control" placeholder="Contact" value="<?= old('txtcontact')?old('txtcontact'):session()->get("User")['contact_no'] ?>" /></div>
								</div>

								<div class="form-group">
									<label class="text-gray-dark">About</label>
									<div><textarea name="txtabout" class="form-control" rows="3" placeholder="About"><?= old('txtabout')?old('txtabout'):session()->get("User")['about'] ?></textarea></div>
								</div>

							</div>

							<div class="col-md-12 mt-2">
								<div class="text-right">
									<button class="btn btn-success">Save</button>
								</div>
							</div>

						</div>
						<!-- /.card-row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-md-6 -->

			

		</div>
		<!-- /row -->
	</form>

</div>


<script type="text/javascript">
$(function() {
	$(".file-upload").fileinput({
		showCaption: false,
		showRemove: false,
		showRemove: false,
		showUpload: false,
		initialPreview: [
		<?php if( !empty(session()->get("User")['img_session']) ) : ?>
			'<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".session()->get("User")['user_avatar'], session()->get("User")['user_dir']) ?>" class="file-preview-image kv-preview-data" alt="<?= session()->get("User")['user_avatar'] ?>">'
		<?php endif ?>],
		initialPreviewConfig: [
		<?php if( !empty(session()->get("User")['img_session']) ): ?>
		{
			caption: '<?= session()->get("User")['user_avatar'] ?>'
		}
		<?php endif ?>],
		initialPreviewShowDelete: false
	});
});
</script>