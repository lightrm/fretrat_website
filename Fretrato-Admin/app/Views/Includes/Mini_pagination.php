<?php

/**
 * @var \CodeIgniter\Pager\PagerRenderer $pager
 */

$pager->setSurroundCount(2);
?>

<nav aria-label="<?= lang('Pager.pageNavigation') ?>">
  <ul class="pagination justify-content-center m-0">
    <?php if ($pager->hasPrevious()) : ?>
      <li class="page-item">
        <button type="button" class="page-link page-btn" data-page="1" aria-label="<?= lang('Pager.first') ?>">
          <span aria-hidden="true"><?= lang('Pager.first') ?></span>
        </button>
      </li>
      <!--li class="page-item">
        <button type="button" class="page-link page-btn" data-href="<?= $pager->getPrevious() ?>" aria-label="<?= lang('Pager.previous') ?>">
          <span aria-hidden="true"><?= lang('Pager.previous') ?></span>
        </button>
      </li-->
    <?php endif ?>

    <?php foreach ($pager->links() as $link) : ?>
      <li class="page-item page-btn <?= $link['active'] ? 'active' : '' ?>">
        <button type="button" class="page-link page-btn" data-page="<?= $link['title'] ?>">
          <?= $link['title'] ?>
        </button>
      </li>
    <?php endforeach ?>

    <?php if ($pager->hasNext()) : ?>
      <!--li class="page-item">
        <button type="button" class="page-link page-btn" data-href="<?= $pager->getNext() ?>" aria-label="<?= lang('Pager.next') ?>">
          <span aria-hidden="true"><?= lang('Pager.next') ?></span>
        </button>
      </li-->
      <li class="page-item">
        <?php
          $output = 1;
          $queries = parse_url($pager->getLast())['query'];
          foreach (explode("&", $queries) as $query) {
            $query = explode("=", $query);
            if(mb_strtolower($query[0]) == "page") {
              $output = $query[1];
              break;
            }
          }
        ?>
        <button type="button" class="page-link page-btn" data-page="<?= $output ?>" aria-label="<?= lang('Pager.last') ?>">
          <span aria-hidden="true"><?= lang('Pager.last') ?></span>
        </button>
      </li>
    <?php endif ?>
  </ul>
</nav>