<!DOCTYPE html>
<html>
<head>
  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fretrato | Administration <?= $headerTitle ?></title>
  <link rel="shortcut icon" href="<?= \App\Libraries\Imagelib::getImageURL('Admin_Icon.ico') ?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Toast -->
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/dist/css/adminlte.min.css">
  
<?php 
  if( isset($hdcss) ):
    foreach ($hdcss as $dir): ?>
<link rel="stylesheet" href="<?= base_url($pluginDir.'/'.$dir); ?>?version=<?= $srcVer ?>">
<?php endforeach; endif; ?>
  <!-- React -->
  <!-- Note: when deploying, replace "development.js" with "production.min.js". -->
  <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
  <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
  <!-- JSX -->
  <script src="https://unpkg.com/babel-standalone@6.26.0/babel.js"></script>
  <!-- jQuery -->
  <script src="<?= base_url($pluginDir); ?>/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url($pluginDir); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Cookie Master -->
  <script src="<?= base_url($pluginDir); ?>/dist/js/js-cookie-master/js.cookie.js"></script>
  <!-- Extra JS -->
  <script type="text/javascript" src="<?= base_url($pluginDir); ?>/dist/js/global.js"></script>
  <script type="text/javascript" src="<?= base_url($pluginDir); ?>/dist/js/notification.js"></script>


<?php 
  if( isset($hdjs) ):
    foreach ($hdjs as $dir): ?>
<script src="<?= base_url($pluginDir.'/'.$dir); ?>?version=<?= $srcVer ?>" type="text/javascript"></script>
<?php endforeach; endif; ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <?php if(isset($back_header)): ?>
      <li class="nav-item">
        <a class="nav-link" href="<?= $back_header ?>" title="Go Back"><i class="fas fa-chevron-circle-left"></i></a>
      </li>
      <?php endif; ?>
    </ul>   

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge" id="notif-badge" style="display: none;"><span class="notif-badge-count">0</span></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header read-all" style="cursor: pointer;"><span class="notif-badge-count">0</span> Notifications <small>(Mark All as Read)</small></span>
          <div class="dropdown-divider"></div>
          <div id="head-notif-list">

          </div>
        </div>
      </li>

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">