<?php if (!empty(session()->has("user_alert"))): ?>
<div class="card card-outline card-<?= session()->getFlashdata("user_alert")['type'] ?>">
  <!-- /.card-header -->
  <div class="card-body">
    <?= session()->getFlashdata("user_alert")['msg'] ?>
  </div>
  <!-- /.card-body -->
</div>
<?php endif ?>