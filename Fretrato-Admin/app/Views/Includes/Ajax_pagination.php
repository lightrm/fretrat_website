<?php

/**
 * @var \CodeIgniter\Pager\PagerRenderer $pager
 */

$pager->setSurroundCount(10);
?>

<nav aria-label="<?= lang('Pager.pageNavigation') ?>">
  <ul class="pagination justify-content-center m-0">
    <?php if ($pager->hasPrevious()) : ?>
      <li class="page-item">
        <button type="button" class="page-link page-btn" data-href="<?= $pager->getFirst() ?>" aria-label="<?= lang('Pager.first') ?>">
          <span aria-hidden="true"><?= lang('Pager.first') ?></span>
        </button>
      </li>
      <!--li class="page-item">
        <button type="button" class="page-link page-btn" data-href="<?= $pager->getPrevious() ?>" aria-label="<?= lang('Pager.previous') ?>">
          <span aria-hidden="true"><?= lang('Pager.previous') ?></span>
        </button>
      </li-->
    <?php endif ?>

    <?php foreach ($pager->links() as $link) : ?>
      <li class="page-item page-btn <?= $link['active'] ? 'active' : '' ?>">
        <button type="button" class="page-link page-btn" data-href="<?= $link['uri'] ?>">
          <?= $link['title'] ?>
        </button>
      </li>
    <?php endforeach ?>

    <?php if ($pager->hasNext()) : ?>
      <!--li class="page-item">
        <button type="button" class="page-link page-btn" data-href="<?= $pager->getNext() ?>" aria-label="<?= lang('Pager.next') ?>">
          <span aria-hidden="true"><?= lang('Pager.next') ?></span>
        </button>
      </li-->
      <li class="page-item">
        <button type="button" class="page-link page-btn" data-href="<?= $pager->getLast() ?>" aria-label="<?= lang('Pager.last') ?>">
          <span aria-hidden="true"><?= lang('Pager.last') ?></span>
        </button>
      </li>
    <?php endif ?>
  </ul>
</nav>