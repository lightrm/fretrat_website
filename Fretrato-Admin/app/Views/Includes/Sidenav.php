<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url() ?>" class="brand-link">
      <img src="<?= \App\Libraries\Imagelib::getImageURL('favicon.png'); ?>" alt="Fretrato Logo" class="brand-image"
           style="opacity: .8">
      <span class="brand-text font-weight-light text-lightblue">Fretrato</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= \App\Libraries\Imagelib::getImageURL(
              "avatar_small-".session()->get("User")['user_avatar'], 
              session()->get("User")['user_dir'], 
              (session()->get("User")['gender'] == 'male'? 'blank-user-male.png':'blank-user-female.png')
            ) ?>" class="img-circle bg-white elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= session()->get("User")['name']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->



          <?php if ( (int) session()->get("User")['role'] <= 1 ): ?>
          <li class="nav-item">
            <a href="<?= base_url() ?>" class="nav-link <?= current_url() == base_url()."/"? "active":'' ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview" data-type="users" data-open-drop="false">
            <a href="#" class="nav-link <?= (stripos(uri_string(), "users") !== false)?"active":'' ?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url("users/roles") ?>" class="nav-link <?= (stripos(uri_string(), "users/roles") !== false)?"active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Roles</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url("users") ?>" class="nav-link <?= (stripos(uri_string(), "users") !== false && stripos(uri_string(), "users/roles") === false && stripos(current_url(true)->getQuery(), "role") === false)?"active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Users</p>
                </a>
              </li>
              <?php foreach (\App\Libraries\Roleslib::getRoles() as $role): ?>
              <li class="nav-item">
                <a href="<?= base_url("users?page=1&role=".strtolower($role['name'])) ?>" class="nav-link  <?= (strpos(current_url(true)->getQuery(), "role=".strtolower($role['name'])) !== false)? "active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p><?= ucfirst($role['name']) ?></p>
                </a>
              </li>
              <?php endforeach ?>
            </ul>
          </li>
          <?php endif; ?>




          <li class="nav-header">REALTY</li>
          <li class="nav-item has-treeview" data-type="properties" data-open-drop="false">
            <a href="#" class="nav-link <?= (stripos(uri_string(), "realty/properties") !== false)?"active":'' ?>">
              <i class="fas fa-home"></i>
              <p>
                Properties
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url("realty/properties") ?>" class="nav-link <?= (stripos(uri_string(), "realty/properties") !== false && (stripos(uri_string(), "properties/recycle_bin") === false) && stripos(current_url(true)->getQuery(), "type") === false)?"active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Properties</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url("realty/properties?type=resale") ?>" class="nav-link  <?= (strpos(current_url(true)->getQuery(), "type=resale") !== false)? "active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Resale</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url("realty/properties?type=rental") ?>" class="nav-link  <?= (strpos(current_url(true)->getQuery(), "type=rental") !== false)? "active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Rental</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url("realty/properties?type=pre-selling") ?>" class="nav-link  <?= (strpos(current_url(true)->getQuery(), "type=pre-selling") !== false)? "active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pre-Selling</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url("realty/properties/recycle_bin") ?>" class="nav-link  <?= (stripos(uri_string(), "properties/recycle_bin") !== false)? "active":'' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Deleted Properties</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("realty/amenities") ?>" class="nav-link <?= current_url() == base_url("realty/amenities")? "active":'' ?>">
              <i class="fas fa-swimming-pool"></i>
              <p>
                Amenities
              </p>
            </a>
          </li> 
          <li class="nav-header">TOUR & TRAVEL</li>
          <li class="nav-item">
            <a href="<?= base_url("tour_and_travel/travel") ?>" class="nav-link <?= current_url() == base_url("tour_and_travel/travel")? "active":'' ?>">
              <i class="fas fa-plane"></i>
              <p>
                Travels
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("tour_and_travel/tour") ?>" class="nav-link <?= current_url() == base_url("tour_and_travel/tour")? "active":'' ?>">
              <i class="fas fa-bus"></i>
              <p>
                Tours
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("tour_and_travel/hotel") ?>" class="nav-link <?= current_url() == base_url("tour_and_travel/hotel")? "active":'' ?>">
              <i class="fas fa-building"></i>
              <p>
                Hotels
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("tour_and_travel/promo") ?>" class="nav-link <?= current_url() == base_url("tour_and_travel/promo")? "active":'' ?>">
              <i class="fas fa-percent"></i>
              <p>
                Promos
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("tour_and_travel/amenities") ?>" class="nav-link <?= current_url() == base_url("tour_and_travel/amenities")? "active":'' ?>">
              <i class="fas fa-swimming-pool"></i>
              <p>
                Amenities
              </p>
            </a>
          </li> 


          <li class="nav-header">BLOGS</li>
          <li class="nav-item">
            <a href="<?= base_url("blogs/Category") ?>" class="nav-link <?= (stripos(uri_string(), "blogs/Category") !== false && stripos(current_url(true)->getQuery(), "type") === false)?"active":'' ?>">
              <i class="nav-icon fas fa-blog"></i>
              <p>
                Category
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("blogs/plan_your_best_life_now") ?>" class="nav-link <?= (stripos(uri_string(), "blogs/plan_your_best_life_now") !== false && stripos(current_url(true)->getQuery(), "type") === false)?"active":'' ?>">
              <i class="nav-icon fas fa-blog"></i>
              <p>
                Real Estate Blogs
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("blogs/real_estate_and_travel") ?>" class="nav-link <?= (stripos(uri_string(), "blogs/real_estate_and_travel") !== false && stripos(current_url(true)->getQuery(), "type") === false)?"active":'' ?>">
              <i class="nav-icon fas fa-blog"></i>
              <p>
                Travel and Tours Blogs
              </p>
            </a>
          </li>


          <?php if ( (int) session()->get("User")['role'] <= 1 ): ?>
          <li class="nav-header">WEBSITE CONFIGURATION</li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/home") ?>" class="nav-link <?= current_url() == base_url("webconfig/home")? "active":'' ?>">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Home
                <?php /* <span class="badge badge-info right">2</span> */ ?>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/contact_us") ?>" class="nav-link <?= current_url() == base_url("webconfig/contact_us")? "active":'' ?>">
              <i class="nav-icon far fa-address-book"></i>
              <p>
                Contact Us
                <?php /* <span class="badge badge-info right">2</span> */ ?>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/about_us") ?>" class="nav-link <?= current_url() == base_url("webconfig/about_us")? "active":'' ?>">
              <i class="nav-icon far fa-question-circle"></i>
              <p>
                About Us
                <?php /* <span class="badge badge-info right">2</span> */ ?>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/privacy_policies") ?>" class="nav-link <?= current_url() == base_url("webconfig/privacy_policies")? "active":'' ?>">
              <i class="nav-icon fas fa-shield-alt"></i>
              <p>
                Privacy Policies
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/terms_and_condition") ?>" class="nav-link <?= current_url() == base_url("webconfig/terms_and_condition")? "active":'' ?>">
              <i class="nav-icon fas fa-file-contract"></i>
              <p>
                Terms and Condition
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/social_media") ?>" class="nav-link <?= current_url() == base_url("webconfig/social_media")? "active":'' ?>">
              <i class="nav-icon fa fa-hashtag"></i>
              <p>
                Social Media Links
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/miscellaneous") ?>" class="nav-link <?= current_url() == base_url("webconfig/miscellaneous")? "active":'' ?>">
              <i class="nav-icon fas fa-infinity"></i>
              <p>
                Miscellaneous
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("webconfig/meta") ?>" class="nav-link <?= current_url() == base_url("webconfig/meta")? "active":'' ?>">
              <i class="nav-icon fas fa-cloud-meatball"></i>
              <p>
                Meta
              </p>
            </a>
          </li>
          <?php endif ?>


          
          <li class="nav-header">MY ACCOUNT</li>
          <li class="nav-item">
            <a href="<?= base_url("account") ?>" class="nav-link <?= current_url() == base_url("account")? "active":'' ?>">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profile
                <?php /* <span class="badge badge-info right">2</span> */ ?>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("logout") ?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
                <?php /* <span class="badge badge-info right">2</span> */ ?>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>