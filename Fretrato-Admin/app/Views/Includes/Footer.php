  </div>

  <div class="modal modal-slg fade" id="return-data">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-body">
          <pre id="return-data-ctx"></pre>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2018-2020 <a href="https://fretrato.com.ph/">Fretrato Website</a></strong>
    
    <div class="float-right d-none d-sm-inline-block">
      All rights reserved.
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  //$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- overlayScrollbars -->
<script src="<?= base_url($pluginDir); ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url($pluginDir); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url($pluginDir); ?>/dist/js/adminlte.js"></script>
<script src="<?= base_url($pluginDir); ?>/dist/js/side-panel.js"></script>
<!-- Toast -->
<script src="<?= base_url($pluginDir); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/plugins/toastr/toastr.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/plugins/moment/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url($pluginDir); ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>


<?php 
  if( isset($ftjs) ):
    foreach ($ftjs as $object): ?>
      <?php if (is_string($object)) { ?>
        <script src="<?= base_url($pluginDir.'/'.$object); ?>?version=<?= $srcVer ?>" type="text/javascript"></script>
      <?php } elseif (is_array($object)) { ?>
        <script src="<?= base_url($pluginDir.'/'.$object['dir']); ?>?version=<?= $srcVer ?>" <?php array_map(function($index, $data){ echo $index.'="'.$data.'"'; }, array_keys($object['attr']), $object['attr']); ?>></script>
      <?php } ?>
<?php endforeach; endif; ?>

</body>
</html>
