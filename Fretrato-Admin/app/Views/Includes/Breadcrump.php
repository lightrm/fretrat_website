<ol class="breadcrumb float-sm-right">
  <?php foreach ($breadCrumps as $htmlval => $link): ?>
    <li class="breadcrumb-item"><?php if($link != '#' && $link != current_url() ){ ?> <a href="<?= $link ?>"> <?php } ?><?= $htmlval ?><?php if($link != '#'){ ?></a><?php } ?></li>
  <?php endforeach ?>
</ol>