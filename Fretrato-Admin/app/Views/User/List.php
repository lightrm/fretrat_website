<?= $this->include("Includes/Header_content") ?>
<section class="content">
  
  <?= $this->include("Includes/Alert") ?>
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12 mb-2">
        <a href="<?= base_url("users/add?".current_url(true)->getQuery()) ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add User</a>
      </div>
      <!-- /.col-md-12 -->

      <div class="col-md-12">

        <!-- Default box -->
        <div class="card card-solid">

          <div class="card-header">
            <section class="navbar navbar-light bg-transparent">

              <div class="content float-left d-flex">
                <span class="text-gray-dark mr-2">Total Record: <span id="total-record"></span></span>
                <div class="filter-div d-flex" data-load="load_users"></div>
              </div>
            
              <div class="content float-right">
                <!-- SEARCH FORM -->
                <div class="input-group input-group-sm">
                  <input type="hidden" id="target-search" value="load_users">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" id="txtsearch">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="button" id="btnsearch">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <div class="card-body pb-0">
            <div class="row d-flex align-items-stretch overlay-wrapper" id="user_list">

              <div class="overlay" id="list-overlay">
                <i class="fas fa-3x fa-sync-alt fa-spin"></i>
              </div>

            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <div class="pagination-ajax" data-load="load_users"></div>
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->
        
      </div>
      <!-- /.col-md-12 -->
      
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->

</section>



<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <input class="hd-user-id" type="hidden">
      <input id="hd-dlt-lnk" type="hidden" value="<?= base_url("users/request/delete_user") ?>">
      <div class="modal-header">
        <h4 class="modal-title">Delete user</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this user?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button id="btn-delete-user" type="button" class="btn btn-danger">
          &nbsp;
          Delete 
          &nbsp;
          <i id="spn-user" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Property List -->
<div class="modal modal-slg fade" id="property-list">
  <input class="hd-user-id" type="hidden">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Listing</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-12 col-md-12 col-sm-12 col-lg-4">
            <div class="card card-lightblue">
                <div class="card-header">
                  <h3 class="card-title">Resale</h3>

                  <div class="card-tools">
                    <div class="pagination-mini" data-load="load_properties" data-id="resale" data-page="1"></div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0 overlay-wrapper">
                  <div class="overlay" id="resale-overlay">
                  <i class="fas fa-3x fa-sync-alt fa-spin"></i>
                </div>
                <table class="table">
                    <tbody id="resale-tbl">
                      <tr>
                        <td colspan="2" class="text-center">No Resale listing</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
          </div>
          <!-- /.col-md-4 -->

          <div class="col-12 col-md-12 col-sm-12 col-lg-4">
            <div class="card card-lightblue">
              <div class="card-header">
                <h3 class="card-title">Rental</h3>

                <div class="card-tools">
                  <div class="pagination-mini" data-load="load_properties" data-id="rental" data-page="1"></div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0 overlay-wrapper">
                <div class="overlay" id="rental-overlay">
                <i class="fas fa-3x fa-sync-alt fa-spin"></i>
              </div>
                <table class="table">
                  <tbody id="rental-tbl">
                    <tr>
                      <td colspan="2" class="text-center">No Rental listing</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col-md-4 -->

          <div class="col-12 col-md-12 col-sm-12 col-lg-4">
            <div class="card card-lightblue">
              <div class="card-header">
                <h3 class="card-title">Pre-selling</h3>

                <div class="card-tools">
                  <div class="pagination-mini" data-load="load_properties" data-id="pre-selling" data-page="1"></div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0 overlay-wrapper">
                <div class="overlay" id="pre-selling-overlay">
                <i class="fas fa-3x fa-sync-alt fa-spin"></i>
              </div>
                <table class="table">
                  <tbody id="pre-selling-tbl">
                    <tr>
                      <td colspan="2" class="text-center">No Pre-selling listing</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col-md-4 -->

        </div>


      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Property List Delete -->
<div class="modal fade" id="modal-dlt-propery">
  <div class="modal-dialog">
    <div class="modal-content">
      <input id="hd-property-id" type="hidden">
      <input id="hd-property-rtn" type="hidden">
      <input id="hd-prty-dlt-lnk" type="hidden" value="<?= base_url("realty/properties/request/delete_property") ?>">
      <div class="modal-header">
        <h4 class="modal-title">Delete property</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this property?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button id="btn-delete-property" type="button" class="btn btn-danger">
          &nbsp;
          Delete 
          &nbsp;
          <i id="spn-property" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

    
<script type="text/javascript">
  
  $("body").on("click", ".user-delete", function() {
    trace("Deleting: " + $(this).data("session-id"));
    $(".hd-user-id").val($(this).data("session-id"));

    $("#modal-default").modal("show");
  });

  $("body").on("change", ".user-tcontact", function() {

    trace("Changing: " + $(this).data("session-id"));
    quick_request(function(data) { 
    Toast.fire({
      icon: 'success',
      title: 'Successfully  the information'
    });
    }, '<?= base_url("users/request/toogle_info?user=") ?>' + $(this).data('session-id') + '&toogle=' + ($(this).is(":checked")?1:0));
  });

  $("#btn-delete-user").click(function() {

    $("#spn-user").show();

    quick_request(function(data) { 
      load_users();
      $("#spn-user").hide();
      $("#modal-default").modal("hide");
      if(data.success) {
        Toast.fire({
          icon: 'success',
          title: 'User successfully deleted'
        });
      } else {
        Toast.fire({
          icon: 'warning',
          title: data.msg
        });
      }
    }, $("#hd-dlt-lnk").val() + "?user=" + $(".hd-user-id").val());

  });

  $(document).ready(function() {

    load_users();

  });

  function load_users(offset = null)
  {
    setup_pagination(offset);

    $("#list-overlay").show();

    quick_request(function(data) { 
      load_pagination(data.pagination);
      load_filter();
      $("#total-record").html(data.totalRecord);
      setup_users(data.userlist);
      $("#list-overlay").hide();
    }, '<?= base_url('users/request/load_users') ?>?' + URLParser().getQuery());


  }

  function setup_users(list)
  {
    remove_allusers();

    trace("loading Users");

    var query = "?" + URLParser().getQuery();
    var i = 1;
    for(var cUser in list)
    {
      var UserDets = list[cUser];
      var NewUser = $("#copy_user").clone();

      NewUser.removeAttr("id");
      NewUser.removeAttr("style");
      NewUser.find(".user-role").html(UserDets.Role);
      NewUser.find(".user-name").html(UserDets.Name);
      NewUser.find(".user-address").html(UserDets.Address);
      NewUser.find(".user-contact").html(UserDets.Contact);
      NewUser.find(".user-email").html(UserDets.Email);
      NewUser.find(".user-username").html(UserDets.Username);
      NewUser.find(".user-about").html(UserDets.About);
      NewUser.find(".user-img").attr("src", UserDets.Image);
      NewUser.find(".user-tcontact").prop("checked", UserDets.Tinfo);
      NewUser.find(".user-tcontact").attr("id", "user-"+i);
      NewUser.find(".user-tcontact-lbl").attr("for", "user-"+i);
      NewUser.find(".user-tcontact").data("session-id", UserDets.SessionID);
      NewUser.find(".user-edit-info").attr("href", NewUser.find(".user-edit-info").attr("href") + "/" +UserDets.SessionID + query);
      NewUser.find(".user-delete").data("session-id", UserDets.SessionID);
      NewUser.find(".listing-btn").data("session-id", UserDets.SessionID);

      $("#user_list").append(NewUser);
      
      i++;
    }



  }

  function remove_allusers()
  {
    trace("Removing Users");
    $("#user_list").children().remove();
  }


  /* Listings */

  $("body").on("click", ".listing-btn", function() {

    $("#property-list").modal("show");

    trace("Getting Listing: " + $(this).data("session-id"));
    $(".hd-user-id").val($(this).data("session-id"))
    load_properties("resale");
    load_properties("rental");
    load_properties("pre-selling");

  });

  $("body").on("click", ".property-dlt", function() {
    trace("Deleting: " + $(this).data("session-id"));
    $("#hd-property-id").val($(this).data("session-id"));
    $("#hd-property-rtn").val($(this).data("id"));

    $("#modal-dlt-propery").modal("show");
  });

  $("#btn-delete-property").click(function() {

    $("#spn-property").show();

    quick_request(function(data) { 
      load_properties($("#hd-property-rtn").val());
      $("#spn-property").hide();
      $("#modal-dlt-propery").modal("hide");
      Toast.fire({
        icon: 'success',
        title: 'Property successfully deleted'
      });
    }, $("#hd-prty-dlt-lnk").val() + "?id=" + $("#hd-property-id").val());

  });

  function load_properties(id) {
    $("#"+id+"-overlay").show();
    quick_request(function(data) { 
      $(".pagination-mini[data-id='"+id+"']").html(data.pagination);
      setup_properties(id, data.userlist);
      $("#"+id+"-overlay").hide();
    }, web_url+'realty/properties/request/load_properties?type='+id+'&user='+$(".hd-user-id").val()+'&page=' + $(".pagination-mini[data-id='"+id+"']").data("page"));
  }

  function setup_properties(id, list)
  {
    if( list.length == 0 )
      return;

    $("#"+id+"-tbl").children().remove();

    trace("loading " + id);

    for(var cUser in list)
    {
      var UserDets = list[cUser];
      var NewPropery = $("#clone-property").clone();

      NewPropery.removeAttr("id");
      NewPropery.find(".property-name").html(UserDets.Name);
      NewPropery.find(".property-name").attr("href", web_url+"realty/properties?type="+UserDets.Type+"&search="+UserDets.SessionID);
      NewPropery.find(".property-dlt").data("id", id);
      NewPropery.find(".property-dlt").data("session-id", UserDets.SessionID);

      $("#"+id+"-tbl").append(NewPropery);
    }
  }

  /* End Listings */

</script>


<div style="display: none;">
  <div id="copy_user" class="col-lg-4 col-sm-12 col-md-6 d-flex align-items-stretch">
    <div class="card bg-light w-100">
      <div class="card-header text-muted border-bottom-0">
        <h6 class="card-title user-role"></h6>
      </div>
      <div class="card-body pt-0">
        <div class="row">
          <div class="col-7">
            <h2 class="lead"><b class="user-name"></b></h2>
            <p class="text-muted text-sm"><i class="fas fa-lg fa-home"></i> <b>Address: </b><span class="user-address"></span> </p>
            <ul class="ml-4 mb-0 fa-ul text-muted">
              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> Username: <span class="user-username text-truncate"></span></li>
              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span> Email: <span class="user-email text-truncate"></span></li>
              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: <span class="user-contact text-truncate"></span></li>
              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-info"></i></span> About: <br><span class="user-about d-inline-block text-truncate" style="max-width: 100%;"></span></li>
            </ul>
          </div>
          <div class="col-5 text-center">
            <img class="img-circle img-fluid img-thumbnail user-img w-100">
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="float-left d-flex">

          <div class="mr-2">
            <i class="fa fa-home text-primary listing-btn" style="cursor: pointer;" title="Listing"></i>
          </div>

          <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input user-tcontact" data-session-id="" id="sick-1">
            <label class="custom-control-label user-tcontact-lbl" for="sick-1">Show Contact Info</label>
          </div>
        </div>
        <div class="float-right">
          <a href="<?= base_url("users/edit") ?>" class="btn btn-sm btn-primary user-edit-info">
            <i class="fas fa-edit"></i> Edit
          </a>
          <button type="button" class="btn btn-sm bg-red user-delete" data-session-id="">
            <i class="fas fa-trash"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<table style="display: none">
  <tr id="clone-property">
    <td><a href="#" target="_blank" class="property-name">Property Name</a></td>
    <td width="1%"><button type="button" class="btn btn-danger btn-sm property-dlt"><i class="fa fa-trash"></i></button></td>
  </tr>
</table>