<?= $this->include("Includes/Header_content") ?>
<div class="content mb-2">
	<div class="callout callout-info">
      <p>Fields with (<span class="text-red">*</span>) is required!</p>
    </div>
    <?= $this->include("Includes/Alert") ?>
    <form action="<?= base_url("users/add?".(current_url(true)->getQuery())) ?>" enctype="multipart/form-data" method="POST">
		<div class="row">
			<div class="col-6">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">Account Details</h6>
					</div>
					<div class="card-body">
						<div class="row">

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Full Name <span class="text-red">*</span></label>
									<div><input type="type" name="txtname" class="form-control" placeholder="Name" value="<?= old('txtname') ?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Role <span class="text-red">*</span></label>
									<div>
										<select name="slcrole" class="form-control" required>
											<?php foreach (\App\Libraries\Roleslib::getRoles() as $role): ?>
												<option value="<?= $role['id'] ?>" <?php if(strtolower($role['name']) == $universallib->getGet("role")) echo "selected"; ?>>
													<?= $role['name'] ?>
												</option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Username <span class="text-red">*</span></label>
									<div><input type="type" name="txtusername" class="form-control" placeholder="Username" value="<?= old('txtusername');?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Email <span class="text-red">*</span></label>
									<div><input type="email" name="txtemail" class="form-control" placeholder="Email" value="<?= old('txtemail') ?>" required/></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Password <span class="text-red">*</span></label>
									<div><input type="Password" name="txtpassword" class="form-control" placeholder="Password" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Confirm Password <span class="text-red">*</span></label>
									<div><input type="Password" name="txtpassword2" class="form-control" placeholder="Confirm" required></div>
								</div>
							</div>

						</div>
						<!-- /.card-row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-md-6 -->

			<div class="col-6">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">User Info</h6>
					</div>
					<div class="card-body">
						<div class="row">

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Image</label>
									<div class="col-6 m-auto">
										<input name=imgThumb class="file-upload" type="file" data-max-file-count="1" data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>	
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Contact</label>
									<div><input type="text" name="txtcontact" class="form-control" placeholder="Contact" value="<?= old('txtcontact') ?>" /></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Toogle Contact Info</label>
									<div class="custom-control custom-switch">
							            <input type="checkbox" class="custom-control-input" id="toogle-contact" name="ckcontact_info" <?= old('ckcontact_info')?"checked":'' ?> >
							            <label class="custom-control-label user-tcontact-lbl" for="toogle-contact">On/Off</label>
							        </div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Address</label>
									<div><textarea name="txtaddress" class="form-control" placeholder="Address"><?= old('txtaddress') ?></textarea></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Position/About</label>
									<div><textarea name="txtabout" class="form-control" placeholder="Position/About"><?= old('txtabout') ?></textarea></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Gender</label>
									<div class="d-flex justify-content-around">
										<div class="custom-control custom-radio">
											<input class="custom-control-input" type="radio" id="male-gen" name="radgender" value="male" <?= old('radgender') == "male"?"checked":'' ?>>
											<label for="male-gen" class="custom-control-label font-weight-normal">Male</label>
										</div>

										<div class="custom-control custom-radio">
											<input class="custom-control-input" type="radio" id="female-gen" name="radgender" value="female" <?= old('radgender') == "female"?"checked":'' ?>>
											<label for="female-gen" class="custom-control-label font-weight-normal">Female</label>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12 d-none" id="links-div">
								<div class="form-group">
									<label class="text-gray-dark">Ordering</label>
									<div><input type="number" name="txtranking" class="form-control" placeholder="Align Employees" value="<?= old('txtranking') ?>" /></div>
								</div>
								<h6 class="text-gray-dark font-weight-bold">Links</h6>
								<?php $counter = 1; foreach ($SocialMediaArr as $name => $option): ?>
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text text-<?= $option['color'] ?>"><i class="<?= $option['icon'] ?>"></i></span>
										</div>
										<input type="text" class="form-control link-inp" name="<?= str_replace(" ", "", strtolower($name)) ?>-link" placeholder="<?= $name ?> Link" value="<?= old(str_replace(" ", "", strtolower($name)).'-link') ?>">
										<div class="input-group-append">
											<span class="input-group-text text-blue">
												<div class="custom-control custom-switch">
													<input type="checkbox" class="custom-control-input link-ck" id="customSwitch<?= $counter ?>" name="<?= str_replace(" ", "", strtolower($name)) ?>-toogle" <?= old(str_replace(" ", "", strtolower($name)).'-toogle') == "male"?"checked":'' ?>>
													<label class="custom-control-label" for="customSwitch<?= $counter ?>">Display Icon</label>
				                   				</div>
				                   			</span>
										</div>
					                </div>
								</div>
								<?php $counter++;endforeach ?>
							</div>

						</div>
						<!-- /.card-row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-md-6 -->


			<div class="col-md-12 mt-2">
				<div class="text-right">
					<button class="btn btn-success">Add</button>
					<a class="btn btn-default" href="<?= base_url('users?'.current_url(true)->getQuery()) ?>">Cancel</a>
				</div>
			</div>

		</div>
		<!-- /row -->
	</form>

</div>


<script type="text/javascript">
$(function() {
	$(".file-upload").fileinput({
		showCaption: false,
		showRemove: false,
		showUpload: false
	});


	// Links Function
	updateLinks();

	$("select[name='slcrole']").change(updateLinks);
	$("input[name='ckcontact_info']").change(updateLinks);


	function updateLinks()
	{
		var select = $("select[name='slcrole']"), tContact = $("input[name='ckcontact_info']"), linksdiv = $("#links-div");
		var rolesWithLink = ['1', '2']; // 1 = Admin, 2 = Employee
		if( rolesWithLink.indexOf(select.val().toLowerCase()) != -1 || tContact.is(":checked") ) {
			linksdiv.removeClass("d-none");
		} else {
			linksdiv.addClass("d-none");
		}
	}
	// End Links Function
});

window.onbeforeunload = function(e) { // for leaving the page
  return 'Are you sure you want to leave this page?';
};

$(document).on("submit", "form", function(event){ // for submitting/creating the form
  window.onbeforeunload = null;
});
</script>