<?= $this->include("Includes/Header_content") ?>
<section class="content">

  <?= $this->include("Includes/Alert") ?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        
        <div class="card card-lightblue">
          <div class="card-header">
            <h6 class="card-title">Add Roles</h6>
          </div>
          <div class="card-body">
            <form action="<?= base_url("users/roles") ?>" method="POST">
              <input type="hidden" name="hdntype" value="add">
              <div class="form-group">
                <label class="text-gray-dark">Role Name</label>
                <input type="text" class="form-control" name="txtrolename" required>
              </div>
              
              <div class="form-group">
                <label class="text-gray-dark">Role Description</label>
                <textarea class="form-control" name="txtroledescription"></textarea>
              </div>

              <div class="form-group">
                <button class="btn btn-sm btn-success float-right">Add Role</button>
              </div>
            </form>

          </div>
        </div>

      </div>
      <!-- /.col-md-6 -->

      <div class="col-md-6">
        <?php foreach ($roles as $role): ?>

          <div class="card card-solid">

            <div class="card-body">
              <div class="d-flex">
                <div class="container">
                  <p><span class="text-bold text-gray-dark">Role Name:</span> <span class="html-role-name"><?= $role['name'] ?></span></p>
                  <p><span class="text-bold text-gray-dark">Role Description:</span> <span class="html-role-desc"><?= $role['description'] ?></span></p>
                </div>

                <div class="text-nowrap">
                  <button class="btn bg-transparent text-info role-edit" data-session-id="<?= $role['id'] ?>"><i class="fas fa-edit"></i></button>
                  <button class="btn bg-transparent text-danger role-delete" data-session-id="<?= $role['id'] ?>"><i class="fas fa-trash"></i></button>
                </div>
              </div>
            </div>
            <!-- /.card-body -->

          </div>
          <!-- /.card -->
          
        <?php endforeach ?>
      </div>
      <!-- /.col-md-6 -->
      
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->

</section>



<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="<?= base_url("users/roles") ?>" method="POST">
        <input type="hidden" name="hdntype" value="delete">
        <input class="hd-role-id" type="hidden" name="hdnid">
        <div class="modal-header">
          <h4 class="modal-title">Delete role</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this role?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button id="btn-delete-user" class="btn btn-danger">Delete</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="modal fade" id="modal-edit-role">
  <form action="<?= base_url("users/roles") ?>" method="POST">
    <input type="hidden" name="hdntype" value="edit">
    <input class="hd-role-id" type="hidden" name="hdnid">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Role</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Role Name</label>
            <div>
              <input type="text" class="form-control" name="txtrolename" id="txt-role-name-edit">
            </div>
          </div>
          <div class="form-group">
            <label>Role Description</label>
            <div>
              <textarea class="form-control" id="txt-role-description-edit" name="txtroledescription"></textarea>
            </div>
          </div>
          <div class="modal-footer text-right">
            <button class="btn btn-primary float-right">Save Changes</button>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </form>
</div>
<!-- /.modal -->


<script type="text/javascript">
$(".role-delete").click(function() {
  trace("Deleting: " + $(this).data("session-id"));
  $(".hd-role-id").val($(this).data("session-id"));

  $("#modal-default").modal("show");
});

$(".role-edit").click(function() {
  $("#modal-edit-role").modal("show");
  trace("Editing: " + $(this).data("session-id"));
  var SepDiv = $($($(this).parent()).parent());
  $(".hd-role-id").val($(this).data("session-id"));
  $("#txt-role-name-edit").val(SepDiv.find('.html-role-name').html());
  $("#txt-role-description-edit").html(SepDiv.find('.html-role-desc').html());

  
});
</script>