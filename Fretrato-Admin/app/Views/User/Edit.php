<?= $this->include("Includes/Header_content") ?>
<div class="content mb-2">
	<div class="callout callout-info">
      <p>Fields with (<span class="text-red">*</span>) is required!</p>
    </div>
    <?= $this->include("Includes/Alert") ?>
    <form action="<?= base_url("users/edit/{$user_detail['id']}?".(current_url(true)->getQuery())) ?>" enctype="multipart/form-data" method="POST">
		<div class="row">
			<div class="col-6">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">Account Details</h6>
					</div>
					<div class="card-body">
						<div class="row">

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Full Name <span class="text-red">*</span></label>
									<div><input type="type" name="txtname" class="form-control" placeholder="Name" value="<?= old('txtname')?old('txtname'):$user_detail['name'] ?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Role <span class="text-red">*</span></label>
									<div>
										<select name="slcrole" class="form-control" required>
											<?php foreach (\App\Libraries\Roleslib::getRoles() as $role): ?>
												<option value="<?= $role['id'] ?>" <?php if(strtolower($user_detail['role']) == $role['id']) echo "selected"; ?>>
													<?= $role['name'] ?>
												</option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Username <span class="text-red">*</span></label>
									<div><input type="type" name="txtusername" class="form-control" placeholder="Username" value="<?= old('txtusername')?old('txtusername'):$user_detail['username'] ?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Email <span class="text-red">*</span></label>
									<div><input type="email" name="txtemail" class="form-control" placeholder="Email" value="<?= old('txtemail')?old('txtemail'):$user_detail['email'] ?>" required/></div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="border border-info rounded p-1">
									<span class="text-gray-dark"><label class="text-info">Note:</label> No need to this fill up if you're not going to change the <?= strtolower($user_detail['role_name']) ?>'s password</span>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Change Password</label>
									<div><input type="Password" name="txtpassword" class="form-control" placeholder="Password"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Confirm Password</label>
									<div><input type="Password" name="txtpassword2" class="form-control" placeholder="Confirm"></div>
								</div>
							</div>

						</div>
						<!-- /.card-row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-md-6 -->

			<div class="col-6">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">User Info</h6>
					</div>
					<div class="card-body">
						<div class="row">

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Image</label>
									<div class="col-6 m-auto">
										<input name=imgThumb class="file-upload" type="file" data-max-file-count="1" data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>	
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Contact</label>
									<div><input type="text" name="txtcontact" class="form-control" placeholder="Contact" value="<?= old('txtcontact')?old('txtcontact'):$user_detail['contact_no'] ?>" /></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Toogle Contact Info</label>
									<div class="custom-control custom-switch">
							            <input type="checkbox" class="custom-control-input" id="toogle-contact" name="ckcontact_info" <?= $user_detail['toogleInfo']?"checked":'' ?> >
							            <label class="custom-control-label user-tcontact-lbl" for="toogle-contact">On/Off</label>
							        </div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Address</label>
									<div><textarea name="txtaddress" class="form-control" placeholder="Address"><?= old('txtaddress')?old('txtaddress'):$user_detail['address'] ?></textarea></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Position/About</label>
									<div><textarea name="txtabout" class="form-control" placeholder="Position/About"><?= old('txtabout')?old('txtabout'):$user_detail['about'] ?></textarea></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Gender</label>
									<div class="d-flex justify-content-around">
										<div class="custom-control custom-radio">
											<input class="custom-control-input" type="radio" id="male-gen" name="radgender" value="male" <?= $user_detail['gender'] == "male"?"checked":'' ?>>
											<label for="male-gen" class="custom-control-label font-weight-normal">Male</label>
										</div>

										<div class="custom-control custom-radio">
											<input class="custom-control-input" type="radio" id="female-gen" name="radgender" value="female" <?= $user_detail['gender'] == "female"?"checked":'' ?>>
											<label for="female-gen" class="custom-control-label font-weight-normal">Female</label>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12 d-none" id="links-div">
								<div class="form-group">
									<label class="text-gray-dark">Ordering</label>
									<div><input type="number" name="txtranking" class="form-control" placeholder="Align Employees" value="<?= $user_detail['order_ranking'] ?>" /></div>
								</div>
								<h6 class="text-gray-dark font-weight-bold">Links</h6>
								<?php $counter = 1; foreach ($SocialMediaArr as $name => $option): ?>
								<div class="col-md-12">
									<div class="form-group">
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text text-<?= $option['color'] ?>"><i class="<?= $option['icon'] ?>"></i></span>
											</div>
											<input type="text" class="form-control link-inp" name="<?= str_replace(" ", "", strtolower($name)) ?>-link" placeholder="<?= $name ?> Link" value="<?= $option['value'] ?>">
											<div class="input-group-append">
												<span class="input-group-text text-blue">
													<div class="custom-control custom-switch">
														<input type="checkbox" class="custom-control-input link-ck" id="customSwitch<?= $counter ?>" name="<?= str_replace(" ", "", strtolower($name)) ?>-toogle" <?= $option['toogle']?"checked":"" ?>>
														<label class="custom-control-label" for="customSwitch<?= $counter ?>">Display Icon</label>
					                   				</div>
					                   			</span>
											</div>
						                </div>
									</div>
								</div>
								<!-- /.col-md-12 -->
								<?php $counter++;endforeach ?>
							</div>



						</div>
						<!-- /.card-row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-md-6 -->

			<div class="col-md-12 mt-2 mb-2">
				<div class="text-right">
					<button class="btn btn-success">Save</button>
					<a class="btn btn-default" href="<?= base_url('users?'.current_url(true)->getQuery()) ?>">Cancel</a>
				</div>
			</div>

			<div class="col-4">
				<div class="card card-lightblue">
	              <div class="card-header">
	                <h3 class="card-title">Resale</h3>

	                <div class="card-tools">
	                  <div class="pagination-mini" data-load="load_properties" data-id="resale" data-page="1"></div>
	                </div>
	              </div>
	              <!-- /.card-header -->
	              <div class="card-body p-0 overlay-wrapper">
	              	<div class="overlay" id="resale-overlay">
		            	<i class="fas fa-3x fa-sync-alt fa-spin"></i>
		            </div>
		            <table class="table">
	                  <tbody id="resale-tbl">
	                  	<tr>
	                  		<td colspan="2" class="text-center">No Resale listing</td>
	                  	</tr>
	                  </tbody>
	                </table>
	              </div>
	              <!-- /.card-body -->
	            </div>
	        </div>
			<!-- /.col-md-4 -->

			<div class="col-4">
				<div class="card card-lightblue">
	              <div class="card-header">
	                <h3 class="card-title">Rental</h3>

	                <div class="card-tools">
	                  <div class="pagination-mini" data-load="load_properties" data-id="rental" data-page="1"></div>
	                </div>
	              </div>
	              <!-- /.card-header -->
	              <div class="card-body p-0 overlay-wrapper">
	              	<div class="overlay" id="rental-overlay">
		            	<i class="fas fa-3x fa-sync-alt fa-spin"></i>
		            </div>
	                <table class="table">
	                  <tbody id="rental-tbl">
	                  	<tr>
	                  		<td colspan="2" class="text-center">No Rental listing</td>
	                  	</tr>
	                  </tbody>
	                </table>
	              </div>
	              <!-- /.card-body -->
	            </div>
	        </div>
			<!-- /.col-md-4 -->

			<div class="col-4">
				<div class="card card-lightblue">
	              <div class="card-header">
	                <h3 class="card-title">Pre-selling</h3>

	                <div class="card-tools">
	                  <div class="pagination-mini" data-load="load_properties" data-id="pre-selling" data-page="1"></div>
	                </div>
	              </div>
	              <!-- /.card-header -->
	              <div class="card-body p-0 overlay-wrapper">
	              	<div class="overlay" id="pre-selling-overlay">
		            	<i class="fas fa-3x fa-sync-alt fa-spin"></i>
		            </div>
	                <table class="table">
	                  <tbody id="pre-selling-tbl">
	                  	<tr>
	                  		<td colspan="2" class="text-center">No Pre-selling listing</td>
	                  	</tr>
	                  </tbody>
	                </table>
	              </div>
	              <!-- /.card-body -->
	            </div>
	        </div>
			<!-- /.col-md-4 -->

		</div>
		<!-- /row -->
	</form>

</div>

<div class="modal fade" id="modal-dlt-propery">
  <div class="modal-dialog">
    <div class="modal-content">
      <input id="hd-property-id" type="hidden">
      <input id="hd-property-rtn" type="hidden">
      <input id="hd-dlt-lnk" type="hidden" value="<?= base_url("realty/properties/request/delete_property") ?>">
      <div class="modal-header">
        <h4 class="modal-title">Delete property</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this property?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button id="btn-delete-user" type="button" class="btn btn-danger">
          &nbsp;
          Delete 
          &nbsp;
          <i id="spn-user" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script type="text/javascript">
$(function() {
	$(".file-upload").fileinput({
		showCaption: false,
		showRemove: false,
		showRemove: false,
		showUpload: false,
		initialPreview: [
		<?php if( !empty($user_detail['img_session']) ) : ?>
			'<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".$user_detail['user_avatar'], $user_detail['user_dir']) ?>" class="file-preview-image kv-preview-data" alt="<?= $user_detail['user_avatar'] ?>">'
		<?php endif ?>],
		initialPreviewConfig: [
		<?php if( !empty($user_detail['img_session']) ): ?>
		{
			caption: '<?= $user_detail['user_avatar'] ?>'
		}
		<?php endif ?>],
		initialPreviewShowDelete: false
	});

	$("body").on("click", ".property-dlt", function() {
	    trace("Deleting: " + $(this).data("session-id"));
	    $("#hd-property-id").val($(this).data("session-id"));
	    $("#hd-property-rtn").val($(this).data("id"));

	    $("#modal-dlt-propery").modal("show");
  	});

  	$("#btn-delete-user").click(function() {

	    $("#spn-user").show();

	    quick_request(function(data) { 
			load_properties($("#hd-property-rtn").val());
			$("#spn-user").hide();
			$("#modal-dlt-propery").modal("hide");
			Toast.fire({
			  icon: 'success',
			  title: 'Property successfully deleted'
			});
		}, $("#hd-dlt-lnk").val() + "?id=" + $("#hd-property-id").val());

	  });

	load_properties("resale");
	load_properties("rental");
	load_properties("pre-selling");

	// Links Function
	updateLinks();

	$("select[name='slcrole']").change(updateLinks);
	$("input[name='ckcontact_info']").change(updateLinks);


	function updateLinks()
	{
		var select = $("select[name='slcrole']"), tContact = $("input[name='ckcontact_info']"), linksdiv = $("#links-div");
		var rolesWithLink = ['1', '2']; // 1 = Admin, 2 = Employee
		if( rolesWithLink.indexOf(select.val().toLowerCase()) != -1 || tContact.is(":checked") ) {
			linksdiv.removeClass("d-none");
		} else {
			linksdiv.addClass("d-none");
		}
	}
	// End Links Function
});

function load_properties(id) {
    $("#"+id+"-overlay").show();
    quick_request(function(data) { 
      $(".pagination-mini[data-id='"+id+"']").html(data.pagination);
      setup_users(id, data.userlist);
      $("#"+id+"-overlay").hide();
    }, web_url+'realty/properties/request/load_properties?type='+id+'&user=<?= $user_detail['id'] ?>&page=' + $(".pagination-mini[data-id='"+id+"']").data("page"));
}

function setup_users(id, list)
{
	if( list.length == 0 )
		return;

	$("#"+id+"-tbl").children().remove();

	trace("loading " + id);

	for(var cUser in list)
	{
	  var UserDets = list[cUser];
	  var NewPropery = $("#clone-property").clone();

	  NewPropery.removeAttr("id");
	  NewPropery.find(".property-name").html(UserDets.Name);
	  NewPropery.find(".property-name").attr("href", web_url+"realty/properties?type="+UserDets.Type+"&search="+UserDets.SessionID);
	  NewPropery.find(".property-dlt").data("id", id);
	  NewPropery.find(".property-dlt").data("session-id", UserDets.SessionID);

	  $("#"+id+"-tbl").append(NewPropery);
	}
}

window.onbeforeunload = function(e) { // for leaving the page
  return 'Are you sure you want to leave this page?';
};

$(document).on("submit", "form", function(event){ // for submitting/creating the form
  window.onbeforeunload = null;
});

	
</script>

<table style="display: none">
	<tr id="clone-property">
	  <td><a href="#" target="_blank" class="property-name">Property Name</a></td>
	  <td width="1%"><button type="button" class="btn btn-danger btn-sm property-dlt"><i class="fa fa-trash"></i></button></td>
	</tr>
</table>