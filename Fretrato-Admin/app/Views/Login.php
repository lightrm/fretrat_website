<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fretrato | Administration <?= $headerTitle ?></title>
  <link rel="shortcut icon" href="<?= base_url($pluginDir); ?>/fretrato/Images/Admin_Icon.ico">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url($pluginDir); ?>/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style type="text/css">
    /*footer {
        position: fixed;
        bottom: 0;
    }*/
  </style>
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <section class="container content-wrapper m-auto">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="text-center">
        <img src="<?= \App\Libraries\Imagelib::getImageURL('fretrato-logo-name-2.png'); ?>" alt="Fretrato Logo" class="brand-image"
           style="opacity: .8"> 
      </div><!-- /.container-fluid -->
    </section>

    <div class="card card-lightblue w-50 m-auto">
      <div class="card-header">
        <h3 class="card-title">Login</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form class="form-horizontal" action="<?= base_url("login") ?>" method="POST">
        <div class="card-body">
          <?= $this->include("Includes/Alert") ?>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-5 col-form-label">Username</label>
            <div class="col-sm-12">
              <input type="text" name="txtuser" class="form-control" id="inputEmail3" placeholder="Username / Email" value="<?= old("txtuser") ?>" required>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-5 col-form-label">Password</label>
            <div class="col-sm-12">
              <input type="password" name="txtpassword" class="form-control" id="inputPassword3" placeholder="Password" required>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button class="btn btn-primary float-right">Sign in</button>
        </div>
        <!-- /.card-footer -->
      </form>
    </div>

  </section>

  <!-- Main Footer -->
  <footer class="container main-footer m-auto">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      All rights reserved.
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018-2020 <a href="https://fretrato.com.ph/">Fretrato Website</a></strong>
  </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url($pluginDir); ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url($pluginDir); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url($pluginDir); ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url($pluginDir); ?>/dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
</body>
</html>
