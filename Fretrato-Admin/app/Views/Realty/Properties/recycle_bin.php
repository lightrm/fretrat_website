<?= $this->include("Includes/Header_content") ?>

<style type="text/css">
  .card .card-widget {

  }
</style>

<section class="content">

  <div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <div class="card card-solid">

          <div class="card-header">
            <section class="navbar navbar-light bg-transparent">
              <div class="content float-left d-flex">
                <span class="text-gray-dark mr-2">Total Record: <span id="total-record"></span></span>
                <div class="filter-div d-flex" data-load="load_properties"></div>
              </div>
              
              <div class="content float-right">
                <!-- SEARCH FORM -->
                <div class="input-group input-group-sm">
                  <input type="hidden" id="target-search" value="load_properties">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" id="txtsearch">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="button" id="btnsearch">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <div class="card-body pb-0">
            <div class="row d-flex align-items-stretch overlay-wrapper" id="user_list">

              <div class="overlay" id="list-overlay">
                <i class="fas fa-3x fa-sync-alt fa-spin"></i>
              </div>

            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <div class="pagination-ajax" data-load="load_properties"></div>
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->

      </div>
      <!-- /.col-md-12 -->

    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->


</section>



<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <input id="hd-user-id" type="hidden">
        <input id="hd-dlt-lnk" type="hidden" value="<?= base_url($spageURL."request/delete_property") ?>">
        <div class="modal-header">
          <h4 class="modal-title">Permanently Delete Property</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to <span class="text-red font-weight-bold">PERMANENTLY</span> delete this property?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button id="btn-delete-user" type="button" class="btn btn-danger">
            &nbsp;
            Delete 
            &nbsp;
            <i id="spn-user" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
          </button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

    
<script type="text/javascript">
  URLParser().removeParam('reference', true)
  
  $("body").on("click", ".property-undo", function() {
    trace("Restoring: " + $(this).data("session-id"));
    quick_request(function(data) { 
      load_properties();
      Toast.fire({
        icon: 'success',
        title: 'Property successfully restored'
      });
    }, web_url + "realty/properties/request/restore_property?id=" + $(this).data("session-id"));
  });

  $("body").on("click", ".property-delete", function() {
    trace("Deleting: " + $(this).data("session-id"));
    $("#hd-user-id").val($(this).data("session-id"));

    $("#modal-default").modal("show");
  });

  $("#btn-delete-user").click(function() {

    $("#spn-user").show();

    quick_request(function(data) { 
      load_properties();
      $("#spn-user").hide();
      $("#modal-default").modal("hide");
      Toast.fire({
        icon: 'success',
        title: 'Property successfully deleted'
      });
    }, $("#hd-dlt-lnk").val() + "?id=" + $("#hd-user-id").val() + "&permanent=true");

  });

  $(document).ready(function() {

    load_properties();

  });

  function load_properties(offset)
  {
    setup_pagination(offset);

    $("#list-overlay").show();

    quick_request(function(data) { 
      load_pagination(data.pagination);
      load_filter();
      $("#total-record").html(data.totalRecord);
      setup_properties(data.userlist);
      $("#list-overlay").hide();
    }, '<?= base_url($spageURL.'request/load_recycle_bin') ?>?' + URLParser().getQuery());

  }

  function setup_properties(list)
  {
    remove_allproperty();

    trace("loading Properties");

    var query = "?" + URLParser().getQuery();
    var i = 1;
    for(var cUser in list)
    {
      var PropertyDets = list[cUser];
      var NewUser = $("#copy_user").clone();

      NewUser.removeAttr("id");
      NewUser.removeAttr("style");
      NewUser.find(".property-name").html(PropertyDets.Name);
      NewUser.find(".property-location").html(PropertyDets.City);
      NewUser.find(".property-price").html(PropertyDets.Price);
      NewUser.find(".property-bedroom").html(PropertyDets.Bedroom);
      NewUser.find(".property-squaremtr").html(PropertyDets.Squaremeter);
      NewUser.find(".property-parking").html((PropertyDets.Parking?"With":"Without"));
      NewUser.find(".property-undo").data("session-id", PropertyDets.SessionID);
      NewUser.find(".property-delete").data("session-id", PropertyDets.SessionID);
      if( !PropertyDets.Image.includes("property_noimage") )
        NewUser.find(".widget-user-header").css("background", "url('"+ PropertyDets.Image + "') center center");
      else
        NewUser.find(".widget-user-header").addClass("bg-lightblue");
      PropertyDets.Tfeature? NewUser.find(".property-feature").show():NewUser.find(".property-feature").hide();



      // Agent      
      NewUser.find(".user-type").html(PropertyDets.Agent.Type);
      NewUser.find(".user-name").html(PropertyDets.Agent.Name);
      NewUser.find(".user-name").attr("href", web_url + 'users?role='+ PropertyDets.Agent.Type.toLowerCase() + '&search=' + PropertyDets.Agent.SessionID);
      NewUser.find(".widget-user-image img").attr("src", PropertyDets.Agent.Image);

      $("#user_list").append(NewUser);
      
      i++;
    }



  }

  function remove_allproperty()
  {
    trace("Removing Properties");
    $("#user_list").children().remove();
  }

</script>


<div style="display: none;">
	<div id="copy_user" class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
		<div class="card card-widget widget-user w-100">
		  <!-- Add the bg color to the header using any of the bg-* classes -->
		  <div class="widget-user-header text-white">

		  	<div class="float-left">
          <button type="button" class="btn btn-sm bg-primary property-undo" data-session-id=""><i class="fas fa-undo"></i></button>   
          <button type="button" class="btn btn-sm bg-red property-delete" data-session-id=""><i class="fas fa-trash"></i></button>   
        </div>

        <div class="property-feature">
          <div class="ribbon-wrapper">
            <div class="ribbon bg-primary">
              Featured
            </div>
          </div>
        </div>

		    <h3 class="widget-user-username text-right property-name">Property Name</h3>
        <h5 class="widget-user-desc text-truncate float-right text-right property-location" style="width: 38%">Location</h5>
		  </div>
		  <div class="widget-user-image">
		    <img class="img-circle bg-white" alt="User Avatar">
		  </div>
		  <div class="card-footer">
		  	<div class="row">
		  		
		      <div class="col-sm-6 border-right">
		        <div class="">
		          <h5 class="description-header font-weight-bold user-type">Agent</h5>
		          <span class="description-text"><a href="<?= base_url("users?search=") ?>" target="_blank" class="user-name">Elizabeth Pierce</a></span>
		        </div>
		        <!-- /.description-block -->
		      </div>
		      <div class="col-sm-6">
		        <div class="description-block">
		          <h5 class="description-header">Price</h5>
		          <span class="description-text property-price">10,600</span>
		        </div>
		        <!-- /.description-block -->
		      </div>

		  	</div>
		    <div class="row">
		      <div class="col-sm-4 border-right">
		        <div class="description-block">
		          <h5 class="description-header property-bedroom">2</h5>
		          <span class="description-text"><i class="fas fa-bed"></i> Bedroom</span>
		        </div>
		        <!-- /.description-block -->
		      </div>
          <div class="col-sm-4">
            <div class="description-block border-right">
              <h5 class="description-header property-squaremtr">1</h5>
              <span class="description-text"><i class="fas fa-ruler-combined"></i> Square Meter</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
		      <!-- /.col -->
		      <div class="col-sm-4">
		        <div class="description-block">
		          <h5 class="description-header property-parking">1</h5>
		          <span class="description-text"><i class="fas fa-parking"></i> Parking</span>
		        </div>
		        <!-- /.description-block -->
		      </div>
		      <!-- /.col -->
		    </div>
		    <div class="row">
		    	<div>
		    		
		    	</div>
		    </div>
		    <!-- /.row -->
		  </div>
		</div>
	</div>
</div>