<?= $this->include("Includes/Header_content") ?>

<div class="content">
	<div class="callout callout-warning">
      <p>Fields with (<span class="text-red">*</span>) is required!</p>
    </div>
    <?= $this->include("Includes/Alert") ?>
	<form action="<?= base_url("realty/properties/edit/".$propertyDets['propery_id']."?".(current_url(true)->getQuery())) ?>" enctype="multipart/form-data" method="POST">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Property Name <span class="text-red">*</span></label>
									<div><input type="type" name="txtname" class="form-control" value="<?= !old('txtname')?$propertyDets['propery_name']:old('txtname') ?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Price <span class="text-red">*</span></label>
									<div><input type="number" step="any" name="txtprice" class="form-control" placeholder="0.00" value="<?= !old('txtprice')?$propertyDets['price']:old('txtprice') ?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Offer Type <span class="text-red">*</span></label>
									<div class="d-flex justify-content-around">
										<?php foreach (array("Resale", "Rental", "Pre-selling") as $type): ?>
											<div class="custom-control custom-radio">
					                        	<input class="custom-control-input" type="radio" name="rdpropertytype" id="<?= strtolower($type) ?>-inp"
					                          		value="<?= $type ?>" 
					                          		<?php if(strtolower($propertyDets['offer_type']) == strtolower($type)) echo "checked"; ?>
				                          		required>
					                          <label class="custom-control-label font-weight-normal" for="<?= strtolower($type) ?>-inp"><?= $type ?></label>
					                        </div>
										<?php endforeach ?>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Quick Options</label>
									<div class="d-flex justify-content-around">
				                        <div class="custom-control custom-checkbox">
				                        	<input class="custom-control-input" type="checkbox" name="ckpublish" id="publish-inp" <?= !empty($propertyDets['published'])?"checked":"" ?>>
				                        	<label for="publish-inp" class="custom-control-label font-weight-normal">Publish</label>
				                        </div>

				                        <div class="custom-control custom-checkbox">
				                        	<input class="custom-control-input" type="checkbox" name="ckdirect" id="direct-inp" <?= !empty($propertyDets['is_direct'])?"checked":"" ?>>
				                        	<label for="direct-inp" class="custom-control-label font-weight-normal">Direct to Property Owner</label>
				                        </div>

				                        <div class="custom-control custom-checkbox">
				                        	<input class="custom-control-input" type="checkbox" name="cktaken" id="taken-inp" <?= !empty($propertyDets['taken'])?"checked":"" ?>>
				                        	<label for="taken-inp" class="custom-control-label font-weight-normal">Taken</label>
				                        </div>

				                        <div class="custom-control custom-checkbox">
				                        	<input class="custom-control-input" type="checkbox" name="ckfeature" id="feature-inp" <?= !empty($propertyDets['toogleFeature'])?"checked":"" ?>>
				                        	<label for="feature-inp" class="custom-control-label font-weight-normal">Featured</label>
				                        </div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Main Category <span class="text-red">*</span></label>
									<div>
										<select name="slcpropertytype" class="form-control" required>
											<?php foreach ($mainCategory as $row): ?>
												<option value="<?= $row['id'] ?>" <?= $propertyDets['property_type'] == $row['id']?"selected":"" ?>><?= $row['name'] ?></option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Sub Category <span class="text-red">*</span></label>
									<div>
										<select name="slcsubctgry" class="form-control" required></select>
									</div>
								</div>
							</div>

							<div class="col-12 row border1" id="paneling-div"></div>

							<?php /*

							<div class="col-md-3">
								<div class="form-group">
									<label class="text-gray-dark">Square Meters <span class="text-red">*</span></label>
									<div><input type="number" step="any" name="txtsqrmtr" class="form-control" placeholder="0.00" value="<?= !old('txtsqrmtr')?$propertyDets['floor_area']:old('txtsqrmtr') ?>" required></div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label class="text-gray-dark">Floor <span class="text-red">*</span></label>
									<div><input type="number" name="txtfloor" class="form-control" value="<?= !old('txtfloor')?$propertyDets['floor_level']:old('txtfloor') ?>" required></div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label class="text-gray-dark">bedroom <span class="text-red">*</span></label>
									<div><input type="number" name="txtbedroom" class="form-control" value="<?= !old('txtbedroom')?$propertyDets['bedroom']:old('bedroom') ?>" required></div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label class="text-gray-dark">bathroom</label>
									<div><input type="number" name="txtbathroom" class="form-control" value="<?= !old('txtbathroom')?$propertyDets['bathroom']:old('txtbathroom') ?>"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Additional Detail</label>
									<div class="d-flex justify-content-around">
				                        <div class="custom-control custom-checkbox">
				                        	<input class="custom-control-input" type="checkbox" name="ckparking" id="parking-inp" <?= !empty($propertyDets['parking'])?"checked":"" ?>>
				                        	<label for="parking-inp" class="custom-control-label font-weight-normal">Parking</label>
				                        </div>

				                        <div class="custom-control custom-checkbox">
				                        	<input class="custom-control-input" type="checkbox" name="ckdfurnished" id="furnished-inp" <?= !empty($propertyDets['is_furnished'])?"checked":"" ?>>
				                        	<label for="furnished-inp" class="custom-control-label font-weight-normal">Furnished</label>
				                        </div>
									</div>
								</div>
							</div>

							*/ ?>

							<div class="col-md-12" id="contract-div" style=" display: none;">
								<label class="text-gray-dark">Contract Start</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label class="text-gray-dark font-weight-normal">Start Date</label>
											<div><input type="text" step="any" name="txtcntrctstrt" class="form-control contract-dpicker" value="<?= !old('txtcntrctstrt')?date('m/d/Y', strtotime($propertyDets['contract_start'])):old('txtcntrctstrt') ?>"></div>
										</div>
									</div>

									<div class="col-6">
										<div class="form-group">
											<label class="text-gray-dark font-weight-normal">End Date</label>
											<div><input type="text" step="any" name="txtcntrctend" class="form-control contract-dpicker" value="<?= !old('txtcntrctend')?date('m/d/Y', strtotime($propertyDets['contract_end'])):old('txtcntrctend') ?>"></div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<!-- \.row -->
					</div>
					<!-- \.card-body -->
				</div>
				<!-- \.card -->
			</div>
			<!-- \.col-6 -->

			<div class="col-12 col-sm-12 col-md-12 col-lg-6">
				<div class="card">
					<div class="card-header">
						<h6 class="card-title">Property Location</h6>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Street Name <span class="text-red">*</span></label>
									<div><input type="text" name="txtstreet" class="form-control" value="<?= !old('txtstreet')?$propertyDets['street']:old('txtstreet') ?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Barangay</label>
									<div><input type="text" name="txtbarangay" class="form-control" value="<?= !old('txtbarangay')?$propertyDets['barangay']:old('txtbarangay') ?>"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">City <span class="text-red">*</span></label>
									<div><input type="text" name="txtcity" class="form-control" placeholder="Ex. Manila/Makati/Quezon" value="<?= !old('txtcity')?$propertyDets['city']:old('txtcity') ?>" required></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Country</label>
									<div><input type="text" name="txtcountry" class="form-control" placeholder="Ex. Philippines" value="<?= !old('txtcountry')?$propertyDets['country']:old('txtcountry') ?>"></div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="text-gray-dark">Province</label>
									<div><input type="text" name="txtprovince" class="form-control" value="<?= !old('txtprovince')?$propertyDets['province']:old('txtprovince') ?>"></div>
								</div>
							</div>

							<div class="col-md-12">
								<span>&nbsp;</span>
							</div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-6 -->

			<?php  if( session()->get("User")['id'] == $propertyDets['user_id'] || session()->get("User")['role'] == 1 ) :  ?>

			<div class="col-12">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">Agent option</h6>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse">
								<i class="fas fa-minus"></i>
							</button>
		                </div>
					</div>

					<div class="card-body row">
						<div class="col-6">
							<div class="form-group">
								<label class="text-gray-dark">Owner Name <span class="text-red">*</span></label>
								<div><input type="text" name="txtownerName" class="form-control" value="<?= !old('txtownerName')?$propertyDets['owner_name']:old('txtownerName') ?>" required></div>
							</div>
						</div>

						<div class="col-6">
							<div class="form-group">
								<label class="text-gray-dark">Owner Contact <span class="text-red">*</span></label>
								<div><input type="text" name="txtownercontact" class="form-control" value="<?= !old('txtownercontact')?$propertyDets['owner_contact']:old('txtownercontact') ?>" required></div>
							</div>
						</div>

						<div class="col-6">
							<div class="form-group">
								<label class="text-gray-dark">Owner Email <span class="text-red">*</span></label>
								<div><input type="text" name="txtownerEmail" class="form-control" value="<?= !old('txtownerEmail')?$propertyDets['owner_email']:old('txtownerEmail') ?>" required></div>
							</div>
						</div>

						<div class="col-6">
							<div class="form-group">
								<label class="text-gray-dark">Last Update <span class="text-red">*</span></label>
								<div><input type="text" name="txtLastupdate" id="lastUpdate" class="form-control" value="<?= !old('txtLastupdate')?date("m/d/Y", strtotime($propertyDets['agent_lastupdate'])):old('txtLastupdate') ?>" required></div>
							</div>
						</div>

					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-12 -->

			<?php endif; ?>

			<div class="col-12">
				<div class="card card-lightblue">
					<div class="card-header">
						<h6 class="card-title">Descriptions</h6>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse">
								<i class="fas fa-minus"></i>
							</button>
		                </div>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label class="text-gray-dark">Description</label>
							<div><textarea class="form-control textarea" name="txtdescription"><?= !old('txtdescription')?$propertyDets['description']:old('txtdescription') ?></textarea></div>
						</div>

						<div class="form-group">
							<label class="text-gray-dark">About</label>
							<div><textarea class="form-control textarea" name="txtabout"><?= !old('txtabout')?$propertyDets['propery_about']:old('txtabout') ?></textarea></div>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-12 -->

			<div class="col-12">
				<div class="card card-lightblue collapsed-card">
					<div class="card-header">
						<h6 class="card-title">Advance Options</h6>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse">
								<i class="fas fa-plus"></i>
							</button>
		                </div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Amenities</label>
									<span class="float-right"><i class="fas fa-sync-alt" id="btn-add-refresh" style="cursor: pointer;"></i></span>
									<div class="overlay-wrapper">
										<div class="overlay" id="list-overlay">
											<i class="fas fa-3x fa-sync-alt fa-spin"></i>
							            </div>
							            <select class="form-control select2" name="slctamenities[]" id="slct-amty" data-placeholder="Select Amenities" multiple></select>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">Embed Video</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text">
												<input type="checkbox" id="ck-video-url" <?= !empty($propertyDets['property_vid_url'])?"checked":"" ?>>
											</span>
										</div>
										<input type="text" class="form-control" id="inp-video-url" name="txtvideo" value="<?= !old('txtvideo')?$propertyDets['property_vid_url']:old('txtvideo') ?>">
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label class="text-gray-dark">SEO Meta Keywords</label>
									<div>
										<select class="form-control select2" name="slctSEOkw[]" data-tags="true" data-placeholder="Enter Keywords" multiple>
											<?php if ( !empty($propertyDets['meta_keywords']) ): ?>
												<?php foreach (explode(", ", $propertyDets['meta_keywords']) as $SEO): ?>
													<option selected><?= $SEO ?></option>
												<?php endforeach ?>
											<?php endif ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-12 -->

			<div class="col-12">
				<div class="card card-lightblue collapsed-card">
					<div class="card-header">
						<h6 class="card-title">Images</h6>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse">
								<i class="fas fa-plus"></i>
							</button>
		                </div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label class="text-gray-dark">Watermark</label>
									<div class="custom-control custom-switch">
							            <input type="checkbox" class="custom-control-input" id="toogle-Watermark" name="ckWatermark" <?= old('ckWatermark')?"checked":'' ?>>
							            <label class="custom-control-label" for="toogle-Watermark">On/Off</label>
							        </div>
								</div>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label class="text-gray-dark">Thumbnail</label>
									<div class="col-6 m-auto">
										<input name=imgThumb class="file-upload" type="file" data-max-file-count="1" data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>		
								</div>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label class="text-gray-dark">Slider</label>
									<div class="col-6 m-auto">
										<input name=imgSlider class="file-upload2" type="file" data-max-file-count="1" data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>		
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label class="text-gray-dark">Gallery</label>
									<div>
										<input name="imgGallery[]" class="gallery-upload" type="file" multiple data-theme="fas" accept="image/jpeg, image/jpg, image/png">
									</div>
								</div>
							</div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-12 -->

		</div>
		<!-- /.row -->

		<div class="text-right mb-2">
			<button class="btn btn-success">Save</button>
			<a class="btn btn-default" href="<?= base_url('realty/properties?'.current_url(true)->getQuery()) ?>">Cancel</a>
		</div>
	</form>

</div>

<script type="text/javascript">
	var prev_amenity = [<?= !empty($propery_amty)?"'".implode("', '", array_map(function($row) { return $row['amenity_property_id'];  }, $propery_amty))."'":'' ?>];
	var valueHolders = {
		'bathroom': '<?= !old('bathroom_txt')? $propertyDets['bathroom']:old('bathroom_txt') ?>',
		'bedroom': '<?= !old('bedroom_txt')? $propertyDets['bedroom']:old('bedroom_txt') ?>',
		'floorarea': '<?= !old('floorarea_txt')? $propertyDets['floor_area']:old('floorarea_txt') ?>',
		'floorlevel': '<?= !old('floorlevel_txt')? $propertyDets['floor_level']:old('floorlevel_txt') ?>',
		'landarea': '<?= !old('landarea_txt')? $propertyDets['land_area']:old('landarea_txt') ?>',
		'parking': '<?= !old('parking_txt')? $propertyDets['parking']:old('parking_txt') ?>',
		'stay-month': '<?= !old('stay-month_txt')? $propertyDets['stayMonth']:old('stay-month_txt') ?>',
		'stay-year': '<?= !old('stay-year_txt')? $propertyDets['stayYear']:old('stay-year_txt') ?>',
		'condition': '<?= !old('condition_slc')? $propertyDets['furnish_type']:old('condition_slc') ?>',
		'housecondition': '<?= !old('housecondition_slc')? $propertyDets['condition']:old('housecondition_slc') ?>',
		'landcondition': '<?= !old('landcondition_slc')? $propertyDets['condition']:old('landcondition_slc') ?>',
		'datefinish': '<?= !old('datefinish_txts')? $propertyDets['dateFinished']:old('datefinish_txt') ?>',
	}
	var subCatSel = '<?= $propertyDets['subcategory'] ?>';
	var subCategory = { 
	<?php foreach ($subCategory as $key => $mainCat): ?>
		'<?= $key ?>': [<?php foreach ($mainCat as $content): ?>
			{<?= "id:'{$content['sub_categ_id']}', value: '{$content['subName']}'" ?>},
		<?php endforeach ?>],
	<?php endforeach ?> }
	$(function() {
		$('.textarea').summernote({
	    	height: 300,
        	fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
	    	toolbar: summernoteToolBar,
	    	callbacks: {
		        onPaste: function (e) {
        			$('#summernote').summernote('removeFormat')
		        }
		    }
	    });
    	$('.note-editable p').each( function() {
	    	$(this).css('margin-bottom', 1)
	    })

	    $("#lastUpdate").daterangepicker({
	    	singleDatePicker: true 
	    });

	    $(".contract-dpicker").daterangepicker({
	    	singleDatePicker: true 
	    })

		$(".file-upload").fileinput({
			showCaption: false,
			showRemove: false,
			showUpload: false,
			initialPreview: [
			<?php if( !empty($propertyDets['propery_imgSession'] && !empty($propertyDets['thumbnail'])) ) : ?>
				'<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".$propertyDets['thumbnail'], $propertyDets['thumbnail_dir']) ?>" class="file-preview-image kv-preview-data" alt="<?= $propertyDets['thumbnail'] ?>">'
			<?php endif ?>],
			initialPreviewConfig: [
			<?php if( !empty($propertyDets['propery_imgSession'] && !empty($propertyDets['thumbnail'])) ): ?>
			{
				caption: '<?= $propertyDets['thumbnail'] ?>',
				url: web_url + 'realty/properties/request/delete_imgGallery?id=<?= $propertyDets['thumbnail_id'] ?>'
			}
			<?php endif ?>]
		});

		$(".file-upload2").fileinput({
			showCaption: false,
			showRemove: false,
			showUpload: false,
			initialPreview: [
			<?php if( !empty($propertyDets['propery_imgSession'] && !empty($propertyDets['slider'])) ) : ?>
				'<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".$propertyDets['slider'], $propertyDets['slider_dir']) ?>" class="file-preview-image kv-preview-data" alt="<?= $propertyDets['slider'] ?>">'
			<?php endif ?>],
			initialPreviewConfig: [
			<?php if( !empty($propertyDets['propery_imgSession'] && !empty($propertyDets['slider'])) ): ?>
			{
				caption: '<?= $propertyDets['slider'] ?>',
				url: web_url + 'realty/properties/request/delete_imgGallery?id=<?= $propertyDets['slider_id'] ?>'
			}
			<?php endif ?>]
		});

		$(".gallery-upload").fileinput({
			showCaption: false,
			showRemove: false,
			showUpload: false,
			initialPreview: [
			<?php foreach ($propery_gallery as $image): ?>
				'<img src="<?= \App\Libraries\Imagelib::getImageURL("original-".$image['filename'], $image['directory']) ?>" class="file-preview-image kv-preview-data" alt="<?= $image['filename'] ?>">',
			<?php endforeach ?>],
			initialPreviewConfig: [
			<?php foreach ($propery_gallery as $image): ?>
			{
				caption: '<?= $image['filename'] ?>',
				url: web_url + 'realty/properties/request/delete_imgGallery?id=<?= $image['id'] ?>'
			},
			<?php endforeach ?>],
		})
		
		$(".gallery-upload").on('filesorted', function(event, params) {
			console.log('File sorted ', params.previewId, params.oldIndex, params.newIndex, params.stack);
			
			var formData = new FormData();
			let images = params.stack.map( img => formData.append("images[]", img.caption) )
	
			quick_request(function( Response ) {
				console.log('Success')
			}, 
			web_url + 'realty/properties/request/sort_images?session_id=<?= $propertyDets['propery_imgSession'] ?>',
			formData)
			
		})
		

		$("select[name='slcpropertytype']").change(categoryChange)

		$("#taken-inp").change(taken_change)

		taken_change()
		categoryChange()
	})

	function taken_change()
	{
		if ( $("#taken-inp").is(":checked") )
			$("#contract-div").show()
		else
			$("#contract-div").hide()
	}

	function categoryChange() 
	{
		var subList = subCategory[$("select[name='slcpropertytype']").val()];
		$("select[name='slcsubctgry']").children().remove();
		$("select[name='slcsubctgry']").append("<option value=''>Select Subcategory</option>")
		for( var subcat of subList ){
			var selected = ( subCatSel == subcat['id'] )? 'selected':''

			$("select[name='slcsubctgry']").append("<option value="+ subcat['id'] +" " + selected +">" + subcat['value'] + "</option>");
		}
	}
</script>