<?= $this->include("Includes/Header_content") ?>
<style type="text/css">
	.td-amentity{
		cursor: pointer;
	}
</style>

<div class="content">

	<div class="mb-2">
		<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-lg"><i class="fa fa-plus"></i> Add Amenity</button>
	</div>

	<div class="card">
      <div class="card-header">
      	<section class="navbar navbar-light bg-transparent">

      		<div class="content float-left d-flex">
	            <span class="text-gray-dark mr-2">Total Record: <span id="total-record"></span></span>
	            <div class="filter-div d-flex" data-load="load_amenties"></div>
	        </div>

		    <div class="content float-right">
		      <!-- SEARCH FORM -->
		      	<div class="input-group input-group-sm">
                  <input type="hidden" id="target-search" value="load_amenties">
                  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" id="txtsearch">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="button" id="btnsearch">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
		    </div>
	    </section>
      </div>
      <!-- /.card-header -->
      <div class="card-body overlay-wrapper">

      	<div class="overlay" id="list-overlay">
          <i class="fas fa-3x fa-sync-alt fa-spin"></i>
        </div>

        <table id="example1" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>Amenety Names</th>
            <th width="2%">Action</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->

      
	    <div class="card-footer">
	      <div class="pagination-ajax" data-load="load_amenties"></div>
	    </div>
    </div>
    <!-- /.card -->

</div>





<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <input class="hd-amty-id" type="hidden">
	    <input id="hd-dlt-lnk" type="hidden" value="<?= base_url($spageURL."request/delete_amenity") ?>">
	    <div class="modal-header">
	      <h4 class="modal-title">Delete Amenity</h4>
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	    </div>
	    <div class="modal-body">
	      <p>Are you sure you want to delete this data?</p>
	    </div>
	    <div class="modal-footer justify-content-between">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      <button id="btn-delete-amty" type="button" class="btn btn-danger">
	        &nbsp;
	        Delete 
	        &nbsp;
	        <i id="spn-amty" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
	      </button>
	    </div>
	  </div>
	  <!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">New Amenity</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
			<div class="form-group d-flex justify-content-around">
				<label>Amenity Name</label>
				<div class="">
					<input type="text" name="form-control" id="txt-amty-name-new">
				</div>
			</div>
			<div class="modal-footer text-right">
				<button type="button" class="btn btn-primary" id="btn-add-amty">
	        		&nbsp;
					Add
	        		&nbsp;
					<i id="spn-amty-add" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
				</button>
			</div>
    	</div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-edit-amty">
	<input class="hd-amty-id" type="hidden">
	<input id="hd-edt-lnk" type="hidden" value="<?= base_url($spageURL."request/edit_amenity") ?>">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Amenity</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
			<div class="form-group d-flex justify-content-around">
				<label>Amenity Name</label>
				<div class="">
					<input type="text" name="form-control" id="txt-amty-name-edit">
				</div>
			</div>
	        <div class="modal-footer text-right">
				<button type="button" class="btn btn-primary float-right" id="btn-edit-amty">
	        		&nbsp;
					Save Changes
	        		&nbsp;
					<i id="spn-amty-edit" class="sm fas fa-sync-alt fa-spin" style="display: none;"></i>
	      		</button>
	        </div>
    	</div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function() {

		load_amenties();

	});

	$("body").on("click", ".amty-delete", function() {
	    trace("Deleting: " + $(this).data("session-id"));
	    $(".hd-amty-id").val($(this).data("session-id"));

	    $("#modal-default").modal("show");
	  });

	$("#btn-delete-amty").click(function() {

		$("#spn-amty").show();

		quick_request(function(data) { 
		  load_amenties();
		  $("#spn-amty").hide();
		  $("#modal-default").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Amenity successfully deleted'
		  });
		}, $("#hd-dlt-lnk").val() + "?amtyid=" + $(".hd-amty-id").val());

	});

	function load_amenties(offset)
	{
		setup_pagination(offset);

		$("#list-overlay").show();

		quick_request(function(data) { 
			load_pagination(data.pagination);
		    load_filter();
		    $("#total-record").html(data.totalRecord);
			setup_ameneties(data.amtylist);
			$("#list-overlay").hide();
		}, '<?= base_url($spageURL.'request/load_amenties') ?>?' + URLParser().getQuery());

	}

	function setup_ameneties(list)
	{
		remove_allamenety();

		trace("loading Amenities");
		var i = 1;
		for(var cAmty in list)
		{
			var AmtyDets = list[cAmty];
			var NewRow = $("#clone-tr").clone();

			NewRow.removeAttr("id");
			NewRow.find(".amty-name").html(AmtyDets.Name);
			NewRow.find(".amty-name").data("session-id", AmtyDets.SessionID);
			NewRow.find(".amty-delete").data("session-id", AmtyDets.SessionID);

			$("#example1 tbody").append(NewRow);

			i++;
		}

	}

	function remove_allamenety()
	{
		trace("Removing Amenities");
		$("#example1 tbody").children().remove();
	}

	$(".content").on("click", ".td-amentity", function() {
		$(".hd-amty-id").val($(this).data("session-id"));
		$("#txt-amty-name-edit").val($(this).html());
		$("#modal-edit-amty").modal("show");
	});

	$("#btn-edit-amty").click(function() {
		$("#spn-amty-edit").show();
		var formData = new FormData();

		formData.append("AmtyName", $("#txt-amty-name-edit").val());
		formData.append("AmtyID", $(".hd-amty-id").val());

		quick_request(function(data) { 
		  load_amenties();
		  $("#spn-amty-edit").hide();
		  $("#modal-edit-amty").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Amenity successfully edited'
		  });
		}, $("#hd-edt-lnk").val(), formData);
	});

	$("#btn-add-amty").click(function() {
		$("#spn-amty-add").show();
		var formData = new FormData();

		formData.append("AmtyName", $("#txt-amty-name-new").val());

		quick_request(function(data) { 
		  load_amenties();
		  $("#spn-amty-add").hide();
		  $("#modal-lg").modal("hide");
		  Toast.fire({
		    icon: 'success',
		    title: 'Amenity successfully added'
		  });
		}, '<?= base_url($spageURL.'/request/add_amenity') ?>', formData);
	});

</script>

<div style="display: none;">
	<table>
		<tr id="clone-tr">
			<td class="td-amentity amty-name" data-session-id="">Pool</td>
			<td>
				<button type="button" class="btn btn-sm bg-red amty-delete" data-session-id="">
					<i class="fas fa-trash"></i>
				</button>
			</td>
		</tr>
	</table>
</div>