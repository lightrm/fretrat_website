<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="error-page">
    <h2 class="headline text-warning"> 404</h2>

    <div class="error-content">
      <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>

      <p>
        We could not find the page you were looking for.
        Meanwhile, you may <a href="<?= base_url() ?>">return to dashboard</a> or contact your <a href="https://www.facebook.com/dapdapandcat/">technical support</a>.
      </p>
    </div>
    <!-- /.error-content -->
  </div>
  <!-- /.error-page -->
</section>
  <!-- /.content -->