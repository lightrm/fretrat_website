
<!-- Banner start -->

<div class="banner" id="banner">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
        <?php foreach ($propertyDatabd as $i => $row) { ?>

                <div class="carousel-item <?= ($i == 0 ? "active" : "") ?> item-bg">
                    <img class="d-block w-100 h-100" src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'original-' . $row['img_filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="banner">
                    <div class="carousel-caption banner-slider-inner d-flex h-100 text-left">
                        <div class="carousel-content container b1-inner">
                            <div class="banner-text-box">
                                <?php $offer_type = ($row['offer_type'] == "rental" || $row['offer_type'] == "Rental") ? "For Lease" : (($row['offer_type'] == "resale" || $row['offer_type'] == "Resale") ? "For Sale" : "Pre-selling"); ?>
                                <div class="plan-price" data-animation="animated fadeInDown delay-05s" style="color: #FFD700"><sup style="color: #FFD700">₱</sup><?= number_format($row['price'], 2) ?><span></span></div>
                                <h3 data-animation="animated fadeInUp delay-05s" title="<?= $row['name'] ?>"><a href="<?= base_url("pages/property_view/" . str_replace(" ", "-", $offer_type) . '/' . $row['slug']) ?>" style="color: #FFFFFF"><?= $row['name'] ?></a></h3>
                                <div class="meta" data-animation="animated fadeInUp delay-05s">
                                    <ul>
                                        <li>
                                            <i class="flaticon-square-layouting-with-black-square-in-east-area"></i><?= $row['size'] ?> Sqm
                                        </li>
                                        <li>
                                            <i class="flaticon-bed"></i><?= $row['bedroom'] ?> Beds
                                        </li>
                                        <li>
                                            <i class="flaticon-hotel-building"></i>Floor Level: <?= number_format($row['floor_level']) ?>
                                        </li>
                                        <li>
                                            <i class="flaticon-balcony-and-door"></i><?= ($row['is_furnished'] == 1 ? "Furnished" : "Unfurnished") ?>
                                        </li>
                                        <li>
                                            <i class="flaticon-car-repair"></i><?= ($row['parking'] == 1 ? "With Parking" : "Without Parking") ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        <?php } ?>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="slider-mover-left" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="slider-mover-right" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </span>
        </a>
        <div class="btn-secton btn-secton-2">
            <ol class="carousel-indicators">
            <?php for ($i=0; $i < count($propertyDatabd); $i++) { ?>

                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>" class="<?= ($i == 0 ? "active" : "") ?>"></li>

            <?php } ?>
            </ol>
        </div>
    </div>

    <!-- Search area start -->
    <div class="search-area search-area-3 d-none d-xl-block d-lg-block">
        <h2>Find Your Destination</h2>
        <p>Discover the world</p>
        <div class="search-area-inner">
            <div class="search-contents">
                <form action="<?= base_url("pages/property_list") ?>" method="GET" id="frm_find_destination">
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="txt_propertyStatus">
                            <option selected disabled>Property Status</option>
                            <option value="resale">For Sale</option>
                            <option value="rental">For Lease</option>
                            <option value="pre-selling">Pre-selling</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="txt_propertyType" id="main_categ">
                            <option selected disabled>Property Types</option>
                        <?php foreach ($main_categories as $row) { ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="txt_subCategory" id="sub_categ">
                            <option selected disabled>Sub Category</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="txt_propertyLocation">
                            <option selected disabled>Location</option>
                            <?php foreach ($locations as $row) { ?>
                                    <option><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="search-button btn-md btn-color">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Search area end -->
</div>
<!-- banner end -->
    <div class="featured-properties mt-5">
        <div class="container">
            <div class="main-title">
                <h1>Featured Properties</h1>
                <ul class="list-inline-listing filters filteriz-navigation">
                    <li class="btn filtr-button filtr active" data-filter="all">All</li>
                    <li data-filter="1" class="btn btn-inline filtr-button filtr">Condominium</li>
                    <li data-filter="2" class="btn btn-inline filtr-button filtr">House and Lot</li>
                    <li data-filter="3" class="btn btn-inline filtr-button filtr">Office</li>
                    <li data-filter="4" class="btn btn-inline filtr-button filtr">Commercial</li>
                </ul>
            </div>
            <div class="row filter-portfolio wow fadeInUp delay-04s">
                <div class="cars">

                    <?php foreach ($propertyDatabd as $i => $row) { ?>
                            <div class="col-lg-4 col-md-6 col-sm-12 filtr-item" data-category="<?= $row['property_type'] ?>">
                                <div class="property-box-7">
                                    <div class="property-thumbnail">
                                        <a href="<?= base_url("pages/property_view/" . str_replace(" ", "-", $offer_type) . '/' . $row['slug']) ?>" class="property-img">
                                            <div class="tag-2"><?= $offer_type ?></div>
                                            <div class="price-box"><span style="color: #FFD700">₱<?= number_format($row['price'], 2) ?></span> </div>
                                            <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'original-' . $row['img_filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-box-7" class="img-fluid" style="max-height: 180px">
                                        </a>
                                        
                                        <div class="property-overlay">
                                            <a class="overlay-link property-video" title="View Gallery" onclick="property_modal('<?= $row['slug'] ?>', '<?= str_replace(" ", "-", $offer_type) ?>', '<?= $row['name'] ?>', '<?= $row['location'] ?>', '<?= $row['property_vid_url'] ?>', '<?= number_format($row['price'], 2) ?>', '<?= $row['size'] ?>', '<?= $row['bedroom'] ?>', '<?= $row['floor_level'] ?>', '<?= ($row['is_furnished'] == 1 ? "Yes" : "No") ?>', '<?= ($row['parking'] == 1 ? "Yes" : "No") ?>', '<?php 

                                                    if (session()->has("User") || $row['agent_role'] == 1 || $row['agent_role'] == 2) {
                                                        if (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2 || $row['agent_role'] == 1 || $row['agent_role'] == 2) {
                                                            echo $row['agent_name'] . ' / ' . $row['agent_contact'];
                                                        }
                                                        else{
                                                            goto b;
                                                        }
                                                    }
                                                    else{
                                                        b:
                                                        $agent_name = explode(" ", $row['agent_name']); 
                                                        echo $agent_name[0];
                                                        
                                                    }

                                                ?>')">
                                                <i class="fa fa-image"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="detail">
                                        <h1 class="title">
                                            <a href="<?= base_url("pages/property_view/" . str_replace(" ", "-", $offer_type) . '/' . $row['slug']) ?>" title="<?= $row['name'] ?>"><?= $row['name'] ?></a>
                                        </h1>
                                        <div class="location">
                                            <a href="<?= base_url("pages/property_view/" . str_replace(" ", "-", $offer_type) . '/' . $row['slug']) ?>" class="limit-text" title="<?= $row['location'] ?>">
                                                <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i><?= $row['location']; ?>
                                            </a>
                                        </div>
                                    </div>
                                    <ul class="facilities-list clearfix">
                                        <li>
                                            <span>Area</span><?= $row['size'] ?> Sqm
                                        </li>
                                        <li>
                                            <span>Beds</span> <?= $row['bedroom'] ?>
                                        </li>
                                        <li>
                                            <span>Furnished</span> <?= ($row['is_furnished'] == 1 ? "Yes" : "No") ?>
                                        </li>
                                        <li>
                                            <span>Parking</span> <?= ($row['parking'] == 1 ? "Yes" : "No") ?>
                                        </li>
                                    </ul>
                                    <div class="footer clearfix">
                                        <div class="pull-left days">
                                            <p><i class="fa fa-user"></i> 
                                                <?php 

                                                    if (session()->has("User") || $row['agent_role'] == 1 || $row['agent_role'] == 2) {
                                                        if (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2 || $row['agent_role'] == 1 || $row['agent_role'] == 2) {
                                                            echo $row['agent_name'] . ' / <i class="fa fa-phone"></i> ' . $row['agent_contact'];
                                                        }
                                                        else{
                                                            goto a;
                                                        }
                                                    }
                                                    else{
                                                        a:
                                                        $agent_name = explode(" ", $row['agent_name']); 
                                                        echo $agent_name[0];
                                                        
                                                    }

                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

    <div class="services-3 content-area-20 bg-white">
        <div class="container">
            <div class="main-title">
                <h1>What Are you Looking For?</h1>
                <p><?= $WAYLF_html['value'] ?></p>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 s-brd-2 wow fadeInLeft delay-04s">
                    <a href="<?= base_url("pages/property_list") . "?txt_propertyType=1"; ?>">
                        <i class="flaticon-hotel-building"></i>
                        <h5>Condominium</h5>
                        <p><?= $WAYLF_AC_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 s-brd-1 wow fadeInUp delay-04s">
                    <a href="<?= base_url("pages/property_list") . "?txt_propertyType=2"; ?>">
                        <i class="flaticon-house"></i>
                        <h5>House and Lot</h5>
                        <p><?= $WAYLF_H_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 wow fadeInRight delay-04s">
                    <a href="<?= base_url("pages/property_list") . "?txt_propertyType=4"; ?>">
                        <i class="flaticon-office-block"></i>
                        <h5>Commercial/Office</h5>
                        <p><?= $WAYLF_C_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 s-brd-3 wow fadeInDown delay-04s">
                    <a href="<?= base_url("pages/contact_us"); ?>">
                        <i class="flaticon-call-center-agent"></i>
                        <h5>Support 24/7</h5>
                        <p><?= $WAYLF_S247_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-12 text-center">
                    <a data-animation="animated fadeInUp delay-10s" href="<?= base_url("pages/property_list"); ?>" class="btn btn-lg btn-theme">More Details</a>
                </div>
            </div>
        </div>
    </div>

    <div class="most-popular-places content-area-3 mt-5">
        <div class="container">
            <div class="main-title">
                <h1>Most Popular Places</h1>
                <p><?= $MPP_html['value'] ?></p>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-pad wow fadeInLeft delay-04s">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="<?= base_url($pluginDir) . "/img/backgrounds/makati.jpg"; ?>" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2>Makati</h2>
                                            <p><?= $makati ?> Properties</p>
                                            <a href="<?= base_url("pages/property_list") . "?txt_propertyLocation=Makati"; ?>" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="<?= base_url($pluginDir) . "/img/backgrounds/taguig.jpg"; ?>" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2>Taguig</h2>
                                            <p><?= $taguig ?> Properties</p>
                                            <a href="<?= base_url("pages/property_list") . "?txt_propertyLocation=Taguig"; ?>" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-pad wow fadeInLeft delay-04s">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="<?= base_url($pluginDir) . "/img/backgrounds/pasig.jpg"; ?>" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2>Pasig</h2>
                                            <p><?= $pasig ?> Properties</p>
                                            <a href="<?= base_url("pages/property_list") . "?txt_propertyLocation=Pasig"; ?>" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="<?= base_url($pluginDir) . "/img/backgrounds/pasay.jpg"; ?>" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2>Pasay</h2>
                                            <p><?= $pasay ?> Properties</p>
                                            <a href="<?= base_url("pages/property_list") . "?txt_propertyLocation=Pasay"; ?>" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-pad wow fadeInRight delay-04s">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="<?= base_url($pluginDir) . "/img/backgrounds/alabang.jpg"; ?>" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2>Alabang</h2>
                                            <p><?= $alabang ?> Properties</p>
                                            <a href="<?= base_url("pages/property_list") . "?txt_propertyLocation=Alabang"; ?>" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="<?= base_url($pluginDir) . "/img/backgrounds/quezon.jpg"; ?>" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2>Quezon City</h2>
                                            <p><?= $quezon ?> Properties</p>
                                            <a href="<?= base_url("pages/property_list") . "?txt_propertyLocation=Quezon City"; ?>" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="simple-content" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/need_assistance_bg.jpg"; ?>') no-repeat; background-size: 100% 100%; background-position: center;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7 align-self-center wow fadeInLeft delay-04s">
                    <h1>Need Assistance</h1>
                    <p><?= $LBHE_html['value'] ?></p>
                    <a data-animation="animated fadeInUp delay-10s" href="<?= base_url("pages/property_list"); ?>" class="btn btn-lg btn-round btn-theme">Inquire Now</a>
                    <a data-animation="animated fadeInUp delay-10s" href="<?= base_url("pages/agents_list"); ?>" class="btn btn-lg btn-round btn-white-lg-outline">Learn More</a>
                </div>
            </div>
            <br><br><br><br><br>
        </div>
    </div>

    <div class="agent content-area-2">
        <div class="container">
            <div class="main-title">
                <h1>Meet Team Fretrato</h1>
                <p><?= $MOA_html['value'] ?></p>
            </div>
            <div class="row">
            <?php foreach ($employees as $row) { ?>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 wow fadeInLeft delay-04s">
                        <div class="agent-1">
                            <div class="agent-photo">
                                <a href="<?= base_url('pages/agents_view/' . $row['id']) ?>">
                                    <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'avatar_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'avatar_medium-' . $row['img_filename'] : base_url($pluginDir) . '/img/blank_user.jpg'); ?>" alt="avatar" class="rounded-circle">
                                </a>
                            </div>
                            <div class="agent-details">
                                <h5><a href="<?= base_url('pages/agents_view/' . $row['id']) ?>"><?= $row['name'] ?></a></h5>
                                <p><?= $row['about'] ?></p>
                                <ul class="social-list clearfix">

                                    <li <?= ($row['telegram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['telegram_link'] != null ? $row['telegram_link'] : "") ?>" class="telegram mb-3"><i class="fa fa-telegram"></i></a></li>
                                    <li <?= ($row['whatsup_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['whatsup_link'] != null ? $row['whatsup_link'] : "") ?>" class="whatsapp mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                    <li <?= ($row['we_chat_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['we_chat_link'] != null ? $row['we_chat_link'] : "") ?>" class="wechat mb-3"><i class="fa fa-wechat"></i></a></li>
                                    <li <?= ($row['viber_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['viber_link'] != null ? $row['viber_link'] : "") ?>" class="viber mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                    <li <?= ($row['facebook_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['facebook_link'] != null ? $row['facebook_link'] : "") ?>" class="facebook mb-3"><i class="fa fa-facebook"></i></a></li>
                                    <li <?= ($row['instagram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['instagram_link'] != null ? $row['instagram_link'] : "") ?>" class="instagram mb-3"><i class="fa fa-instagram"></i></a></li>
                                    <li <?= ($row['linkedin_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['linkedin_link'] != null ? $row['linkedin_link'] : "") ?>" class="linkedin mb-3"><i class="fa fa-linkedin"></i></a></li>
                                    <li <?= ($row['google_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['google_link'] != null ? $row['google_link'] : "") ?>" class="google mb-3"><i class="fa fa-google"></i></a></li>
                                    <li <?= ($row['twitter_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['twitter_link'] != null ? $row['twitter_link'] : "") ?>" class="twitter mb-3"><i class="fa fa-twitter"></i></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
            <?php } ?>
            </div>
        </div>
    </div>


<?= $this->include("Includes/property_modal") ?>
<?= $this->include("Includes/category_ajax") ?>

