$(function() {
  update_paneling()
  load_amenties()
  toogle_video_url()
  $("#ck-video-url").on("change", toogle_video_url);
  $("#btn-add-refresh").click(load_amenties);

  $(".select2").select2({
    theme: "classic",
    allowClear: true
  });

  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
      alwaysShowClose: true
    });
  });
});

const config_paneling = {
  'bathroom': {'name': 'Bathrooms', 'type': 'float' },
  'bedroom': {'name': 'Bedrooms', 'type': 'float', 'attributes': { 'required': null } },
  'floorarea': {'name': 'Floor Area', 'type': 'float', 'attributes': { 'required': null } },
  'floorlevel': {'name': 'Floor Level', 'type': 'float', 'attributes': { 'required': null } },
  'landarea': {'name': 'Land Area', 'type': 'float', 'attributes': { 'required': null } },
  'parking': {'name': 'Parking', 'type': 'int' },
  'stay-month': {'name': 'Stay Months', 'type': 'int' },
  'stay-year': {'name': 'Stay Years', 'type': 'int' },
  'condition': {'name': 'Condition', 'type': 'select', value: ['Fully Furnished', 'Semi-Furnished', 'Unfurnished'], 'attributes': { 'required': null } },
  'housecondition': {'name': 'House Condition', 'type': 'select', value: ['Brand New', 'Used', 'Old'], 'attributes': { 'required': null } },
  'landcondition': {'name': 'Land Condition', 'type': 'select', value: ['Structured', 'Vacant'], 'attributes': { 'required': null } },
  'datefinish': {'name': 'Date Finished', 'type': 'date' },
},
paneling = {
  'land': ['landarea', 'landcondition'],
  'apartment': [ 'floorarea', 'floorlevel', 'condition', 'parking' ],
  'commercial': ['floorarea', 'floorlevel', 'parking'],
  'office': ['floorarea', 'floorlevel', 'parking', 'condition'],
  'house and lot': ['bathroom', 'bedroom', 'floorarea', 'floorlevel', 'landarea', 'condition', 'housecondition', 'parking', 'datefinish'],
  'condominium': ['bathroom', 'bedroom', 'floorarea', 'floorlevel', 'condition', 'parking', 'stay-month', 'stay-year'],
  'lot': ['landarea', 'landcondition']
}


function toogle_video_url() {
  if( $("#ck-video-url").is(":checked") ) {
    $("#inp-video-url").prop("disabled", false);
  } else {
    $("#inp-video-url").prop("disabled", true);
  }
}

function load_amenties()
{
    $("#list-overlay").show();

    quick_request(function(data) { 
    setup_ameneties(data.amtylist);
    $("#list-overlay").hide();
    }, web_url + 'realty/amenities/request/load_amenties?no_pager=true');

}
function setup_ameneties(list)
{
  remove_allamenety();

  trace("loading Amenities");
  var i = 1;
  for(var cAmty in list)
  {
    var AmtyDets = list[cAmty];
    var option = $("<option></option>");

    if(prev_amenity.includes(AmtyDets.SessionID))
      option.prop("selected", true);

    option.val(AmtyDets.SessionID);
    option.html(AmtyDets.Name);

    $("#slct-amty").append(option);
    i++;
  }

}

function remove_allamenety()
{
  trace("Removing Amenities");
  $("#slct-amty").children().remove();
}

function update_paneling()
{
  $("#paneling-div").empty()
  let panel = null

  try {
    panel = paneling[ ($("select[name='slcpropertytype'] option:selected").text() || '').toLowerCase() ]
  } catch ( e ) {
    console.error('[Panel Error]', e)
    return
  }

  for( const category of panel ) {
    const categ_details = config_paneling[ category ],
    required = categ_details.hasOwnProperty('attributes') && categ_details['attributes'].hasOwnProperty('required')
    let user_input = `<input type="number" name="${category}_txt" class="form-control" value="<?=  isset($propRef)? $propRef['floor_level']: old('txtfloor') ?>" required>`

    if( categ_details.type == 'select' ) {
      user_input = $(`<select name="${category}_slc" class="form-control" required></select>`)
      for( const item of categ_details.value )
        user_input.append(`<option>${item}</option>`)
    } else if( categ_details.type == 'float' )
      user_input = $(`<input type="number" name="${category}_txt" step="any" class="form-control">`)
    else if ( categ_details.type == 'int' )
      user_input = $(`<input type="number" name="${category}_txt" class="form-control">`)
    else if  ( categ_details.type == 'date' )
      user_input = $(`<input type="date" name="${category}_txt" class="form-control">`)
    else
      user_input = $(`<input type="text" name="${category}_txt" class="form-control">`)

    user_input.prop('required', required)
    if( valueHolders[category] && categ_details.type != 'select' || ( categ_details.type == 'select' && categ_details.value.includes( valueHolders[category] ) ) )
      user_input.val( valueHolders[category] )

    const div = $(`<div class="col-6">
      <div class="form-group">
        <label class="text-gray-dark">${categ_details['name']} ${required?'<span class="text-red">*</span>':''}</label>
        <div id="inputholder"></div>
      </div>
    </div>`)

    $("#inputholder", div).append( user_input )
    $("#paneling-div").append(div)

  }
}


$("select[name='slcpropertytype']").change(function() {
  update_paneling()
})

//Remove Return If done
window.onbeforeunload = function(e) { // for leaving the page
  return 'Are you sure you want to leave this page?';
};

$(document).on("submit", "form", function(event){ // for submitting/creating the form
  window.onbeforeunload = null;
});
