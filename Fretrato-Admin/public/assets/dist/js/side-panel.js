var merged = {"side-toogle":true, "side-scroll":0};
var forbbiden_ea = ['side-toogle', 'side-scroll'];
var all_panel_active = Cookies.get("side-panel-active");


if(all_panel_active != null && all_panel_active != "" && all_panel_active != undefined) {
  var PanelObj = JSON.parse(all_panel_active);
  for(var cPanel in PanelObj)
  {
    if(forbbiden_ea.includes(cPanel))
      continue;

    if(PanelObj[cPanel]){
      $(".has-treeview[data-type='"+cPanel+"']").addClass("menu-open");
      $(".has-treeview[data-type='"+cPanel+"']").data("open-drop", PanelObj[cPanel]);
    }

    merged[cPanel] = PanelObj[cPanel];
  }

  merged['side-toogle'] = PanelObj['side-toogle'];
  merged['side-scroll'] = PanelObj['side-scroll'];
}




$('[data-widget="pushmenu"]').PushMenu((merged['side-toogle']?"expand":"collapse"));
$('[data-widget="pushmenu"]').click(function() {

  merged["side-toogle"] = !merged['side-toogle'];
  refresh_cookie_panel();

});

$(".sidebar .has-treeview a").click(function() {

  if($($(this).parent()).data("open-drop")){
    $($(this).parent()).data("open-drop", false);
  } else {
    $($(this).parent()).data("open-drop", true);
  }

  set_side_panel();

});


{
  $('.sidebar').overlayScrollbars({
    callbacks: { 
        onScroll:function(e){
        merged['side-scroll'] = e.target.scrollTop;
        refresh_cookie_panel();
      }
    }
  });

  $('.sidebar').overlayScrollbars().scroll({y: merged['side-scroll']});
}

function set_side_panel()
{
  $(".sidebar .has-treeview").each(function() {
    merged[$(this).data("type")] = $(this).data("open-drop");
  });

  refresh_cookie_panel();
}

function refresh_cookie_panel()
{
  Cookies.set("side-panel-active", JSON.stringify(merged), {
   expires : 1,
   path: "/"
  });
}