var Toast, allow_debug = true, web_url = "http://admin.localfretrato.com.ph/";
var summernoteToolBar = [
  ['style', ['style']],
  ['font', ['bold', 'underline', 'clear']],
  ['fontname', ['fontname']],
  ['fontsize', ['fontsize']],
  ['color', ['color']],
  ['para', ['ul', 'ol', 'paragraph']],
  ['table', ['table']],
  ['insert', ['link', 'video']],
  ['view', ['fullscreen', 'codeview', 'help']],
]
$(function() {
		Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000
	});
});

quick_request(function(data) { for(var index in data){ window[index] = data[index]; } }, web_url + 'request/ajax/web_configure');


function trace(message) 
{
  if( !allow_debug )
    return;

  console.log(message);
}

function quick_request(callback, url, formData = {}, type = "POST")
{
    madeRequest = new Promise(function(resolve, reject) {
        $.ajax({
          url: url,
          type: type,
          xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            return myXhr;
          },
          success: resolve,
          error : reject,
          data: formData,
          cache: false,
          dataType : 'json',
          contentType: false,
          processData: false
        });
    });

    madeRequest.then(callback).catch(function( XMLHttpRequest, textStatus, errorThrown ){
        if ( typeof allow_debug !== undefined ) {

                trace(XMLHttpRequest.responseText);
                $("#return-data").modal("show");
                $("#return-data-ctx").html(XMLHttpRequest.responseText);
            }
    });
}

function URLParser(u = (window.location.href)){
    var path="",query="", newQuery="", hash="",params = {};
    if(u.indexOf("#") > 0){
        hash = u.substr(u.indexOf("#") + 1);
        u = u.substr(0 , u.indexOf("#"));
    }
    if(u.indexOf("?") > 0){
        path = u.substr(0 , u.indexOf("?"));
        query = u.substr(u.indexOf("?") + 1);
        for(var cParam in query.split('&') ) {
            var param = query.split('&')[cParam];
            param = param.split("=");
            param[0] = decodeURI(param[0]);
            param[1] = decodeURI(param[1]);
            params[param[0]] = param[1];

        }
    }else
        path = u;

    function seperateParam(param) {
        return param.split('=');
    }
    return {
        getUrl: function(query = null) {
            return window.location.protocol + "//" + window.location.host + window.location.pathname + (query != null? "?"+query:'');
        },
        getHost: function(){
            var hostexp = /\/\/([\w.-]*)/;
            var match = hostexp.exec(path);
            if (match != null && match.length > 1)
                return match[1];
            return "";
        },
        getPath: function(){
            var pathexp = /\/\/[\w.-]*(?:\/([^?]*))/;
            var match = pathexp.exec(path);
            if (match != null && match.length > 1)
                return match[1];
            return "";
        },
        getHash: function(){
            return hash;
        },
        getParams: function(){
            return params
        },
        getQuery: function(){
            return query;
        },
        setHash: function(value){
            if(query.length > 0)
                query = "?" + query;
            if(value.length > 0)
                query = query + "#" + value;
            return path + query;
        },
        setParam: function(name, value, put_url = false){
            if(!params){
                params= new Array();
            }
            if( !(name in params) )
                params[name] = value;

            params[name] = value;

            for (var paramName in params) {
                if(newQuery.length > 0)
                    newQuery += "&";
                if( paramName == name )
                    params[paramName] = value;

                newQuery += paramName + "=" + params[paramName];
            }

            if( newQuery.length > 0 )
                query = "?" + newQuery;
            if(hash.length > 0)
                query = query + "#" + hash;

            if(put_url)
                history.pushState({path:path + query}, '', path + query);
            return path + query;
        },
        getParam: function(name){
            return (name in params)? params[name]:undefined;
        },
        hasParam: function(name){
            return (name in params);
        },
        removeParam: function(name, put_url = false){
            if( name in params )
                delete params[name];
            
            for (var Name in params) {
                if(newQuery.length > 0)
                    newQuery += "&";
                newQuery += Name + "=" + params[Name];
            }

            if( newQuery.length > 0 )
                query = "?" + newQuery;
            if(hash.length > 0)
                query = query + "#" + hash;

            if(put_url)
                history.pushState({path:path + query}, '', path + query);
            return path + query;
        },
        changeURL: function(string, reload = true) {
            reload?location.href = string:history.pushState({path:string}, '', string);
            return "success";
        }
    }
}

function processAjaxData(response, urlPath){
    document.getElementById("content").innerHTML = response.html;
    document.title = response.pageTitle;
    window.history.pushState({"html":response.html,"pageTitle":response.pageTitle},"", urlPath);
}