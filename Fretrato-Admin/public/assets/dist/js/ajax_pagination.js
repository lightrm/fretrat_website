

function setup_pagination(offset = null) {
    if( !URLParser().hasParam("page") ) 
        URLParser().setParam("page", 1, true);

    if(offset != null) {
      URLParser.setParam("page", offset, true);
    }
}

function load_pagination(page_html){
    $(".pagination-ajax").html(page_html);
}

function load_minipagination(page_html, id){
    $(".pagination-mini[data-id='"+ id +"']").html(page_html);
}

$(function() {

    $(".pagination-mini").on("click", ".page-btn", function() {
        var parent_div = $(this).parents(".pagination-mini");

        parent_div.data("page", $(this).data("page"));
        if( typeof parent_div.data('load') !== 'undefined') {
            window[parent_div.data("load")](parent_div.data("id"));
        }
    });

    $(".pagination-ajax").on("click", ".page-btn", function() {
    	URLParser().changeURL($(this).data("href"), false);

    	var parent_div = $(this).parents(".pagination-ajax");
    	if( typeof parent_div.data('load') !== 'undefined') {
    		window[parent_div.data("load")]();
    	}
    });




});