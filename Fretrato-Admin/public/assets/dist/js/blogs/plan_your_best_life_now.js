$(function() {
  load_amenties();
  toogle_video_url();
  $("#ck-video-url").on("change", toogle_video_url);
  $("#btn-add-refresh").click(load_amenties);

  $(".select2").select2({
    theme: "classic",
    allowClear: true
  });

  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
      alwaysShowClose: true
    });
  });
});


function toogle_video_url() {
  if( $("#ck-video-url").is(":checked") ) {
    $("#inp-video-url").prop("disabled", false);
  } else {
    $("#inp-video-url").prop("disabled", true);
  }
}

function load_amenties()
{
    $("#list-overlay").show();

    quick_request(function(data) { 
    setup_ameneties(data.categlist);
    $("#list-overlay").hide();
    }, web_url + 'blogs/category/request/load_category?no_pager=true&type=plan_your_best_life_now');

}

function setup_ameneties(list)
{
  remove_allamenety();

  trace("loading Amenities");
  var i = 1;
  for(var cAmty in list)
  {
    var AmtyDets = list[cAmty];
    var option = $("<option></option>");

    if(prev_amenity.includes(AmtyDets.SessionID))
      option.prop("selected", true);

    option.val(AmtyDets.SessionID);
    option.html(AmtyDets.Name);

    $("#slct-ctgry").append(option);
    i++;
  }

}

function remove_allamenety()
{
  trace("Removing Amenities");
  $("#slct-ctgry").children().remove();
}


//Remove Return If done
window.onbeforeunload = function(e) { // for leaving the page
  return 'Are you sure you want to leave this page?';
};

$(document).on("submit", "form", function(event){ // for submitting/creating the form
  window.onbeforeunload = null;
});
