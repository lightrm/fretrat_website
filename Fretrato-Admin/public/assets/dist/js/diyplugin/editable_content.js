var ListContent = [];

$(function() {

  $(".div-container").each(function() {

    $(this).append('<div class="edit-content text-left" style="display: none"> \
                <div class="d-flex justify-content-end mb-2">\
                  <button class="btn btn-success btn-save-edit" data-id="about">Save</button>\
                  &nbsp;\
                  <div id="btn-cancel">\
                    <button class="btn btn-default btn-cncl-edit" data-id="about">Cancel</button>\
                  </div>\
                </div>\
                <textarea class="textarea"></textarea>\
              </div>\
                <div class="div-content"></div>');


    ListContent.push( $(this).data("id") );
  });

  $('.textarea').summernote({
      placeholder: 'Edit Content Here',
      height: 100,
      toolbar: summernoteToolBar,
      callbacks: {
          onPaste: function (e) {
              var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
              e.preventDefault();
              document.execCommand('insertText', false, bufferText);
          }
      }
  });

  get_editable_content();

  $(".div-container").on("click", ".div-content", function() {
    change_appearance(
      $(this).parents(".div-container").first().data("id"),
      "edit",
    );
  });

  $(".edit-content").on("click", ".btn-save-edit", function() {
    var divID = $(this).parents(".div-container").first().data("id");
    var container = $(".div-container[data-id='" + divID + "'");
    var edit_content = container.find(".edit-content");
    var content = container.find(".div-content");
    var saveVal =  edit_content.find(".textarea").val();

    var formData = new FormData();
    formData.append("content_type", divID);
    formData.append("content_val", saveVal);

    quick_request(
      function(data) {
        Toast.fire({
          icon: 'success',
          title: 'Successfully Saved!'
        });

        content.html(saveVal);
        change_appearance(
          divID,
          "display"
        );

      },
      web_url + "webconfig/request/save_content?type=home",
      formData
    )
  });

  $(".edit-content").on("click", ".btn-cncl-edit", function() {
    change_appearance(
      $(this).parents(".div-container").first().data("id"),
      "display",
    );
  });




});



function change_appearance(id, type, canclable = true)
{

  var container = $(".div-container[data-id='" + id + "'");
  var content = container.find(".div-content");
  var edit_content = container.find(".edit-content");




  switch(type.toLowerCase())
  {
    case 'edit':
      content.hide();
      edit_content.show();
      edit_content.find(".textarea").summernote('code', content.html());
      break;
    case 'display':
      content.show();
      edit_content.hide();
      break;
  }


  if( !canclable )
    edit_content.find(".btn-cncl-edit").hide();

}

function get_editable_content() {


  var formData = new FormData();


  formData.append("Window", "asdasd");
  for(var cList in ListContent)
  {
    if( formData.content === undefined )
      formData.content = [];

    formData.append("content[]", ListContent[cList]); 
  }

  quick_request(
    setupContent, 
    web_url + "webconfig/request/load_content?type=home",
    formData
  );

}

function setupContent(data)
{
  for(var cData in data)
  {
    var container = $(".div-container[data-id='" + cData + "'");
    var content = container.find(".div-content");
    var edit_content = container.find(".edit-content");

    if( data[cData] == null || data[cData] === undefined || data[cData] === "" )
    {
      change_appearance(cData, "edit", false);
      continue;
    }

    content.html(data[cData]);
  }
}