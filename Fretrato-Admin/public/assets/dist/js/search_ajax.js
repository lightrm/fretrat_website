$(function() {


    $("#btnsearch").click(search);
    $("#txtsearch").on('keypress',function(e) {
        if(e.which == 13) search();
    });

    function search() {
        var CurrentUrl = URLParser().setParam("page", 1);
        trace(CurrentUrl);
        CurrentUrl = URLParser(CurrentUrl).setParam("search", $("#txtsearch").val(), true);
        $("#txtsearch").val("");
        trace(CurrentUrl);
        window[$("#target-search").val()]();
    }

    $(".filter-div").on("click", "a", function() {
        trace($(this).data("info"));
        URLParser().changeURL($(this).data("info"), false);
        window[$(".filter-div").data("load")]();
        $($(this).parent()).remove();
    });
});


/*
    <label class="badge badge-primary align-items-center font-weight-normal" data-info="">
        <span>Profile<span>
        <a class="badge badge-light text-dark">✕</a>
    </label>
*/
function load_filter() {
    var parameters = URLParser().getParams();
    $(".filter-div").children().remove();
    for(var Name in parameters) {
        if( acpptblFilter.includes(Name) ) {
            trace("asd");
            var label = $("<label></label");
            label.attr("class", "badge badge-primary d-flex font-weight-normal mr-1");


            var spanTxt = $("<div></div>");
            spanTxt.html(parameters[Name]);
            spanTxt.addClass("text-truncate mr-1");
            spanTxt.css("maxWidth", "200px");

            var removeBtn = $("<a></a");
            removeBtn.html("✕");
            removeBtn.css({cursor: "pointer"});
            removeBtn.attr("class", "badge badge-light text-dark filter-close");
            removeBtn.data("info", URLParser().removeParam(Name));

            label.append(spanTxt);
            label.append(removeBtn);

            $(".filter-div").append(label);
        }
    }
}