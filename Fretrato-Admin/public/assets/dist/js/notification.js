$(function() {
    quick_request(set_up_notification, web_url + 'request/ajax/header_notif');

    $(document).on("click", ".read-all", function() {
      trace("Read All")
      quick_request(set_up_notification, web_url + 'request/ajax/header_notif?read-all=true');
    });

});

function set_up_notification(data)
{
  if(data.unread > 0) { $("#notif-badge").show(); } else { $("#notif-badge").hide() }
  $(".notif-badge-count").html(data.unread);

  $("#head-notif-list").children().remove();
  for(var index in data.notifs)
  {
    var object = data.notifs[index];
    add_new_notification(URLParser(object.url).setParam("notif_id", object.notif_id), object.title, object.time, object.icon);

  }
  

}

function add_new_notification(url, title, time, icon = "fas fa-envelope")
{

  $("#head-notif-list").append('<a href="' + url + '" class="dropdown-item"> \
    <i class="' + icon + ' mr-2"></i> ' + title + ' \
    <span class="float-right text-muted text-sm">' + time + '</span> \
  </a><div class="dropdown-divider"></div>');
}