// ReactJS
var cemtrick1 = false, cemtrick2 = false, cemtrick3 = false, cemtrick4 = false, cemtrick5 = false;

function checkOverAll()
{
	// trace("CheckOverAll: " + (cemtrick1 && cemtrick2 && cemtrick3 && cemtrick4 && cemtrick5));
	OverLay((cemtrick1 && cemtrick2 && cemtrick3 && cemtrick4 && cemtrick5))
}

function OverLay($diplsay = false)
{
	$diplsay?$("#list-overlay").hide():$("#list-overlay").show();
}

class Contact extends React.Component {

	constructor() 
	{
		super();
		this.state = {
      inEdit: false,
			list: null,
			error: null
		}
    	this._isMounted = false;

		this.clickEdit = this.editContent.bind(this);
	}

	editContent() {
		change_setup("about", "edit");
  }

    componentDidMount()
    {
    	this._isMounted = true;
    	window.trace("Mounting Contact Info");
    	window.cemtrick1 = true;
    	window.checkOverAll();
    	let url = window.web_url + "webconfig/request/load_content?type=contact";

    	fetch(url)
    	.then(response => response.json())
    	.then(this.mountContact)
    	.catch(error => {
    		this.defaultState;
    	})
    }

    mountContact = (data) =>
    {
    	if(!this._isMounted)
    		return;

    	this.setState({list: data});

    	window.trace(this.state.list);
    	window.trace("Mounted Contact State");
    }

    defaultState()
    {

    	window.trace("Reseting Contact State");
    	this._isMounted = false;
    	this.setState({
			list: null,
			error: null
		});
    }

    render() 
    {
    	window.trace("Rendering Contact Info");
    	window.cemtrick1 = true;
    	window.checkOverAll();
      change_setup("about", "edit", false);
    	
    	if(this.state.list !== null && this.state.list.content !== null){
    		$('#content-about-editor').summernote('code', this.state.list.content);
        change_setup("about", "display");
    	}

      	return (
      		<div onClick={this.clickEdit} className="parag-content no-wrapper">
      			<span id="html-content">
    				{ 
              this.state.list !== null && this.state.list.realEstate !== null &&
              <span dangerouslySetInnerHTML={{__html:this.state.list.content}}>
              </span> 
    				}
    				</span>
			   </div>
  		)
    }
}

class Contact_card1 extends React.Component {

  constructor() 
  {
    super();
    this.state = {
      list: null,
      error: null
    }

    this.clickEdit = this.editContent.bind(this);
  }

  editContent() {
    change_setup("address", "edit");
    }

    componentDidMount()
    {
      window.trace("Mounting Contact Info");
      window.cemtrick2 = true;
      window.checkOverAll();
      let url = window.web_url + "webconfig/request/load_content?type=contact";

      fetch(url)
      .then(response => response.json())
      .then(this.mountContact)
      .catch(error => {
        this.defaultState;
      })
    }

    mountContact = (data) =>
    {
      this.setState({list: data});

      window.trace(this.state.list);
      window.trace("Mounted Contact State");
    }

    defaultState()
    {

      window.trace("Reseting Contact State");
      this.setState({
      list: null,
      error: null
    });
    }

    render() 
    {
      window.trace("Rendering Contact Info");
      window.cemtrick2 = true;
      window.checkOverAll();
      change_setup("address", "edit", false);
      
      if(this.state.list !== null && this.state.list.address !== null){
        $('#content-address-editor').summernote('code', this.state.list.address);
        change_setup("address", "display");
      }

        return (
          <div onClick={this.clickEdit} className="parag-content no-wrapper">
            
          { 
            this.state.list !== null && this.state.list.address !== null &&
            <span dangerouslySetInnerHTML={{__html:this.state.list.address}}>
            </span>
          }
          
      </div>
      )
    }
}

class Contact_card2 extends React.Component {

  constructor() 
  {
    super();
    this.state = {
      list: null,
      error: null
    }

    this.clickEdit = this.editContent.bind(this);
  }

  editContent() {
    change_setup("email", "edit");
    }

    componentDidMount()
    {
      window.trace("Mounting Contact Info");
      window.cemtrick3 = true;
      window.checkOverAll();
      let url = window.web_url + "webconfig/request/load_content?type=contact";

      fetch(url)
      .then(response => response.json())
      .then(this.mountContact)
      .catch(error => {
        this.defaultState;
      })
    }

    mountContact = (data) =>
    {
      this.setState({list: data});

      window.trace(this.state.list);
      window.trace("Mounted Contact State");
    }

    defaultState()
    {

      window.trace("Reseting Contact State");
      this.setState({
      list: null,
      error: null
    });
    }

    render() 
    {
      window.trace("Rendering Contact Info");
      window.cemtrick3 = true;
      window.checkOverAll();
      change_setup("email", "edit", false);
      
      if(this.state.list !== null && this.state.list.email !== null){
        $('#content-email-editor').summernote('code', this.state.list.email);
        change_setup("email", "display", true);
      }

        return (
          <div onClick={this.clickEdit} className="parag-content no-wrapper">
            
          { 
            this.state.list !== null && this.state.list.email !== null &&
            <span dangerouslySetInnerHTML={{__html:this.state.list.email}}>
            </span>
          }
      </div>
      )
    }
}


class Contact_card3 extends React.Component {

  constructor() 
  {
    super();
    this.state = {
      list: null,
      error: null
    }

    this.clickEdit = this.editContent.bind(this);
  }

  editContent() {
    change_setup("contact", "edit");
    }

    componentDidMount()
    {
      window.trace("Mounting Contact Info");
      window.cemtrick4 = true;
      window.checkOverAll();
      let url = window.web_url + "webconfig/request/load_content?type=contact";

      fetch(url)
      .then(response => response.json())
      .then(this.mountContact)
      .catch(error => {
        this.defaultState;
      })
    }

    mountContact = (data) =>
    {
      this.setState({list: data});

      window.trace(this.state.list);
      window.trace("Mounted Contact State");
    }

    defaultState()
    {

      window.trace("Reseting Contact State");
      this.setState({
      list: null,
      error: null
    });
    }

    render() 
    {
      window.trace("Rendering Contact Info");
      window.cemtrick4 = true;
      window.checkOverAll();
      change_setup("contact", "edit", false);
      
      if(this.state.list !== null && this.state.list.contact !== null){
        $('#content-contact-editor').summernote('code', this.state.list.contact);
        change_setup("contact", "display");
      }

        return (
          <div onClick={this.clickEdit} className="parag-content no-wrapper">
            
          { 
            this.state.list !== null && this.state.list.realEstate !== null &&
            <span dangerouslySetInnerHTML={{__html:this.state.list.contact}}>
            </span>
          }
      </div>
      )
    }
}

class Contact_card4 extends React.Component {

  constructor() 
  {
    super();
    this.state = {
      list: null,
      error: null
    }

    this.clickEdit = this.editContent.bind(this);
  }

    editContent() {
      change_setup("landline", "edit");
    }

    componentDidMount()
    {
      window.trace("Mounting Contact Info");
      window.cemtrick5 = true;
      window.checkOverAll();
      let url = window.web_url + "webconfig/request/load_content?type=contact";

      fetch(url)
      .then(response => response.json())
      .then(this.mountContact)
      .catch(error => {
        this.defaultState;
      })
    }

    mountContact = (data) =>
    {
      this.setState({list: data});

      window.trace(this.state.list);
      window.trace("Mounted Contact State");
    }

    defaultState()
    {

      window.trace("Reseting Contact State");
      this.setState({
      list: null,
      error: null
    });
    }

    render() 
    {
      window.trace("Rendering Contact Info");
      window.cemtrick5 = true;
      window.checkOverAll();
      change_setup("landline", "edit", false);
      
      if(this.state.list !== null && this.state.list.landline !== null){
        $('#content-landline-editor').summernote('code', this.state.list.landline);
        change_setup("landline", "display", true);
      }

        return (
          <div onClick={this.clickEdit} className="parag-content no-wrapper">
            
          { 
            this.state.list !== null && this.state.list.landline !== null &&
            <span dangerouslySetInnerHTML={{__html:this.state.list.landline}}>
            </span>
          }
      </div>
      )
    }
}

var aboutDOM = ReactDOM.render(<Contact/>, $("#content-about")[0]);
var addressDOM = ReactDOM.render(<Contact_card1 />, $("#content-address")[0]);
var emailDOM = ReactDOM.render(<Contact_card2 />, $("#content-email")[0]);
var contactDOM = ReactDOM.render(<Contact_card3 />, $("#content-contact")[0]);
var landlineDOM = ReactDOM.render(<Contact_card4 />, $("#content-landline")[0]);