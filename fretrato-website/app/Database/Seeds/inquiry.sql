-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 13, 2020 at 02:29 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fretrato_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `inquiry`
--

DROP TABLE IF EXISTS `inquiry`;
CREATE TABLE IF NOT EXISTS `inquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `message` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inquiry`
--

INSERT INTO `inquiry` (`id`, `name`, `email`, `about`, `contact_no`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Rafael', 'rwm36481@cuoly.com', 'asdas', '0997 555 5555', 'dasd', '2020-11-13 07:47:02', '2020-11-13 07:47:02', NULL),
(5, 'Rafael', 'rwm36481@cuoly.com', 'asd', '0997 555 5555', 'asd', '2020-11-13 07:55:04', '2020-11-13 07:55:04', NULL),
(4, 'Rafael', 'rwm36481@cuoly.com', 'asdasd', '0997 555 5555', 'ads', '2020-11-13 07:52:01', '2020-11-13 07:52:01', NULL),
(6, 'Rafael', 'rwm36481@cuoly.com', NULL, '0997 555 5555', 'asdasd', '2020-11-13 08:05:41', '2020-11-13 08:05:41', NULL),
(7, 'Rafael', 'rwm36481@cuoly.com', NULL, '0997 555 5555', 'ddd', '2020-11-13 08:07:42', '2020-11-13 08:07:42', NULL),
(8, 'Rafael', 'rwm36481@cuoly.com', 'Sample Property 656', '0997 555 5555', 'dsads', '2020-11-13 08:08:11', '2020-11-13 08:08:11', NULL),
(9, 'Rafael', 'rwm36481@cuoly.com', NULL, '0997 555 5555', 'ddd', '2020-11-13 08:17:47', '2020-11-13 08:17:47', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
