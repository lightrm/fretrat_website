-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 05, 2020 at 04:55 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fretrato_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `name`) VALUES
(1, 'Special Property'),
(2, 'Penthouse'),
(3, 'Loft'),
(4, 'Attic Apartment'),
(5, 'Duplex Apartment'),
(6, 'Apartments'),
(7, 'Condominium'),
(8, 'Room'),
(9, 'Retail'),
(10, 'Offices'),
(11, 'Other'),
(12, 'Building'),
(13, 'Warehouse'),
(14, 'Serviced Office'),
(15, 'Studio'),
(16, '1 Bedroom'),
(17, '2 Bedroom'),
(18, '3 Bedroom'),
(19, '4 Bedroom'),
(20, '5+ Bedroom'),
(21, 'Condotel'),
(22, 'Townhouse'),
(23, 'House and Lot'),
(24, 'Beach House'),
(25, 'Beach Lot'),
(26, 'Commercial Lot'),
(27, 'Memorial'),
(28, 'Residential Lot'),
(29, 'Agricurtural Lot');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
