-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 05, 2020 at 04:55 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fretrato_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `main_sub_categories`
--

DROP TABLE IF EXISTS `main_sub_categories`;
CREATE TABLE IF NOT EXISTS `main_sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_categ_id` int(11) NOT NULL,
  `sub_categ_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_sub_categories`
--

INSERT INTO `main_sub_categories` (`id`, `main_categ_id`, `sub_categ_id`) VALUES
(1, 5, 1),
(2, 5, 2),
(3, 5, 3),
(4, 5, 4),
(5, 5, 5),
(6, 5, 6),
(7, 5, 7),
(8, 5, 8),
(9, 5, 11),
(10, 4, 9),
(11, 4, 10),
(12, 4, 12),
(13, 4, 13),
(14, 4, 14),
(15, 4, 11),
(16, 1, 2),
(17, 1, 3),
(18, 1, 16),
(19, 1, 17),
(20, 1, 18),
(21, 1, 19),
(22, 1, 20),
(23, 1, 21),
(24, 1, 11),
(25, 2, 22),
(26, 2, 23),
(27, 2, 24),
(28, 2, 11),
(29, 6, 25),
(30, 6, 26),
(31, 6, 27),
(32, 6, 28),
(33, 6, 29),
(34, 6, 11),
(35, 3, 10),
(36, 3, 12),
(37, 3, 14),
(38, 3, 15),
(39, 3, 11);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
