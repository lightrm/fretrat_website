<?php namespace App\Models;

use \App\Models\BaseModel;

class Blogsmod extends BaseModel
{
    protected $db;
    protected $table      = 'blogs';
    protected $primaryKey = 'id';    

    protected $returnType     = 'array';
    protected $tempReturnType     = 'array';

    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';


    public function get_blogs($limit = 0, $offset = 0, $returnType = "multiple", $id = null)
    {

        // $this->db;
        $this->select("blogs.id, blogs.title, blogs.created_at, blogs.body, blogs.img_session, blogs.video_url,
            (SELECT directory FROM photos WHERE session = blogs.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = blogs.img_session AND directory LIKE '%Thumbnail%' ORDER BY created_at DESC LIMIT 1) AS img_filename,
            (SELECT name FROM users WHERE id = blogs.user_id LIMIT 1) AS author_name,
            (SELECT img_session FROM users WHERE id = blogs.user_id LIMIT 1) AS agent_img_session,
            (SELECT directory FROM photos WHERE session = agent_img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS avatar_directory,
            (SELECT filename FROM photos WHERE session = agent_img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS avatar_filename
        ");
        $this->where('blogs.published', 1);
        $this->orderBy("blogs.created_at", "DESC");

        if ($txt_search = \App\Libraries\Universallib::getGet("search")) {
            $this->like('lower(blogs.title)', "%" . strtolower($txt_search) . "%");
        }

        if ($txt_type = \App\Libraries\Universallib::getGet("type")) {

            switch ($txt_type) {
                case '1':
                    $txt_type = "plan_your_best_life_now";
                    break;
                
                default:
                    $txt_type = "real_estate_and_travel";
                    break;
            }

            $this->where('blogs.type', strtolower($txt_type));

            if ($txt_sub_type = \App\Libraries\Universallib::getGet("sub_type")) {

                $this->join('blog_category_concat ', 'blogs.id = blog_category_concat.blog_id', 'left');
                $this->where('blog_category_concat.category_id', $txt_sub_type);

            }


        }

        if ($returnType == "single") {
            $this->where('blogs.id', $id);
            return $this->get()->getRow(1, 'array');
        }
        elseif ($returnType == "count") {
            return $this->countAllResults(false);
        }
        else{

            $this->limit($limit, ($offset > 0 ? (($offset - 1) * 10) : 0));

            return $this->get()->getResult('array');
        }

    }

    public function search_pagination()
    {

        if ($txt_search = \App\Libraries\Universallib::getGet("search")) {

            return $this->table('blogs')->like('lower(title)', "%" . strtolower($txt_search) . "%");
        }
        elseif ($txt_type = \App\Libraries\Universallib::getGet("type")) {

            switch ($txt_type) {
                case '1':
                    $txt_type = "plan_your_best_life_now";
                    break;
                
                default:
                    $txt_type = "real_estate_and_travel";
                    break;
            }

            if ($txt_sub_type = \App\Libraries\Universallib::getGet("sub_type")) {

                return $this->table('blogs bl')->join('blog_category_concat blc', 'blogs.id = blc.blog_id', 'left')->where('type', strtolower($txt_type))->where('blc.category_id', $txt_sub_type);

            }
            else{

                return $this->table('blogs')->where('type', strtolower($txt_type));

            }

        }
        else{

            return $this->table('blogs');

        }

    }

    public function get_blogs_categories()
    {

        // $this->db; (SELECT COUNT(id) FROM blogs WHERE published = 1 AND type = 'real_estate_and_travel') AS travel_ctr
        $this->select("type,
            (SELECT COUNT(id) FROM blogs WHERE published = 1 AND type = 'plan_your_best_life_now') AS real_estate_ctr
        ");
        $this->where('published', 1);
        $this->limit(1);

        return $this->get()->getRow(1, 'array');

    }

    public function get_blogs_sub_categories($type)
    {

        // $this->db;
        $builder = $this->db->table('blog_categories');
        $builder->select("id, category_name,
            (SELECT COUNT(id) FROM blog_category_concat WHERE category_id = blog_categories.id) AS sub_ctr
        ");
        $builder->where('type', $type);
        
        return $builder->get()->getResult('array');

    }

    public function get_blogs_categ($id)
    {

        // $this->db;
        $builder = $this->db->table('blog_category_concat');
        $builder->select("
            (SELECT category_name FROM blog_categories WHERE id = blog_category_concat.category_id LIMIT 1) AS category_name
        ");
        $builder->where("blog_id", $id);
        $builder->orderBy("id", "DESC");

        return $builder->get()->getResult('array');
        

    }

}