<?php namespace App\Models;

class Usermod extends BaseModel
{
	// protected $DBGroup = 'default';
	protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $tempReturnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'username', 'email', 'password', 'contact_no', 'about', 'address', 'role', 'gender', 'toogleInfo', 'active', 'img_session', 'facebook_link', 'twitter_link', 'linkedin_link', 'telegram_link', 'viber_link', 'whatsup_link', 'instagram_link', 'confirmation_code'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function get_agents($limit = 0, $offset = 0, $returnType = "multiple", $id = null)
    {

        // $this->db;
        $this->select("*,
            (SELECT directory FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_filename
        ");
        $this->where('deleted_at', null);
        //$this->where('role !=', 1); // expect admin accounts
        $this->where("active", 1); // only active account
        $this->orderBy("role", "ASC");
        $this->orderBy("order_ranking", "ASC");
        $this->orderBy("name", "ASC");

        if ($txt_search = \App\Libraries\Universallib::getGet("search")) {
            $this->like('lower(name)', "%" . strtolower($txt_search) . "%");
        }

        if ($returnType == "single") {
            $this->where('id', $id);
            return $this->get()->getRow(1, 'array');
        }
        elseif ($returnType == "count") {
            $this->where('role', 2);
            return $this->countAllResults(false);
        }
        else{

            $this->where('role', 2);
            $this->limit($limit, ($offset > 0 ? (($offset - 1) * 10) : 0));

            return $this->get()->getResult('array');
        }

    }

    public function search_pagination()
    {

        if ($txt_search = \App\Libraries\Universallib::getGet("search")) {
            //->where('role !=', 1)
            return $this->table('users')->where('role', 2)->where('deleted_at', null)->where("active", 1)->where('role !=', 1)->like('lower(name)', "%" . strtolower($txt_search) . "%");
        }
        else{
            //->where('role !=', 1)
            return $this->table('users')->where('role', 2)->where("active", 1)->where('role !=', 1)->where('deleted_at', null);

        }

    }


    public function get_user_by_type($type)
    {

        // $this->db;
        $this->select("*,
            (SELECT directory FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_filename
        ");
        $this->where('deleted_at', null);
        $this->where("active", 1); // only active account
        $this->where('role', $type);
        $this->orderBy("order_ranking", "ASC");

        return $this->get()->getResult('array');
        

    }


    public function verify_login($id) {

        $this->select("*, 
            (SELECT directory FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_filename,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $this->where("id", $id);
        $this->where("active", 1); // only active account
        $this->where('deleted_at', null);
        
        return $this->get()->getRow(1, 'array');

    }

    public function request_login($username, $password) {
        $result = array("error" => true, "msg" => "Something went wrong!");

        $this->select("*, 
            (SELECT directory FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_filename,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $this->where('(lower(email) = "'.mb_strtolower($username).'" OR lower(username) = "'.mb_strtolower($username).'")');
        $this->where('deleted_at', null);
        //$this->where('role <= ', 2);
        $result['user'] = $this->get()->getRow(1, 'array');


        if( !empty($result['user']) ) {

            if ($result['user']['active'] == 1) {

                if( password_verify($password, $result['user']['password']) ) {
                    $result['error'] = false;
                } else {
                    $result['msg'] = "Password does not match in our system.";
                }

            }
            else{
                $result['msg'] = "Account is not yet verified.";
            }

        } else {
            $result['msg'] = "Username / Email does not exist.";
        }

        return $result;
    }

    public function refresh_userdata($id) {
        $result = array("error" => true, "msg" => "Something went wrong!");

        $this->select("*, 
            (SELECT directory FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS img_filename,
            (SELECT name FROM roles WHERE roles.id = role) as role_name"
        );
        $this->where('id', $id);
        $this->where('deleted_at', null);
        //$this->where('role <= ', 2);
        $result['user'] = $this->get()->getRow(1, 'array');
        return $result;

    }

    public function get_admin_accounts() {

        $this->select("id, email");
        $this->where('role', 1);
        $this->where('deleted_at', null);

        return $this->get()->getResult('array');

    }

    public function get_user_by_email($email) {

        $this->select("id, name, username, email");
        $this->where("email", $email);
        $this->where('deleted_at', null);
        
        return $this->get()->getRow(1, 'array');

    }

    public function check_credentials_exist($column, $value, $column_2 = null, $value_2 = null) {

        $this->where('id !=', session()->get("User")['id']);
        $this->where($column, $value);
        if ($column_2 != null) {
            $this->where($column_2, $value_2);
        }
        //$this->where('deleted_at', null);
        
        return $this->countAllResults();

    }

}