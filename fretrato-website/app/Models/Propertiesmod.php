<?php namespace App\Models;

use \App\Models\BaseModel;

class Propertiesmod extends BaseModel
{
    protected $db;
    protected $table      = 'properties';
    protected $primaryKey = 'id';    

    protected $returnType     = 'array';
    protected $tempReturnType     = 'array';

    protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'name', 'about', 'description', 'street', 'city', 'barangay', 'province', 'country', 'location', 'bedroom', 'bathroom', 'floor_area', 'floor_level', 'furnish_type', 'offer_type', 'property_type', 'parking', 'subcategory', 'price', 'img_session', 'toogleFeature', 'property_vid_url', 'published', 'slug', 'created_at', 'stayMonth', 'stayYear', 'land_area', 'condition', 'dateFinished', 'updated_at', 'deleted_at'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';


    public function get_properties($limit = 0, $offset = 0, $pagination = false, $featured = "none", $user_id = null)
    {
        // $this->db;
        $this->select("id, name, about, description, street, city, barangay, province, country, location, bedroom, bathroom, floor_area, floor_level, furnish_type, offer_type, property_type, parking, subcategory, price, img_session, is_owner, is_direct, created_at, toogleFeature, property_vid_url, slug, contract_start, contract_end, taken, stayMonth, stayYear, land_area, condition, dateFinished,
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_filename,

            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Slider%' ORDER BY created_at DESC LIMIT 1) as slider_filename, 
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Slider%' ORDER BY created_at DESC LIMIT 1) as slider_directory,

            (SELECT name FROM users WHERE id = properties.user_id LIMIT 1) AS agent_name,
            (SELECT role FROM users WHERE id = properties.user_id LIMIT 1) AS agent_role,
            (SELECT toogleInfo FROM users WHERE id = properties.user_id LIMIT 1) AS agent_info,
            (SELECT about FROM users WHERE id = properties.user_id LIMIT 1) AS agent_position,
            (SELECT contact_no FROM users WHERE id = properties.user_id LIMIT 1) AS agent_contact
        ");
        $this->where('properties.published', 1);
        $this->where('properties.deleted_at', NULL);

        if ($featured == "true_toogle") {
            $this->where('properties.toogleFeature', 1);
            $this->orderBy('rand()');
            //$this->where('(properties.property_type = "1" OR properties.property_type = "2" OR properties.property_type = "3" OR properties.property_type = "4")');
            $this->limit(8);
        }

        if ($featured == "false_toogle") {
            $this->where('properties.toogleFeature', 0);
        }

        if ($featured == "order_by") {
            $this->orderBy('properties.toogleFeature', "DESC");
        }

        if ($pagination == false && $limit > 0) {
            $this->orderBy('properties.id', "DESC");
            $this->limit($limit);
        }

        if ($pagination == true) {

            $this->search_options("result");

            if (\App\Libraries\Universallib::getGet("order_by") != "latest" && \App\Libraries\Universallib::getGet("order_by") != null) {            
                $this->orderBy("properties.price", \App\Libraries\Universallib::getGet("order_by"));
            }
            else{
                $this->orderBy("properties.created_at", "DESC");
            }

            $this->limit($limit, ($offset > 0 ? (($offset - 1) * 10) : 0));
        }

        if ($user_id != null) {
            $this->where('user_id', $user_id);
        }

        if ($featured == "count") {

            $this->search_options("result");

            return $this->countAllResults(false);
        }
        else{
            return $this->get()->getResult('array');
        }

    }

    public function search_options($type)
    {


        if (\App\Libraries\Universallib::getGet("search") != null) {
            $data['txt_search'] = \App\Libraries\Universallib::getGet("search");
            $column[] = 'name';
        }
        if (\App\Libraries\Universallib::getGet("status") != null) {
            $data['txt_propertyStatus'] = \App\Libraries\Universallib::getGet("status");
            $column[] = 'offer_type';
        }
        if (\App\Libraries\Universallib::getGet("type") != null) {
            $data['txt_propertyType'] = \App\Libraries\Universallib::getGet("type");
            $column[] = 'property_type';
        }
        if (\App\Libraries\Universallib::getGet("location") != null) {
            $data['txt_propertyLocation'] = \App\Libraries\Universallib::getGet("location");
            $column[] = 'location';
        }
        if (\App\Libraries\Universallib::getGet("subCategory") != null) {
            $data['txt_subCategory'] = \App\Libraries\Universallib::getGet("subCategory");
            $column[] = 'subcategory';
        }
        if (\App\Libraries\Universallib::getGet("txt_bedrooms") != null) {
            $data['txt_bedrooms'] = \App\Libraries\Universallib::getGet("txt_bedrooms");
            $column[] = 'bedroom';
        }
        if (\App\Libraries\Universallib::getGet("txt_bathrooms") != null) {
            $data['txt_bathrooms'] = \App\Libraries\Universallib::getGet("txt_bathrooms");
            $column[] = 'bathroom';
        }

        if ($type == "result") {

            if (isset($data)) {
                if (count($data) > 0) {

                    $x = 0;

                    foreach ($data as $row) {

                        //if ($column[$x] != "name") { 
                            $this->like('lower(' . $column[$x] . ')', strtolower($row));
                        /*}
                        else{
                            $this->orLike($column[$x], $row);
                        }*/
                        
                        $x++;
                    }

                }
            }

        }
        else{

            if (isset($data)) {

                if (count($data) > 0) {

                    $builder = $this->table('properties');
                    $builder->where('properties.published', 1);
                    $builder->where('properties.deleted_at', NULL);

                    $x = 0;

                    foreach ($data as $row) {

                        //if ($column[$x] != "name") { 
                            $builder->like($column[$x], $row);
                        /*}
                        else{
                            $builder->orLike($column[$x], $row);
                        }*/

                        $x++;
                    }
                    return $builder;

                }

            }
            else{

                return $this->table('properties')->where('properties.published', 1)->where('properties.deleted_at', NULL);

            }

        }


    }

    public function search_pagination()
    {

        return $this->search_options("pagination");

    }

    public function get_property($slug)
    {

        // $this->db;
        $this->select("user_id, id, name, about, description, street, city, barangay, province, country, location, bedroom, bathroom, floor_area, floor_level, furnish_type, offer_type, property_type, parking, subcategory, price, img_session, is_owner, is_direct, created_at, toogleFeature, meta_keywords, slug, contract_start, contract_end, taken, stayMonth, stayYear, land_area, condition, dateFinished,
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_directory, property_vid_url,
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_filename,
            (SELECT name FROM users WHERE id = properties.user_id LIMIT 1) AS agent_name,
            (SELECT role FROM users WHERE id = properties.user_id LIMIT 1) AS agent_role,
            (SELECT toogleInfo FROM users WHERE id = properties.user_id LIMIT 1) AS agent_info,
            (SELECT img_session FROM users WHERE id = properties.user_id LIMIT 1) AS agent_img_session,
            (SELECT contact_no FROM users WHERE id = properties.user_id LIMIT 1) AS agent_contact,
            (SELECT contact_no FROM users WHERE id = properties.user_id LIMIT 1) AS agent_contact_num,
            (SELECT about FROM users WHERE id = properties.user_id LIMIT 1) AS agent_position,

            (SELECT facebook_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_facebook_link,
            (SELECT twitter_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_twitter_link,
            (SELECT instagram_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_instagram_link,
            (SELECT linkedin_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_linkedin_link,

            (SELECT telegram_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_telegram_link,
            (SELECT viber_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_viber_link,
            (SELECT whatsup_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_whatsapp_link,
            (SELECT google_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_google_link,
            (SELECT we_chat_link FROM users WHERE id = properties.user_id LIMIT 1) AS agent_we_chat_link,

            (SELECT directory FROM photos WHERE session = agent_img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS avatar_directory,
            (SELECT filename FROM photos WHERE session = agent_img_session AND directory LIKE '%Avatar%' ORDER BY created_at DESC LIMIT 1) AS avatar_filename,
            (SELECT email FROM users WHERE id = properties.user_id LIMIT 1) AS agent_email
        ");
        $this->where('properties.published', 1);
        $this->where('properties.deleted_at', NULL);
        $this->where('slug', $slug);
        return $this->get()->getRow(1, 'array');

    }

    public function get_related_property($id, $offer_type, $property_type, $sub_categ)
    {

        // $this->db;
        $this->select("id, name, about, description, street, city, barangay, province, country, location, bedroom, bathroom, floor_area, floor_level, offer_type, property_type, parking, subcategory, price, img_session, is_owner, is_direct, created_at, toogleFeature, furnish_type, property_vid_url, slug, contract_start, contract_end, taken, stayMonth, stayYear, land_area, condition, dateFinished,
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_filename,
            (SELECT name FROM users WHERE id = properties.user_id LIMIT 1) AS agent_name,
            (SELECT role FROM users WHERE id = properties.user_id LIMIT 1) AS agent_role,
            (SELECT about FROM users WHERE id = properties.user_id LIMIT 1) AS agent_position,
            (SELECT toogleInfo FROM users WHERE id = properties.user_id LIMIT 1) AS agent_info,
            (SELECT contact_no FROM users WHERE id = properties.user_id LIMIT 1) AS agent_contact
        ");
        $this->where('properties.published', 1);
        $this->where('properties.deleted_at', NULL);
        $this->where('id !=', $id);
        //$this->where('property_type', $property_type);
        //$this->where('subcategory', $sub_categ);
        $this->like('offer_type', "%" . $offer_type . "%");
        $this->orderBy("properties.created_at", "DESC");
        $this->limit(4);
        return $this->get()->getResult('array');

    }

    public function get_property_by_user_id($user_id = null)
    {

        // $this->db;
        $this->select("id, name, about, description, street, city, barangay, province, country, location, bedroom, bathroom, floor_area, floor_level, furnish_type, offer_type, property_type, parking, subcategory, price, img_session, is_owner, is_direct, created_at, toogleFeature, meta_keywords, slug, contract_start, contract_end, taken, stayMonth, stayYear, land_area, condition, dateFinished,
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_directory, property_vid_url,
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_filename,
            (SELECT name FROM users WHERE id = properties.user_id LIMIT 1) AS agent_name,
            (SELECT role FROM users WHERE id = properties.user_id LIMIT 1) AS agent_role,
            (SELECT about FROM users WHERE id = properties.user_id LIMIT 1) AS agent_position,
            (SELECT toogleInfo FROM users WHERE id = properties.user_id LIMIT 1) AS agent_info,
            (SELECT contact_no FROM users WHERE id = properties.user_id LIMIT 1) AS agent_contact,
            (SELECT contact_no FROM users WHERE id = properties.user_id LIMIT 1) AS agent_contact_num,
            (SELECT email FROM users WHERE id = properties.user_id LIMIT 1) AS agent_email
        ");
        $this->where('properties.published', 1);
        $this->where('properties.deleted_at', NULL);
        if ($user_id != null) {
            $this->where('user_id', $user_id);
        }
        $this->orderBy('rand()');
        $this->limit(6);
        return $this->get()->getResult('array');

    }

    public function get_location_num($city)
    {

        // $this->db;
        $this->like('city', "%" . $city . "%");
        return $this->countAllResults();

    }

    public function get_property_gallery($img_session, $type)
    {
        
        // $this->db;   
        $builder = $this->db->table('photos');
        $builder->select("*");
        $builder->like('directory', "%" . $type . "%");
        $builder->where('session', $img_session);
        $builder->orderBy("sequence", "ASC");

        if ($type == "Thumbnail") {
            return $builder->get()->getRow(0, 'array');
        }

        return $builder->get()->getResult('array');

    }

    public function get_property_amenities($id)
    {
        
        // $this->db;
        $builder = $this->db->table('amenity_property');
        $builder->select("
            (SELECT amenity_name FROM amenity_properties WHERE id = amenity_property.amenity_property_id LIMIT 1) AS amenity_name
        ");
        $builder->where('property_id', $id);
        $builder->orderBy("id", "DESC");

        return $builder->get()->getResult('array');


    }

    public function get_main_categ()
    {

        // $this->db;
        $builder = $this->db->table('main_category');
        $builder->select('*');
        $builder->orderBy("name", "ASC");

        return $builder->get()->getResult('array');
        
    }

    public function get_main_categ_ctr()
    {

        // $this->db;
        $builder = $this->db->table('main_category');
        $builder->select('id, name,
            (SELECT count(id) FROM properties WHERE property_type = main_category.id LIMIT 1) AS property_ctr
        ');
        $builder->orderBy("name", "ASC");

        return $builder->get()->getResult('array');

    }

    public function get_sub_categ($id, $option = null)
    {

        // $this->db;
        $builder = $this->db->table('main_sub_categories');
        $builder->select('*,
            (SELECT name FROM sub_category WHERE id = sub_categ_id LIMIT 1) AS sub_categ_name
        ');
        if ($option == "submit") {
            $builder->where("sub_categ_id !=", 35);
        }
        $builder->where("main_categ_id", $id);
        $builder->orderBy("sub_categ_name", "ASC");

        return $builder->get()->getResult('array');
        

    }

}