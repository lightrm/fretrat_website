<?php namespace App\Models;

class Functionsmod extends BaseModel
{

    protected $table      = 'inquiry';
    protected $primaryKey = 'id';    

    protected $returnType     = 'array';

    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'email', 'property_id', 'about', 'contact_no', 'message', 'created_at', 'updated_at', 'deleted_at'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    public function create_notification($data)
    {

        $data['unread'] = 1;
        $data['created_at'] = date('Y-m-d h:i:s');
        $data['updated_at'] = date('Y-m-d h:i:s');

        $builder = $this->db->table('notification');
        $builder->insert($data);

    }

    public function create_property_approval($data)
    {
        
        $data['date_requested'] = date('Y-m-d h:i:s');

        $builder = $this->db->table('property_approval');
        $builder->insert($data);

    }

}