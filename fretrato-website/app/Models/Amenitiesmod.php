<?php namespace App\Models;

use \App\Models\BaseModel;

class Amenitiesmod extends BaseModel
{

    protected $db;
    protected $table      = 'amenity_property';
    protected $primaryKey = 'id';    

    protected $returnType     = 'array';
    protected $tempReturnType     = 'array';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['property_id', 'amenity_property_id', 'created_at', 'updated_at'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

}