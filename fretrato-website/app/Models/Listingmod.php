<?php namespace App\Models;

use \App\Models\BaseModel;

class Listingmod extends BaseModel
{
    protected $db;
    protected $table      = 'properties';
    protected $primaryKey = 'id';    

    protected $returnType     = 'array';
    protected $tempReturnType     = 'array';

    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    public function get_my_listing($limit = 0, $offset = 0, $returnType = null, $id = null)
    {
        // $this->db;
        $this->select("id, name, about, description, street, city, barangay, province, country, location, bedroom, bathroom, floor_area, floor_level, furnish_type, offer_type, property_type, parking, subcategory, price, img_session, is_owner, is_direct, created_at, toogleFeature, property_vid_url, slug,
            (SELECT directory FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_directory,
            (SELECT filename FROM photos WHERE session = properties.img_session AND directory LIKE '%Thumbnail%' ORDER BY id DESC LIMIT 1) AS img_filename,
            (SELECT name FROM users WHERE id = properties.user_id LIMIT 1) AS agent_name,
            (SELECT role FROM users WHERE id = properties.user_id LIMIT 1) AS agent_role
        ");
        $this->where('properties.user_id', session()->get("User")['id']);
        $this->where('properties.published', 1);
        $this->where('properties.deleted_at', NULL);
        $this->orderBy('properties.id', "DESC");

        if ($txt_search = \App\Libraries\Universallib::getGet("search")) {
            $this->like('name', "%" . $txt_search . "%");
        }

        if ($returnType == "single") {
            $this->where('id', $id);
            return $this->get()->getRow(1, 'array');
        }
        elseif ($returnType == "count") {
            return $this->countAllResults(false);
        }
        else{

            $this->limit($limit,  ($offset > 0 ? (($offset - 1) * 10) : 0));

            return $this->get()->getResult('array');
        }

    }

    public function search_pagination()
    {

        if ($txt_search = \App\Libraries\Universallib::getGet("search")) {

            return $this->table('properties')->where('properties.deleted_at', NULL)->where('properties.user_id', session()->get("User")['id'])->where('properties.published', 1)->like('name', "%" . $txt_search . "%");
        }
        else{

            return $this->table('properties');

        }

    }

    public function get_property_amenities()
    {

        $builder = $this->db->table('amenity_properties');
        $builder->select('id, amenity_name');
        $builder->orderBy('amenity_name', 'ASC');
        return $builder->get()->getResult('array');

    }

    public function get_amenities($property_id)
    {

        $builder = $this->db->table('amenity_property');
        $builder->select('amenity_property_id');
        $builder->where('property_id', $property_id);
        
        return $builder->get()->getResult('array');

    }


}