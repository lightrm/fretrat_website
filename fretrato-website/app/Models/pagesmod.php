<?php namespace App\Models;

use \App\Models\BaseModel;

class Pagesmod extends BaseModel
{
    protected $db;
    protected $table      = 'website_detail';
    protected $primaryKey = 'id';    

    protected $returnType     = 'array';
    protected $tempReturnType     = 'array';

    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    public function get_cities()
    {

        // $this->db;
        $builder = $this->db->table('cities');
        $builder->select("name");
        $builder->orderBy("name", "ASC");

        return $builder->get()->getResult('array');

    }

    public function web_config($source = null, $returnType = "multiple", $type = null)
    {

        // $this->db;
        $this->where('toogle', 1);
        $this->orderBy("created_at", "DESC");

        if ($source != null) {
            $this->where('source', $source);
        }

        if ($type != null) {
            $this->where('type', $type);
        }

        if ($returnType == "single") {
            return $this->get()->getRow(1, 'array');
        }
        else{
            return $this->get()->getResult('array');
        }

    }

}