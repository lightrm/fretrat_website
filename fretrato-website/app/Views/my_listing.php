
<?= $this->include("Includes/sub_banner") ?>


<!-- User page start -->
<div class="user-page content-area-2">
    <div class="container">
    <?= $this->include("Includes/alert") ?>
        <div class="row">

            <?= $this->include("Includes/profile_menu") ?>

            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="my-properties mb-5">

                    <div class="option-bar">
                        <div class="row clearfix">
                            <div class="col-xl-4 col-lg-5 col-md-5 col-sm-5 col-3">
                                <h4>
                                    <a href="<?= base_url('pages/submit_property') ?>" class="btn btn-xm ml-3 btn-color"><i class="flaticon-add"></i> Submit New Property</a>
                                </h4>
                            </div>
                            <div class="col-xl-8 col-lg-7 col-md-7 col-sm-7 col-9 mt-1">
                                <div class="col-lg-7 col-md-9 float-right">
                                    <form class="form-search" method="GET" action="<?= base_url("pages/my_listing"); ?>">
                                        <input type="text" class="form-control" placeholder="Search" name="search" value="<?= (isset($_GET['search']) ? $_GET['search'] : "") ?>">
                                        <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= $this->include("Includes/results_ctr");  ?>
                    <table class="manage-table">
                        <thead>
                        <tr>
                            <th>My Listing</th>
                            <th></th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody class="responsive-table">
                            <?php 
                            if (count($propertyDatabd) > 0) {
                                foreach ($propertyDatabd as $i => $row) { ?>
                                    <tr>
                                        <td class="listing-photoo">
                                            <div class="">
                                                <div class="property-thumbnail">
                                                    <?php $offer_type = ($row['offer_type'] == "rental" || $row['offer_type'] == "Rental") ? "For Lease" : (($row['offer_type'] == "resale" || $row['offer_type'] == "Resale") ? "For Sale" : "Pre-selling"); ?>
                                                    <a href="#" class="property-img">
                                                            <img alt="my-properties" src="<?= 
                                                        (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'thumbnail_medium-' . $row['img_filename'] 
                                                        : base_url($pluginDir) . '/img/no_image.png'); ?>" class="img-fluid">
                                                    </a>

                                                    <div class="property-overlay">
                                                        <a href="<?= base_url('pages/edit_listing/' . $row['id']); ?>" title="Edit Record" class="overlay-link">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <div>
                                                            <a title="Delete Record" class="overlay-link property-video" onclick="delete_modal('<?= $row['id'] ?>')">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="title-container">
                                            <h5><a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>"><?= $row['name']; ?></a></h5>
                                            <h6><span>₱<?= number_format($row['price'], 2) ?></span></h6>
                                            <p><i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> <?= $row['location']; ?></p>
                                        </td>
                                        <td class="action">
                                            <ul>
                                                <li>
                                                    <a href="<?= base_url('pages/edit_listing/' . $row['id']); ?>"><i class="fa fa-pencil"></i> Edit</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="delete_modal('<?= $row['id'] ?>')" class="delete property-video"><i class="fa fa-remove"></i> Delete</a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                        <?php }
                            }
                            else{
                            ?>
                                    <tr>
                                        <td colspan="4" class="listing-photoo">
                                            <div class="alert alert-warning">
                                                <div>
                                                    <strong>No Results Found! </strong> You can try to Submit your property <a href="<?= base_url("pages/submit_property"); ?>"><strong>Here!</strong></a>.
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                            <?php
                            }
                         ?>
                        </tbody>
                    </table>
                </div>
                <?= $this->include("Includes/pagination") ?>
            </div>
        </div>
    </div>
</div>
<!-- User page end -->  

<div class="modal property-modal fade" id="propertyModal" tabindex="-1" role="dialog" aria-labelledby="propertyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7" style="margin-left: 180px">
                        <div class="modal-right-content bg-white">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <section>
                                <h3>Delete Record</h3>
                                <form action="<?= base_url("functions/delete_property"); ?>" method="POST">
                                    <input type="hidden" name="txt_property_id" id="txt_property_id">
                                    <div class="form-group">
                                        <div class="alert alert-warning">
                                            <div>
                                                <strong>Woah, hold on there! </strong> Are you sure you want to delete this record? Once the record is deleted it can't be undone.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0" style="text-align: right;">
                                        <button type="button" style="cursor: pointer;" class="btn btn-xm mt-5 mr-3 btn-" data-dismiss="modal" aria-label="Close">Close</button>
                                        <button type="submit" style="cursor: pointer; background: #DD4F43; color: #ffffff" class="btn btn-xm mt-5 mr-1"> Delete</button>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    function delete_modal(id){

        $("#txt_property_id").val(id);

    }

</script>