
<?= $this->include("Includes/sub_banner") ?>

<!-- Properties details page start -->
<div class="properties-details-page content-area-15">
    <div class="container">
        <?= $this->include("Includes/alert") ?> 
        <div class="row">
            <div class="col-lg-8 col-md-12 col-xs-12 slider">
                <div id="propertiesDetailsSlider" class="carousel properties-details-sliders slide mb-60">
                    <div class="heading-properties">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <h3><?= $property['name'] ?></h3>
                                    <p><i class="fa fa-map-marker"></i> <?= $property['location'] ?></p>
                                    <p style="color: #2B7DA2">
                                        <?= $property['is_direct']?'Direct to Owner':'With broker' ?>
                                    </p>
                                </div>
                                <div class="p-r">
                                    <h3>₱<?= number_format($property['price'], 2) ?></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- main slider carousel items -->
                    <div class="carousel-inner text-center">

                    <?php 
                        $x = 0;
                        if ($property_thumbnail != null && $property_thumbnail != "") {
                    ?>

                            <div class="<?= ($property_thumbnail != null && $property_thumbnail != "" ? "active" : "") ?> item carousel-item" data-slide-number="<?= $x ?>">
                                
                                <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$property_thumbnail['directory'] . "/" . 'thumbnail_medium-' . $property_thumbnail['filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $property_thumbnail['directory'] . 'original-' . $property_thumbnail['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" class="img-fluid property_view_img" alt="property-<?= $x ?>">
                                <div class="contact-property-view" <?= ($property['taken'] != 0 ? " " : "hidden") ?>>
                                        OCCUPIED    <br>
                                        START DATE: <?= date("F d, Y", strtotime($property['contract_start'])) ?><br>
                                        END DATE: <?= date("F d, Y", strtotime($property['contract_end'])) ?>
                                </div>
                                
                            </div>

                    <?php 
                        $x++;
                        
                            foreach ($property_gallery as $i => $row) { ?>

                                <div class="<?= ($x == 0 ? "active" : "") ?> item carousel-item" data-slide-number="<?= $x ?>">
                                    <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['directory'] . "/" . 'thumbnail_medium-' . $row['filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['directory'] . 'original-' . $row['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" class="img-fluid property_view_img" style="object-fit: cover;" alt="property-<?= $x ?>">
                                    <div class="contact-property-view" <?= ($property['taken'] != 0 ? " " : "hidden") ?>>
                                            OCCUPIED    <br>
                                            START DATE: <?= date("F d, Y", strtotime($property['contract_start'])) ?><br>
                                            END DATE: <?= date("F d, Y", strtotime($property['contract_end'])) ?>
                                    </div>
                                </div>

                    <?php   
                                $x++;
                            }
                        }
                        else{
                        ?>

                            <div class="active item carousel-item" data-slide-number="0">
                                <img src="<?= base_url($pluginDir) . '/img/no_image.png'; ?>" class="img-fluid" alt="property-0" style="min-height: 500px; max-height: 500px">
                            </div>

                        <?php
                        }

                    ?>

                        <a <?= (count($property_gallery) > 0) ? "" : "hidden" ?> class="carousel-control left" href="#propertiesDetailsSlider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                        <a <?= (count($property_gallery) > 0) ? "" : "hidden" ?> class="carousel-control right" href="#propertiesDetailsSlider" data-slide="next"><i class="fa fa-angle-right"></i></a>

                    </div>
                    <!-- main slider carousel nav controls -->
                    <ul class="carousel-indicators smail-properties list-inline nav nav-justified">

                    <?php 
                        $x = 0;
                        if ($property_thumbnail != null && $property_thumbnail != "") {
                    ?>

                            <li class="list-inline-item <?= ($x == 0 ? "active" : "") ?>">
                                <a id="carousel-selector-<?= $x ?>" class="selected" data-slide-to="<?= $x ?>" data-target="#propertiesDetailsSlider">
                                    <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$property_thumbnail['directory'] . "/" . 'thumbnail_medium-' . $property_thumbnail['filename'] ) ? base_url($pluginDir) . '/fretrato/Images/' . $property_thumbnail['directory'] . 'thumbnail_small-' . $property_thumbnail['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-<?= $x ?>" class="img-fluid property_gallery_img" style="max-width: 146px; object-fit: cover;">
                            </li>

                    <?php 
                        $x++;

                            foreach ($property_gallery as $i => $row) { ?>

                            <li class="list-inline-item">
                                <a id="carousel-selector-<?= $x ?>" class="selected" data-slide-to="<?= $x ?>" data-target="#propertiesDetailsSlider">
                                    <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['directory'] . "/" . 'thumbnail_medium-' . $row['filename'] ) ? base_url($pluginDir) . '/fretrato/Images/' . $row['directory'] . 'thumbnail_small-' . $row['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-<?= $x ?>" class="img-fluid property_gallery_img" style="max-width: 146px; object-fit: cover;">
                                </a>
                            </li>

                    <?php   
                                $x++;
                            }

                        }
                        else{
                        ?>

                            <li class="list-inline-item active">
                                <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#propertiesDetailsSlider">
                                    <img src="<?= base_url($pluginDir) . '/img/no_image.png'; ?>" class="img-fluid" alt="property-0" class="img-fluid" alt="property-0" style="min-height: 80px; max-height: 80px; max-width: 146px">
                                </a>
                            </li>

                        <?php
                        }

                    ?>


                    </ul>
                </div>
                <!-- Tabbing box start -->
                <div class="tabbing tabbing-box mb-60">

                    <div class="amenities-box mb-45">
                        <h3 class="heading-3">Technical Details</h3>
                        <div class="row">

                            <?php if ($property['property_type'] != 6 && ( $property['floor_area'] != null || $property['floor_area'] != "" ) ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Floor Area: <?= $property['floor_area'] ?> Sqm</span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                            <?php if ($property['property_type'] == 2 || $property['property_type'] == 6 && ( $property['land_area'] != null || $property['land_area'] != "" ) ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Land Area: <?= $property['land_area'] ?> Sqm</span></li>
                                    </ul>
                                </div>
                            
                            <?php } ?>

                            <?php if ($property['property_type'] != 6  && ( $property['floor_level'] != null || $property['floor_level'] != "" ) ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-hotel-building"></i>Floor Level: <?= number_format($property['floor_level']) ?></span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                            <?php if ( ( $property['property_type'] != 4 && $property['property_type'] != 6 ) && ( $property['furnish_type'] != null || $property['furnish_type'] != "" ) ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-balcony-and-door"></i><?= $property['furnish_type'] ?></span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                            <?php if ($property['property_type'] != 6 && $property['parking'] > 0) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-car-repair"></i>Parking: <?= $property['parking'] ?></span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                            <?php if ( ( $property['property_type'] == 1 || $property['property_type'] == 2 ) && ( $property['bedroom'] != null || $property['bedroom'] != "" ) ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-bed"></i> <?= $property['bedroom'] ?> Bedroom </span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                            <?php if ( ($property['property_type'] == 1 || $property['property_type'] == 2) && ( $property['bathroom'] != null || $property['bathroom'] != "" ) ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-bath"></i> <?= $property['bathroom'] ?> Bathroom</span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                            <?php if ( ( $property['property_type'] == 2 || $property['property_type'] == 6 ) && ( $property['condition'] != null || $property['condition'] != "" ) ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-hotel-building"></i> <?= ($property['property_type'] == 6 ? "Land" : "") ?> Condition: <?= $property['condition'] ?></span></li>
                                    </ul>
                                </div>
                            
                            <?php } ?>

                            <?php if ( $property['property_type'] == 2 && ($property['dateFinished'] < "01-01-2000") ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-calendar"></i> Date Finished: <?= date("F d, Y", strtotime($property['dateFinished'])) ?></span></li>
                                    </ul>
                                </div>
                            
                            <?php } ?>

                            <?php if ($property['property_type'] == 1 && ( $property['stayMonth'] != null || $property['stayMonth'] != "" )  && $property['stayMonth'] != 0 ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-hotel-building"></i> Stay in <?= $property['stayMonth'] ?> Month(s)</span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                            <?php if ($property['property_type'] == 1 && ( $property['stayYear'] != null || $property['stayYear'] != "" ) && $property['stayYear'] != 0 ) { ?>

                                <div class="col-md-6 col-sm-6">
                                    <ul>
                                        <li><span><i class="flaticon-hotel-building"></i> Stay in <?= $property['stayYear'] ?> Year(s)</span></li>
                                    </ul>
                                </div>

                            <?php } ?>

                        </div>
                    </div>

                    <h3 class="heading-3">Property Description</h3>
                    <?= $property['description'] ?>
                    <ul class="nav nav-tabs" id="carTab" role="tablist">
                        <?php /* <li class="nav-item">
                            <a class="nav-link active show" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="one" aria-selected="false">Description</a>
                        </li> */ ?>
                        <li class="nav-item" hidden>
                            <a class="nav-link" id="2-tab" data-toggle="tab" href="#2" role="tab" aria-controls="2" aria-selected="true">About</a>
                        </li>
                        <li class="nav-item" hidden>
                            <a class="nav-link" id="3-tab" data-toggle="tab" href="#3" role="tab" aria-controls="3" aria-selected="true">Location</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active show" id="4-tab" data-toggle="tab" href="#4" role="tab" aria-controls="4" aria-selected="true">Video</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="5-tab" data-toggle="tab" href="#5" role="tab" aria-controls="5" aria-selected="true">Related Properties</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="carTabContent">
                        <div class="tab-pane fade" id="one" role="tabpanel" aria-labelledby="one-tab">
                            <h3 class="heading-3">Property Description</h3>
                           <?= $property['description'] ?>
                        </div>
                        <div class="tab-pane fade " id="2" role="tabpanel" aria-labelledby="2-tab">
                            <h3 class="heading-3">Property About</h3>
                           <?= $property['about'] ?>
                        </div>
                        <div class="tab-pane fade " id="3" role="tabpanel" aria-labelledby="3-tab">
                            <div class="section location">
                                <h3 class="heading-3">Property Location</h3>
                                <div class="map">
                            <?php if ($property['location'] != null) { 

                                    $location = str_replace(" ", "%20", $property['location']);
                                    $location = str_replace(",", "%2C", $location);
                                    $location = str_replace(".", "", $location);

                            ?>

                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d247104.43946162003!2d120.97725093922855!3d14.598684527121936!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c91b33df36f9!1s<?= $location ?>!5e0!3m2!1sen!2sph!4v1607351616082!5m2!1sen!2sph" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                    
                            <?php }
                                  else{
                                ?>
                                    <div class="alert alert-warning">
                                        <div>
                                            <strong>No location found! </strong>
                                        </div>
                                    </div>
                                <?php
                                  }
                            ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade active show" id="4" role="tabpanel" aria-labelledby="4-tab">
                            <div>
                                <h3 class="heading-3">Property Video</h3>
                            <?php if ($property['property_vid_url'] != null) { 
                                    parse_str( parse_url( $property['property_vid_url'], PHP_URL_QUERY ), $my_array_of_vars );
                                ?>
                                        <iframe src="<?= 'https://www.youtube.com/embed/' . $my_array_of_vars['v']; ?>"></iframe>
                                    
                            <?php }
                                  else{
                                ?>
                                    <div class="alert alert-warning">
                                        <div>
                                            <strong>No video found! </strong>
                                        </div>
                                    </div>
                                <?php
                                  }
                            ?>
                            </div>
                        </div>
                        <div class="tab-pane fade " id="5" role="tabpanel" aria-labelledby="5-tab">
                            <div class="related-properties">
                                <h3 class="heading-3">Related Properties</h3>
                                <div class="row">
                                <?php 
                                if (count($related_properties) > 0) {
                                    foreach ($related_properties as $row) { 

                                        if ( (session()->has("User") && (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2)) || $row['agent_role'] == 1 || $row['agent_role'] == 2 || $row['agent_info'] == 1) {
                                            $agent_name = $row['agent_name'] . " / <i class='fa fa-phone'></i> " . $row['agent_contact'];
                                        }
                                        else{
                                            a:
                                            $agent_name = explode(" ", $row['agent_name']); 
                                            $agent_name = $agent_name[0];
                                            
                                        }

                                        $offer_type = (strtolower($row['offer_type']) == "rental") ? "For Lease" : ((strtolower($row['offer_type']) == "resale") ? "For Sale" : "Pre-selling"); ?>
                                        <div class="col-md-6">
                                            <div class="property-box">
                                                <div class="property-thumbnail">
                                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>" class="property-img">
                                                        <?php if ($row['toogleFeature'] == "1") { ?>
                                                            <div class="listing-badges">
                                                                <span class="featured">Featured</span>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="tag-for"><?= $offer_type ?></div>
                                                        <div class="price-ratings-box" style="color: #DEBF49"><sup style="color: #DEBF49">₱</sup><?= number_format($row['price'], 2) ?> </div>
                                                            <img src="<?= 
                                                            (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'thumbnail_medium-' . $row['img_filename'] 
                                                            : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-3" class="img-fluid" style="min-height: 300px; max-height: 300px; object-fit: cover">
                                                        <div class="contact-property-list" <?= ($row['taken'] != 0 ? " " : "hidden") ?>>
                                                            <div class="div-val">
                                                                OCCUPIED    <br>
                                                                START DATE: <?= date("F d, Y", strtotime($row['contract_start'])) ?><br>
                                                                END DATE: <?= date("F d, Y", strtotime($row['contract_end'])) ?>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="property-overlay">
                                                        <a class="overlay-link property-video" title="View Gallery" onclick="property_modal(`<?= $row['slug'] ?>`, `<?= str_replace(" ", "-", $offer_type) ?>`, `<?= $row['name'] ?>`, `<?= $row['location'] ?>`, `<?= $row['property_vid_url'] ?>`, `<?= number_format($row['price'], 2) ?>`, `<?= $row['floor_area'] ?>`, `<?= $row['bedroom'] ?>`, `<?= $row['floor_level'] ?>`, `<?= $row['furnish_type'] ?>`, `<?= $row['parking'] ?>`, `<?= $agent_name ?>`, `<?= ($row['taken'] != 0 ? date('F d, Y', strtotime($row['contract_start'])) : null) ?>`, `<?= ($row['taken'] != 0 ? date('F d, Y', strtotime($row['contract_end'])) : null) ?>`)">
                                                            <i class="fa fa-image"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="detail">
                                                    <h1 class="title limit-text">
                                                        <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>"><?= $row['name'] ?></a>
                                                    </h1>
                                                    <div class="location limit-text">
                                                        <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">
                                                            <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i><?= $row['location'] ?>
                                                        </a>
                                                    </div>
                                                    <ul class="facilities-list clearfix">

                                                        <?php if ($row['property_type'] != 6 && ( $row['floor_area'] != null || $row['floor_area'] != "" ) ) { ?>

                                                            <li>
                                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['floor_area'] ?>
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ($row['property_type'] == 2 || $row['property_type'] == 6 && ( $row['land_area'] != null || $row['land_area'] != "" ) ) { ?>

                                                            <li>
                                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['land_area'] ?>
                                                            </li>
                                                        
                                                        <?php } ?>

                                                        <?php if ($row['property_type'] != 6  && ( $row['floor_level'] != null || $row['floor_level'] != "" ) ) { ?>

                                                            <li>
                                                                <i class="flaticon-hotel-building"></i>Floor Level: <?= number_format($row['floor_level']) ?>
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ( ( $row['property_type'] != 4 && $row['property_type'] != 6 ) && ( $row['furnish_type'] != null || $row['furnish_type'] != "" ) ) { ?>

                                                            <li>
                                                                <i class="flaticon-balcony-and-door"></i><?= $row['furnish_type'] ?>
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ($row['property_type'] != 6 && $row['parking'] > 0) { ?>

                                                            <li>
                                                                <i class="flaticon-car-repair"></i>Parking: <?= $row['parking'] ?>
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ( ( $row['property_type'] == 1 || $row['property_type'] == 2 ) && ( $row['bedroom'] != null || $row['bedroom'] != "" ) ) { ?>

                                                            <li>
                                                                <i class="flaticon-bed"></i> <?= $row['bedroom'] ?> Bedroom
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ( ($row['property_type'] == 1 || $row['property_type'] == 2) && ( $row['bathroom'] != null || $row['bathroom'] != "" ) ) { ?>

                                                            <li>
                                                                <i class="flaticon-bath"></i> <?= $row['bathroom'] ?> Bathroom
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ( ( $row['property_type'] == 2 || $row['property_type'] == 6 ) && ( $row['condition'] != null || $row['condition'] != "" ) ) { ?>

                                                            <li>
                                                                <i class="flaticon-hotel-building"></i> <?= ($row['property_type'] == 6 ? "Land" : "") ?> Condition: <?= $row['condition'] ?>
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ( $row['property_type'] == 2 && ($row['dateFinished'] < "01-01-2000") ) { ?>

                                                            <li>
                                                                <i class="flaticon-calendar"></i> Date Finished: <?= date("F d, Y", strtotime($row['dateFinished'])) ?>
                                                            </li>
                                                        
                                                        <?php } ?>

                                                        <?php if ($row['property_type'] == 1 && ( $row['stayMonth'] != null || $row['stayMonth'] != "" ) && $row['stayMonth'] != 0 ) { ?>

                                                            <li>
                                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayMonth'] ?> Month(s)
                                                            </li>

                                                        <?php } ?>

                                                        <?php if ($row['property_type'] == 1 && ( $row['stayYear'] != null || $row['stayYear'] != "" ) && $row['stayYear'] != 0 ) { ?>

                                                            <li>
                                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayYear'] ?> Year(s)
                                                            </li>

                                                        <?php } ?>

                                                    </ul>
                                                </div>
                                                <div class="footer">
                                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">
                                                        <i class="fa fa-user"></i> 
                                                    <?= $agent_name ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                <?php }
                                    }
                                    else{
                                        ?>
                                            <div class="alert alert-warning">
                                                <div>
                                                    <strong>No Results Found! </strong>
                                                </div>
                                            </div>
                                        <?php
                                    }

                                 ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Features opions start -->
                <div class="features-opions mb-45" style="font-floor_area: 15px">
                    <h3 class="heading-3">Amenities</h3>
                    <div class="row">

                    <?php
                        if (count($amenities) > 0) {
                            foreach ($amenities as $row) { ?>

                                <div class="col-md-4 col-sm-6">
                                    <?= $row['amenity_name'] ?> 
                                </div>

                    <?php }
                        }
                        else{
                        ?>

                            <div class="col-md-4 col-sm-6">
                                None
                            </div>

                        <?php
                        }
                     ?>

                    </div>
                </div>
                <!-- Contact 3 start -->
                <div class="contact-3">
                    <h3 class="heading-3">Inquiry Form</h3>
                    <div class="container">
                        <div class="row">
                            <form action="<?= base_url("functions/inquiry/" . $property['id'] . '/' .  $property['name'] . '/' .  $property['slug'] . '/' .  str_replace(" ", "-", (strtolower($property['offer_type']) == "rental") ? "For Lease" : ((strtolower($property['offer_type']) == "resale") ? "For Sale" : "Pre-selling"))) ?>" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group email">
                                            <input type="text" name="txt_name" class="form-control" placeholder="Name" value="<?= session()->get("User")? session()->get("User")['name']:"" ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group email">
                                            <input type="email" name="txt_email" class="form-control" placeholder="Email" value="<?= session()->get("User")? session()->get("User")['email']:'' ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group number">
                                            <input type="text" name="txt_inquiryAbout" class="form-control" placeholder="Inqury About" value="<?= $property['name'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group number">
                                            <input type="text" name="txt_number" class="form-control" placeholder="Contact Number" value="<?= session()->get("User")? session()->get("User")['contact_no']:'' ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group message">
                                            <textarea class="form-control" name="txt_message" placeholder="Write message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="6Le_CHQaAAAAACOH1KivFaL1-Xu-vLw0GPu-JNMR"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                        <div class="send-btn mb-50">
                                            <button type="submit" class="btn btn-color btn-md btn-message">Send Message</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <?php 

                if ( (session()->has("User") && (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2)) || $property['agent_role'] == 1 || $property['agent_role'] == 2 || $property['agent_info'] == 1) {
                    $agent_name = $property['agent_name'];
                    $contact_no = $property['agent_contact_num'];
                    $email = $property['agent_email'];
                    $bool = true;
                }
                else{
                    b:
                    $agent_name = explode(" ", $property['agent_name']); 
                    $agent_name = $agent_name[0];
                    $bool = false;
                }

            ?>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">

                    <div class="social-list widget clearfix text-center">

                        <div class="photo">

                            <a href="<?= base_url("agents_view/" . $property['user_id'] . '-' . $agent_name); ?>">
                                <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$property['avatar_directory'] . "/" . 'avatar_medium-' . $property['avatar_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $property['avatar_directory'] . 'avatar_medium-' . $property['avatar_filename'] : base_url($pluginDir) . '/img/blank_user.jpg'); ?>" alt="avatar-4" class="img-fluid profile-img mb-4"  style="border-radius: 90px;">
                            </a>

                        </div>
                        <?php if ($bool) { ?>
                        <a href="<?= base_url("agents_view/" . $property['user_id'] . '-' . $agent_name); ?>"><h6 style="color: #2B7DA2">
                        <?= $agent_name ?>
                        </h6></a>
                        <?php } ?>
                        <h6><?= $property['agent_position'] ?></h6>
                        <br>
                        <?php if ($bool) { ?>
                        <h6>Contact Info: <br><br>
                            <i class="fa fa-phone"></i> <?= $contact_no ?><br>
                            <i class="fa fa-envelope"></i> <?= $email ?>
                        </h6><br>

                        <h6>Social Media Links:<br><br>

                            <ul class="social-list clearfix">

                                <li <?= ($property['agent_telegram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_telegram_link'] != null ? $property['agent_telegram_link'] : "") ?>" class="telegram-bg mb-3"><i class="fa fa-telegram"></i></a></li>
                                <li <?= ($property['agent_whatsapp_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_whatsapp_link'] != null ? $property['agent_whatsapp_link'] : "") ?>" class="whatsapp-bg mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                <li <?= ($property['agent_we_chat_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_we_chat_link'] != null ? $property['agent_we_chat_link'] : "") ?>" class="wechat-bg mb-3"><i class="fa fa-wechat"></i></a></li>
                                <li <?= ($property['agent_viber_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_viber_link'] != null ? $property['agent_viber_link'] : "") ?>" class="viber-bg mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                <li <?= ($property['agent_facebook_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_facebook_link'] != null ? $property['agent_facebook_link'] : "") ?>" class="facebook-bg mb-3"><i class="fa fa-facebook"></i></a></li>
                                <li <?= ($property['agent_instagram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_instagram_link'] != null ? $property['agent_instagram_link'] : "") ?>" class="instagram-bg mb-3"><i class="fa fa-instagram"></i></a></li>
                                <li <?= ($property['agent_linkedin_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_linkedin_link'] != null ? $property['agent_linkedin_link'] : "") ?>" class="linkedin-bg mb-3"><i class="fa fa-linkedin"></i></a></li>
                                <li <?= ($property['agent_google_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_google_link'] != null ? $property['agent_google_link'] : "") ?>" class="google-bg mb-3"><i class="fa fa-google"></i></a></li>
                                <li <?= ($property['agent_twitter_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($property['agent_twitter_link'] != null ? $property['agent_twitter_link'] : "") ?>" class="twitter-bg mb-3"><i class="fa fa-twitter"></i></a></li>

                            </ul>

                        </h6>
                        <?php 
                            }
                        ?>

                    </div>

                    <div class="widget search-area">

                        <form class="form-search" method="GET" action="<?= base_url('property_list') ?>">
                            <?= $this->include("Includes/property_search_sidebar") ?>
                        </form>
                        
                    </div>
                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>

                        <?= $this->include("Includes/recent_properties") ?>

                    </div>
                    <!-- Social list start -->
                        <?= $this->include("Includes/follow_us_sidebar") ?>
                        <?= $this->include("Includes/helping_center_sidebar") ?>
                    <?php 

                        $property_offer_type = ($property['offer_type'] == "rental" || $property['offer_type'] == "Rental") ? "For Lease" : (($property['offer_type'] == "resale" || $property['offer_type'] == "Resale") ? "For Sale" : "Pre-selling"); 
                        
                    ?>
                    <div class="social-list widget clearfix">
                        <h5 class="sidebar-title">Share Property</h5>
                        <iframe src="https://www.facebook.com/plugins/share_button.php?href=<?= base_url(str_replace(" ", "-", $property_offer_type) . "/property_view/" . $property['slug']) ?>&layout=button_count&floor_area=large&width=111&height=28&appId" width="111" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>

                        <div style="float: right;">
                            <button style="cursor: pointer;" class="btn btn-sm btn-theme" onclick="history.back()">Go Back</button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Properties details page end -->

<?= $this->include("Includes/property_modal") ?>
<?= $this->include("Includes/category_ajax") ?>