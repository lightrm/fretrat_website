
<?= $this->include("Includes/sub_banner") ?>

<!-- User page start -->
<div class="user-page content-area-7 submit-property">
    <div class="container">
    <?= $this->include("Includes/alert") ?>
        <div class="row">

            <?= $this->include("Includes/profile_menu") ?>

            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="my-address contact-2">
                    <h3 class="heading-3">Change Password</h3>
                    <form action="<?= base_url("functions/edit_profile/password") ?>" method="POST" enctype="multipart/form-data">
                        <div class="search-area contact-2">
                            <div class="search-area-inner">
                                <div class="search-contents ">
                                    <div class="row">
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Old Password</label>
                                                <input type="password" name="txt_oldPassword" class="form-control" placeholder="Old Password">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input type="password" name="txt_password" class="form-control" placeholder="New Password">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input type="password" name="txt_confirmPassword" class="form-control" placeholder="Confirm Password">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="send-btn">
                                                <button type="submit" class="btn btn-color btn-md btn-message">Save Changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- User page end -->