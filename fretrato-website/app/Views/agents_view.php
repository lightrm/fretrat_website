
<?= $this->include("Includes/sub_banner") ?>

<!-- Agent page start -->
<div class="agent-page content-area-2">
    <!-- Agent detail start -->
    <div class="agent-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="agent-5 mb-50">
                        <div class="row agent_img">
                            <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$agent['img_directory'] . "/" . 'avatar_large-' . $agent['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $agent['img_directory'] . 'avatar_large-' . $agent['img_filename'] : base_url($pluginDir) . '/img/blank_user.jpg'); ?>" alt="avatar-4" class="img-fluid" style="object-fit: cover">
                            <div class="col-lg-8 col-md-8 col-pad align-self-center agent_details">
                                <div class="details">
                                    <h3><?php 

                                            $fretratoTeam = session()->has("User")? (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2):false;


                                            if ($fretratoTeam || $agent['role'] == 1 || $agent['role'] == 2 || $agent['toogleInfo'] == 1) {
                                                if ( $fretratoTeam || $agent['role'] == 1 || $agent['role'] == 2 || $agent['toogleInfo'] == 1) {
                                                    $agent_name = $agent['name'];
                                                    $bool = true;
                                                }
                                                else{
                                                    goto a;
                                                }
                                            }
                                            else{
                                                a:
                                                $agent_name = explode(" ", $agent['name']); 
                                                $agent_name = $agent_name[0];
                                                $bool = false;
                                            }

                                            if ($bool) {
                                                echo $agent_name;
                                            }

                                        ?>
                                    </h3>
                                    <p><?= $agent['about'] ?></p>
                                    <?php if ($bool){ ?>
                                    <div class="contact">
                                        <p>
                                            <a href="mailto:<?= $agent['email'] ?>"><i class="fa fa-envelope-o"></i><?= $agent['email'] ?></a>
                                        </p>
                                        <p>
                                            <a href="tel:<?= $agent['contact_no'] ?>"> <i class="fa fa-phone"></i><?= $agent['contact_no'] ?></a>
                                        </p>
                                    </div>
                                    <ul class="social-list clearfix">

                                        <li <?= ($agent['telegram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['telegram_link'] != null ? $agent['telegram_link'] : "") ?>" class="telegram mb-3"><i class="fa fa-telegram"></i></a></li>
                                        <li <?= ($agent['whatsup_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['whatsup_link'] != null ? $agent['whatsup_link'] : "") ?>" class="whatsapp mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                        <li <?= ($agent['we_chat_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['we_chat_link'] != null ? $agent['we_chat_link'] : "") ?>" class="wechat mb-3"><i class="fa fa-wechat"></i></a></li>
                                        <li <?= ($agent['viber_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['viber_link'] != null ? $agent['viber_link'] : "") ?>" class="viber mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                        <li <?= ($agent['facebook_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['facebook_link'] != null ? $agent['facebook_link'] : "") ?>" class="facebook mb-3"><i class="fa fa-facebook"></i></a></li>
                                        <li <?= ($agent['instagram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['instagram_link'] != null ? $agent['instagram_link'] : "") ?>" class="instagram mb-3"><i class="fa fa-instagram"></i></a></li>
                                        <li <?= ($agent['linkedin_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['linkedin_link'] != null ? $agent['linkedin_link'] : "") ?>" class="linkedin mb-3"><i class="fa fa-linkedin"></i></a></li>
                                        <li <?= ($agent['google_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['google_link'] != null ? $agent['google_link'] : "") ?>" class="google mb-3"><i class="fa fa-google"></i></a></li>
                                        <li <?= ($agent['twitter_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($agent['twitter_link'] != null ? $agent['twitter_link'] : "") ?>" class="twitter mb-3"><i class="fa fa-twitter"></i></a></li>

                                    </ul>
                                    <?php 
                                        }
                                        else{
                                            ?>
                                            <div class="alert alert-warning">
                                                <div>
                                                    Due to data privacy contact info will be hidden.
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="agent-description">
                        <div class="tab-pane fade active show" id="one" role="tabpanel" aria-labelledby="one-tab">
                            <h3 class="heading-3" hidden>About</h3>
                            <?php //($agent['about'] != null ? $agent['about'] : "No info entered.") ?>

                                <!-- Featured properties start -->
                                <div class="featured-properties">
                                    <div class="container">
                                        <h3 class="heading-3">Agent's Listing</h3>
                                        <div class="row">

                                        <?php
                                        if (count($featured_properties) > 0) {
                                            foreach ($featured_properties as $row) { 

                                                $offer_type = ($row['offer_type'] == "rental" || $row['offer_type'] == "Rental") ? "For Lease" : (($row['offer_type'] == "resale" || $row['offer_type'] == "Resale") ? "For Sale" : "Pre-selling");

                                        ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="property-box-8">
                                                        <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">
                                                            <div class="property-photo">
                                                                    <img src="<?= 
                                                                            (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'thumbnail_medium-' . $row['img_filename'] 
                                                                            : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-8" class="img-fluid" style="display: block; margin: 0 auto; min-height: 300px; max-height: 300px; object-fit: cover">
                                                                <div class="tag-for"><?= $offer_type ?></div>
                                                                <div class="price-ratings-box">
                                                                    <p class="price" style="color: #DEBF49">
                                                                        ₱<?= number_format($row['price'], 2) ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="detail">
                                                            <div class="heading">
                                                                <h3 class="limit-text">
                                                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>"><?= $row['name'] ?></a>
                                                                </h3>
                                                                <div class="location limit-text">
                                                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">
                                                                        <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i><?= $row['location'] ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="properties-listing">
                                                                <span>Sqm: <?= number_format($row['floor_area']) ?></span>
                                                                <span><?= $row['bedroom'] ?> Beds</span>
                                                                <span><?= $row['bedroom'] ?> Baths</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                        <?php }
                                            }
                                            else{
                                                ?>
                                                    <div class="alert alert-warning">
                                                        <div>
                                                            <strong>No Results Found! </strong>
                                                        </div>
                                                    </div>
                                                <?php
                                            }

                                         ?>

                                        </div>
                                    </div>

                                    <?= $this->include("Includes/pagination") ?>
                                    
                                </div>
                                <!-- Featured properties end -->


                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="sidebar mbl">
                        <!-- Recent posts start -->
                        <div class="widget search-area">

                            <form class="form-search" method="GET" action="<?= base_url('property_list') ?>">
                                <?= $this->include("Includes/property_search_sidebar") ?>
                            </form>
                            
                        </div>
                        <div class="widget recent-posts">
                            <h5 class="sidebar-title">Recent Properties</h5>

                            <?= $this->include("Includes/recent_properties") ?>

                        </div>
                        <?= $this->include("Includes/follow_us_sidebar") ?>
                        <?= $this->include("Includes/helping_center_sidebar") ?>


                        <div class="social-list widget clearfix">
                            <div style="float: right;">
                                <button style="cursor: pointer;" class="btn btn-sm btn-theme" onclick="history.back()">Go Back</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Agent detail end -->

</div>
<!-- Agent page end -->

<?= $this->include("Includes/category_ajax") ?>