
<?= $this->include("Includes/sub_banner") ?>

<!-- Blog section start -->
<div class="blog-section content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="option-bar">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <h4>
                                <span class="heading-icon">
                                    <i class="fa fa-caret-right icon-design"></i>
                                    <i class="fa fa-th-list"></i>
                                </span>
                                <span class="heading">Agents List</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <?= $this->include("Includes/results_ctr");  ?>
                <div class="row">

                <?php 
                if (count($agent_list) > 0) {
                    foreach ($agent_list as $row) { 

                            if ( (session()->has("User") && (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2)) || $row['role'] == 1 || $row['role'] == 2 || $row['toogleInfo'] == 1) {
                                $agent_name = $row['name'];
                                $bool = true;
                            }
                            else{
                                a:
                                $agent_name = explode(" ", $row['name']); 
                                $agent_name = $agent_name[0];
                                $bool = false;
                            }

                        ?>
                        <div class="col-md-12">
                            <div class="agent-5">
                                <div class="row agent_img">
                                    <a href="<?= base_url("agents_view/" . $row['id'] . '-' . $agent_name) ?>">
                                        <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'avatar_large-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'avatar_large-' . $row['img_filename'] : base_url($pluginDir) . '/img/blank_user.jpg'); ?>" alt="avatar-5">
                                    </a>
                                    <div class="media-body align-self-center agent_details">
                                        <h3>
                                            <a href="<?= base_url("agents_view/" . $row['id'] . '-' . $agent_name) ?>"> <?= $agent_name;?> </a>
                                            <p><?= $row['about'] ?></p>
                                        </h3>
                                    <?php 
                                        if ($bool) {
                                    ?>
                                        <div class="contact clearfix">
                                            <ul>
                                                <li>
                                                    <a href="mailto:<?= $row['email'] ?>"><i class="fa fa-envelope-o"></i><?= $row['email'] ?></a>
                                                </li>
                                                <li>
                                                    <a href="tel:<?= $row['contact_no'] ?>"> <i class="fa fa-phone"></i><?= $row['contact_no'] ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <ul class="social-list clearfix">

                                            <li <?= ($row['telegram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['telegram_link'] != null ? $row['telegram_link'] : "") ?>" class="telegram mb-3"><i class="fa fa-telegram"></i></a></li>
                                            <li <?= ($row['whatsup_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['whatsup_link'] != null ? $row['whatsup_link'] : "") ?>" class="whatsapp mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                            <li <?= ($row['we_chat_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['we_chat_link'] != null ? $row['we_chat_link'] : "") ?>" class="wechat mb-3"><i class="fa fa-wechat"></i></a></li>
                                            <li <?= ($row['viber_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['viber_link'] != null ? $row['viber_link'] : "") ?>" class="viber mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                            <li <?= ($row['facebook_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['facebook_link'] != null ? $row['facebook_link'] : "") ?>" class="facebook mb-3"><i class="fa fa-facebook"></i></a></li>
                                            <li <?= ($row['instagram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['instagram_link'] != null ? $row['instagram_link'] : "") ?>" class="instagram mb-3"><i class="fa fa-instagram"></i></a></li>
                                            <li <?= ($row['linkedin_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['linkedin_link'] != null ? $row['linkedin_link'] : "") ?>" class="linkedin mb-3"><i class="fa fa-linkedin"></i></a></li>
                                            <li <?= ($row['google_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['google_link'] != null ? $row['google_link'] : "") ?>" class="google mb-3"><i class="fa fa-google"></i></a></li>
                                            <li <?= ($row['twitter_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['twitter_link'] != null ? $row['twitter_link'] : "") ?>" class="twitter mb-3"><i class="fa fa-twitter"></i></a></li>

                                        </ul>
                                    <?php
                                        }
                                        else{
                                            ?>  
                                            
                                            <div class="alert alert-warning">
                                                <div>
                                                    Due to data privacy contact info will be hidden.
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>  
                <?php }
                    }
                    else{
                        ?>
                        <div class="alert alert-warning">
                            <div>
                                <strong>No Results Found! </strong>
                            </div>
                        </div>
                        <?php
                    }
                 ?>
                
                <?= $this->include("Includes/pagination") ?>

                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    <!-- Search box start -->
                    <div class="widget search-box">
                        <h5 class="sidebar-title">Agent Search</h5>
                        <form class="form-search" method="GET" action="<?= base_url('agents_list') ?>">
                            <input type="text" class="form-control" placeholder="Search" name="search" value="<?= (isset($_GET['search']) ? $_GET['search'] : "") ?>">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <!-- Search box start -->
                    <div class="widget search-area">

                        <form class="form-search" method="GET" action="<?= base_url('property_list') ?>">
                            <?= $this->include("Includes/property_search_sidebar") ?>
                        </form>
                        
                    </div>
                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>

                        <?= $this->include("Includes/recent_properties") ?>

                    </div>
                    
                    <?= $this->include("Includes/follow_us_sidebar") ?>
                    <?= $this->include("Includes/helping_center_sidebar") ?>
                    <?= $this->include("Includes/contact_us_sidebar") ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Blog section end -->

<?= $this->include("Includes/category_ajax") ?>