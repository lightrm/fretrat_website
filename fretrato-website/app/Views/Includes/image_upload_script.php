
<script type="text/javascript">

    $('#thumbnail_div').click(function(){

        $('#txt_thumbnail').click();
    

    });

    $('#txt_thumbnail').change(function(){

        var value = $(this).val();

        if (value != null && value != "") {

            $('#thumbnail_div_msg').hide();
            $('#thumbnail_div_img').show();

        }
        else{

            $('#thumbnail_div_img').hide();
            $('#thumbnail_div_msg').show();

        }

    });

    $('#gallery_div').click(function(){
        $('#txt_galleryImages').click();
    });

    $('#txt_galleryImages').change(function(){

        var value = $(this).val();

        if (value != null && value != "") {

            $('#gallery_div_msg').hide();
            $('#gallery_div_img').show();

        }
        else{

            $('#gallery_div_img').hide();
            $('#gallery_div_msg').show();

        }

    });

</script>