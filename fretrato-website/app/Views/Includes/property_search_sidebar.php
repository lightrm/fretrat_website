
<h5 class="sidebar-title">Property Search</h5>
<div class="search-area-inner">
    <div class="search-contents ">
            <!-- Search box start -->
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Type Property Name or Location" name="search" value="<?= ($banner_title == "Property List" ? (isset($_GET['search']) ? $_GET['search'] : "") : "") ?>">
            </div>
            <div class="form-group">
                <select class="selectpicker search-fields" name="status" required>
                    <option <?= (isset($_GET['status']) ? "" : "selected") ?> disabled>Property Status</option>
                    <option value="resale" <?= (isset($_GET['status']) ? ($_GET['status'] == "resale" ? "selected" : "") : "") ?>>For Sale</option>
                    <option value="rental" <?= (isset($_GET['status']) ? ($_GET['status'] == "rental" ? "selected" : "") : "") ?>>For Lease</option>
                    <option value="pre-selling" <?=  (isset($_GET['status']) ? ($_GET['status'] == "pre-selling" ? "selected" : "") : "") ?>>Pre Selling</option>
                </select>
            </div>
            <div class="form-group">
                <select class="selectpicker search-fields" name="type" id="main_categ">
                    <option <?= (isset($_GET['type']) ? "" : "selected") ?> disabled>Property Types</option>
                <?php foreach ($main_categories as $row) { ?>
                            <option value="<?= $row['id'] ?>" <?= (isset($_GET['type']) ? ($_GET['type'] == $row['id'] ? "selected" : "") : "") ?>><?= $row['name'] ?></option>
                <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select class="selectpicker search-fields" name="subCategory" id="sub_categ">
                    <option <?= (isset($_GET['subCategory']) ? "" : "selected") ?> disabled>Sub Category</option>
                <?php 
                    if (isset($sub_categories)) {
                        foreach ($sub_categories as $row) { ?>
                            <option value="<?= $row['sub_categ_id'] ?>" <?= (isset($_GET['subCategory']) ? ($_GET['subCategory'] == $row['sub_categ_id'] ? "selected" : "") : "") ?>><?= $row['sub_categ_name'] ?></option>
                <?php   } 
                    }
                ?>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Type Village or City" name="location" value="<?= (isset($_GET['location']) ? $_GET['location'] : "") ?>">
            </div>
            <?php /*
            <div class="form-group">
                <select class="selectpicker search-fields" name="location" required>
                    <option <?= (isset($_GET['location']) ? "" : "selected") ?> disabled>Location</option>
                    <?php foreach ($locations as $row) { ?>
                            <option <?= (isset($_GET['location']) ? ($_GET['location'] == $row ? "selected" : "") : "") ?>><?= $row ?></option>
                    <?php } ?>
                </select>
            </div>
            */?>
            <div class="form-group" hidden>
                <select class="selectpicker search-fields" name="txt_bedrooms">
                    <option <?= (isset($_GET['txt_bedrooms']) ? "" : "selected") ?> disabled>Bedrooms</option>
                <?php for ($i=1; $i <= 10; $i++) { ?>
                        <option <?= (isset($_GET['txt_bedrooms']) ? ($_GET['txt_bedrooms'] == $i ? "selected" : "") : "") ?>><?= $i ?></option>
                <?php } ?>
                </select>
            </div>
            <div class="form-group mb-30" hidden>
                <select class="selectpicker search-fields" name="txt_bathrooms">
                    <option <?= (isset($_GET['txt_bathrooms']) ? "" : "selected") ?> disabled>Bathrooms</option>
                <?php for ($i=1; $i <= 5; $i++) { ?>
                        <option <?= (isset($_GET['txt_bathrooms']) ? ($_GET['txt_bathrooms'] == $i ? "selected" : "") : "") ?>><?= $i ?></option>
                <?php } ?>
                </select>
            </div>
            <button type="submit" class="search-button btn-md btn-color mb-3" id="hidden_btn">Search</button>
            <a <?= ($banner_title != "Property List") ? "hidden" : "" ?> href="<?= base_url("property_list"); ?>"><button type="button" class="search-button btn-md btn-color">Clear Filter</button></a>
    </div>
</div>