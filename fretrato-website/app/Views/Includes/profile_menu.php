
<div class="col-lg-4 col-md-12 col-sm-12">
    <div class="user-profile-box mrb">
            <!--header -->
            <div class="header clearfix" style="background: #2B7DA2">
                <h2><?= session()->get("User")['name'] ?></h2>
                <h4><?= session()->get("User")['about'] ?></h4>
                <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/" . session()->get("User")['img_directory'] . "/" . 'avatar_large-' . session()->get("User")['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . session()->get("User")['img_directory'] . 'avatar_large-' . session()->get("User")['img_filename'] : base_url($pluginDir) . '/img/blank_user.jpg'); ?>" alt="avatar" class="img-fluid profile-img" style="object-fit: cover;">
            </div>
            <!-- Detail -->
            <div class="detail clearfix" >
                <ul>
                    <li>
                        <a href="<?= base_url('pages/my_profile') ?>" class="<?= ($banner_title == "My Profile") ? "active" : "" ?>">
                            <i class="flaticon-user"></i>Profile
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('pages/my_listing') ?>" class="<?= ($banner_title == "My Listing") ? "active" : "" ?>">
                            <i class="flaticon-house"></i>My Listing
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('pages/change_password') ?>" class="<?= ($banner_title == "Change Password") ? "active" : "" ?>">
                            <i class="flaticon-locked-padlock"></i>Change Password
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url("functions/accountControls/logout") ?>">
                            <i class="flaticon-logout"></i>Log Out
                        </a>
                    </li>
                </ul>
            </div>
        </div>
</div>