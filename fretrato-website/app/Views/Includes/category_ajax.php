
<script type="text/javascript">

    $(document).ready(function(){

        $("body").on("change", "#main_categ", function(){
          
          var formData = new FormData($("#frm_find_destination")[0]);

            $.ajax({
                url: '<?= base_url('pages/sub_category') ?>' + '/' + $(this).val(),
                type: 'POST',
                xhr: function() {
                        var myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                },
                success: function (data) {
                        
                    $("#sub_categ").children().remove();
                    $("#sub_categ").append('<option value="" selected disabled>Sub Category</option>');

                    data = JSON.parse(data);

                    if (data.length > 0) {

                        data.forEach(function(row) {

                            $("#sub_categ").append(new Option(row["sub_categ_name"], row["sub_categ_id"]));
                        
                        });

                    }

                    $("#sub_categ").selectBox("refresh");


                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.responseText);
                    alert("Opps, something wrong, please reload the system");
                },
                  data: formData,
                  cache: false,
                  dataType : 'html',
                  contentType: false,
                  processData: false
            });
          
        });
        
        $("body").on("change", "#main_categ1", function(){
          
          var formData = new FormData($("#frm_find_destination1")[0]);

            $.ajax({
                url: '<?= base_url('pages/sub_category') ?>' + '/' + $(this).val(),
                type: 'POST',
                xhr: function() {
                        var myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                },
                success: function (data) {
                        
                    $("#sub_categ1").children().remove();
                    $("#sub_categ1").append('<option value="" selected disabled>Sub Category</option>');

                    data = JSON.parse(data);

                    if (data.length > 0) {

                        data.forEach(function(row) {

                            $("#sub_categ1").append(new Option(row["sub_categ_name"], row["sub_categ_id"]));
                        
                        });

                    }

                    $("#sub_categ1").selectBox("refresh");


                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.responseText);
                    alert("Opps, something wrong, please reload the system");
                },
                  data: formData,
                  cache: false,
                  dataType : 'html',
                  contentType: false,
                  processData: false
            });
          
        });

        $("body").on("change", "#main_categ2", function(){
          
          var formData = new FormData($("#frm_find_destination1")[0]);

            $.ajax({
                url: '<?= base_url('pages/sub_category') ?>' + '/' + $(this).val() + '/submit',
                type: 'POST',
                xhr: function() {
                        var myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                },
                success: function (data) {
                        
                    $("#sub_categ2").children().remove();
                    $("#sub_categ2").append('<option value="" selected disabled>Sub Category</option>');

                    data = JSON.parse(data);

                    if (data.length > 0) {

                        data.forEach(function(row) {

                            $("#sub_categ2").append(new Option(row["sub_categ_name"], row["sub_categ_id"]));
                        
                        });

                    }

                    $("#sub_categ2").selectBox("refresh");


                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest.responseText);
                    alert("Opps, something wrong, please reload the system");
                },
                  data: formData,
                  cache: false,
                  dataType : 'html',
                  contentType: false,
                  processData: false
            });
          
        });

    });

</script>