
<footer class="footer main-footer" style="background: url('/assets/img/backgrounds/footer_bg.jpg')  no-repeat; background-size: cover; background-position: center;">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-3 col-lg-2 col-md-5 col-sm-5 wow fadeInLeft delay-04s">
                <div class="footer-item">
                    <h4>Contact Us</h4>
                    <ul class="contact-info">
                        <li>
                            <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i>Cityland 10 Tower II Corner Valero and HV Dela Costa Street Salcedo Village Makati City, Metro Manila, Philippines
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i><a href="mailto:info@fretrato.com">info@fretrato.com.ph</a>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i><a href="mailto:info@fretrato.com">inquiry@fretrato.com.ph</a>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i><a href="tel:(02) 7906 1307">(02) 7906 1307</a>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i><a href="tel:+63 917 677 8878">+63 917 677 8878</a>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i><a href="tel:+63 917 677 8875">+63 917 677 8875</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 wow fadeInLeft delay-04s">
                <div class="footer-item">
                    <h4>
                        Useful Links
                    </h4>
                    <ul class="links">
                        <li>
                            <a href="/"><i class="fa fa-angle-right"></i>Home</a>
                        </li>
                        <li>
                            <a href="/agents_list"><i class="fa fa-angle-right"></i>Agents</a>
                        </li>
                        <li>
                            <a href="/property_list"><i class="fa fa-angle-right"></i>Properties</a>
                        </li>
                        <li>
                            <a href="/pages/about_us"><i class="fa fa-angle-right"></i>About us</a>
                        </li>
                        <li>
                            <a href="/pages/contact_us"><i class="fa fa-angle-right"></i>Contact Us</a>
                        </li>
                        <li>
                            <a href="/pages/blogs_list"><i class="fa fa-angle-right"></i>Blogs</a>
                        </li>
                        <li>
                            <a href="/pages/privacy_policy"><i class="fa fa-angle-right"></i>Privacy Policy</a>
                        </li>
                        <li>
                            <a href="/pages/terms_of_use"><i class="fa fa-angle-right"></i>Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-5 col-md-8 col-sm-8 wow fadeInRight delay-04s">
                <div class="footer-item recent-posts">
                    <h4>Recent Properties</h4>

                <?php foreach ($propertyDataft as $i => $row) { ?>

                        <div class="media mb-4" title="<?= $row['name'] ?>">
                            <?php $offer_type = ($row['offer_type'] == "rental" || $row['offer_type'] == "Rental") ? "For Lease" : (($row['offer_type'] == "resale" || $row['offer_type'] == "Resale") ? "For Sale" : "Pre-selling"); ?>
                            <a href="/<?= str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug'] ?>">
                                <img src="/assets/fretrato/Images/<?= $row['img_directory'] . 'thumbnail_small-' . $row['img_filename'] ?>" alt="sub-property" style="max-height: 50px; min-height: 50px; object-fit: cover;">
                            </a>
                            <div class="media-body align-self-center limit-text"> <!-- limit-text -->
                                <h5>
                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>"><?= $row['name']; ?></a>
                                </h5>
                                <p>₱<?= number_format($row['price'], 2); ?></p>
                            </div>
                        </div>

                <?php } ?>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 wow fadeInRight delay-04s">
                <div class="footer-item clearfix">
                    <h4>Contact us</h4>
                    <div class="Subscribe-box">

                        <form action="/functions/inquiry" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" name="txt_name" class="form-control" placeholder="Name" value="<?= session()->get("User")?session()->get("User")['name'] : "" ?>" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="txt_email" class="form-control" placeholder="Email" value="<?= session()->get("User")?session()->get("User")['email']: "" ?>" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="txt_inquiryAbout" class="form-control" placeholder="Inquiry About" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="txt_number" class="form-control" placeholder="Contact Number" value="<?= session()->get("User")?session()->get("User")['contact_no'] : "" ?>" required>
                            </div>

                            <div class="form-group">
                                <textarea class="form-control" name="txt_message" placeholder="Write message" required></textarea>
                            </div>

                            <div class="form-group">
                                <div class="g-recaptcha" data-sitekey="6Le_CHQaAAAAACOH1KivFaL1-Xu-vLw0GPu-JNMR"></div>
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-color btn-md btn-message btn-block">Send Message</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <p class="copy">© 2018 <a href="/">Fretrato Realty</a></p>
                </div>
                <div class="col-lg-8 col-md-12">
                    <ul class="social-list clearfix">

                    <?php foreach ($social_medias as $i => $row) { 

                            switch ($row['type']) {
                                case 'facebook_link':
                                    $type = "facebook";
                                    $type_bg = "facebook-bg";
                                    break;
                                
                                case 'instagram_link':
                                    $type = "instagram";
                                    $type_bg = "instagram-bg";
                                    break;

                                case 'linkedin_link':
                                    $type = "linkedin";
                                    $type_bg = "linkedin-bg";
                                    break;

                                case 'googleplus_link':
                                    $type = "google-plus";
                                    $type_bg = "google-bg";
                                    break;

                                case 'twitter_link':
                                    $type = "twitter";
                                    $type_bg = "twitter-bg";
                                    break;
                                    
                                case 'viber_link':
                                    $type = "whatsapp";
                                    $type_bg = "whatsapp-bg";
                                    break;
                                    
                                case 'wechat_link':
                                    $type = "wechat";
                                    $type_bg = "wechat-bg";
                                    break;

                                case 'telegram_link':
                                    $type = "telegram";
                                    $type_bg = "telegram-bg";
                                    break;            
                                    
                                default:
                                    $type = "whatsapp";
                                    $type_bg = "whatsapp-bg";
                                    break;
                            }

                    ?>

                        <li><a href="<?= $row['value'] ?>" class="<?= $type_bg ?>" target="_blank"><i class="fa fa-<?= $type ?>"></i></a></li>

                    <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- External JS libraries -->
<script src="/assets/js/jquery-2.2.0.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/jquery.selectBox.js"></script>
<script src="/assets/js/rangeslider.js"></script>
<script src="/assets/js/jquery.magnific-popup.min.js"></script>
<script src="/assets/js/jquery.filterizr.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/backstretch.js"></script>
<script src="/assets/js/jquery.countdown.js"></script>
<script src="/assets/js/jquery.scrollUp.js"></script>
<script src="/assets/js/particles.min.js"></script>
<script src="/assets/js/typed.min.js"></script>
<script src="/assets/js/dropzone.js"></script>
<script src="/assets/js/jquery.mb.YTPlayer.js"></script>
<script src="/assets/js/leaflet.js"></script>
<script src="/assets/js/leaflet-providers.js"></script>
<script src="/assets/js/leaflet.markercluster.js"></script>
<script src="/assets/js/slick.min.js"></script>
<script src="/assets/js/maps.js"></script>
<script src="/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/assets/js/ie-emulation-modes-warning.js"></script>
<!-- Custom JS Script -->
<script  src="/assets/js/app.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/knockout/3.1.0/knockout-min.js'></script>
<script  src="/assets/js/knockout-file-bindings.js"></script>
<script type="text/javascript">
    
    var viewModel = {};
    viewModel.fileData = ko.observable({
        dataURL: ko.observable(),
        // can add "fileTypes" observable here, and it will override the "accept" attribute on the file input
        // fileTypes: ko.observable('.xlsx,image/png,audio/*')
    });
    viewModel.multiFileData = ko.observable({ dataURLArray: ko.observableArray() });
    viewModel.onClear = function (fileData) {
        if (confirm('Are you sure?')) {
            fileData.clear && fileData.clear();
        }
    };
    viewModel.debug = function () {
        window.viewModel = viewModel;
        console.log(ko.toJSON(viewModel));
        console.log(viewModel.multiFileData())
        console.log(viewModel.multiFileData().dataURLArray());
        console.log(viewModel.multiFileData().fileArray());
        debugger;
    };
    viewModel.onInvalidFileDrop = function(failedFiles) {
        var fileNames = [];
        for (var i = 0; i < failedFiles.length; i++) {
            fileNames.push(failedFiles[i].name);
        }
        var message = 'Invalid file type: ' + fileNames.join(', ');
        alert(message)
        console.error(message);
    };
    ko.applyBindings(viewModel);

</script>
<?php 
  if( isset($ftjs) ):
    foreach ($ftjs as $object): ?>
      <?php if (is_string($object)) { ?>
        <script src="/assets/<?= $object ?>" type="text/javascript"></script>
      <?php } elseif (is_array($object)) { ?>
        <script src="/assets/<?= $object['dir'] ?>" <?php array_map(function($index, $data){ echo $index.'="'.$data.'"'; }, array_keys($object['attr']), $object['attr']); ?>></script>
      <?php } ?>
<?php endforeach; endif; ?>

</body>
</html>