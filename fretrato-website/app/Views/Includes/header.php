<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138721145-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-138721145-1');
    </script>

    <title><?= $meta_title['value'] ?></title>
    <meta name="description" content="<?= isset($property['description']) ? strip_tags($property['description']) : $meta_description['value'] ?>">
    <meta name="keywords" content="Makati Condo for Rent, Makati Condo for Sale, The Fort Condo for Rent, The Fort Condo for Sale, Global City Condo for Sale, Global City Condo for Rent, McKinley Hill Condominium, Condo in Makati For Sale and Rent, Condo in Makati City, Condominium in Makati City<?= isset($property['meta_keywords']) ? ", ".$property['meta_keywords'] : "" ?>">
    <meta name="author" content="Fretrato - Real Estate">
    <meta name="robots" content="index, follow"/>

    <?php 
    if (isset($property['offer_type'])) {
        
        $property_offer_type = ($property['offer_type'] == "rental" || $property['offer_type'] == "Rental") ? "For Lease" : (($property['offer_type'] == "resale" || $property['offer_type'] == "Resale") ? "For Sale" : "Pre-selling"); 
    
    ?>

    <!-- opengraph meta -->
    <meta property="og:title" content="<?= isset($property['name']) ? $property['name'] : $meta_title['value'] ?>">
    <meta property="og:description" content="<?= isset($property['description']) ? strip_tags($property['description']) : $meta_description['value'] ?>">
    <meta property="og:image" content="<?= isset($property['img_directory']) ? (file_exists(ROOTPATH."public/assets/fretrato/Images/".$property['img_directory'] . "/" . 'thumbnail_medium-' . $property['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $property['img_directory'] . 'thumbnail_medium-' . $property['img_filename'] : base_url($pluginDir) . '/img/no_image.png') : ""; ?>">
    <meta property="og:url" content="<?= isset($property['id']) ? base_url(str_replace(" ", "-", $property_offer_type) . "/property_view/" . $property['slug']) : "" ?>">
    <meta property="og:type" content="Property">


    <!-- twitter meta -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?= isset($property['name']) ? $property['name'] : $meta_title['value'] ?>">
    <meta name="twitter:description" content="<?= isset($property['description']) ? strip_tags($property['description']) : $meta_description['value'] ?>">
    <meta name="twitter:image" content="<?= isset($property['img_directory']) ? (file_exists(ROOTPATH."public/assets/fretrato/Images/".$property['img_directory'] . "/" . 'thumbnail_medium-' . $property['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $property['img_directory'] . 'thumbnail_medium-' . $property['img_filename'] : base_url($pluginDir) . '/img/no_image.png') : ""; ?>">
    <meta name="twitter:url" content="<?= isset($property['id']) ? base_url(str_replace(" ", "-", $property_offer_type) . "/property_view/" . $property['slug']) : "" ?>">

    <?php 
    }
    if (isset($blogs['author_name'])) {
        $url_title = str_replace(" ", "-", $blogs['title'])
    ?>

    <!-- opengraph meta -->
    <meta property="og:title" content="<?= isset($blogs['title']) ? $blogs['title'] : $meta_title['value'] ?>">
    <meta property="og:description" content="<?= isset($blogs['body']) ? strip_tags($blogs['body']) : $meta_description['value'] ?>">
    <meta property="og:image" content="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$blogs['img_directory'] . "/" . 'thumbnail_medium-' . $blogs['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $blogs['img_directory'] . 'original-' . $blogs['img_filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>">
    <meta property="og:url" content="<?= base_url("pages/blogs_view/" . $blogs['id'] . "-" . $url_title); ?>">
    <meta property="og:type" content="Blogs">


    <!-- twitter meta -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?= isset($blogs['title']) ? $blogs['title'] : $meta_title['value'] ?>">
    <meta name="twitter:description" content="<?= isset($blogs['body']) ? strip_tags($blogs['body']) : $meta_description['value'] ?>">
    <meta name="twitter:image" content="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$blogs['img_directory'] . "/" . 'thumbnail_medium-' . $blogs['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $blogs['img_directory'] . 'original-' . $blogs['img_filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>">
    <meta name="twitter:url" content="<?= base_url("pages/blogs_view/" . $blogs['id'] . "-" . $url_title); ?>">

    <?php 
    }
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/magnific-popup.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/jquery.selectBox.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/dropzone.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/rangeslider.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/animate.min.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/leaflet.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/slick.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/slick-theme.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/slick-theme.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/map.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/jquery.mCustomScrollbar.css">
    <link type="text/css" rel="stylesheet" href="/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/assets/fonts/flaticon/font/flaticon.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/MSFmultiSelect.css">
    <link type="text/css" rel="stylesheet" href="/assets/css/custom.css">
    <script src="/assets/js/MSFmultiSelect.js" type="text/javascript"></script>
    <script src="/assets/js/knockout-file-bindings.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    
    <?php 
      if( isset($hdcss) ):
        foreach ($hdcss as $dir): ?>
    <link rel="stylesheet" href="/assets/<?= $dir ?>">
    <?php endforeach; endif; ?>

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="/assets/fretrato/Images/favicon.png" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="/assets/css/style.css?version=2">
    <link rel="stylesheet" type="text/css" href="/assets/css/skins/default.css">

    <?php 
      if( isset($hdjs) ):
        foreach ($hdjs as $dir): ?>
    <script src="/assets/<?= $dir ?>" type="text/javascript"></script>
    <?php endforeach; endif; ?>


    <style type="text/css">
        .loader {
          border: 7px solid #f3f3f3;
          border-radius: 50%;
          border-top: 7px solid #2B7DA2;
          width: 50px;
          height: 50px;
          -webkit-animation: spin 1s linear infinite; /* Safari */
          animation: spin 1s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        /* Extra small devices (phones, 600px and down) */
        @media only screen and (max-width: 600px) {
            .property_view_img {
                min-height: 300px; 
                max-height: 300px;
            }
            .property_gallery_img {
                min-height: 65px; 
                max-height: 65px;
            }
            .agent_img {
                margin: 0 60px;
            }
            .agent_details{
                margin-top: 40px;
            }
            .nav-user-detail-btn{
                display: none;
            }
            .nav-user-detail-list{
                display: block;
            }
            .contact-propert-view{
                font-size: 13px;
            }
        }

        /* Small devices (portrait tablets and large phones, 600px and up) */
        @media only screen and (min-width: 600px) {
            .property_view_img {
                min-height: 400px; 
                max-height: 400px;
            }
            .property_gallery_img {
                min-height: 65px; 
                max-height: 65px;
            }
            .nav-user-detail-btn{
                display: none;
            }
            .nav-user-detail-list{
                display: block;
            }
        }

        /* Medium devices (landscape tablets, 768px and up) */
        @media only screen and (min-width: 768px) {
            .property_view_img {
                min-height: 500px; 
                max-height: 500px;
            }
            .property_gallery_img {
                min-height: 80px; 
                max-height: 80px;
            }
            .nav-user-detail-btn{
                display: none;
            }
            .nav-user-detail-list{
                display: block;
            }
        }

        /* Large devices (laptops/desktops, 992px and up) */
        @media only screen and (min-width: 992px) {
            .property_view_img {
                min-height: 500px; 
                max-height: 500px;
            }
            .nav-user-detail-list{
                display: none;
            }
            .nav-user-detail-btn{
                display: block;
            }
        }

        /* Extra large devices (large laptops and desktops, 1200px and up) */
        @media only screen and (min-width: 1200px) {
            .property_view_img {
                min-height: 500px; 
                max-height: 500px;
            }
            .property_gallery_img {
                min-height: 80px; 
                max-height: 80px;
            }
            .nav-user-detail-list{
                display: none;
            }
            .nav-user-detail-btn{
                display: block;
            }
        }


    </style>
</head>
<body id="top">
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v9.0'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="272016260394221"
  theme_color="#0A7CFF"
    logged_in_greeting="Hello! We are available for your questions or concerns. Chat with us now! "
    logged_out_greeting="Hello! We are available for your questions or concerns. Chat with us now! ">
</div>

<!-- Top header start -->
<header class="top-header th-bg nav-user-detail-btn" id="top-header-2" style="background: #2B7DA2">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-7">
                <div class="list-inline">
                    <a href="tel:+63 917 677 8878"><i class="fa fa-phone"></i>+63 917 677 8878</a>
                    <a href="tel:info@themevessel.com"><i class="fa fa-envelope"></i>info@fretrato.com.ph</a>
                    <a href="#" class="mr-0"><i class="fa fa-clock-o"></i>Mon - Fri: 8:30am - 5:30pm / Sat: 8:30am - 4:00pm</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-5">
                <ul class="top-social-media pull-right">

                    <?php foreach ($social_medias as $i => $row) { 

                            switch ($row['type']) {
                                case 'facebook_link':
                                    $type = "facebook";
                                    $type_bg = "facebook";
                                    break;
                                
                                case 'instagram_link':
                                    $type = "instagram";
                                    $type_bg = "instagram";
                                    break;

                                case 'linkedin_link':
                                    $type = "linkedin";
                                    $type_bg = "linkedin";
                                    break;

                                case 'googleplus_link':
                                    $type = "google-plus";
                                    $type_bg = "google";
                                    break;

                                case 'twitter_link':
                                    $type = "twitter";
                                    $type_bg = "twitter";
                                    break;
                                    
                                case 'viber_link':
                                    $type = "whatsapp";
                                    $type_bg = "viber";
                                    break;
                                    
                                case 'wechat_link':
                                    $type = "wechat";
                                    $type_bg = "wechat";
                                    break;

                                case 'telegram_link':
                                    $type = "telegram";
                                    $type_bg = "telegram";
                                    break;                     
                                                   
                                default:
                                    $type = "whatsapp";
                                    $type_bg = "whatsapp";
                                    break;
                            }

                    ?>

                        <li><a href="<?= $row['value'] ?>" class="<?= $type_bg ?>" target="_blank"><i class="fa fa-<?= $type ?>"></i></a></li>

                    <?php } ?>

                </ul>
            </div>
        </div>
    </div>
</header>
<!-- Top header end -->

<!-- main header start -->
<header class="main-header sticky-header" id="main-header-2">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-lg navbar-light rounded">
                    <a class="navbar-brand logo navbar-brand d-flex mr-auto" href="/">
                        <img src="/assets/fretrato/Images/fretrato-logo-transparent.png" alt="logo" style="width: 200px; height: 90px; object-fit: cover;">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation" style="width: 50px; height: 50px">
                        <span class="fa fa-bars"></span>
                    </button>
                    <div class="navbar-collapse collapse w-100" id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle text-nowrap" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                    Condominium
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/property_list?type=1&location=Makati">Makati</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=1&location=Taguig">Taguig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=1&location=Pasig">Pasig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=1&location=Pasay">Pasay</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=1&location=Alabang">Alabang</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=1&location=Quezon City">Quezon City</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=1">Other Location</a></li>

                                    <!-- <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">List Layout</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="properties-list-rightside.html">Right Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-list-leftside.html">Left Sidebar</a></li>
                                            <li><a class="dropdown-item" href="properties-list-fullwidth.html">Fullwidth</a></li>
                                        </ul>
                                    </li> -->
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-nowrap" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    House and Lot
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/property_list?type=2&location=Makati">Makati</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=2&location=Taguig">Taguig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=2&location=Pasig">Pasig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=2&location=Pasay">Pasay</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=2&location=Alabang">Alabang</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=2&location=Quezon City">Quezon City</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=2">Other Location</a></li>

                                </ul>
                            </li>
                            <?php /*
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-nowrap" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lot
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <?php 
                                        foreach ($lot_sub_categ as $row) {
                                            ?>
                                            <li><a class="dropdown-item" href="/property_list?type=11&subCategory="<?= $row['id'] ?>><?= $row['sub_categ_name'] ?></a></li>
                                            <?php
                                        }
                                    ?>
                                </ul>
                            </li>*/?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-nowrap" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Land
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <?php 
                                        foreach ($land_sub_categ as $row) {
                                            ?>
                                            <li><a class="dropdown-item" href="/property_list?type=6&subCategory="<?= $row['id'] ?>><?= $row['sub_categ_name'] ?></a></li>
                                            <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-nowrap" href="#" id="navbarDropdownMenuLink7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Office
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/property_list?type=3&location=Makati">Makati</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=3&location=Taguig">Taguig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=3&location=Pasig">Pasig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=3&location=Pasay">Pasay</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=3&location=Alabang">Alabang</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=3&location=Quezon City">Quezon City</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=3">Other Location</a></li>

                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-nowrap" href="#" id="navbarDropdownMenuLink6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Commercial
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/property_list?type=4&location=Makati">Makati</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=4&location=Taguig">Taguig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=4&location=Pasig">Pasig</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=4&location=Pasay">Pasay</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=4&location=Alabang">Alabang</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=4&location=Quezon City">Quezon City</a></li>
                                    <li><a class="dropdown-item" href="/property_list?type=4">Other Location</a></li>

                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-nowrap" href="/pages/contact_us" aria-haspopup="true" aria-expanded="false">
                                    Contact us
                                </a>
                            </li>
                        <?php /*if (session()->has("User")) { ?>
                            <li class="nav-item nav-user-detail-btn">
                                <!-- Example split danger button -->
                                <div class="btn-group">
                                  <a href="/pages/my_profile" type="button" class="btn btn-danger signup-link"><?= session()->get("User")['username'] ?></a>
                                  <button type="button" class="btn btn-danger signup-link dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer;">
                                    <span class="sr-only">Toggle Dropdown</span>
                                  </button>
                                  <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/pages/my_profile">My Profile</a>
                                    <a class="dropdown-item" href="/pages/my_listing">My Listing</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="/functions/accountControls/logout">Logout</a>
                                  </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown nav-user-detail-list">
                                <a class="nav-link dropdown-toggle text-nowrap" href="/pages/my_profile" id="navbarDropdownMenuLink6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= session()->get("User")['username'] ?>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="/pages/my_profile">My Profile</a></li>
                                    <li><a class="dropdown-item" href="/pages/my_listing">My Listing</a></li>
                                    <div class="dropdown-divider"></div>
                                    <li><a class="dropdown-item" href="/functions/accountControls/logout">Logout</a></li>

                                </ul>
                            </li>
                        <?php } ?>
                            <li class="nav-item nav-user-detail-btn">
                                <a class="btn btn-sm btn-theme signup-link bg-active" href="<?= (session()->has("User") ? "/pages/submit_property" : "/pages/login"); ?>">Submit Property</a>
                            </li>
                            <li class="nav-item dropdown nav-user-detail-list">
                                <a class="nav-link text-nowrap" href="<?= (session()->has("User") ? "/pages/submit_property" : "/pages/login"); ?>" aria-haspopup="true" aria-expanded="false">
                                    Submit Property
                                </a>
                            </li>
                            */ ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- main header end -->