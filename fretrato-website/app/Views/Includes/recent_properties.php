
<?php foreach ($propertyDataft as $i => $row) { ?>

        <div class="media mb-4" title="<?= $row['name'] ?>">
            <?php $offer_type = ($row['offer_type'] == "rental" || $row['offer_type'] == "Rental") ? "For Lease" : (($row['offer_type'] == "resale" || $row['offer_type'] == "Resale") ? "For Sale" : "Pre-selling"); ?>
            <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>" >
                <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'thumbnail_small-' . $row['img_filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="sub-property" style="max-height: 50px; min-height: 50px; object-fit: cover;">
            </a>
            <div class="media-body align-self-center limit-text"> <!-- limit-text -->
                <h5>
                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>"title="<?= $row['name'] ?>"><?= $row['name']; ?></a>
                </h5>
                <p>₱<?= number_format($row['price'], 2); ?></p>
            </div>
        </div>

<?php } ?>