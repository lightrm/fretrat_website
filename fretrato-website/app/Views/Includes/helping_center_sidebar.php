
<!-- Helping center start -->
<div class="helping-center clearfix" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/property_bg.jpg"; ?>') no-repeat; background-size: 100% 100%; background-position: center;">
    <div class="media">
        <i class="fa fa-mobile"></i>
        <div class="media-body  align-self-center" >
            <h5 class="mt-0">Contact Us</h5>
            <h4 hidden><a href="tel:<?= strip_tags($help_center_contact_html['value']) ?>"><?= strip_tags($help_center_contact_html['value']) ?></a></h4>
            <h4>
            	<a href="tel:+63 917 677 8878">+63 917 677 8878</a><br>
            	<a href="tel:+63 917 677 8875">+63 917 677 8875</a>
            </h4>
        </div>
    </div>
</div>