
<div class="contact-3 contact-agent widget-3">
    <?= $this->include("Includes/alert")  ?>
    <h5 class="sidebar-title">Contact Us</h5>
    <form action="<?= base_url("functions/inquiry/" . $banner_title) ?>" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <input type="text" name="txt_name" class="form-control" placeholder="Name" value="<?= session()->get("User")?session()->get("User")['name'] : "" ?>" required>
        </div>
        <div class="form-group">
            <input type="email" name="txt_email" class="form-control" placeholder="Email" value="<?= session()->get("User")?session()->get("User")['email']: "" ?>" required>
        </div>
        <div class="form-group">
            <input type="text" name="txt_inquiryAbout" class="form-control" placeholder="Inquiry About" required>
        </div>
        <div class="form-group">
            <input type="text" name="txt_number" class="form-control" placeholder="Contact Number" value="<?= session()->get("User")?session()->get("User")['contact_no'] : "" ?>" required>
        </div>

        <div class="form-group">
            <textarea class="form-control" name="txt_message" placeholder="Write message" required></textarea>
        </div>

        <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6Le_CHQaAAAAACOH1KivFaL1-Xu-vLw0GPu-JNMR"></div>
        </div>

        <div class="form-group mb-0">
            <button type="submit" class="btn btn-color btn-md btn-message btn-block">Send Massage</button>
        </div>
    </form>
</div>