
<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/need_assistance_bg.jpg"; ?>') no-repeat; background-size: cover; background-position: center;">
    <div class="container">
        <div class="breadcrumb-area">
            <h1><?= $banner_title ?></h1>
            <ul class="breadcrumbs">
                <li><a href="<?= base_url(); ?>">Home</a></li>
				<?php 
					$i = 1;
					foreach ($breadCrumps as $htmlval => $link): ?>
				    <li class="active">

				    	<?php if ($i != count($breadCrumps)) { ?>
				    			<a href="<?= $link ?>">
				    	<?php } ?>

				    		<?= $htmlval ?>
				    	
				    	<?php if ($link) { ?>
				    			</a>
				    	<?php } ?>

					</li>
			<?php 	$i++;
					endforeach ?>
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->