<?php if (!empty(session()->has("user_alert"))): ?>
	<div class="alert alert-<?= session()->getFlashdata("user_alert")['type'] ?>">
	  	<div>
	  		<strong><?= session()->getFlashdata("user_alert")['initMsg'] ?>! </strong> <?= session()->getFlashdata("user_alert")['msg'] ?>
		</div>
	</div>
<?php endif ?>