
<form method="POST" action="" id="frm_property_modal">
<!-- Property Video Modal -->
    <div class="modal property-modal fade" id="propertyModal" tabindex="-1" role="dialog" aria-labelledby="propertyModalLabel" aria-hidden="true">
        <div class="loader" style="position: absolute;right: 50%; top: 50%; display: none;"></div>
        <div class="modal-dialog modal-lg" id="modal_details" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="propertyModalLabel">
                        <span id="propertyModal_title"></span>
                    </h5>
                    <p>
                        <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i> <span id="propertyModal_address"></span>
                    </p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 modal-left">
                            <div class="modal-left-content">
                                <div id="modalCarousel" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox" id="property_modal_gallery">


                                    </div>
                                    <a class="control control-prev property_modal_gallery_control" href="#modalCarousel" role="button" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="control control-next property_modal_gallery_control" href="#modalCarousel" role="button" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                                <div class="description"><h3>Description</h3>
                                    <p>
                                        <span id="propertyModal_description"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 modal-right">
                            <div class="modal-right-content bg-white">
                                <strong class="price">
                                    ₱<span id="propertyModal_price"></span>
                                </strong>
                                <section>
                                    <h3>Amenities</h3>
                                    <ul id="property_modal_amenities" class="bullets">
                                    </ul>
                                </section>
                                <section>
                                    <h3>Overview</h3>
                                    <dl>
                                        <dt>Area</dt>
                                        <dd>Sqm: <span id="propertyModal_sqm"></span></dd>
                                        <dt>Beds</dt>
                                        <dd><span id="propertyModal_beds"></span></dd>
                                        <dt>Floor Level</dt>
                                        <dd><span id="propertyModal_floor_level"></span></dd>
                                        <dt>Furnish Type</dt>
                                        <dd><span id="propertyModal_furnished"></span></dd>
                                        <dt>Parking</dt>
                                        <dd><span id="propertyModal_parking"></span></dd>
                                        <dt>Agent</dt>
                                        <dd><span id="propertyModal_agent"></span></dd>
                                    </dl>
                                </section>
                                <a id="property_modal_url" class="btn btn-show btn-theme">Show Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
	

	function property_modal(slug, offer_type, name, location, youtube_code, price, sqm, beds, floor_level, furnished, parking, agent, contact_start, contact_end){

          $(".loader").show();
          $("#modal_details").hide();
          var formData = new FormData($("#frm_property_modal")[0]);
          

            $.ajax({
                url: '<?= base_url('pages/modal_details') ?>' + '/' + slug,
                type: 'POST',
                xhr: function() {
                        var myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                },
                success: function (data) {
                        
					$("#property_modal_url").attr("href", '<?= base_url() ?>' + '/' + offer_type + '/property_view/' + slug);
					$("#propertyModal_title").html(name);
					$("#propertyModal_address").html(location);
					$("#propertyModal_price").html(price);
					$("#propertyModal_sqm").html(sqm);
					$("#propertyModal_beds").html(beds);
					$("#propertyModal_floor_level").html(floor_level);
					$("#propertyModal_furnished").html(furnished);
					$("#propertyModal_parking").html(parking);
					$("#propertyModal_agent").html(agent);

                    data = JSON.parse(data);

					$("#propertyModal_description").html(data.description);

                    $("#property_modal_amenities").children().remove();
                    if (data.amenities.length > 0) {

                        data.amenities.forEach(function(row) {

        					$("#property_modal_amenities").append('<li> ' + row['amenity_name'] + '</li>');
                        
                        });

                    }
                    else{

    					$("#property_modal_amenities").append('<li> None</li>');

                    }

                    $("#property_modal_gallery").children().remove();


                    if (youtube_code != null && youtube_code != "") {

                        $("#property_modal_gallery").append('<div class="carousel-item active"><iframe class="modalIframe" src="https://www.youtube.com/embed/' + youtube_code + '" allowfullscreen="" style="height: 200px;"></iframe></div>');

                    }
                    
                    var base_url = '<?= base_url($pluginDir) ?>';

                    var x = 0;
                    var active = null;
                    var image_src;
                    var hidden = "";
                    console.log(contact_start);

                    if (data.thumbnail != null && data.thumbnail != "") {

                        if (youtube_code == null || youtube_code == "") {
                          active = (x == 0 ? "active" : "");
                        }
                        image_src = base_url + '/fretrato/Images/' + data.thumbnail['directory'] + 'original-' + data.thumbnail['filename'];

                        $("#property_modal_gallery").append('<div id="gallery_thumbnail" class="carousel-item ' + active + '"><img src="' + image_src + '" alt="' + name + '" onerror=this.src="' + base_url + '/img/no_image.png" style="max-height: 300px; min-height: 300px; object-fit: cover"></div>');

                        if (contact_start == "" && contact_end == "") {
                            hidden = "hidden";
                        }

                            $("#gallery_thumbnail").append('<div class="contact-property-list" ' + hidden + ' style="text-align: center;">OCCUPIED<br>START DATE: ' + contact_start + '<br>END DATE: ' + contact_end + '</div>');


                        x++;

                        $(".property_modal_gallery_control").show();

                    }
                    else{

                        if (youtube_code == null || youtube_code == "") {
                            active = "active";
                            $(".property_modal_gallery_control").hide();
                        }

                        $("#property_modal_gallery").append('<div class="carousel-item ' + active + '"><img src="' + base_url + '/img/no_image.png" alt="' + name + '" style="max-height: 300px; min-height: 300px; object-fit: cover"></div>');

                    }

                    if (data.gallery.length > 0) {

	                    data.gallery.forEach(function(row) {

	                    	active = (x == 0 ? "active" : "");

                            image_src = base_url + '/fretrato/Images/' + row['directory'] + 'original-' + row['filename'];

	    					$("#property_modal_gallery").append('<div class="carousel-item ' + active + '"><img src="' + image_src + '" alt="' + name + '" onerror=this.src="' + base_url + '/img/no_image.png" style="max-height: 300px; min-height: 300px; object-fit: cover"></div>');

	                    	x++;
	                    
	                    });

                    	$(".property_modal_gallery_control").show();

	            	}

                    $(".loader").hide();
                    $("#modal_details").show();


                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Opps, something wrong, please reload the system");
                },
                  data: formData,
                  cache: false,
                  dataType : 'html',
                  contentType: false,
                  processData: false
            });

	}


</script>