
<div class="social-list widget clearfix">
    <h5 class="sidebar-title">Follow Us</h5>
    <ul>
<?php foreach ($social_medias as $i => $row) { 

        switch ($row['type']) {
            case 'facebook_link':
                $type = "facebook";
                $type_bg = "facebook-bg";
                break;
            
            case 'instagram_link':
                $type = "instagram";
                $type_bg = "instagram-bg";
                break;

            case 'linkedin_link':
                $type = "linkedin";
                $type_bg = "linkedin-bg";
                break;

            case 'googleplus_link':
                $type = "google-plus";
                $type_bg = "google-bg";
                break;

            case 'twitter_link':
                $type = "twitter";
                $type_bg = "twitter-bg";
                break;
                
            case 'viber_link':
                $type = "whatsapp";
                $type_bg = "viber-bg";
                break;
                
            case 'wechat_link':
                $type = "wechat";
                $type_bg = "wechat-bg";
                break;

            case 'telegram_link':
                $type = "telegram";
                $type_bg = "telegram-bg";
                break;     
                
            default:
                $type = "whatsapp";
                $type_bg = "whatsapp-bg";
                break;
        }

?>

    <li><a href="<?= $row['value'] ?>" class="<?= $type_bg ?>" target="_blank"><i class="fa fa-<?= $type ?>"></i></a></li>

<?php } ?>

    </ul>
</div>