
<!-- Contact section start -->
<div class="contact-section" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/office.jpg"; ?>') no-repeat; background-size: cover; background-position: center;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-inner-form">
                    <div class="details">
                        <a href="<?= base_url(); ?>">
                            <img src="<?= base_url($pluginDir); ?>/fretrato/Images/fretrato-logo-transparent.png" alt="logo" style="width: 150px; height: 70px">
                        </a>
                        <h3>Reset your account here!</h3>
                        <?= $this->include("Includes/alert") ?>
                        <form action="<?= base_url("functions/reset_password/" . $id . '/' . $confirmation_code); ?>" method="POST">
                            <div class="form-group">
                                <input type="password" name="txt_password" class="input-text" placeholder="Password">
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md btn-theme btn-block">Reset Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact section end -->