
<?= $this->include("Includes/sub_banner");  ?>

<!-- Blog section start -->
<div class="blog-section content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="option-bar">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <h4>
                                <span class="heading-icon">
                                    <i class="fa fa-caret-right icon-design"></i>
                                    <i class="fa fa-th-list"></i>
                                </span>
                                <span class="heading">Fretrato Real Estate Blogs</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <?= $this->include("Includes/results_ctr");  ?>
                <div class="row">

                <?php 
                if (count($blogs_list) > 0) {
                    foreach ($blogs_list as $row) { 
                        $url_title = str_replace(" ", "-", $row['title'])
                ?>

                        <div class="col-lg-6 col-md-6 col-sm-12 mt-5">
                            <div class="blog-1">
                                <div class="blog-photo">
                                    <a href="<?= base_url("pages/blogs_view/" . $row['id'] . "-" . $url_title); ?>">
                                        <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'thumbnail_medium-' . $row['img_filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="blog" class="img-fluid" style="max-height: 250px; min-height: 250px">
                                    </a>
                                    <div class="user">
                                        <div class="avatar">
                                            <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['avatar_directory'] . "/" . 'avatar_large-' . $row['avatar_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['avatar_directory'] . 'avatar_large-' . $row['avatar_filename'] : base_url($pluginDir) . '/img/blank_user.jpg'); ?>" alt="avatar" class="img-fluid rounded-circle">
                                        </div>
                                        <div class="name">
                                            <h5><?= $row['author_name'] ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="detail" style="height: 340px;">
                                    <h3>
                                        <a title="<?= $row['title'] ?>" href="<?= base_url("pages/blogs_view/" . $row['id'] . "-" . $url_title); ?>" class="limit-text"><?= $row['title'] ?></a>
                                    </h3>
                                    <div class="blog-footer clearfix">
                                        <div class="float-left">
                                            <p class="date"><i class="flaticon-calendar"></i> <?= date("M d, Y", strtotime($row['created_at']))  ?></p>
                                        </div>
                                    </div>
                                    <p><?= word_limiter(strip_tags($row['body']), 50)  ?> <a href="<?= base_url("pages/blogs_view/" . $row['id'] . "-" . $url_title); ?>" style="color: #2B7DA2">Read more</a></p>
                                </div>
                            </div>
                        </div>

                <?php }
                    }
                    else{
                        ?>
                        <div class="alert alert-warning">
                            <div>
                                <strong>No Results Found! </strong>
                            </div>
                        </div>
                        <?php
                    }
                 ?>
                <?= $this->include("Includes/pagination") ?>


                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    
                    <div class="widget search-box">
                        <h5 class="sidebar-title">Blogs Search</h5>
                        <form class="form-search" method="GET" action="<?= base_url('pages/blogs_list') ?>">
                            <input type="text" class="form-control" placeholder="Search" name="search" value="<?= (isset($_GET['search']) ? $_GET['search'] : "") ?>">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- Categories start -->
                    <div class="widget categories">
                        <h5 class="sidebar-title">Blogs Categories</h5>
                        <ul>
                            <li><a href="<?= base_url('pages/blogs_list?type=1') ?>">Real Estate Blogs<span>(<?= $blogs_categories['real_estate_ctr'] ?>)</span></a></li>

                            <?php 
                                foreach ($real_estate_sub_categories as $row) {
                                ?>

                                    <li><a href="<?= base_url('pages/blogs_list?type=1&sub_type=' . $row['id']) ?>"><?= $row['category_name'] ?><span>(<?= $row['sub_ctr'] ?>)</span></a></li>

                                <?php
                                }
                            ?>

                        </ul>
                    </div>

                    <!-- Search box start -->
                    <div class="widget search-area">

                        <form class="form-search" method="GET" action="<?= base_url('property_list') ?>">
                            <?= $this->include("Includes/property_search_sidebar") ?>
                        </form>
                        
                    </div>

                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>

                        <?= $this->include("Includes/recent_properties") ?>

                    </div>
                    
                    <?= $this->include("Includes/follow_us_sidebar") ?>
                    <?= $this->include("Includes/helping_center_sidebar") ?>
                    <?= $this->include("Includes/contact_us_sidebar") ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Blog section end -->

<?= $this->include("Includes/category_ajax") ?>