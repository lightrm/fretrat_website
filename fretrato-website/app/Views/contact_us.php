
<?= $this->include("Includes/sub_banner") ?>

<!-- Contact 2 start -->
<div class="contact-2 content-area-2">
    <div class="container">
        <div class="main-title">
            <h1>Contact Us</h1>
            <p><?= $contact_us['value'] ?></p>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 contact-info">
                    <i class="fa fa-map-marker"></i>
                    <h5>Office Address</h5>
                    <p><?= $address_html['value'] ?></p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 contact-info">
                    <i class="fa fa-envelope"></i>
                    <h5>Email Address</h5>
                    <p><?= $email_html['value'] ?></p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 contact-info">
                    <i class="fa fa-phone"></i>
                    <h5>Mobile Number</h5>
                    <p><?= $contact_html['value'] ?></p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 contact-info">
                    <i class="fa fa-fax"></i>
                    <h5>Landline</h5>
                    <p><?= $landline_html['value'] ?></p>
                </div>
            </div>
        </div>
        <?= $this->include("Includes/alert") ?>
        <form action="<?= base_url("functions/inquiry") ?>" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="form-section">
                        <div class="row">

                            <div class="col-lg-12">
                                <h3>Inquiry Form</h3>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group name">
                                    <input type="text" name="txt_name" class="form-control" placeholder="Name" value="<?= session()->get("User")?session()->get("User")['name'] : "" ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group email">
                                    <input type="email" name="txt_email" class="form-control" placeholder="Email" value="<?= session()->get("User")?session()->get("User")['email']: "" ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group subject">
                                    <input type="text" name="txt_inquiryAbout" class="form-control" placeholder="Inquiry About" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group number">
                                    <input type="text" name="txt_number" class="form-control" placeholder="Contact Number" value="<?= session()->get("User")?session()->get("User")['contact_no'] : "" ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group message">
                                    <textarea class="form-control" name="txt_message" placeholder="Write message" required></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="g-recaptcha" data-sitekey="6Le_CHQaAAAAACOH1KivFaL1-Xu-vLw0GPu-JNMR"></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="send-btn">
                                    <button type="submit" class="btn btn-color btn-md">Send Message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                                <!-- Featured properties start -->
                                <div class="featured-properties">
                                    <div class="container">
                                        <h3 class="heading-3">Featured Properties</h3>
                                        <div class="row">

                                        <?php
                                        if (count($featured_properties) > 0) {
                                            foreach ($featured_properties as $row) { 

                                                $offer_type = ($row['offer_type'] == "rental" || $row['offer_type'] == "Rental") ? "For Lease" : (($row['offer_type'] == "resale" || $row['offer_type'] == "Resale") ? "For Sale" : "Pre-selling");

                                        ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="property-box-8">
                                                        <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">
                                                            <div class="property-photo">
                                                                    <img src="<?= 
                                                                            (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'thumbnail_medium-' . $row['img_filename'] 
                                                                            : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-8" class="img-fluid" style="min-height: 300px; max-height: 300px; object-fit: cover">
                                                                <div class="tag-for"><?= $offer_type ?></div>
                                                                <div class="price-ratings-box">
                                                                    <p class="price" style="color: #DEBF49">
                                                                        ₱<?= number_format($row['price'], 2) ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="detail">
                                                            <div class="heading">
                                                                <h3 class="limit-text">
                                                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>"><?= $row['name'] ?></a>
                                                                </h3>
                                                                <div class="location limit-text">
                                                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">
                                                                        <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i><?= $row['location'] ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="properties-listing">
                                                                <span>Sqm: <?= number_format($row['floor_area']) ?></span>
                                                                <span><?= $row['bedroom'] ?> Beds</span>
                                                                <span><?= $row['bedroom'] ?> Baths</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php }
                                            }
                                            else{
                                                ?>
                                                    <div class="alert alert-warning">
                                                        <div>
                                                            <strong>No Results Found! </strong>
                                                        </div>
                                                    </div>
                                                <?php
                                            }

                                         ?>

                                        </div>
                                    </div>
                                </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="opening-hours">
                        <h3>Opening Hours</h3>
                        <ul class="day">
                            <li><strong>Sunday</strong> <span class="text-red"> closed</span></li>
                            <li><strong>Monday</strong> <span> 8:30 AM - 5:30 PM</span></li>
                            <li><strong>Tuesday </strong> <span> 8:30 AM - 5:30 PM</span></li>
                            <li><strong>Wednesday </strong> <span> 8:30 AM - 5:30 PM</span></li>
                            <li><strong>Thursday </strong> <span> 8:30 AM - 5:30 PM</span></li>
                            <li><strong>Friday </strong> <span> 8:30 AM - 5:30 PM</span></li>
                            <li><strong>Saturday </strong> <span> 8:30 AM - 1:00 PM</span></li>
                        </ul>
                        <!-- Search box start -->
                        <div class="widget search-area">

                            <form class="form-search" method="GET" action="<?= base_url('property_list') ?>">
                                <?= $this->include("Includes/property_search_sidebar") ?>
                            </form>
                            
                        </div>

                        <!-- Recent posts start -->
                        <div class="widget recent-posts">
                            <h5 class="sidebar-title">Recent Properties</h5>

                            <?= $this->include("Includes/recent_properties") ?>

                        </div>
                        <?= $this->include("Includes/follow_us_sidebar") ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Contact 2 end -->

<!-- Maps start -->
<div class="section maps">
    <div class="map">
        <div id="contactMap" class="contact-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.678658112453!2d121.01502201483959!3d14.560359689828083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c9096ec55555%3A0xaf0d621b7e9c77c1!2sCityland%2010%20Tower%202!5e0!3m2!1sen!2sph!4v1605064454605!5m2!1sen!2sph" width="100%" height="347"></iframe>
        </div>
    </div>
</div>
<!-- Maps end -->
