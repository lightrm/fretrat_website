
<?= $this->include("Includes/sub_banner") ?>

<!-- Blog section start -->
<div class="blog-section content-area-21">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <!-- Blog grid box start -->
                <div class="blog-1 mb-50">
                    
                    <div class="blog-photo">

                        <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$blogs['img_directory'] . "/" . 'thumbnail_large-' . $blogs['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $blogs['img_directory'] . 'original-' . $blogs['img_filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" class="img-fluid" style="max-height: 500px">

                        <div class="user">
                            <div class="avatar">
                                <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$blogs['avatar_directory'] . "/" . 'avatar_large-' . $blogs['avatar_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $blogs['avatar_directory'] . 'avatar_large-' . $blogs['avatar_filename'] : base_url($pluginDir) . '/img/blank_user.jpg'); ?>" alt="avatar" class="img-fluid rounded-circle">
                            </div>
                            <div class="name">
                                <h5><?= $blogs['author_name'] ?></h5>
                            </div>
                        </div>
                    </div>

                    <div class="detail">
                        <h2>
                            <?= $blogs['title'] ?>
                        </h2>
                        <?= $blogs['body'] ?>

                    <?php 
                        if (count($blogs_gallery) > 0 || $blogs['video_url'] != null || $blogs['video_url'] != "") { 
                            $x = 0;
                            $vid_ctr = 0;
                    ?>
                        <div id="propertiesDetailsSlider" class="carousel properties-details-sliders slide mb-60 mt-5">

                            <div class="carousel-inner text-center">
                            <?php 
                                if ($blogs['video_url'] != null || $blogs['video_url'] != "") { ?>
                                    <div class="<?= ($x == 0 ? "active" : "") ?> item carousel-item" data-slide-number="<?= $x ?>">
                                        <iframe src="<?= 'https://www.youtube.com/embed/' . $blogs['video_url']; ?>" style="width: 100%" class="property_view_img"></iframe>
                                    </div>
                            <?php 
                                    $vid_ctr = 1;
                                    $x++;
                                }
                             ?>

                            <?php 
                                foreach ($blogs_gallery as $i => $row) { ?>

                                        <div class="<?= ($x == 0 ? "active" : "") ?> item carousel-item" data-slide-number="<?= $x ?>">
                                            <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['directory'] . "/" . 'thumbnail_large-' . $row['filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['directory'] . 'original-' . $row['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" class="img-fluid property_view_img" alt="property-<?= $x ?>">
                                        </div>

                            <?php   
                                    $x++;
                                }

                            ?>

                                <a <?= ((count($blogs_gallery) + $vid_ctr) > 0) ? "" : "hidden" ?> class="carousel-control left" href="#propertiesDetailsSlider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                <a <?= ((count($blogs_gallery) + $vid_ctr) > 0) ? "" : "hidden" ?> class="carousel-control right" href="#propertiesDetailsSlider" data-slide="next"><i class="fa fa-angle-right"></i></a>

                            </div>
                            <!-- main slider carousel nav controls -->
                            <ul class="carousel-indicators smail-properties list-inline nav nav-justified">

                            <?php 
                                $x = 0;
                                if ($blogs['video_url'] != null || $blogs['video_url'] != "") {
                                ?>

                                    <li class="list-inline-item <?= ($x == 0 ? "active" : "") ?>">
                                        <a id="carousel-selector-<?= $x ?>" class="selected" data-slide-to="<?= $x ?>" data-target="#propertiesDetailsSlider">
                                            <img src="<?= base_url($pluginDir) . '/img/youtube_img.jpg'; ?>" alt="property-<?= $x ?>" class="img-fluid property_gallery_img" alt="property-<?= $x ?>" style="max-width: 146px; object-fit: cover;">
                                        </a>
                                    </li>

                                <?php
                                }

                            ?>

                            <?php 
                                $x++;

                                foreach ($blogs_gallery as $i => $row) { ?>

                                    <li class="list-inline-item <?= ($x == 0 ? "active" : "") ?>">
                                        <a id="carousel-selector-<?= $x ?>" class="selected" data-slide-to="<?= $x ?>" data-target="#propertiesDetailsSlider">
                                            <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['directory'] . "/" . 'thumbnail_large-' . $row['filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['directory'] . 'original-' . $row['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-<?= $x ?>" class="img-fluid property_gallery_img" alt="property-<?= $x ?>" style="max-width: 146px; object-fit: cover;">
                                        </a>
                                    </li>

                            <?php   
                                    $x++;
                                }

                            ?>


                            </ul>
                        </div>
                    <?php } ?>

                        <div class="row clearfix tags-socal-box mb-5">
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <div class="tags">
                                    <h2>Categories</h2>
                                    <ul>
                                    <?php foreach ($blogs_categ as $row) { ?>

                                            <li><a href="#"><?= $row['category_name'] ?></a></li>

                                    <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5" hidden>
                                <div class="social-list">
                                    <h2>Share</h2>
                                    <ul>
                                        <li>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="google">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="linkedin">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="rss">
                                                <i class="fa fa-rss"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div style="position: absolute; right: 10px; bottom: 10px">
                            <button style="cursor: pointer;" class="btn btn-sm btn-theme" onclick="history.back()">Go Back</button>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    <!-- Search box start -->
                    <div class="widget search-box">
                        <h5 class="sidebar-title">Search</h5>
                        <form class="form-search" method="GET" action="<?= base_url('pages/blogs_list'); ?>">
                            <input type="text" class="form-control" placeholder="Search" name="search" value="<?= (isset($_GET['search']) ? $_GET['search'] : "") ?>">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- Categories start -->
                    <div class="widget categories">
                        <h5 class="sidebar-title">Blogs Categories</h5>
                        <ul>
                            <li><a href="<?= base_url('pages/blogs_list?type=1') ?>">Real Estate Blogs<span>(<?= $blogs_categories['real_estate_ctr'] ?>)</span></a></li>

                            <?php 
                                foreach ($real_estate_sub_categories as $row) {
                                ?>

                                    <li><a href="<?= base_url('pages/blogs_list?type=1&sub_type=' . $row['id']) ?>"><?= $row['category_name'] ?><span>(<?= $row['sub_ctr'] ?>)</span></a></li>

                                <?php
                                }
                            ?>

                        </ul>
                    </div>
                    
                    <!-- Search box start -->
                    <div class="widget search-area">

                        <form class="form-search" method="GET" action="<?= base_url('property_list') ?>">
                            <?= $this->include("Includes/property_search_sidebar") ?>
                        </form>
                        
                    </div>

                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>

                        <?= $this->include("Includes/recent_properties") ?>

                    </div>
                    
                    <?= $this->include("Includes/follow_us_sidebar") ?>
                    <?= $this->include("Includes/helping_center_sidebar") ?>
                    <?= $this->include("Includes/contact_us_sidebar") ?>

                </div>
            </div>

        </div>
    </div>
</div>
<!-- Blog section end -->
