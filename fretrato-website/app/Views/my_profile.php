
<?= $this->include("Includes/sub_banner") ?>

<!-- User page start -->
<div class="user-page content-area-7 submit-property">
    <div class="container">
    <?= $this->include("Includes/alert") ?>
        <div class="row">

            <?= $this->include("Includes/profile_menu") ?>

            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="my-address contact-2">
                    <h3 class="heading-3">Profile Details</h3>
                    <form action="<?= base_url("functions/edit_profile/record") ?>" method="POST" enctype="multipart/form-data">
                        <div class="search-area contact-2">
                            <div class="search-area-inner">
                                <div class="search-contents ">
                                    <div class="row">
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Avatar</label>
                                                <div id="thumbnail_div" class="dropzone dropzone-design" data-bind="fileDrag: fileData" style="cursor: pointer;">
                                                    <div id="thumbnail_div_msg" class="dz-default dz-message"><span>Drop file here to upload</span></div>
                                                    <div class="form-group row">
                                                        <div class="col-md-12" id="thumbnail_div_img">
                                                            <img style="height: 125px;" class="img-rounded  thumb" data-bind="attr: { src: fileData().dataURL }, visible: fileData().dataURL">
                                                            <div data-bind="ifnot: fileData().dataURL">
                                                                <label class="drag-label"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" hidden>
                                                            <input style="display: none" id="txt_thumbnail"  type="file" name="txt_thumbnail" data-bind="fileInput: fileData, customFileInput: {
                                                              buttonClass: 'btn btn-success',
                                                              fileNameClass: 'disabled form-control',
                                                              onClear: onClear,
                                                              onInvalidFileDrop: onInvalidFileDrop
                                                            }" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" name="txt_fullName" class="form-control" placeholder="Name" required value="<?= session()->get("User")['name'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12" hidden>
                                            <div class="form-group">
                                                <label>Position</label>
                                                <input type="text" name="txt_position" class="form-control" placeholder="Name" value="<?= session()->get("User")['about'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Gender</label>
                                                <select class="selectpicker search-fields" name="txt_gender">
                                                    <option <?= ( session()->get("User")['gender'] == null ? "selected" : "") ?> disabled>Gender</option>
                                                    <option <?= ( session()->get("User")['gender'] == "male" ? "selected" : "") ?> value="male">Male</option>
                                                    <option <?= ( session()->get("User")['gender'] == "female" ? "selected" : "") ?> value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" name="txt_username" class="form-control" placeholder="Username" required value="<?= session()->get("User")['username'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" name="txt_email" class="form-control" placeholder="Email Address" required value="<?= session()->get("User")['email'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Contact No.</label>
                                                <input type="text" name="txt_contact_no" class="form-control" placeholder="Contact No." required value="<?= session()->get("User")['contact_no'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label>Roles</label>
                                                <select class="selectpicker search-fields" name="txt_roles" <?= (session()->get("User")['role'] == 1) ? "disabled" : "" ?>>
                                                    <option disabled>Roles</option>
                                                    <?php if (session()->get("User")['role'] == 1) { ?>
                                                        <option selected value="1">Admin</option>
                                                    <?php } ?> 
                                                    <option value="3" <?= session()->get("User")['role'] == "3" ? "selected" : '' ?>>Agent</option>
                                                    <option value="6" <?= session()->get("User")['role'] == "6" ? "selected" : '' ?>>Broker</option>
                                                    <option value="4" <?= session()->get("User")['role'] == "4" ? "selected" : '' ?>>Owner</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 " hidden>
                                            <div class="form-group message">
                                                <label>About </label>
                                                <textarea class="form-control" name="txt_about" placeholder="Write About here..."><?= session()->get("User")['about'] ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="send-btn">
                                                <button type="submit" class="btn btn-color btn-md btn-message">Save Changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- User page end -->


<?= $this->include("Includes/image_upload_script") ?>