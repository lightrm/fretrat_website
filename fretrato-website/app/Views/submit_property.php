
<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/office.jpg"; ?>')">
    <div class="container">
        <div class="breadcrumb-area">
            <h1><?= $banner_title ?></h1>
            <ul class="breadcrumbs">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <?php 
                    $i = 1;
                    foreach ($breadCrumps as $htmlval => $link): ?>
                    <li class="active">

                        <?php if ($i != count($breadCrumps)) { ?>
                                <a href="<?= $link ?>">
                        <?php } ?>

                            <?= $htmlval ?>
                        
                        <?php if ($link) { ?>
                                </a>
                        <?php } ?>

                    </li>
            <?php   $i++;
                    endforeach ?>
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

<!-- User page start -->
<div class="user-page submit-property content-area-7">
    <div class="container">
    <?= $this->include("Includes/alert") ?>
        <div class="row">
            <div class="col-lg-12">

                <div class="search-area contact-2">
                    <div class="search-area-inner">
                        <div class="search-contents ">
                            <form action="<?= base_url("functions/create_property"); ?>" method="POST" enctype='multipart/form-data'>
                                <h3 class="heading-3">Basic Information</h3>
                                <div class="row mb-30">
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Property Title <i style="color: red">*</i></label>
                                            <input type="text" name="txt_name" class="form-control" placeholder="Property Title" value="<?= session()->get("inp_name") ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Offer Type <i style="color: red">*</i></label>
                                            <select class="selectpicker search-fields" name="txt_propertyStatus" required>
                                                <option <?= ( session()->get("inp_propertyStatus") == null ? "selected" : "") ?> disabled>Offer Type</option>
                                                <option <?= ( session()->get("inp_propertyStatus") == "resale" ? "selected" : "") ?> value="resale">For Sale</option>
                                                <option <?= ( session()->get("inp_propertyStatus") == "rental" ? "selected" : "") ?> value="rental">For Lease</option>
                                                <option <?= ( session()->get("inp_propertyStatus") == "pre-selling" ? "selected" : "") ?> value="pre-selling">Pre-selling</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Property Type <i style="color: red">*</i></label>
                                            <select class="selectpicker search-fields" name="txt_propertyType" id="main_categ" required>
                                                <option <?= ( session()->get("inp_propertyType") == null ? "selected" : "") ?> disabled>Property Type</option>
                                        <?php foreach ($main_categories as $row) { ?>
                                                    <option <?= ( session()->get("inp_propertyType") == $row['id'] ? "selected" : "") ?> value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                        <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Sub Category </label>
                                            <select class="selectpicker search-fields" name="txt_subCategory" id="sub_categ" required>
                                                <option <?= ( session()->get("inp_subCategory") == null ? "selected" : "") ?> disabled>Sub Category</option>
                                                <?php 
                                                    if (isset($sub_categories)) {
                                                        foreach ($sub_categories as $row) { ?>
                                                            <option value="<?= $row['id'] ?>" <?= ( session()->get("inp_subCategory") == $row['id'] ? "selected" : "") ?>><?= $row['sub_categ_name'] ?></option>
                                                <?php   } 
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Price <i style="color: red">*</i></label>
                                            <input type="number" step="any" name="txt_price" class="form-control" placeholder="PHP" required value="<?= session()->get("inp_price") ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Area/Size <i style="color: red">*</i></label>
                                            <input type="number" step="any" name="txt_size" class="form-control" placeholder="Sqm" required value="<?= session()->get("inp_size") ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Floor Level <i style="color: red">*</i></label>
                                            <input type="number" name="txt_floor_level" class="form-control" placeholder="Floor Level" required value="<?= session()->get("inp_floor_level") ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Bedrooms <i style="color: red">*</i></label>
                                            <input type="number" name="txt_bedroom" class="form-control" placeholder="Bedrooms" value="<?= session()->get("inp_bedroom") ?>" required>
                                            <?php /* <select class="selectpicker search-fields" name="txt_bedroom" required>
                                                <option <?= ( session()->get("inp_bedroom") == null ? "selected" : "") ?> disabled>Bedrooms</option>
                                            <?php for ($i=1; $i <= 10; $i++) { ?>
                                                    <option <?= ( session()->get("inp_bedroom") == $i ? "selected" : "") ?>><?= $i ?></option>
                                            <?php } ?>
                                            </select> */ ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Bathroom <i style="color: red">*</i></label>
                                            <input type="number" name="txt_bathroom" class="form-control" placeholder="Bathroom" value="<?= session()->get("inp_bathroom") ?>" required>
                                            <?php /* <select class="selectpicker search-fields" name="" required>
                                                <option <?= ( session()->get("inp_bathroom") == null ? "selected" : "") ?> disabled>Bathrooms</option>
                                            <?php for ($i=1; $i <= 5; $i++) { ?>
                                                    <option <?= ( session()->get("inp_bathroom") == $i ? "selected" : "") ?>><?= $i ?></option>
                                            <?php } ?>
                                            </select> */ ?>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Additional Information</h3>
                                <div class="row mb-30">
                                    <div class="col-md-12 mb-5">
                                        <label>Insert Video Code (Get the code of the youtube video Eg.:</label><br>
                                        <label>https://www.youtube.com/watch?v=<strong>kOHB85vDuow</strong> highlighted text is the video code.) </label>
                                        <input type="text" name="txt_video" class="form-control" placeholder="Insert Video Code" value="<?= session()->get("inp_video") ?>">
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-check checkbox-theme">
                                                <input class="form-check-input" type="checkbox" value="1" name="txt_parking" id="parking" <?= ( session()->get("inp_parking") == 1 ? "checked" : "") ?>>
                                                <label class="form-check-label" for="parking">
                                                    Parking
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-check checkbox-theme">
                                                <input class="form-check-input" type="checkbox" value="1" name="txt_furnished" id="Furnished" <?= ( session()->get("inp_furnished") == 1 ? "checked" : "") ?>>
                                                <label class="form-check-label" for="Furnished">
                                                    Furnished
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Location</h3>
                                <div class="row mb-30">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Street Name <i style="color: red">*</i></label>
                                            <input type="text" name="txt_streetName" class="form-control" placeholder="Address" required value="<?= session()->get("inp_streetName") ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Barangay </label>
                                            <input type="text" name="txt_barangay" class="form-control" placeholder="Barangay" value="<?= session()->get("inp_barangay") ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>City <i style="color: red">*</i></label>
                                            <input type="text" name="txt_city" class="form-control" placeholder="City" required value="<?= session()->get("inp_city") ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Province </label>
                                            <input type="text" name="txt_province" class="form-control" placeholder="Province" value="<?= session()->get("inp_province") ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Country </label>
                                            <input type="text" name="txt_country" class="form-control" placeholder="Country" value="<?= session()->get("inp_country") ?>">
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Detailed Information</h3>
                                <div class="row mb-30">
                                    <div class="col-lg-12">
                                        <div class="form-group message">
                                            <label>Description <i style="color: red">*</i></label>
                                            <textarea id="summernote_desc" name="txt_description"><?= session()->get("inp_description") ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12" hidden>
                                        <div class="form-group message">
                                            <label>About <i style="color: red">*</i></label>
                                            <textarea id="summernote_about" name="txt_about"><?= session()->get("inp_about") ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Features (optional)</h3>
                                <div class="row mb-30">

                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <div id="myselect">
                                              <select id="multiselect" class="form-control" multiple name="amenities[]">

                                                <?php foreach ($amenities as $row) { ?>
                                                    <option value="<?= $row['id'] ?>"><?= $row['amenity_name'] ?></option>
                                                <?php } ?>                   

                                              </select>
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>

                                <h3 class="heading-3">Thumbnail</h3>
                                <div class="row mb-45">
                                    <div class="col-lg-12">

                                        <div id="thumbnail_div" class="dropzone dropzone-design" data-bind="fileDrag: fileData" style="cursor: pointer;">
                                            <div id="thumbnail_div_msg" class="dz-default dz-message"><span>Drop file here to upload</span></div>
                                            <div class="form-group row">
                                                <div class="col-md-12" id="thumbnail_div_img">
                                                    <img style="height: 125px;" class="img-rounded  thumb" data-bind="attr: { src: fileData().dataURL }, visible: fileData().dataURL">
                                                    <div data-bind="ifnot: fileData().dataURL">
                                                        <label class="drag-label"></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" hidden>
                                                    <input style="display: none" id="txt_thumbnail"  type="file" name="txt_thumbnail" data-bind="fileInput: fileData, customFileInput: {
                                                      buttonClass: 'btn btn-success',
                                                      fileNameClass: 'disabled form-control',
                                                      onClear: onClear,
                                                      onInvalidFileDrop: onInvalidFileDrop
                                                    }" accept="image/*">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                                <h3 class="heading-3">Property Gallery</h3>
                                <div class="row mb-45">
                                    <div class="col-lg-12">

                                        <div id="gallery_div" class="dropzone dropzone-design" data-bind="fileDrag: multiFileData"  style="cursor: pointer;">
                                            <div id="gallery_div_msg" class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                            <div class="form-group row">
                                                <div class="col-md-12" id="gallery_div_img">
                                                    <!-- ko foreach: {data: multiFileData().dataURLArray, as: 'dataURL'} -->
                                                    <img style="height: 100px; margin: 5px;" class="img-rounded  thumb" data-bind="attr: { src: dataURL }, visible: dataURL">
                                                    <!-- /ko -->
                                                    <div data-bind="ifnot: fileData().dataURL">
                                                        <label class="drag-label"></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" hidden>
                                                    <input style="display: none" id="txt_galleryImages" type="file" name="txt_galleryImages[]" multiple data-bind="fileInput: multiFileData, customFileInput: {
                                                      buttonClass: 'btn btn-success',
                                                      fileNameClass: 'disabled form-control',
                                                      onClear: onClear,
                                                      onInvalidFileDrop: onInvalidFileDrop
                                                    }" accept="image/*">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-md btn-color float-right">Submit</button>
                                    <a href="<?= base_url("pages/my_listing"); ?>" class="btn btn-md btn-secondary float-right mr-4">Discard</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- User page end -->

<?= $this->include("Includes/image_upload_script") ?>
<?= $this->include("Includes/multiple_selection_script") ?>
<?= $this->include("Includes/category_ajax") ?>

<script type="text/javascript">

      $('#summernote_desc').summernote({
        placeholder: 'Write description here...',
        tabsize: 2,
        height: 300,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ],
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
      });

      $('#summernote_about').summernote({
        placeholder: 'Write about here...',
        tabsize: 2,
        height: 300,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ],
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
      });

</script>