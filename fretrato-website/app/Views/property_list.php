
<?= $this->include("Includes/sub_banner") ?>

<!-- properties list rightside start -->
<div class="properties-list-rightside content-area-2">
    <form action="<?= base_url('property_list') ?>" method="GET">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="option-bar">
                    <div class="row clearfix">
                        <div class="col-xl-4 col-lg-5 col-md-5 col-sm-5 col-3">
                            <h4>
                                <span class="heading-icon">
                                    <i class="fa fa-caret-right icon-design"></i>
                                    <i class="fa fa-th-list"></i>
                                </span>
                                <span class="heading">Properties List</span>
                            </h4>
                        </div>
                        <div class="col-xl-8 col-lg-7 col-md-7 col-sm-7 col-9">
                            <div class="search-area mr-3">
                                    <select class="selectpicker search-fields" name="order_by" onchange="$('#hidden_btn').click();">
                                        <option <?= (isset($_GET['order_by']) ? ($_GET['order_by'] == "Latest" ? "selected" : "") : "selected") ?>>Latest</option>
                                        <option value="DESC" <?= (isset($_GET['order_by']) ? ($_GET['order_by'] == "DESC" ? "selected" : "") : "") ?>>High to Low</a></option>
                                        <option value="ASC" <?= (isset($_GET['order_by']) ? ($_GET['order_by'] == "ASC" ? "selected" : "") : "") ?>>Low to High</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $this->include("Includes/results_ctr");  ?>
            <?php 
                if (count($propertyDatabd) > 0) {
                    foreach ($propertyDatabd as $i => $row) { 

                        if ( (session()->has("User") && (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2)) || $row['agent_role'] == 1 || $row['agent_role'] == 2 || $row['agent_info'] == 1) {
                            $agent_name = $row['agent_name'] . " / <i class='fa fa-phone'></i> " . $row['agent_contact'];
                        }
                        else{
                            a:
                            $agent_name = explode(" ", $row['agent_name']); 
                            $agent_name = $agent_name[0];
                            
                        }

            ?>

                    <div class="property-box-5">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-pad">
                                <div class="property-thumbnail">
                                    <?php $offer_type = (strtolower($row['offer_type']) == "rental") ? "For Lease" : ((strtolower($row['offer_type']) == "resale") ? "For Sale" : "Pre-selling"); ?>
                                    <a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>" class="property-img" style="max-height: 250px">
                                <?php if ($row['toogleFeature'] == "1") { ?>
                                        <div class="listing-badges">
                                            <span class="featured">Featured</span>
                                        </div>
                                <?php } ?>
                                        <div class="tag-for"><?= $offer_type ?></div>
                                        <div class="price-ratings-box">
                                            <p class="price" style="color: #DEBF49">
                                                ₱<?= number_format($row['price'], 2) ?>
                                            </p>
                                        </div>
                                        <img src="<?= 
                                        (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['img_directory'] . "/" . 'thumbnail_medium-' . $row['img_filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['img_directory'] . 'thumbnail_medium-' . $row['img_filename'] 
                                        : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="property-box-5" class="img-fluid">
                                        <div class="contact-property-list" <?= ($row['taken'] != 0 ? " " : "hidden") ?>>
                                            <div class="ml-4" style="font-floor_area: 10px; width: 45%;">
                                                OCCUPIED    <br>
                                                START DATE: <?= date("F d, Y", strtotime($row['contract_start'])) ?><br>
                                                END DATE: <?= date("F d, Y", strtotime($row['contract_end'])) ?>
                                            </div>
                                        </div>  
                                    </a>
                                    <div class="property-overlay">
                                        <a class="overlay-link property-video" title="View Gallery" onclick="property_modal(`<?= $row['slug'] ?>`, `<?= str_replace(" ", "-", $offer_type) ?>`, `<?= $row['name'] ?>`, `<?= $row['location'] ?>`, `<?= $row['property_vid_url'] ?>`, `<?= number_format($row['price'], 2) ?>`, `<?= $row['floor_area'] ?>`, `<?= $row['bedroom'] ?>`, `<?= $row['floor_level'] ?>`, `<?= $row['furnish_type'] ?>`, `<?= $row['parking'] ?>`, `<?= $agent_name ?>`, `<?= ($row['taken'] != 0 ? date('F d, Y', strtotime($row['contract_start'])) : null) ?>`, `<?= ($row['taken'] != 0 ? date('F d, Y', strtotime($row['contract_end'])) : null) ?>`)">
                                            <i class="fa fa-image"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 align-self-center col-pad">
                                <div class="detail">
                                    <!-- title limit-text -->
                                    <h1 class="title"> 
                                        <a title="<?= $row['name'] ?>" href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>"><?= $row['name'] ?></a>
                                    </h1>
                                    <!-- Location limit-text -->
                                    <div class="location">
                                        <a title="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">
                                            <i class="fa fa-map-marker"></i><?= $row['location']; ?>
                                        </a>
                                    </div>
                                    <!-- Paragraph -->
                                    <p><?php //word_limiter(strip_tags($row['description']), 25) ?></p>
                                    <!--  Facilities list -->
                                    <ul class="facilities-list clearfix">

                                        <?php if ($row['property_type'] != 6 && ( $row['floor_area'] != null || $row['floor_area'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['floor_area'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] == 2 || $row['property_type'] == 6 && ( $row['land_area'] != null || $row['land_area'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['land_area'] ?>
                                            </li>
                                        
                                        <?php } ?>

                                        <?php if ($row['property_type'] != 6  && ( $row['floor_level'] != null || $row['floor_level'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i>Floor Level: <?= number_format($row['floor_level']) ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] != 4 && $row['property_type'] != 6 ) && ( $row['furnish_type'] != null || $row['furnish_type'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-balcony-and-door"></i><?= $row['furnish_type'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] != 6 && $row['parking'] > 0) { ?>

                                            <li>
                                                <i class="flaticon-car-repair"></i>Parking: <?= $row['parking'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] == 1 || $row['property_type'] == 2 ) && ( $row['bedroom'] != null || $row['bedroom'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-bed"></i> <?= $row['bedroom'] ?> Bedroom
                                            </li>

                                        <?php } ?>

                                        <?php if ( ($row['property_type'] == 1 || $row['property_type'] == 2) && ( $row['bathroom'] != null || $row['bathroom'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-bath"></i> <?= $row['bathroom'] ?> Bathroom
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] == 2 || $row['property_type'] == 6 ) && ( $row['condition'] != null || $row['condition'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> <?= ($row['property_type'] == 6 ? "Land" : "") ?> Condition: <?= $row['condition'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( $row['property_type'] == 2 && ($row['dateFinished'] < "01-01-2000") ) { ?>

                                            <li>
                                                <i class="flaticon-calendar"></i> Date Finished: <?= date("F d, Y", strtotime($row['dateFinished'])) ?>
                                            </li>
                                        
                                        <?php } ?>

                                        <?php if ($row['property_type'] == 1 && ( $row['stayMonth'] != null || $row['stayMonth'] != "" ) && $row['stayMonth'] != 0 ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayMonth'] ?> Month(s)
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] == 1 && ( $row['stayYear'] != null || $row['stayYear'] != "" ) && $row['stayYear'] != 0 ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayYear'] ?> Year(s)
                                            </li>

                                        <?php } ?>

                                        <li>
                                            <i class="fa fa-user"></i> 
                                                <?= $agent_name ?>
                                        </li>
                                    </ul>
                                    <br>
                                    <a class="btn btn-color btn-md w-100" title="<?= $row['name'] ?>" href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>">Learn More</a>
                                </div>
                            </div>
                                
                        </div>
                    </div>
            <?php       }
                    }
                    else{
                        ?>
                            <div class="alert alert-warning">
                                <div>
                                    <strong>No Results Found! </strong>
                                </div>
                            </div>
                        <?php
                    }
             ?>
            
            <?= $this->include("Includes/pagination") ?>

            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    <!-- Search area start -->
                    <div class="widget search-area">
                        <?= $this->include("Includes/property_search_sidebar") ?>
                    </div>
                </form>
                    <!-- Categories start -->
                    <div class="widget categories">
                        <h5 class="sidebar-title">Categories</h5>
                        <ul>
                            <?php foreach ($main_categories_ctr as $row) { ?>
                                <li><a href="<?= base_url('property_list?type=' . $row['id']) ?>"><?= $row['name'] ?><span>(<?= $row['property_ctr'] ?>)</span></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>

                        <?= $this->include("Includes/recent_properties") ?>

                    </div>
                    <?= $this->include("Includes/follow_us_sidebar") ?>
                    <?= $this->include("Includes/helping_center_sidebar") ?>
                    <?= $this->include("Includes/contact_us_sidebar") ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- properties list rightside end -->

<?= $this->include("Includes/property_modal") ?>
<?= $this->include("Includes/category_ajax") ?>