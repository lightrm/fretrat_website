<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Fretrato - Real Estate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/magnific-popup.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/jquery.selectBox.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/dropzone.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/rangeslider.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/animate.min.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/leaflet.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/slick.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/slick-theme.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/slick-theme.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/map.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/jquery.mCustomScrollbar.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/fonts/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/fonts/flaticon/font/flaticon.css">

    <?php 
      if( isset($hdcss) ):
        foreach ($hdcss as $dir): ?>
    <link rel="stylesheet" href="<?= base_url($pluginDir.'/'.$dir); ?>">
    <?php endforeach; endif; ?>

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?= base_url($pluginDir); ?>/fretrato/Images/favicon.png" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="<?= base_url($pluginDir); ?>/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url($pluginDir); ?>/css/skins/default.css">

    <?php 
      if( isset($hdjs) ):
        foreach ($hdjs as $dir): ?>
    <script src="<?= base_url($pluginDir.'/'.$dir); ?>" type="text/javascript"></script>
    <?php endforeach; endif; ?>

</head>
<body id="top">

    <!-- Contact section start -->
    <div class="contact-section" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/office.jpg"; ?>')">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="login-inner-form">
                        <div class="details">
                            <a href="<?= base_url(); ?>">
                                <img src="<?= base_url($pluginDir); ?>/fretrato/Images/fretrato-logo-transparent.png" alt="logo" style="width: 150px; height: 70px">
                            </a>
                            <h3><?= $header_msg ?><br> <?= $body_msg ?></h3>
                            <a class="btn btn-color" href="<?= base_url($url_link) ?>"><?= $url_txt ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact section end -->

<!-- External JS libraries -->
<script src="<?= base_url($pluginDir); ?>/js/jquery-2.2.0.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/popper.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/bootstrap.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/jquery.selectBox.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/rangeslider.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/jquery.filterizr.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/wow.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/backstretch.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/jquery.countdown.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/jquery.scrollUp.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/particles.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/typed.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/dropzone.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/jquery.mb.YTPlayer.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/leaflet.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/leaflet-providers.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/leaflet.markercluster.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/slick.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/maps.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?= base_url($pluginDir); ?>/js/ie-emulation-modes-warning.js"></script>
<!-- Custom JS Script -->
<script  src="<?= base_url($pluginDir); ?>/js/app.js"></script>

<?php 
  if( isset($ftjs) ):
    foreach ($ftjs as $object): ?>
      <?php if (is_string($object)) { ?>
        <script src="<?= base_url($pluginDir.'/'.$object); ?>" type="text/javascript"></script>
      <?php } elseif (is_array($object)) { ?>
        <script src="<?= base_url($pluginDir.'/'.$object['dir']); ?>" <?php array_map(function($index, $data){ echo $index.'="'.$data.'"'; }, array_keys($object['attr']), $object['attr']); ?>></script>
      <?php } ?>
<?php endforeach; endif; ?>

</body>
</html>

