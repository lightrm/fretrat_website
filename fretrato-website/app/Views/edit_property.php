
<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/office.jpg"; ?>')">
    <div class="container">
        <div class="breadcrumb-area">
            <h1><?= $banner_title ?></h1>
            <ul class="breadcrumbs">
                <li><a href="<?= base_url(); ?>">Home</a></li>
                <?php 
                    $i = 1;
                    foreach ($breadCrumps as $htmlval => $link): ?>
                    <li class="active">

                        <?php if ($i != count($breadCrumps)) { ?>
                                <a href="<?= $link ?>">
                        <?php } ?>

                            <?= $htmlval ?>
                        
                        <?php if ($link) { ?>
                                </a>
                        <?php } ?>

                    </li>
            <?php   $i++;
                    endforeach ?>
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

<!-- User page start -->
<div class="user-page submit-property content-area-7">
    <div class="container">
    <?= $this->include("Includes/alert"); ?>
        <div class="row">
            <div class="col-lg-12">

                <div class="search-area contact-2">
                    <div class="search-area-inner">
                        <div class="search-contents ">
                            <form action="<?= base_url("functions/edit_property/" . $property['id']); ?>" method="POST" enctype='multipart/form-data'>
                                <input type="hidden" name="txt_hidden_img_session" value="<?= $property['img_session'] ?>">
                                <h3 class="heading-3">Basic Information</h3>
                                <div class="row mb-30">
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Property Title <i style="color: red">*</i></label>
                                            <input type="text" name="txt_name" class="form-control" placeholder="Property Title" value="<?= $property['name'] ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Offer Type <i style="color: red">*</i></label>
                                            <select class="selectpicker search-fields" name="txt_propertyStatus" required>
                                                <option <?= ( $property['offer_type'] == null ? "selected" : "") ?> disabled>Offer Type</option>
                                                <option <?= ( $property['offer_type'] == "resale" ? "selected" : "") ?> value="resale">For Sale</option>
                                                <option <?= ( $property['offer_type'] == "rental" ? "selected" : "") ?> value="rental">For Lease</option>
                                                <option <?= ( $property['offer_type'] == "pre-selling" ? "selected" : "") ?> value="pre-selling">Pre-selling</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Property Type <i style="color: red">*</i></label>
                                            <select class="selectpicker search-fields" name="txt_propertyType" id="main_categ" required>
                                                <option <?= ( $property['property_type'] == null ? "selected" : "") ?> disabled>Property Type</option>
                                        <?php foreach ($main_categories as $row) { ?>
                                                    <option <?= ( $property['property_type'] == $row['id'] ? "selected" : "") ?> value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                        <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Sub Category </label>
                                            <select class="selectpicker search-fields" name="txt_subCategory" id="sub_categ" required>
                                                <option <?= ( $property['subcategory'] == null ? "selected" : "") ?> disabled>Sub Category</option>
                                                <?php 
                                                    if (isset($sub_categories)) {
                                                        foreach ($sub_categories as $row) { ?>
                                                            <option value="<?= $row['id'] ?>" <?= ( $property['subcategory'] == $row['id'] ? "selected" : "") ?>><?= $row['sub_categ_name'] ?></option>
                                                <?php   } 
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Price <i style="color: red">*</i></label>
                                            <input type="number" step="any" name="txt_price" class="form-control" placeholder="PHP" required value="<?= $property['price'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Area/Size <i style="color: red">*</i></label>
                                            <input type="number" step="any" name="txt_size" class="form-control" placeholder="Sqm" required value="<?= $property['size'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Floor Level <i style="color: red">*</i></label>
                                            <input type="number" name="txt_floor_level" class="form-control" placeholder="Floor Level" required value="<?= $property['floor_level'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Bedrooms <i style="color: red">*</i></label>
                                            <input type="number" name="txt_bedroom" class="form-control" placeholder="Bedrooms" value="<?= $property['bedroom'] ?>" required>
                                            <?php /* <select class="selectpicker search-fields" name="txt_bedroom" required>
                                                <option <?= ( $property['bedroom'] == null ? "selected" : "") ?> disabled>Bedrooms</option>
                                            <?php for ($i=1; $i <= 10; $i++) { ?>
                                                    <option <?= ( $property['bedroom'] == $row ? "selected" : "") ?>><?= $i ?></option>
                                            <?php } ?>
                                            </select> */ ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Bathroom <i style="color: red">*</i></label>
                                            <input type="number" name="txt_bathroom" class="form-control" placeholder="Bathroom" value="<?= $property['bathroom'] ?>" required>
                                            <?php /* <select class="selectpicker search-fields" name="txt_bathroom" required>
                                                <option <?= ( $property['bathroom'] == null ? "selected" : "") ?> disabled>Bathrooms</option>
                                            <?php for ($i=1; $i <= 5; $i++) { ?>
                                                    <option <?= ( $property['bathroom'] == $row ? "selected" : "") ?>><?= $i ?></option>
                                            <?php } ?>
                                            </select> */ ?>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Additional Information</h3>
                                <div class="row mb-30">
                                    <div class="col-md-12 mb-5">
                                        <label>Insert Video Code (Get the code of the youtube video Eg.:</label><br>
                                        <label>https://www.youtube.com/watch?v=<strong>kOHB85vDuow</strong> highlighted text is the video code.) </label>
                                        <input type="text" name="txt_video" class="form-control" placeholder="Insert Video Code" value="<?= $property['property_vid_url'] ?>">
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-check checkbox-theme">
                                                <input class="form-check-input" type="checkbox" value="1" name="txt_parking" id="parking" <?= ( $property['parking'] == 1 ? "checked" : "") ?>>
                                                <label class="form-check-label" for="parking">
                                                    Parking
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-check checkbox-theme">
                                                <input class="form-check-input" type="checkbox" value="1" name="txt_furnished" id="Furnished" <?= ( $property['is_furnished'] == 1 ? "checked" : "") ?>>
                                                <label class="form-check-label" for="Furnished">
                                                    Furnished
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Location</h3>
                                <div class="row mb-30">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Street Name <i style="color: red">*</i></label>
                                            <input type="text" name="txt_streetName" class="form-control" placeholder="Address" required value="<?= $property['street'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Barangay </label>
                                            <input type="text" name="txt_barangay" class="form-control" placeholder="Barangay" value="<?= $property['barangay'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>City <i style="color: red">*</i></label>
                                            <input type="text" name="txt_city" class="form-control" placeholder="City" required value="<?= $property['city'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Province </label>
                                            <input type="text" name="txt_province" class="form-control" placeholder="Province" value="<?= $property['province'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Country </label>
                                            <input type="text" name="txt_country" class="form-control" placeholder="Country" value="<?= $property['country'] ?>">
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Detailed Information</h3>
                                <div class="row mb-30">
                                    <div class="col-lg-12">
                                        <div class="form-group message">
                                            <label>Description <i style="color: red">*</i></label>
                                            <textarea id="summernote_desc" name="txt_description"><?= $property['description'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group message">
                                            <label>About <i style="color: red">*</i></label>
                                            <textarea id="summernote_about" name="txt_about"><?= $property['about'] ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="heading-3">Features (optional)</h3>
                                <div class="row mb-30">

                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <div id="myselect">
                                              <select id="multiselect" class="form-control" multiple name="amenities[]">

                                                <?php 
                                                    $new_amenities_property = array();
                                                    foreach ($amenities_property as $row) {
                                                        $new_amenities_property[] = $row['amenity_property_id'];
                                                    }

                                                    foreach ($amenities as $row) { ?>
                                                    <option value="<?= $row['id'] ?>" <?= (in_array($row['id'], $new_amenities_property) ? "selected" : "") ?>><?= $row['amenity_name'] ?></option>
                                                <?php } ?>                   

                                              </select>
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>

                                
                                <h3 class="heading-3">Thumbnail</h3>
                                <div class="row mb-45">
                                    <div class="col-lg-5" style="margin-right: auto; margin-left: auto;">

                                        <label>Current Thumbnail</label>

                                            <div class="carousel properties-details-sliders slide mb-60">

                                                <!-- main slider carousel items -->
                                                <div class="carousel-inner">

                                                    <img class="d-block w-100 h-100" src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$thumbnail['directory'] . "/" . 'thumbnail_medium-' . $thumbnail['filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $thumbnail['directory'] . 'original-' . $thumbnail['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" alt="banner">
                                                    <small>Note: Uploading another thumbnail will replace the current thumbnail.</small>

                                                </div>

                                            </div>


                                    </div>

                                    <div class="col-lg-12">

                                        <div id="thumbnail_div" class="dropzone dropzone-design" data-bind="fileDrag: fileData" style="cursor: pointer;">
                                            <div id="thumbnail_div_msg" class="dz-default dz-message"><span>Drop file here to upload</span></div>
                                            <div class="form-group row">
                                                <div class="col-md-12" id="thumbnail_div_img">
                                                    <img style="height: 125px;" class="img-rounded  thumb" data-bind="attr: { src: fileData().dataURL }, visible: fileData().dataURL">
                                                    <div data-bind="ifnot: fileData().dataURL">
                                                        <label class="drag-label"></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" hidden>
                                                    <input style="display: none" id="txt_thumbnail"  type="file" name="txt_thumbnail" data-bind="fileInput: fileData, customFileInput: {
                                                      buttonClass: 'btn btn-success',
                                                      fileNameClass: 'disabled form-control',
                                                      onClear: onClear,
                                                      onInvalidFileDrop: onInvalidFileDrop
                                                    }" accept="image/*">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                                <h3 class="heading-3">Property Gallery</h3>
                                <div class="row mb-45">
                                    <div class="col-lg-5" style="margin-right: auto; margin-left: auto;">

                                        <label>Current Gallery (<?= count($gallery) ?>)</label>


                                            <div id="propertiesDetailsSlider" class="carousel properties-details-sliders slide mb-60">

                                                <!-- main slider carousel items -->
                                                <div class="carousel-inner">

                                                <?php 
                                                    if (count($gallery) > 0) {
                                                        foreach ($gallery as $i => $row) { ?>

                                                            <div class="<?= ($i == 0 ? "active" : "") ?> item carousel-item" data-slide-number="<?= $i ?>">
                                                                <img src="<?= (file_exists(ROOTPATH."public/assets/fretrato/Images/".$row['directory'] . "/" . 'thumbnail_medium-' . $row['filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $row['directory'] . 'original-' . $row['filename'] : base_url($pluginDir) . '/img/no_image.png'); ?>" class="img-fluid" alt="property-<?= $i ?>" style="min-height: 250px; max-height: 250px">

                                                                <div class="" >
                                                                    <div class="col-md-12 form-group" style="position: absolute; z-index: 1000; bottom: -20px; left: 0px; background: black; padding-top: 10px">
                                                                        <div class="form-check checkbox-theme">
                                                                            <input class="form-check-input" type="checkbox" value="1" name="txt_image_toggle_<?= $row['id'] ?>" id="id_image_toggle_<?= $row['id'] ?>" checked>
                                                                            <label class="form-check-label" for="id_image_toggle_<?= $row['id'] ?>">
                                                                                
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                <?php   }
                                                    }
                                                    else{
                                                    ?>

                                                        <div class="active item carousel-item" data-slide-number="0">
                                                            <img src="<?= base_url($pluginDir) . '/img/no_image.png'; ?>" class="img-fluid" alt="property-0" style="min-height: 250px; max-height: 250px">
                                                        </div>

                                                    <?php
                                                    }

                                                ?>

                                                    <a <?= (count($gallery) > 0) ? "" : "hidden" ?> class="carousel-control left" href="#propertiesDetailsSlider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                                    <a <?= (count($gallery) > 0) ? "" : "hidden" ?> class="carousel-control right" href="#propertiesDetailsSlider" data-slide="next"><i class="fa fa-angle-right"></i></a>

                                                </div>
                                                <!-- main slider carousel nav controls -->
                                                <small>Note: Uncheck the box to delete image from the gallery.</small>
                                            </div>
                                    </div>

                                    <div class="col-lg-12">

                                        <div id="gallery_div" class="dropzone dropzone-design" data-bind="fileDrag: multiFileData"  style="cursor: pointer;">
                                            <div id="gallery_div_msg" class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                            <div class="form-group row">
                                                <div class="col-md-12" id="gallery_div_img">
                                                    <!-- ko foreach: {data: multiFileData().dataURLArray, as: 'dataURL'} -->
                                                    <img style="height: 100px; margin: 5px;" class="img-rounded  thumb" data-bind="attr: { src: dataURL }, visible: dataURL">
                                                    <!-- /ko -->
                                                    <div data-bind="ifnot: fileData().dataURL">
                                                        <label class="drag-label"></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" hidden>
                                                    <input style="display: none" id="txt_galleryImages" type="file" name="txt_galleryImages[]" multiple data-bind="fileInput: multiFileData, customFileInput: {
                                                      buttonClass: 'btn btn-success',
                                                      fileNameClass: 'disabled form-control',
                                                      onClear: onClear,
                                                      onInvalidFileDrop: onInvalidFileDrop
                                                    }" accept="image/*">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-md btn-color float-right">Submit</button>
                                    <a href="<?= base_url("pages/my_listing"); ?>" class="btn btn-md btn-secondary float-right mr-4">Discard</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- User page end -->

<?= $this->include("Includes/image_upload_script") ?>
<?= $this->include("Includes/category_ajax") ?>
<?= $this->include("Includes/multiple_selection_script") ?>

<script type="text/javascript">

      $('#summernote_desc').summernote({
        placeholder: 'Write description here...',
        tabsize: 2,
        height: 300,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ],
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
      });

      $('#summernote_about').summernote({
        placeholder: 'Write about here...',
        tabsize: 2,
        height: 300,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ],
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
      });

</script>