
<!-- Banner start -->

<div class="banner" id="banner">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
        <?php foreach ($propertyDatabd as $i => $row) { ?>

                <div class="carousel-item <?= ($i == 0 ? "active" : "") ?> item-bg">
                    <img class="d-block" style="object-fit: cover; width: 100%; height: 100%;" src="/assets/fretrato/Images/<?= $row['slider_directory'] . 'original-' . $row['slider_filename'] ?>" alt="banner">
                    <div class="carousel-caption banner-slider-inner d-flex h-100 text-left">
                        <div class="carousel-content container b1-inner">
                            <div class="banner-text-box">
                                <?php $offer_type = (strtolower($row['offer_type']) == "rental") ? "For Lease" : ((strtolower($row['offer_type']) == "resale") ? "For Sale" : "Pre-selling"); ?>
                                <div class="plan-price" data-animation="animated fadeInDown delay-05s" style="color: #DEBF49"><sup style="color: #DEBF49">₱</sup><?= number_format($row['price'], 2) ?><span></span></div>
                                <h3 data-animation="animated fadeInUp delay-05s" title="<?= $row['name'] ?>"><a href="<?= base_url(str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug']) ?>" style="color: #FFFFFF"><?= $row['name'] ?></a></h3>
                                <div class="meta" data-animation="animated fadeInUp delay-05s">
                                    <ul>
                                    
                                        <?php if ($row['property_type'] != 6 && ( $row['floor_area'] != null || $row['floor_area'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['floor_area'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] == 2 || $row['property_type'] == 6 && ( $row['land_area'] != null || $row['land_area'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['land_area'] ?>
                                            </li>
                                        
                                        <?php } ?>

                                        <?php if ($row['property_type'] != 6  && ( $row['floor_level'] != null || $row['floor_level'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i>Floor Level: <?= number_format($row['floor_level']) ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] != 4 && $row['property_type'] != 6 ) && ( $row['furnish_type'] != null || $row['furnish_type'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-balcony-and-door"></i><?= $row['furnish_type'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] != 6 && $row['parking'] > 0) { ?>

                                            <li>
                                                <i class="flaticon-car-repair"></i>Parking: <?= $row['parking'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] == 1 || $row['property_type'] == 2 ) && ( $row['bedroom'] != null || $row['bedroom'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-bed"></i> <?= $row['bedroom'] ?> Bedroom
                                            </li>

                                        <?php } ?>

                                        <?php if ( ($row['property_type'] == 1 || $row['property_type'] == 2) && ( $row['bathroom'] != null || $row['bathroom'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-bath"></i> <?= $row['bathroom'] ?> Bathroom
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] == 2 || $row['property_type'] == 6 ) && ( $row['condition'] != null || $row['condition'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> <?= ($row['property_type'] == 6 ? "Land" : "") ?> Condition: <?= $row['condition'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( $row['property_type'] == 2 && ($row['dateFinished'] < "01-01-2000") ) { ?>

                                            <li>
                                                <i class="flaticon-calendar"></i> Date Finished: <?= date("F d, Y", strtotime($row['dateFinished'])) ?>
                                            </li>
                                        
                                        <?php } ?>

                                        <?php if ($row['property_type'] == 1 && ( $row['stayMonth'] != null || $row['stayMonth'] != "" ) && $row['stayMonth'] != 0 ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayMonth'] ?> Month(s)
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] == 1 && ( $row['stayYear'] != null || $row['stayYear'] != "" ) && $row['stayYear'] != 0 ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayYear'] ?> Year(s)
                                            </li>

                                        <?php } ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        <?php } ?>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="slider-mover-left" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="slider-mover-right" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </span>
        </a>
        <div class="btn-secton btn-secton-2">
            <ol class="carousel-indicators">
            <?php for ($i=0; $i < count($propertyDatabd); $i++) { ?>

                    <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>" class="<?= ($i == 0 ? "active" : "") ?>"></li>

            <?php } ?>
            </ol>
        </div>
    </div>

    <!-- Search area start -->
    <div class="search-area search-area-3 d-none d-xl-block d-lg-block">
        <h2>Find a Property</h2>
        <p>That is perfect for you</p>
        <div class="search-area-inner">
            <div class="search-contents">
                <form action="/property_list" method="GET" id="frm_find_destination">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Type Property Name or Location" name="search">
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="status">
                            <option selected disabled>Property Status</option>
                            <option value="resale">For Sale</option>
                            <option value="rental">For Lease</option>
                            <option value="pre-selling">Pre-selling</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="type" id="main_categ">
                            <option selected disabled>Property Types</option>
                        <?php foreach ($main_categories as $row) { ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="subCategory" id="sub_categ">
                            <option selected disabled>Sub Category</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Type Village or City" name="location">
                    </div>
                    <?php /*
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="location">
                            <option selected disabled>Location</option>
                            <?php foreach ($locations as $row) { ?>
                                    <option><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    */?>
                    <div class="form-group">
                        <button type="submit" class="search-button btn-md btn-color">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Search area end -->
</div>

<!-- Search area start -->
<div class="search-area sa-show" id="search-area-1">
    <div class="container">
        <div class="search-area-inner">
            <div class="search-contents ">
                <form action="/property_list" method="GET" id="frm_find_destination1">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Type Property Name or Location" name="search">
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="status">
                            <option selected disabled>Property Status</option>
                            <option value="resale">For Sale</option>
                            <option value="rental">For Lease</option>
                            <option value="pre-selling">Pre-selling</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="type" id="main_categ1">
                            <option selected disabled>Property Types</option>
                        <?php foreach ($main_categories as $row) { ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="subCategory" id="sub_categ1">
                            <option selected disabled>Sub Category</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Type Village or City" name="location">
                    </div>
                    <?php /*
                    <div class="form-group">
                        <select class="selectpicker search-fields" name="location">
                            <option selected disabled>Location</option>
                            <?php foreach ($locations as $row) { ?>
                                    <option><?= $row ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    */?>
                    <div class="form-group">
                        <button type="submit" class="search-button btn-md btn-color">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Search area end -->
<!-- banner end -->
    <div class="featured-properties mt-5">
        <div class="container">
            <div class="main-title">
                <h1>Featured Properties</h1>
                <ul class="list-inline-listing filters filteriz-navigation" hidden>
                    <li class="btn filtr-button filtr active" data-filter="all">All</li>
                    <li data-filter="1" class="btn btn-inline filtr-button filtr">Condominium</li>
                    <li data-filter="2" class="btn btn-inline filtr-button filtr">House and Lot</li>
                    <li data-filter="3" class="btn btn-inline filtr-button filtr">Office</li>
                    <li data-filter="4" class="btn btn-inline filtr-button filtr">Commercial</li>
                </ul>
            </div>
            <div class="row filter-portfolio wow fadeInUp delay-04s">
                <!-- <div class="cars"> -->

                    <?php foreach ($propertyDatabd as $i => $row) { 

                                if ( (session()->has("User") && (session()->get("User")['role'] == 1 || session()->get("User")['role'] == 2)) || ($row['agent_role'] == 1 || $row['agent_role'] == 2 || $row['agent_info'] == 1) ) {
                                    $agent_name = $row['agent_name'] . " / <i class='fa fa-phone'></i> " . $row['agent_contact'];
                                } else {
                                    a:
                                    $agent_name = explode(" ", $row['agent_name']); 
                                    $agent_name = $agent_name[0];
                                    
                                }

                                $offer_type = (strtolower($row['offer_type']) == "rental") ? "For Lease" : ((strtolower($row['offer_type']) == "resale") ? "For Sale" : "Pre-selling");
                    ?>  
                            <div class="col-lg-4 col-md-6 col-sm-12" data-category="<?= $row['property_type'] ?>">
                                <div class="property-box-7">
                                    <div class="property-thumbnail">
                                        <a href="/<?= str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug'] ?>" class="property-img" style="">
                                            <div class="tag-2"><?= $offer_type ?></div>
                                            <div class="price-box"><span style="color: #DEBF49">₱<?= number_format($row['price'], 2) ?></span> </div>
                                            <img src="/assets/fretrato/Images/<?=  $row['img_directory'] . 'original-' . $row['img_filename'] ?>" alt="property-box-7" class="img-fluid" style="min-height: 300px; max-height: 300px; object-fit: cover">
                                            <div class="contact-property-list" <?= ($row['taken'] != 0 ? " " : "hidden") ?>>
                                                <div class="div-val">
                                                    OCCUPIED    <br>
                                                    START DATE: <?= date("F d, Y", strtotime($row['contract_start'])) ?><br>
                                                    END DATE: <?= date("F d, Y", strtotime($row['contract_end'])) ?>
                                                </div>
                                            </div>  
                                        </a>
                                        
                                        <div class="property-overlay">
                                            <a class="overlay-link property-video" title="View Gallery" onclick="property_modal(`<?= $row['slug'] ?>`, `<?= str_replace(" ", "-", $offer_type) ?>`, `<?= $row['name'] ?>`, `<?= $row['location'] ?>`, `<?= $row['property_vid_url'] ?>`, `<?= number_format($row['price'], 2) ?>`, `<?= $row['floor_area'] ?>`, `<?= $row['bedroom'] ?>`, `<?= $row['floor_level'] ?>`, `<?= $row['furnish_type'] ?>`, `<?= $row['parking'] ?>`, `<?= $agent_name ?>`, `<?= ($row['taken'] != 0 ? date('F d, Y', strtotime($row['contract_start'])) : null) ?>`, `<?= ($row['taken'] != 0 ? date('F d, Y', strtotime($row['contract_end'])) : null) ?>`)">
                                                <i class="fa fa-image"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="detail">
                                        <!-- limit-text -->
                                        <h1 class="title">
                                            <a href="/<?= str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug'] ?>" title="<?= $row['name'] ?>"><?= $row['name'] ?></a>
                                        </h1>
                                        <div class="location">
                                            <a href="/<?= str_replace(" ", "-", $offer_type) . "/property_view/" . $row['slug'] ?>" title="<?= $row['location'] ?>">
                                                <i class="flaticon-facebook-placeholder-for-locate-places-on-maps"></i><?= $row['location']; ?>
                                            </a>
                                        </div>
                                    </div>
                                    <ul class="facilities-list clearfix">

                                        <?php if ($row['property_type'] != 6 && ( $row['floor_area'] != null || $row['floor_area'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['floor_area'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] == 2 || $row['property_type'] == 6 && ( $row['land_area'] != null || $row['land_area'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-square-layouting-with-black-square-in-east-area"></i> Sqm: <?= $row['land_area'] ?>
                                            </li>
                                        
                                        <?php } ?>

                                        <?php if ($row['property_type'] != 6  && ( $row['floor_level'] != null || $row['floor_level'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i>Floor Level: <?= number_format($row['floor_level']) ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] != 4 && $row['property_type'] != 6 ) && ( $row['furnish_type'] != null || $row['furnish_type'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-balcony-and-door"></i><?= $row['furnish_type'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] != 6 && $row['parking'] > 0) { ?>

                                            <li>
                                                <i class="flaticon-car-repair"></i>Parking: <?= $row['parking'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] == 1 || $row['property_type'] == 2 ) && ( $row['bedroom'] != null || $row['bedroom'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-bed"></i> <?= $row['bedroom'] ?> Bedroom
                                            </li>

                                        <?php } ?>

                                        <?php if ( ($row['property_type'] == 1 || $row['property_type'] == 2) && ( $row['bathroom'] != null || $row['bathroom'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-bath"></i> <?= $row['bathroom'] ?> Bathroom
                                            </li>

                                        <?php } ?>

                                        <?php if ( ( $row['property_type'] == 2 || $row['property_type'] == 6 ) && ( $row['condition'] != null || $row['condition'] != "" ) ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> <?= ($row['property_type'] == 6 ? "Land" : "") ?> Condition: <?= $row['condition'] ?>
                                            </li>

                                        <?php } ?>

                                        <?php if ( $row['property_type'] == 2 && ($row['dateFinished'] < "01-01-2000") ) { ?>

                                            <li>
                                                <i class="flaticon-calendar"></i> Date Finished: <?= date("F d, Y", strtotime($row['dateFinished'])) ?>
                                            </li>
                                        
                                        <?php } ?>

                                        <?php if ($row['property_type'] == 1 && ( $row['stayMonth'] != null || $row['stayMonth'] != "" ) && $row['stayMonth'] != 0 ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayMonth'] ?> Month(s)
                                            </li>

                                        <?php } ?>

                                        <?php if ($row['property_type'] == 1 && ( $row['stayYear'] != null || $row['stayYear'] != "" ) && $row['stayYear'] != 0 ) { ?>

                                            <li>
                                                <i class="flaticon-hotel-building"></i> Stay in <?= $row['stayYear'] ?> Year(s)
                                            </li>

                                        <?php } ?>

                                    </ul>
                                    <div class="footer clearfix">
                                        <div class="pull-left days">
                                            <p><i class="fa fa-user"></i> 
                                                <?= $agent_name ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php } ?>

                <!-- </div> -->
            </div>
        </div>
    </div>

    <div class="services-3 content-area-20 bg-white">
        <div class="container">
            <div class="main-title">
                <h1>What Are you Looking For?</h1>
                <p><?= $WAYLF_html['value'] ?></p>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 s-brd-2 wow fadeInLeft delay-04s">
                    <a href="/property_list?type=1">
                        <i class="flaticon-hotel-building"></i>
                        <h5>Condominium</h5>
                        <p><?= $WAYLF_AC_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 s-brd-1 wow fadeInUp delay-04s">
                    <a href="/property_list?type=2">
                        <i class="flaticon-house"></i>
                        <h5>House and Lot</h5>
                        <p><?= $WAYLF_H_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 wow fadeInRight delay-04s">
                    <a href="/property_list?type=3">
                        <i class="flaticon-office-block"></i>
                        <h5>Commercial/Office</h5>
                        <p><?= $WAYLF_C_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 services-info-3 s-brd-3 wow fadeInDown delay-04s">
                    <a href="/pages/contact_us">
                        <i class="flaticon-call-center-agent"></i>
                        <h5>Support 24/7</h5>
                        <p><?= $WAYLF_S247_html['value'] ?></p>
                    </a>
                </div>
                <div class="col-lg-12 text-center">
                    <a data-animation="animated fadeInUp delay-10s" href="/property_list" class="btn btn-lg btn-theme">More Details</a>
                </div>
            </div>
        </div>
    </div>

    <div class="most-popular-places content-area-3 mt-5">
        <div class="container">
            <div class="main-title">
                <h1>Most Popular Places</h1>
                <p><?= $MPP_html['value'] ?></p>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-pad wow fadeInLeft delay-04s">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="/assets/img/backgrounds/makati.jpg" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2 style="text-shadow: 0 0 5px #000, 0 0 5px #000;">Makati</h2>
                                            <p style="text-shadow: 0 0 5px #000, 0 0 5px #000;"><?= $makati ?> Properties</p>
                                            <a href="/property_list?location=Makati" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="/assets/img/backgrounds/taguig.jpg" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2 style="text-shadow: 0 0 5px #000, 0 0 5px #000;">Taguig</h2>
                                            <p style="text-shadow: 0 0 5px #000, 0 0 5px #000;"><?= $taguig ?> Properties</p>
                                            <a href="/property_list?location=Taguig" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-pad wow fadeInLeft delay-04s">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="/assets/img/backgrounds/pasig.jpg" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2 style="text-shadow: 0 0 5px #000, 0 0 5px #000;">Pasig</h2>
                                            <p style="text-shadow: 0 0 5px #000, 0 0 5px #000;"><?= $pasig ?> Properties</p>
                                            <a href="/property_list?location=Pasig" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="/assets/img/backgrounds/pasay.jpg" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2 style="text-shadow: 0 0 5px #000, 0 0 5px #000;">Pasay</h2>
                                            <p style="text-shadow: 0 0 5px #000, 0 0 5px #000;"><?= $pasay ?> Properties</p>
                                            <a href="/property_list?location=Pasay" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-pad wow fadeInRight delay-04s">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="/assets/img/backgrounds/alabang.jpg" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2 style="text-shadow: 0 0 5px #000, 0 0 5px #000;">Alabang</h2>
                                            <p style="text-shadow: 0 0 5px #000, 0 0 5px #000;"><?= $alabang ?> Properties</p>
                                            <a href="/property_list?location=Alabang" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6 cp-2">
                                <div class="overview overview-box">
                                    <img src="/assets/img/backgrounds/quezon.jpg" alt="popular-places">
                                    <div class="mask">
                                        <div class="info">
                                            <h2 style="text-shadow: 0 0 5px #000, 0 0 5px #000;">Quezon City</h2>
                                            <p style="text-shadow: 0 0 5px #000, 0 0 5px #000;"><?= $quezon ?> Properties</p>
                                            <a href="/property_list?location=Quezon City" class="btn btn-border">See more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="simple-content" style="background: url('/assets/img/backgrounds/need_assistance_bg.jpg') no-repeat; background-floor_area: cover; background-position: center;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7 align-self-center wow fadeInLeft delay-04s" style="background-color: rgba(0, 0, 0, 0.5); padding: 20px">
                    <h1>Need Assistance?</h1>
                    <p><?= $LBHE_html['value'] ?></p>
                    <a data-animation="animated fadeInUp delay-10s" href="/pages/contact_us" class="btn btn-lg btn-round btn-theme">Inquire Now</a>
                    <a data-animation="animated fadeInUp delay-10s" href="/agents_list" class="btn btn-lg btn-round btn-white-lg-outline">Learn More</a>
                </div>
            </div>
            <br><br><br><br><br>
        </div>
    </div>

    <div class="agent content-area-2">
        <div class="container">
            <div class="main-title">
                <h1>Meet Team Fretrato</h1>
                <p><?= $MOA_html['value'] ?></p>
            </div>
            <div class="row">
            <?php foreach ($employees as $row) { 

                    $agent_name = $row['name'];

            ?>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 wow fadeInLeft delay-04s">
                        <div class="agent-1">
                            <div class="agent-photo">
                                <a href="/agents_view/<?= $row['id'] . '-' . $agent_name ?>">
                                    <img src="/assets/fretrato/Images/<?= $row['img_directory'] . 'avatar_large-' . $row['img_filename'] ?>" alt="avatar" class="rounded-circle">
                                </a>
                            </div>
                            <div class="agent-details">
                                <h5><a href="/agents_view/<?= $row['id'] . '-' . $agent_name ?>"><?= $row['name'] ?></a></h5>
                                <p><?= $row['about'] ?></p>
                                <ul class="social-list clearfix">

                                    <li <?= ($row['telegram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['telegram_link'] != null ? $row['telegram_link'] : "") ?>" class="telegram mb-3"><i class="fa fa-telegram"></i></a></li>
                                    <li <?= ($row['whatsup_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['whatsup_link'] != null ? $row['whatsup_link'] : "") ?>" class="whatsapp mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                    <li <?= ($row['we_chat_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['we_chat_link'] != null ? $row['we_chat_link'] : "") ?>" class="wechat mb-3"><i class="fa fa-wechat"></i></a></li>
                                    <li <?= ($row['viber_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['viber_link'] != null ? $row['viber_link'] : "") ?>" class="viber mb-3"><i class="fa fa-whatsapp"></i></a></li>
                                    <li <?= ($row['facebook_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['facebook_link'] != null ? $row['facebook_link'] : "") ?>" class="facebook mb-3"><i class="fa fa-facebook"></i></a></li>
                                    <li <?= ($row['instagram_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['instagram_link'] != null ? $row['instagram_link'] : "") ?>" class="instagram mb-3"><i class="fa fa-instagram"></i></a></li>
                                    <li <?= ($row['linkedin_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['linkedin_link'] != null ? $row['linkedin_link'] : "") ?>" class="linkedin mb-3"><i class="fa fa-linkedin"></i></a></li>
                                    <li <?= ($row['google_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['google_link'] != null ? $row['google_link'] : "") ?>" class="google mb-3"><i class="fa fa-google"></i></a></li>
                                    <li <?= ($row['twitter_link'] == null ? "hidden" : "") ?>><a target="_blank" href="<?= ($row['twitter_link'] != null ? $row['twitter_link'] : "") ?>" class="twitter mb-3"><i class="fa fa-twitter"></i></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
            <?php } ?>
            </div>
        </div>
    </div>


<?= $this->include("Includes/property_modal") ?>
<?= $this->include("Includes/category_ajax") ?>

