
<!-- Contact section start -->
<div class="contact-section" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/office.jpg"; ?>') no-repeat; background-size: cover; background-position: center;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-inner-form">
                    <div class="details">
                        <a href="<?= base_url(); ?>">
                            <img src="<?= base_url($pluginDir); ?>/fretrato/Images/fretrato-logo-transparent.png" alt="logo" style="width: 150px; height: 70px">
                        </a>
                        <h3>Reset your account here!</h3>
                        <?= $this->include("Includes/alert") ?>
                        <form action="<?= base_url("functions/forgot_password"); ?>" method="POST">
                            <div class="form-group">
                                <input type="email" name="txt_email" class="input-text" placeholder="Email Address">
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md btn-theme btn-block">Reset Password</button>
                            </div>
                        </form>
                    </div>
                    <div class="footer" style="color: #535353 !important">
                        <span><a href="<?= base_url("pages/login") ?>" style="color: #535353 !important">Login Here!</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact section end -->