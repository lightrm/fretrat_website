
<?= $this->include("Includes/sub_banner") ?>

<div class="typography-2 content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 sd">

                <img src="<?= (!empty($img_thumbnail) && file_exists(ROOTPATH."public/assets/fretrato/Images/".$img_thumbnail['directory'] . "/" . 'thumbnail_large-' . $img_thumbnail['filename']) ? base_url($pluginDir) . '/fretrato/Images/' . $img_thumbnail['directory'] . 'original-' . $img_thumbnail['filename'] : ""); ?>" class="img-fluid" style="max-height: 500px" <?= (empty($img_thumbnail) ? "hidden" : "") ?>>

                <span class="text-justify">
                    <?= $terms_and_condition['value'] ?>
                </span>

            </div>

            <div class="col-lg-4 col-md-12">
                <div class="sidebar mbl">
                    
                    <!-- Search box start -->
                    <div class="widget search-area">

                        <form class="form-search" method="GET" action="<?= base_url('property_list') ?>">
                            <?= $this->include("Includes/property_search_sidebar") ?>
                        </form>
                        
                    </div>

                    <!-- Recent posts start -->
                    <div class="widget recent-posts">
                        <h5 class="sidebar-title">Recent Properties</h5>

                        <?= $this->include("Includes/recent_properties") ?>

                    </div>
                    
                    <?= $this->include("Includes/follow_us_sidebar") ?>
                    <?= $this->include("Includes/helping_center_sidebar") ?>
                    <?= $this->include("Includes/contact_us_sidebar") ?>

                </div>
            </div>
        </div>
    </div>
</div>