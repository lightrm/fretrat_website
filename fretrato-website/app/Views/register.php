
<!-- Contact section start -->
<div class="contact-section" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/office.jpg"; ?>') no-repeat; background-size: cover; background-position: center;">
    <div class="container mt-5">
        <div class="row mt-5">
            <div class="col-md-12 mt-5">
                <div class="login-inner-form">
                    <div class="details">
                        <a href="<?= base_url(); ?>">
                            <img src="<?= base_url($pluginDir); ?>/fretrato/Images/fretrato-logo-transparent.png" alt="logo" style="width: 150px; height: 70px">
                        </a>
                        <h3>Register here it's free!</h3>
                        <?= $this->include("Includes/alert") ?>
                        <form action="<?= base_url("functions/accountControls/register"); ?>" method="POST">
                            <div class="form-group">
                                <input type="text" name="txt_fullName" class="input-text" placeholder="Full Name" value="<?= session()->get("reg_name") ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="txt_username" class="input-text" placeholder="Username" value="<?= session()->get("reg_username") ?>">
                            </div>
                            <div class="form-group">
                                <input type="email" name="txt_email" class="input-text" placeholder="Email Address" value="<?= session()->get("reg_email") ?>">
                            </div>
                            <div class="form-group">
                                <div class="search-area">
                                    <select class="selectpicker search-fields" name="txt_role">
                                        <option disabled selected>Roles</option>
                                        <option value="3" <?= session()->get("reg_role") == "3" ? "selected" : '' ?>>Agent</option>
                                        <option value="6" <?= session()->get("reg_role") == "6" ? "selected" : '' ?>>Broker</option>
                                        <option value="4" <?= session()->get("reg_role") == "4" ? "selected" : '' ?>>Owner</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="password" name="txt_password" class="input-text" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input type="password" name="txt_confirmPassword" class="input-text" placeholder="Confirm Password">
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md btn-theme btn-block">Register</button>
                            </div>
                        </form>
                    </div>
                    <div class="footer">
                        <span>Already have an account? <a href="<?= base_url("pages/login") ?>">Login here</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact section end -->