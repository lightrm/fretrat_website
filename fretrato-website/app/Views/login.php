
<!-- Contact section start -->
<div class="contact-section" style="background: url('<?= base_url($pluginDir) . "/img/backgrounds/office.jpg"; ?>') no-repeat; background-size: cover; background-position: center;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-inner-form">
                    <div class="details">
                        <a href="<?= base_url(); ?>">
                            <img src="<?= base_url($pluginDir); ?>/fretrato/Images/fretrato-logo-transparent.png" alt="logo" style="width: 150px; height: 70px">
                        </a>
                        <h3>Sign into your account!</h3>
                        <?= $this->include("Includes/alert") ?>
                        <form action="<?= base_url("functions/accountControls/login"); ?>" method="POST">
                            <div class="form-group">
                                <input type="text" name="txt_username" class="input-text" placeholder="Email or Username">
                            </div>
                            <div class="form-group">
                                <input type="password" name="txt_password" class="input-text" placeholder="Password">
                            </div>
                            <div class="checkbox clearfix">
                                <div class="form-check checkbox-theme">
                                    <input class="form-check-input" type="checkbox" value="" id="rememberMe">
                                    <label class="form-check-label" for="rememberMe">
                                        Remember me
                                    </label>
                                </div>
                                <a href="<?= base_url("pages/forgot_password") ?>">Forgot Password</a>
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md btn-theme btn-block">Login</button>
                            </div>
                        </form>
                    </div>
                    <div class="footer">
                        <span>Don't have an account? <a href="<?= base_url("pages/register") ?>">Register here</a></span><!--class="property-video" -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact section end -->


<!-- Register Modal --><!-- 
<div class="modal property-modal fade" id="propertyModal" tabindex="-1" role="dialog" aria-labelledby="propertyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7" style="margin-left: 180px">
                        <div class="modal-right-content bg-white">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <section>
                                <h3>Register here it's free!</h3>
                                <form action="" method="GET">
                                    <div class="form-group">
                                        <input type="text" name="txt_fullName" class="input-text" placeholder="Full Name" style="width: 100%; ">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="txt_username" class="input-text" placeholder="Username" style="width: 100%">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="txt_email" class="input-text" placeholder="Email Address" style="width: 100%">
                                    </div>
                                    <div class="form-group">
                                        <div class="search-area">
                                            <select class="selectpicker search-fields" name="location">
                                                <option disabled selected>Roles</option>
                                                <option value="Agent">Agent</option>
                                                <option value="Broker">Broker</option>
                                                <option value="Owner">Owner</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="txt_password" class="input-text" placeholder="Password" style="width: 100%">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="txt_confirmPassword" class="input-text" placeholder="Confirm Password" style="width: 100%">
                                    </div>
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn-md btn-theme btn-block">Register</button>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->