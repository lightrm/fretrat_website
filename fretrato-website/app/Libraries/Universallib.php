<?php namespace App\Libraries;

class Universallib 
{

    public static function createSessionCode(){

        $session = '';
        /*

        Method 1

        $session .= date("y") * 8 * 2;
        $session .= rand(11,99);
        $session .= date("m") * 8 * 2;
        $session .= rand(11,99);
        $session .= date("d") * 8 * 2;
        $session .= rand(11,99);
        $session .= date("h") * 8 * 2;
        $session .= rand(11,99);
        $session .= date("i") * 8 * 2;
        $session .= rand(11,99);
        $session .= date("s") * 8 * 2;

        */

        //Method 2
        $rand1 = rand(11111,99999);
        $rand2 = rand(11,99);
        $time = strtotime(date("Y-m-d H:i:s"));

        $session = $rand1 . ($time * $rand2) . $rand2;

        return $session;
    }

    public static function decodeSession($session, $isDate = false){
        //Broken needs to realign


        $newID = '';
        /*
        Method 1

        $skipstep = 6;
        $spliter = 3;
        $isTime = true;
        $i = strlen($session) - 1;

        while ($i > 0) {

            if($skipstep == 0)
                $skipstep = 2;
            else {

                if($spliter == 0){
                    $newID = " ". $newID;
                    $spliter = 3;
                    $isTime = false;
                } else {
                    if($isTime)
                        $newID = ":". $newID;
                    else 
                        $newID = "-". $newID;
                }
                $newID = $session[$i - 1] . $session[$i] . $newID;
                $spliter--;
                $skipstep -= 2;
            }

            if($i >= 2)
                $i-= 2;
            else
                $i--;

        }

        $newID = rtrim($newID, ":");

        */

        //Method 2
        $divisor = '';
        $dividen = intval($session[(strlen($session) - 2)] . $session[(strlen($session) - 1)]);
        $i = strlen($session) - 1;
        while ($i >= 0) {
            if($i < (strlen($session) - 2) && $i > 4)
                $newID = $session[$i] . $newID;
            elseif ($i <= 4)
                $divisor = $session[$i] . $divisor;

            $i--;
        }

        $newID = $newID / $dividen;

        if($isDate)
            $newID = date("Y-m-d H:i:s", $newID);

        return $newID;
    }

    public static function getGet($specific = null) 
    {
        return empty($specific)? $_GET:(isset($_GET[$specific])?$_GET[$specific]:null);
    }

    public function mimic_var_dump($batch, $title = NULL, $keyRep = NULL, $stringyfy = false){
        $trn = "";
        $onRepeat = ($keyRep !== NULL);
        $titleT = ($title !== NULL);
        if(!empty($batch) && !$onRepeat)
            $titleT = is_array($batch[0]);

        $adder = "&nbsp; &nbsp;";
        $adderRep = '&emsp;';
        $arrow = "<font color='#888a85'>=></font>";

        if (!$onRepeat) {
            $trn .= "<p style='font-family: times-new-roman;'>";
            if( is_array($batch) ) {
                $trn .= "<b>".gettype($batch). "</b> <i>(size=".count($batch).")</i>"; 
                $trn .=  "<br>";
            }
            
        }
        
        if($titleT) {
            // str_pad(" ". $title. " ", 50, '=', STR_PAD_BOTH);
            if( is_array($batch) ) {

                $colorVal = "";
                $colorType = "";
                $prefix = "'";
                $htmlVal = key($batch);
                $showCount = true;

                switch (strtolower(gettype($title))) {
                    case 'string':
                        $colorVal = "#cc0000";
                        break;
                    case 'integer':
                        $prefix = "";
                        $colorVal = "#4e9a06";
                        break;
                    case 'boolean':
                        $showCount = false;
                        $htmlVal = $value?"true":"false";
                        $prefix = "";
                        $colorVal = "#75507b";
                        break;
                    case 'null':
                        $colorType = "#3465a4";
                        break;
                }

                $trn .= ($onRepeat?$keyRep:'');
                $trn .= ($onRepeat?"{$prefix}".$title."{$prefix}": (is_array($batch)?key($batch):"") ) . " $arrow ";
                $trn .= "<br>";
                $trn .= ($onRepeat?$keyRep:'').$adder;
                $trn .= "<b>".gettype($batch). "</b> <i>(size=".count($batch).")</i>"; 
            }
            $trn .=  "<br>";
        }

        if (is_array($batch)) {
            foreach ($batch as $key => $value) {
                
                if(is_array($value)) {
                    $trn .= $this->mimic_var_dump($value, $key, (!$onRepeat?$adderRep:($keyRep.$adderRep.$adderRep)), true);
                } else {

                    $colorVal = "";
                    $colorType = "";
                    $prefix = "'";
                    $htmlVal = $value;
                    $showCount = true;
                    switch (strtolower(gettype($value))) {
                        case 'string':
                            $colorVal = "#cc0000";
                            break;
                        case 'integer':
                            $prefix = "";
                            $colorVal = "#4e9a06";
                            break;
                        case 'boolean':
                            $showCount = false;
                            $htmlVal = $value?"true":"false";
                            $prefix = "";
                            $colorVal = "#75507b";
                            break;
                        case 'null':
                            $colorType = "#3465a4";
                            break;
                    }
                    $trn .= ($onRepeat?$keyRep.($titleT?$adderRep:''):($titleT?$adderRep:'')).$adder;
                    $trn .= "'".$key. "' $arrow <small><font color='{$colorType}'>".gettype($value) ."</font></small> ";
                    if($value !== null) {
                        $trn .= "<font color='{$colorVal}'>{$prefix}" . $htmlVal . "{$prefix}</font>";
                        if($showCount)
                            $trn .= " <i>(length=" . strlen($value) . ")</i>";
                    }

                }

                $trn .=  "<br>";
            }
        } else {

            $colorVal = "";
            $colorType = "";
            $prefix = "'";
            $htmlVal = $batch;
            $showCount = true;
            switch (strtolower(gettype($batch))) {
                case 'string':
                    $colorVal = "#cc0000";
                    break;
                case 'integer':
                    $prefix = "";
                    $colorVal = "#4e9a06";
                    break;
                case 'boolean':
                    $showCount = false;
                    $htmlVal = $value?"true":"false";
                    $prefix = "";
                    $colorVal = "#75507b";
                    break;
                case 'null':
                    $colorType = "#3465a4";
                    break;
            }
            $trn .= ($onRepeat?$keyRep.($titleT?$adderRep:''):($titleT?$adderRep:''));
            $trn .= "<font color='{$colorType}'>".gettype($batch) ."</font></small> ";
            $trn .= "<font color='{$colorVal}'>{$prefix}" . $htmlVal . "{$prefix}</font>";
            if($showCount)
                $trn .= " <i>(length=" . strlen($batch) . ")</i>";

        }
        

        if($titleT) {
            $trn .= ($onRepeat?$keyRep:'');
            //$trn .=  str_pad(" End " .$title. " ", 50, '=', STR_PAD_BOTH);
            //$trn .=  "<br>".($onRepeat?$keyRep:'');

        }

        if(!$onRepeat) {
            $trn .= "</p>";
        }


        if($stringyfy)
            return $trn;
        else
            echo $trn;
    }

    public function convert_num_to_letter($num) { // convert numbers to letters

        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);

        if ($num2 > 0) {
            return $this->convert_num_to_letter($num2 - 1) . $letter;
        } else {
            return $letter;
        }
        
    }

    // PRIVATE FUNCTIONS 
    private function arrayInsertKey($array, $afterKey, $inserArr, $before = 0, $preserve_keys = true){
        $pos = (array_search($afterKey, array_keys($array)) +1) - $before;

        return array_merge(
            array_slice($array, 0, max(0, $pos), $preserve_keys),
            $inserArr,
            array_slice($array, max(0, $pos), count($array), $preserve_keys)
        );
    }


}