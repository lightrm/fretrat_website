<?php namespace App\Libraries;


// use \App\ThirdParty\PHPWatermark\Watermark;

class Imagelib 
{

    // Directories
    private static $fretratoPublic = ROOTPATH."/public/";
    private $uploadPath = ROOTPATH."/public/assets/fretrato/";
    private $uploadFilePath = ROOTPATH."/public/assets/fretrato/Files/";
    private static $uploadImagePath = ROOTPATH."/public/assets/fretrato/Images/";
    private $uploadWMPath = ROOTPATH."/public/assets/fretrato/Images/watermark/";

    // Defaults
    private $defaultWM = 'watermark_330px.png';

    function  __construct(){
        // $this->CI =& get_instance();

        // exit("shit");
    }

    public static function getImageURL($imageName, $folderName=null, $defaultImg = "no-image.png") {
        
        $imgsrc = "assets/fretrato/Images/";
        if(!empty($folderName)) {
            $imgsrc .=  $folderName;
        }
        $imgsrc .= $imageName;

        if(!file_exists(self::$fretratoPublic.$imgsrc)){
            $imgsrc = "assets/fretrato/defaults/" . $defaultImg;
        }

        return base_url($imgsrc);
    }

    public static function upload_image($ImageDetail, $uploadDir = "", $additional = array())
    {
        $session = ( isset($additional['sessionid']) && !empty($additional['sessionid']) ) ?$additional['sessionid']:\App\Libraries\Universallib::createSessionCode();
        $imageService = \Config\Services::image('gd');


        $randomFolderName = \App\Libraries\Universallib::createSessionCode();
        $filePath = isset($additional['file_mark'])? "{$uploadDir}/{$session}/{$additional['file_mark']}/{$randomFolderName}":"{$uploadDir}/{$session}/Avatar/{$randomFolderName}";
        if( !is_dir(self::$uploadImagePath.$filePath) ) {
            if(!mkdir(self::$uploadImagePath.$filePath, 0777, true))
                return array("Error" => "Failed to create folder");
        }

        $newfileName = $ImageDetail->getRandomName();

        $duplicate = isset($additional['option'])? self::readCopy($additional['option']):array("original" => []);
        foreach ($duplicate as $size => $option) {
            $imageService->withFile($ImageDetail->getTempName());

            if( isset($additional['crop']) ) {
                $info = \Config\Services::image('gd')
                        ->withFile($ImageDetail->getTempName())
                        ->getFile()
                        ->getProperties(true); 

                $defxOffset = false;
                $defyOffset = false;

                if( strtolower($additional['crop']) === "auto" ) {

                    if( $info['width'] > $info['height'] ) {
                        $xStart = ($info['width'] / 2) + ($info['height'] / 3);
                        $yStart = $info['height'];
                        $xOffset = ($info['width'] / 2) - ($info['height'] / 3);
                        $yOffset = 0;
                    } else if ( $info['height'] > $info['width']  ) {
                        $xStart = $info['width'];
                        $yStart = ($info['height'] / 2) + ($info['width'] / 3) ;
                        $xOffset = 0;
                        $yOffset = ($info['height'] / 2) - ($info['width'] / 3);
                    } else {
                        $xStart = $info['width'];
                        $yStart = $info['height'];
                        $xOffset = 0;
                        $yOffset = 0;
                    }

                } else {
                    if( isset($additional['crop']['if_width']) && $info['width'] > $additional['crop']['if_width'] ) {
                        $xStart = $additional['crop']['if_width'];
                        $xOffset = ($info['width'] / 2) - ($additional['crop']['if_width'] / 2);
                    } elseif( isset($additional['crop']['width']) ) {
                        $xStart = $additional['crop']['width'];
                        $xOffset = ($info['width'] / 2) - ($additional['crop']['width'] / 2);
                    } else {
                        $defxOffset = true;
                        $xStart = $info['width'];
                        $xOffset = 0;
                    }

                    if( isset($additional['crop']['if_height']) && $info['height'] > $additional['crop']['if_height'] ) {
                        $yStart = $additional['crop']['if_height'];
                        $yOffset = ($info['height'] / 2) - ($additional['crop']['if_height'] / 2);
                    } elseif( isset($additional['crop']['height']) ) {
                        $yStart = $additional['crop']['height'];
                        $yOffset = ($info['height'] / 2) - ($additional['crop']['height'] / 2);
                    } else{
                        $defyOffset = true;
                        $yStart = $info['height'];
                        $yOffset = 0;
                    }

                    if($defxOffset && $defyOffset) {
                        if($info['width'] > $info['height'])
                            goto Getwidthscaling;
                        elseif( $info['height'] > $info['width'] )
                            goto Getheightscaling;
                    } elseif(!$defxOffset && $defyOffset) {
                        Getheightscaling:
                        $xStart = $yStart;
                        $xOffset = ($info['width'] / 2) - ($yStart / 2);
                    }  elseif($defxOffset && !$defyOffset) {
                        Getwidthscaling:
                        $yStart = $xStart;
                        $yOffset = ($info['height'] / 2) - ($xStart / 2);
                    }
                }

                $imageService->crop(
                    $xStart, 
                    $yStart, 
                    $xOffset, 
                    $yOffset
                );
            }

            if( $size !== 'original' ) {
                $imageService->resize(
                    $option['width'], 
                    $option['height'], 
                    (isset($option['ratio'])?$option['ratio']:false), 
                    (isset($option['dimension'])?$option['dimension']:'auto')
                );
            }
            

            if( isset($additional['waterMark']) && $additional['waterMark'] ) {
                // $watermark = new \Ajaxray\PHPWatermark\Watermark($ImageDetail->getTempName());

                // // Watermark with Image
                // $watermark->withImage(self::$uploadImagePath."watermark/watermark.png", self::$uploadImagePath.$filePath."/".$newfileName);

                if( !is_dir(self::$uploadImagePath.$filePath."/witoutwaterMark") ) {
                    mkdir(self::$uploadImagePath.$filePath."/witoutwaterMark", 0777, true);
                }
                $imageService->save(self::$uploadImagePath.$filePath."/witoutwaterMark/{$size}-{$newfileName}");

                // Open the image to draw a watermark
                $image = new \Imagick();
                $image->readImage(self::$uploadImagePath.$filePath."/witoutwaterMark/{$size}-{$newfileName}");

                // Open the watermark image
                // Important: the image should be obviously transparent with .png format
                $watermark = new \Imagick();
                $watermark->readImage(self::$uploadImagePath."watermark/watermark_630px-min.png");

                // The resize factor can depend on the size of your watermark, so heads up with dynamic size watermarks !
                $watermarkResizeFactor = 2;

                // Retrieve size of the Images to verify how to print the watermark on the image
                $img_Width = $image->getImageWidth();
                $img_Height = $image->getImageHeight();
                $watermark_Width = $watermark->getImageWidth();
                $watermark_Height = $watermark->getImageHeight();

                // Resize the watermark with the resize factor value
                $watermark->scaleImage( (($img_Width / $watermarkResizeFactor)), ($img_Height/($watermarkResizeFactor+2)) );

                // Update watermark dimensions
                $watermark_Width = $watermark->getImageWidth();
                $watermark_Height = $watermark->getImageHeight();

                // Draw the watermark on your image (top left corner)
                $image->compositeImage($watermark, \Imagick::COMPOSITE_OVER, 0, ($image->getImageHeight() - $watermark_Height));
                // $image->setFilename( $newfileName );
                $image->writeImage( self::$uploadImagePath.$filePath."/{$size}-{$newfileName}" );

                // From now on depends on you what you want to do with the image
                // for example save it in some directory etc.
                // In this example we'll Send the img data to the browser as response
                // with Plain PHP
                // header("Content-Type: image/" . $image->getImageFormat());

            } else {
                $imageService->save(self::$uploadImagePath.$filePath."/{$size}-{$newfileName}");
            }

        }

        if( !isset($additional['db']) || (isset($additional['db']) && $additional['db']) ) {
            $imageMod = new \App\Models\Imagemod();
            $result = $imageMod->insert(array(
                'session' => $session,
                'filename' => $newfileName,
                'directory' => $filePath."/"
            ));

            if( !$result )
                return array("Error" => "Failed to insert in database");
        }

        return $session;
    }

    private static function readCopy($data) {
        $thumbnail_small = ['width' => 350, 'height' => 350, 'ratio' => true];
        $thumbnail_medium = ['width' => 450, 'height' => 450, 'ratio' => true];
        $thumbnail_large = ['width' => 700, 'height' => 700, 'ratio' => true];
        $avatar_small = ['width' => 50, 'height' => 50, 'ratio' => true];
        $avatar_medium = ['width' => 125, 'height' => 125, 'ratio' => true];
        $avatar_large = ['width' => 300, 'height' => 300, 'ratio' => true];
        $original = [];
        $turner = array();
        foreach ($data as $index => $option) {
            if(is_string($option))
                $turner[$option] = ${$option};
            else 
                $turner[$index] = $option;
        }

        return $turner;
    }

    public static function upload_file_options($upload_file_options) {

        /*

            $upload_file_options = array(
                "upload_path"   =>  "<file_path>", // **required**
                "allowed_types" =>  "<set_allowable_types>", // **required**
                "max_size"      =>  "<set_max_size>", // **required**
                "file_name"     =>  "<set_file_name>", // **required**

                // ------------------------- OTHER OPTIONS ----------------------------------

                "max_width"     =>  "<set_max_width>",
                "max_height"    =>  "<set_max_height>",
                "min_width"     =>  "<set_min_width>",
                "min_height"    =>  "<set_min_height>",

                // ------------------------- OTHER OPTIONS ----------------------------------
            );

            upload file options
        */ 

        $config = array();
        $config['upload_path']      =   $upload_file_options['upload_path'];
        $config['allowed_types']    =   $upload_file_options['allowed_types'];
        $config['max_size']         =   $upload_file_options['max_size'];
        $config['file_name']        =   $upload_file_options['file_name'];
        $config['remove_spaces']    =   TRUE;
        $config['detect_mime']      =   TRUE;
        $config['overwrite']        =   TRUE;
        $config['file_ext_tolower'] =   TRUE;
        //$config['max_width']      =   $upload_file_options['max_width'];
        //$config['max_height']     =   $upload_file_options['max_height'];
        //$config['min_width']      =   $upload_file_options['max_width'];
        //$config['min_height']     =   $upload_file_options['max_height'];

        return $config;

    }

}