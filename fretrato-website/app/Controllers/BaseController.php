<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];
	protected $universallib;
	protected $request;
	public $pluginDir = "assets/";
	public $inlcudesDir = "Includes/";
	public $headerData = array();
	public $footerData = array();
	private $pagesmod;

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//Add Libraries to Pass on our view for basic page
		$this->librarytoView = array(
			// '(Varable)' => (Class)
			// Example: 'Foo' => FooClass

			'universallib' => $this->universallib,
		);
		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
		$this->request = \Config\Services::request();
		$this->universallib = new \App\Libraries\Universallib();

		$this->headerData["pluginDir"] = $this->pluginDir;
		$this->headerData["headerTitle"] = "";
		$this->headerData["pageTitle"] = "Title";
		$this->headerData["breadCrumps"] = array();
		$this->headerData['hdjs'] = array();
		$this->headerData['hdcss'] = array();
		$this->footerData['ftjs'] = array();

	}

	public function basic_page($page, $bodyData = array())
	{
		$this->pagesmod = model('\App\Models\pagesmod');
		$this->propertiesmod = model('\App\Models\propertiesmod');
		$bodyData = $this->AddLibraries($bodyData);
		$bodyData['locations'] = array('Alabang', 'Antipolo', 'Batangas', 'Boracay', 'Cebu', 'Caloocan', 'Cavite', 'Ilo-Ilo', 'Laguna', 'Las Pinas', 'Makati', 'Manila', 'Mandaluyong', 'Marikina', 'Muntinlupa', 'Navotas', 'Tagaytay', 'Taguig', 'Palawan', 'Pampanga' , 'Paranaque', 'Pasay', 'Pasig', 'Quezon City', 'Rizal', 'San Juan', 'Siargao', 'Navotas', 'Valenzuela', 'Other Location');
		$this->headerData['lot_sub_categ']  = $this->propertiesmod->get_sub_categ(11);
		$this->headerData['land_sub_categ']  = $this->propertiesmod->get_sub_categ(6);
		$this->headerData['social_medias']  = $this->pagesmod->web_config("social_media");
		$this->footerData['social_medias'] = $this->headerData['social_medias'];
		$bodyData['help_center_contact_html'] = $this->pagesmod->web_config("web_config", "single", "contact_html");
		$bodyData['propertyDataft'] = $this->propertiesmod->get_properties(5, 0, false); // limit, offset, pagination, featured
		$this->headerData['meta_title'] = $this->pagesmod->web_config("web_config", "single", "meta_title");
		$this->headerData['meta_description'] = $this->pagesmod->web_config("web_config", "single", "meta_description");

		$pageV = view($this->inlcudesDir . 'header', $this->headerData);
		$pageV .= view($page, $bodyData);
		$pageV .= view($this->inlcudesDir . 'footer', $this->footerData);
		
		return $pageV;

	}

	private function AddLibraries($data) {

		foreach ($this->librarytoView as $nameIndex => $class) {
			$this->headerData[$nameIndex] = $class;
			$data[$nameIndex] = $class;
			$this->footerData[$nameIndex] = $class;
		}

		return $data;
	}

	public function seo_friendly_url($string) {
		$string = str_replace(array('[\', \']'), '', $string);
		$string = preg_replace('/\[.*\]/U', '', $string);
		$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
		$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
		return strtolower(trim($string, '-'));
	}
	
}
