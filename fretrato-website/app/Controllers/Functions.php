<?php namespace App\Controllers;

 
class Functions extends BaseController
{
	private $email;

	public function __construct()
	{
		$this->usermod = model('\App\Models\Usermod');
		$this->functionsmod = model('\App\Models\Functionsmod');
		$this->propertiesmod = model('\App\Models\Propertiesmod');
		$this->amenitiesmod = model('\App\Models\Amenitiesmod');
		$this->imagemod = model('\App\Models\Imagemod');
		$this->adminURL = "http://admin.fretrato.com.ph/";
		$this->email = \Config\Services::email();

	}
		
	public function accountControls($action)
	{

        helper(['form', 'url']);
		$validator = \Config\Services::validation();

		if ($action == "login") { // for login

			$rule = array(
				"txt_username" 		=> ['label' => "Email or Username", 'rules' => 'required'],
				"txt_password" 		=> ['label' => "Password", 'rules' => 'required']
			);

			if( $this->validate($rule) ) {

				$result = $this->usermod->request_login($this->request->getPost("txt_username"), $this->request->getPost("txt_password"));
				if( !$result['error'] ) {
					// Successful login
					session()->set("User", $result['user']);
					return redirect()->to(base_url());
					
				} else {
					session()->setFlashdata("user_alert", array(
						"type" 		=> "danger",
						"initMsg" 	=> "Oops",
						"msg"		=> $result['msg']
					));
					return redirect()->to(base_url("pages/login"));
				}

			} else {

				session()->setFlashdata("user_alert", array(
					"type" 		=> "danger",
					"initMsg" 	=> "Oops",
					"msg"		=> $validator->listErrors()
				));

				return redirect()->to(base_url("pages/login"));

			}

		}
		elseif ($action == "register") { // for register
			
			$rule = array(
				"txt_fullName" 		=> ['label' => "Full Name", 'rules' => 'required'],
				"txt_username" 		=> ['label' => "Username", 'rules' => 'required'],
				"txt_email" 		=> ['label' => "Email Address", 'rules' => 'required'],
				"txt_role" 			=> ['label' => "Role", 'rules' => 'required'],
				"txt_password" 		=> ['label' => "Password", 'rules' => 'required'],
			);

			if( $this->validate($rule) ) {

				$account_username = $this->usermod->check_credentials_exist("username", $this->request->getPost('txt_username'));


				$account_email = $this->usermod->check_credentials_exist("email", $this->request->getPost('txt_email'));

				if ($account_email == 0) {

					$account_username = $this->usermod->check_credentials_exist("username", $this->request->getPost('txt_username'));

					if ($account_username == 0) {

			            if( $this->request->getPost('txt_password') == $this->request->getPost('txt_confirmPassword') ) {

							$userRole = ['3' => 'Agent', '4' => 'Owner', '6' => 'Broker'];
                			$hash = password_hash($this->request->getPost('txt_password'), PASSWORD_DEFAULT);
                			$confimartion_code = rand(00000, 99999);

							$this->usermod->insert(array(
								'name'				=> $this->request->getPost('txt_fullName'),
								'username'			=> $this->request->getPost('txt_username'),
								'email'				=> $this->request->getPost('txt_email'),
								'password'			=> $hash,
								'about'				=> "Partner " . $userRole[$this->request->getPost('txt_role')],
								'role'				=> $this->request->getPost('txt_role'),
								'confirmation_code'	=> $confimartion_code,
								'active'			=> 0
							));// inserting the data
							$userid = $this->usermod->getInsertID();

							$reclist = $this->usermod->get_admin_accounts();

							foreach ($reclist as $row) {

								$this->functionsmod->create_notification(array(
									'user_id'			=> $row['id'],
									'icon'				=> "fas fa-envelope",
									'title'				=> "Register Account",
									'url'				=> $this->adminURL . "users?search={$userid}", // DEFAULT
									'description'		=> "Registered Account for " . $this->request->getPost('txt_fullName'),
								));// inserting the data for notification

							}

							$urlForConfirmation = base_url("pages/verification/" . $userid . "?confirmation_code=" . $confimartion_code);

			            	// Send Client
							{
								$this->email->setFrom('info@fretrato.com.ph', 'Fretrato | Your Real Estate Buddies');
								$this->email->setTo($this->request->getPost('txt_email'));

								$this->email->setSubject('Email Confirmation');
								$this->email->setMessage($this->email_contructor(
									$this->request->getPost('txt_name'),
									"We Received your registration <a href='{$urlForConfirmation}' target='_blank'>Click Here</a> to confirm your account."
								));

								$this->email->send();
							}

							// Send Server
							{
								$this->email->setFrom('info@fretrato.com.ph', 'Fretrato | Your Real Estate Buddies');
								$this->email->setTo('info@fretrato.com.ph');
								$this->email->setCC('site@fretrato.com.ph');

								$this->email->setSubject('New User');
								$this->email->setMessage($this->email_contructor(
									"Admin",
									"We have a new Registration:<br>".
									"User Name: " . $this->request->getPost('txt_fullName') . "<br>" .
									"User Username: " . $this->request->getPost('txt_username') . "<br>" .
									"User Email: " . $this->request->getPost('txt_email') ."<br>" .
									"User Role: " . $userRole[$this->request->getPost('txt_role')]
								));

								$this->email->send();
							}

							/* //session for the logged in account
								$result = $this->usermod->verify_login($userid);
								session()->set("User", $result);
								\App\Libraries\Roleslib::RefreshRoles();	
							*/

							session()->remove("reg_name");
							session()->remove("reg_username");
							session()->remove("reg_email");
							session()->remove("reg_role");

							return redirect()->to(base_url("pages/message/account_registered"));


			            } else {
			                $result['msg'] = "Password does not seem to match. Please re-enter your password again.";
			            }

					}
					else{
	                	$result['msg'] = "Sorry, That username has already been taken. Please choose another.";
					}

				}
				else{
	                $result['msg'] = "Sorry, That email has already been taken. Please choose another.";
				}

				session()->setFlashdata("user_alert", array(
					"type" 		=> "danger",
					"initMsg" 	=> "Oops",
					"msg"		=> $result['msg']
				));

				session()->set("reg_name", $this->request->getPost('txt_fullName'));
				session()->set("reg_username", $this->request->getPost('txt_username'));
				session()->set("reg_email", $this->request->getPost('txt_email'));
				session()->set("reg_role", $this->request->getPost('txt_role'));

				return redirect()->to(base_url("pages/register"));

	        } 
	        else {

				session()->setFlashdata("user_alert", array(
					"type" 		=> "danger",
					"initMsg" 	=> "Oops",
					"msg"		=> $validator->listErrors()
				));

				return redirect()->to(base_url("pages/register"));

			}


		}
		else{ // for logout

			session()->remove("User");
			session()->remove(\App\Libraries\Roleslib::$role_sessName);
			return redirect()->to(base_url("pages/login"));

		}

	}

	public function forgot_password()
	{

		$email = $this->request->getPost("txt_email");

		$check_email_exist = $this->usermod->check_credentials_exist("email", $email);

		if ($check_email_exist == 1) {

			$user_detail = $this->usermod->get_user_by_email($email);
			$confimartion_code = rand(00000, 99999);

			$this->usermod->update($user_detail['id'], array('confirmation_code' => $confimartion_code));

			$passwordRenewalURL = base_url("pages/reset_password/" . $user_detail['id'] . '?confirmation_code=' . $confimartion_code);

			// Send Email
			{
				$this->email->setFrom('info@fretrato.com.ph', 'Fretrato | Your Real Estate Buddies');
				$this->email->setTo($email);

				$this->email->setSubject('Approval Request');
				$this->email->setMessage($this->email_contructor(
					$user_detail["name"],
					"You have requested for password renewal. If this is not you, please ignore the message. <br><br>".
					"<a href='{$passwordRenewalURL}' target='_blank'> Click Here </a>"
				));

				$this->email->send();
			}


			return redirect()->to(base_url("pages/message/reset_password"));

		}
		else{

			session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> "Email is not yet registered in our system."
			));

			return redirect()->to(base_url("pages/forgot_password"));

		}

	}

	public function reset_password($id, $confirmation_code)
	{

        helper(['form', 'url']);
		$validator = \Config\Services::validation();

		$rule = array(
			"txt_password" 		=> ['label' => "Password", 'rules' => 'required']
		);

		if( $this->validate($rule) ) {

		    $hash = password_hash($this->request->getPost('txt_password'), PASSWORD_DEFAULT);

			$this->usermod->update($id, array('password' => $hash, 'confirmation_code' => null));
			return redirect()->to(base_url("pages/message/password_reset"));

		}
		else{

			session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> $validator->listErrors()
			));
			return redirect()->to(base_url("pages/reset_password/" . $id . '?confirmation_code=' . $confirmation_code));

		}


	}

	public function inquiry($id = null, $name = null, $slug = null, $offer_type = null)
	{

        helper(['form', 'url']);

		$validator = \Config\Services::validation();

		$rule = array(
			"txt_name" 			=> ['label' => "Name", 'rules' => 'required'],
			"txt_email" 		=> ['label' => "Email", 'rules' => 'required'],
			"txt_number" 		=> ['label' => "Contact Number", 'rules' => 'required'],
			"txt_message" 		=> ['label' => "Message", 'rules' => 'required']
		);

		if ($id == null || $id == "Property List" || $id == "Blogs List" || $id == "Agents List") {
			$rule['txt_inquiryAbout'] = ['label' => "Inquiry About", 'rules' => 'required'];
		}
		
		if( !$this->request->getPost('g-recaptcha-response') ) {
			session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> 'You need to validate your captcha'
			));

			if ($id == null) {
				$return_url = redirect()->to(base_url("pages/contact_us"));
			}
			elseif ($id == "Property List"){
				$return_url = redirect()->to(base_url("property_list"));
			}
			elseif ($id == "Blogs List"){
				$return_url = redirect()->to(base_url("pages/blogs_list"));
			}
			elseif ($id == "Agents List"){
				$return_url = redirect()->to(base_url("pages/agents_list"));
			}
			else{
				$return_url = redirect()->to(base_url($offer_type . "/property_view/" . $slug));
			}

			return $return_url;
		}

		$post = [
            'secret' => '6Le_CHQaAAAAAIA-dSS8S-LYJjvxlUrcTc7kKYEr',
            'response' => $_REQUEST['g-recaptcha-response'],
        ];
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = json_decode(curl_exec($ch));

        curl_close ($ch);

        if( !$server_output->success ) {
        	session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> 'Invalid reCaptcha'
			));

			if ($id == null) {
				$return_url = redirect()->to(base_url("pages/contact_us"));
			}
			elseif ($id == "Property List"){
				$return_url = redirect()->to(base_url("property_list"));
			}
			elseif ($id == "Blogs List"){
				$return_url = redirect()->to(base_url("pages/blogs_list"));
			}
			elseif ($id == "Agents List"){
				$return_url = redirect()->to(base_url("pages/agents_list"));
			}
			else{
				$return_url = redirect()->to(base_url($offer_type . "/property_view/" . $slug));
			}

			return $return_url;
        }

		if( $this->validate($rule) ) {

			$data = array(
				'name'			=> $this->request->getPost('txt_name'),
				'email'			=> $this->request->getPost('txt_email'),
				'about'			=> ($name == null ? $this->request->getPost('txt_inquiryAbout') : $name),
				'contact_no'	=> $this->request->getPost('txt_number'),
				'message'		=> $this->request->getPost('txt_message'),
				'created_at'	=> date('Y-m-d h:i:s')
			);

			if ($id != null) {
				$data['property_id'] = $id;
			}

			$this->functionsmod->insert($data);// inserting the data for inquiry
			$inquiryID = $this->functionsmod->getInsertID();
			$notificationURL = $this->adminURL . "inquiry/message/".$inquiryID;

			
			$reclist = $this->usermod->get_admin_accounts();

			foreach ($reclist as $row) {

				$this->functionsmod->create_notification(array(
					'user_id'			=> $row['id'],
					'icon'				=> "fas fa-envelope",
					'title'				=> "New Inquiry",
					'url'				=> $notificationURL , // DEFAULT
					'description'		=> "New Inquiry for " . $this->request->getPost('txt_name'),
				));// inserting the data for notification

			}


	    	/* INSERT EMAIL CONFIMATION HERE FOR INQUIRY */

			// Send Client
			{
				$this->email->setFrom('info@fretrato.com.ph', 'Fretrato | Your Real Estate Buddies');
				$this->email->setTo($this->request->getPost('txt_email'));

				$this->email->setSubject('New Inquiry');
				$this->email->setMessage($this->email_contructor(
					$this->request->getPost('txt_name'),
					"We have received your inquiry for " . $this->request->getPost('txt_inquiryAbout') . " and your Fretrato-Real Estate Buddy will get back to you the soonest possible time."
				));

				$this->email->send();
			}

			// Send Server
			{
				$this->email->setFrom('info@fretrato.com.ph', 'Fretrato | Your Real Estate Buddies');
				$this->email->setTo('info@fretrato.com.ph');
				$this->email->setCC('site@fretrato.com.ph');

				$this->email->setSubject('New Inquiry');
				$this->email->setMessage($this->email_contructor(
					"Admin",
					"We have a new Inquiry:<br>".
					"Inquirer Name: " . $this->request->getPost('txt_name') . "<br>" .
					"Inquirer Email: " . $this->request->getPost('txt_email') ."<br>" .
					"Inquirer Contact: " . $this->request->getPost('txt_number') ."<br>" .
					"Inquirer Inquiring: " . $this->request->getPost('txt_inquiryAbout') ."<br>" .
					"Inquirer Message: " . $this->request->getPost('txt_message'). "<br>".
					"<a href='{$notificationURL}' target='_blank'>Click Here </a>"
				));

				$this->email->send();
			}

			session()->setFlashdata("user_alert", array(
				"type" 		=> "success",
				"initMsg" 	=> "Well Done",
				"msg"		=> "Your inquiry has been successfully sent."
			));


			if ($id == null) {
				$return_url = redirect()->to(base_url("pages/contact_us"));
			}
			elseif ($id == "Property List"){
				$return_url = redirect()->to(base_url("property_list"));
			}
			elseif ($id == "Blogs List"){
				$return_url = redirect()->to(base_url("property_list"));
			}
			elseif ($id == "Agents List"){
				$return_url = redirect()->to(base_url("property_list"));
			}
			else{
				$return_url = redirect()->to(base_url($offer_type . "/property_view/" . $slug));
			}

			return $return_url;


		}
		else{

			session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> $validator->listErrors()
			));

			if ($id == null) {
				$return_url = redirect()->to(base_url("pages/contact_us"));
			}
			elseif ($id == "Property List"){
				$return_url = redirect()->to(base_url("property_list"));
			}
			elseif ($id == "Blogs List"){
				$return_url = redirect()->to(base_url("pages/blogs_list"));
			}
			elseif ($id == "Agents List"){
				$return_url = redirect()->to(base_url("pages/agents_list"));
			}
			else{
				$return_url = redirect()->to(base_url($offer_type . "/property_view/" . $slug));
			}

			return $return_url;

		}

	}

	public function edit_profile($type)
	{

        helper(['form', 'url']);

		$validator = \Config\Services::validation();
		$id = session()->get("User")['id'];

		if ($type == "record") {	

			$rule = array(
				"txt_fullName" 	=> ['label' => "Name", 'rules' => 'required'],
				"txt_username" 	=> ['label' => "Username", 'rules' => 'required'],
				"txt_email" 	=> ['label' => "Email", 'rules' => 'required']
			);

			if (session()->get("User")['role'] != 1) {
				$rule['txt_roles'] = ['label' => "Roles", 'rules' => 'required'];
			}


		}
		else{

			$rule = array(
				"txt_oldPassword"		=> ['label' => "Old Password", 'rules' => 'required'],
				"txt_password" 			=> ['label' => "New Password", 'rules' => 'required'],
				"txt_confirmPassword" 	=> ['label' => "Confirm Password", 'rules' => 'required']
			);
			
		}

		if( $this->validate($rule) ) {

			if ($type == "record") {

				$account_username = $this->usermod->check_credentials_exist("username", $this->request->getPost('txt_username'));

				if ($account_username == 0) {

					$account_email = $this->usermod->check_credentials_exist("email", $this->request->getPost('txt_email'));

					if ($account_email == 0) {
						
						// image Uploading
						$userRole = ['3' => 'Agent', '4' => 'Owner', '6' => 'Broker'];
						$imagefile = $this->request->getFile("txt_thumbnail");
						$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
							$imagefile, 
							"Users", 
							[
								'sessionid' => session()->get("User")['img_session'],
								'option' 	=> ['avatar_small', 'avatar_medium', 'avatar_large', 'original'],
								// 'crop'		=> ['if_width' => 215, 'if_height' => 215]
							]
						):session()->get("User")['img_session'];

						$data = array(
							'name'			=> $this->request->getPost('txt_fullName'),
							'username'		=> $this->request->getPost('txt_username'),
							'email'			=> $this->request->getPost('txt_email'),
							'contact_no'	=> $this->request->getPost('txt_contact_no'),
							//'about'			=> $this->request->getPost('txt_position'),
							'gender'		=> $this->request->getPost('txt_gender'),
							'img_session'	=> $img_data
							//'updated_at'	=> date('Y-m-d h:i:s')
						);

						if (session()->get("User")['role'] != 1) {
							$data['role'] = $this->request->getPost('txt_roles');
							$data['about'] = "Partner " . $userRole[$this->request->getPost('txt_roles')];
						}

						$this->usermod->update($id, $data);// update user record

						$result = $this->usermod->refresh_userdata($id);
						session()->set("User", $result['user']);

						$alert_type = "success";
						$alert_init_msg = "Well Done";
						$alert_msg = "Profile successfully saved.";

					}
					else{

						$alert_type = "danger";
						$alert_init_msg = "Oops";
						$alert_msg = "Sorry, That email has already been taken. Please choose another.";

					}

				}
				else{

					$alert_type = "danger";
					$alert_init_msg = "Oops";
					$alert_msg = "Sorry, That username has already been taken. Please choose another.";

				}

				$return_url = redirect()->to(base_url("pages/my_profile"));

			}
			else{

				$txt_oldPassword = $this->request->getPost('txt_oldPassword');
				$old_password = session()->get("User")['password'];

            	if( password_verify($txt_oldPassword, $old_password) ) {

		            if( $this->request->getPost('txt_password') == $this->request->getPost('txt_confirmPassword') ) {

		    			$hash = password_hash($this->request->getPost('txt_password'), PASSWORD_DEFAULT);

						$data = array(
							'password'	=> $hash
						);

						$this->usermod->update($id, $data);// update user record
						$result = $this->usermod->refresh_userdata($id);
						session()->set("User", $result['user']);

						$alert_type = "success";
						$alert_init_msg = "Well Done";
						$alert_msg = "Password successfully changed.";

					}
					else{

						$alert_type = "danger";
						$alert_init_msg = "Oops";
						$alert_msg = "Password does not match. Re-enter password again.";

					}

            	}
            	else{

					$alert_type = "danger";
					$alert_init_msg = "Oops";
					$alert_msg = "Password incorrect.";

            	}

				$return_url = redirect()->to(base_url("pages/change_password"));

			}

			session()->setFlashdata("user_alert", array(
				"type" 		=> $alert_type,
				"initMsg" 	=> $alert_init_msg,
				"msg"		=> $alert_msg
			));

			return $return_url;

		}
		else{

			session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> $validator->listErrors()
			));

			if ($type == "record") {
				$return_url = redirect()->to(base_url("pages/my_profile"));
			}
			else{
				$return_url = redirect()->to(base_url("pages/change_password"));
			}

			return $return_url;

		}

	}

	public function create_property(){

        helper(['form', 'url']);

		$validator = \Config\Services::validation();
		$id = session()->get("User")['id'];


		$rule = array(
			"txt_name" 				=> ['label' => "Name", 'rules' => 'required'],
			"txt_propertyStatus" 	=> ['label' => "Offer Type", 'rules' => 'required'],
			"txt_propertyType" 		=> ['label' => "Property Type", 'rules' => 'required'],
			"txt_subCategory" 		=> ['label' => "Sub Category", 'rules' => 'required'],
			"txt_price" 			=> ['label' => "Price", 'rules' => 'required'],
			"txt_size" 				=> ['label' => "Area/Size", 'rules' => 'required'],
			"txt_floor_level" 		=> ['label' => "Floor Level", 'rules' => 'required'],
			"txt_bedroom" 			=> ['label' => "Bedrooms", 'rules' => 'required'],
			"txt_bathroom" 			=> ['label' => "Bathroom", 'rules' => 'required'],
			"txt_streetName" 		=> ['label' => "Street Name", 'rules' => 'required'],
			"txt_city" 				=> ['label' => "City", 'rules' => 'required'],
			//"txt_country" 			=> ['label' => "Country", 'rules' => 'required'],
			//"txt_about" 			=> ['label' => "About", 'rules' => 'required'],
			"txt_description" 		=> ['label' => "Description", 'rules' => 'required']
		);

		if( $this->validate($rule) ) {

			$imagefile = $this->request->getFile("txt_thumbnail");
			$galleryFile = $this->request->getFiles()['txt_galleryImages'];
			$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
				$imagefile, 
				"Realty", 
				[
					'file_mark' 	=> "Thumbnail",
					'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
					'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
				]):null;

			foreach ($galleryFile as $imageInfo) {
				$result = $imageInfo->isValid()? \App\Libraries\Imagelib::upload_image(
					$imageInfo, 
					"Realty", 
					[
						'sessionid' 	=> $img_data,
						'file_mark' 	=> "Gallery",
						'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
						'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
					]
				):null;

				if( $result !== $img_data ) $img_data = $result;
				if( is_array($img_data) && isset($img_data['Error']) ) { goto Cerror_image_upload; break; }
			}

			if( is_array($img_data) && isset($img_data['Error']) ) {
				Cerror_image_upload:
				session()->setFlashdata("user_alert", array(
					"type" 	=> "danger",
					"msg"	=> "Something went wrong in uploading the Images."
				));
				goto return_c_inp;
			}

			$street = $this->request->getPost('txt_streetName');
			$barangay = $this->request->getPost('txt_barangay');
			$city = $this->request->getPost('txt_city');
			$province = $this->request->getPost('txt_province');
			$country = $this->request->getPost('txt_country');

			$location = $street;

			if ($barangay != null) {
				$location .= " " . $barangay;
			}

			if ($city != null) {
				$location .= ", " . $city;
			}

			if ($province != null) {
				$location .= ", " . $province;
			}
			
			if ($country != null) {
				$location .= ", " . $country;
			}

			$property_id = $this->propertiesmod->insert(array(
				'user_id'			=> $id,
				'name'				=> $this->request->getPost('txt_name'),
				'about'				=> $this->request->getPost('txt_about'),
				'description'		=> $this->request->getPost('txt_description'),
				'street'			=> $this->request->getPost('txt_streetName'),
				'city'				=> $this->request->getPost('txt_city'),
				'barangay'			=> $this->request->getPost('txt_barangay'),
				'province'			=> $this->request->getPost('txt_province'),
				'country'			=> $this->request->getPost('txt_country'),
				'location'			=> $location,
				'bedroom'			=> $this->request->getPost('txt_bedroom'),
				'bathroom'			=> $this->request->getPost('txt_bathroom'),
				'floor_area'		=> $this->request->getPost('txt_size'),
				'floor_level'		=> $this->request->getPost('txt_floor_level'),
				'furnish_type'		=> $this->request->getPost('txt_furnished') + 0,
				'offer_type'		=> $this->request->getPost('txt_propertyStatus'),
				'parking'			=> $this->request->getPost('txt_parking') + 0,
				'property_type'		=> $this->request->getPost('txt_propertyType'),
				'subcategory'		=> $this->request->getPost('txt_subCategory'),
				'price'				=> $this->request->getPost('txt_price'),
				'slug'				=> $this->seo_friendly_url( $this->request->getPost('txt_name') . '-' . $id ),
				//'status'			=> "approved",
				'toogleFeature'		=> 0,
				'property_vid_url'	=> $this->request->getPost('txt_video'),
				'published'			=> 0,
				'img_session'		=> $img_data
			));// insert property record

			$amenities = $this->request->getPost('amenities');

			if (!empty($amenities)) {

				foreach ($amenities as $row) {
				
					$this->amenitiesmod->insert(array(
						'property_id' 			=> $property_id, 
						'amenity_property_id'	=> $row
					)); // create the amenities of the property

				}

			}

			$this->functionsmod->create_property_approval(array(
				'user_id'		=> session()->get("User")['id'],
				'property_id'	=> $property_id
			));// inserting the data for approval request 

			$reclist = $this->usermod->get_admin_accounts();

			foreach ($reclist as $row) {

				$this->functionsmod->create_notification(array(
					'user_id'			=> $row['id'],
					'icon'				=> "fas fa-envelope",
					'title'				=> "New Property Approval",
					'url'				=> $this->adminURL, // DEFAULT
					'description'		=> "New property for approval" . $this->request->getPost('txt_name'),
				));// inserting the data for notification

				// Send Email
				{
					$this->email->setFrom('info@fretrato.com.ph', 'Fretrato | Your Real Estate Buddies');
					$this->email->setTo("info@fretrato.com.ph");

					$this->email->setSubject('Approval Request');
					$this->email->setMessage($this->email_contructor(
						"Admin",
						"A New Property is for approval <br><br>".
						"<a href='".$this->adminURL."' target='_blank'> Click Here </a>"
					));

					$this->email->send();
				}

			}

			session()->setFlashdata("user_alert", array(
				"type" 		=> "success",
				"initMsg" 	=> "Well Done",
				"msg"		=> "Record successfully saved."
			));

			session()->remove("inp_name");
			session()->remove("inp_description");
			session()->remove("inp_about");
			session()->remove("inp_streetName");
			session()->remove("inp_city");
			session()->remove("inp_barangay");
			session()->remove("inp_province");
			session()->remove("inp_country");
			session()->remove("inp_bedroom");
			session()->remove("inp_bathroom");
			session()->remove("inp_size");
			session()->remove("inp_floor_level");
			session()->remove("inp_furnished");
			session()->remove("inp_propertyStatus");
			session()->remove("inp_parking");
			session()->remove("inp_propertyType");
			session()->remove("inp_subCategory");
			session()->remove("inp_price");
			session()->remove("inp_video");

			return redirect()->to(base_url("pages/my_listing"));
		
		}
		else{

			session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> $validator->listErrors()
			));

			return_c_inp:

			session()->set("inp_name", $this->request->getPost('txt_name'));
			session()->set("inp_description", $this->request->getPost('txt_description'));
			session()->set("inp_about", $this->request->getPost('txt_about'));
			session()->set("inp_streetName", $this->request->getPost('txt_streetName'));
			session()->set("inp_city", $this->request->getPost('txt_city'));
			session()->set("inp_barangay", $this->request->getPost('txt_barangay'));
			session()->set("inp_province", $this->request->getPost('txt_province'));
			session()->set("inp_country", $this->request->getPost('txt_country'));
			session()->set("inp_bedroom", $this->request->getPost('txt_bedroom'));
			session()->set("inp_bathroom", $this->request->getPost('txt_bathroom'));
			session()->set("inp_size", $this->request->getPost('txt_size'));
			session()->set("inp_floor_level", $this->request->getPost('txt_floor_level'));
			session()->set("inp_furnished", $this->request->getPost('txt_furnished'));
			session()->set("inp_propertyStatus", $this->request->getPost('txt_propertyStatus'));
			session()->set("inp_parking", $this->request->getPost('txt_parking'));
			session()->set("inp_propertyType", $this->request->getPost('txt_propertyType'));
			session()->set("inp_subCategory", $this->request->getPost('txt_subCategory'));
			session()->set("inp_price", $this->request->getPost('txt_price'));
			session()->set("inp_video", $this->request->getPost('txt_video'));

			return redirect()->to(base_url("pages/submit_property"));

		}

	}


	public function edit_property($property_id){

        helper(['form', 'url']);

		$validator = \Config\Services::validation();
		$id = session()->get("User")['id'];


		$rule = array(
			"txt_name" 				=> ['label' => "Name", 'rules' => 'required'],
			"txt_propertyStatus" 	=> ['label' => "Offer Type", 'rules' => 'required'],
			"txt_propertyType" 		=> ['label' => "Property Type", 'rules' => 'required'],
			"txt_subCategory" 		=> ['label' => "Sub Category", 'rules' => 'required'],
			"txt_price" 			=> ['label' => "Price", 'rules' => 'required'],
			"txt_size" 				=> ['label' => "Area/Size", 'rules' => 'required'],
			"txt_floor_level" 		=> ['label' => "Floor Level", 'rules' => 'required'],
			"txt_bedroom" 			=> ['label' => "Bedrooms", 'rules' => 'required'],
			"txt_bathroom" 			=> ['label' => "Bathroom", 'rules' => 'required'],
			"txt_streetName" 		=> ['label' => "Street Name", 'rules' => 'required'],
			"txt_city" 				=> ['label' => "City", 'rules' => 'required'],
			//"txt_country" 			=> ['label' => "Country", 'rules' => 'required'],
			//"txt_about" 			=> ['label' => "About", 'rules' => 'required'],
			"txt_description" 		=> ['label' => "Description", 'rules' => 'required']
		);


		if( $this->validate($rule) ) {

			$gallery = $this->propertiesmod->get_property_gallery($this->request->getPost('txt_hidden_img_session'), "Gallery");

			foreach ($gallery as $row) {

				$gallery_toggle = $this->request->getPost('txt_image_toggle_' . $row['id']) + 0;

				if ($gallery_toggle == 0) {
					$this->imagemod->delete($row['id']); // delete image gallery
				}

			}

			$imagefile = $this->request->getFile("txt_thumbnail");
			$galleryFile = $this->request->getFiles()['txt_galleryImages'];

			$img_data = $imagefile->isValid()? \App\Libraries\Imagelib::upload_image(
				$imagefile, 
				"Realty", 
				[
					'sessionid' 	=> $this->request->getPost('txt_hidden_img_session'),
					'file_mark' 	=> "Thumbnail",
					'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
					'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
				]
			):$this->request->getPost('txt_hidden_img_session');

			foreach ($galleryFile as $imageInfo) {
				$result = $imageInfo->isValid()? \App\Libraries\Imagelib::upload_image(
					$imageInfo, 
					"Realty", 
					[
						'sessionid' 	=> $img_data,
						'file_mark' 	=> "Gallery",
						'option' 		=> ['thumbnail_small', 'thumbnail_medium', 'thumbnail_large', 'original'],
						'waterMark' 	=> (bool) $this->request->getPost('ckWatermark')
					]
				):$img_data;

				if( $result !== $img_data ) $img_data = $result;
				if( is_array($img_data) && isset($img_data['Error']) ) { goto Eerror_image_upload; break; }
			}

			if( is_array($img_data) && isset($img_data['Error']) ) {
				Eerror_image_upload:
				session()->setFlashdata("user_alert", array(
					"type" 	=> "danger",
					"msg"	=> "Something went wrong in uploading the Images."
				));
				goto return_e_inp;
			}

			$street = $this->request->getPost('txt_streetName');
			$barangay = $this->request->getPost('txt_barangay');
			$city = $this->request->getPost('txt_city');
			$province = $this->request->getPost('txt_province');
			$country = $this->request->getPost('txt_country');

			$location = $street;

			if ($barangay != null) {
				$location .= " " . $barangay;
			}

			if ($city != null) {
				$location .= ", " . $city;
			}

			if ($province != null) {
				$location .= ", " . $province;
			}
			
			if ($country != null) {
				$location .= ", " . $country;
			}
			
			$this->propertiesmod->update($property_id, array(
				'name'				=> $this->request->getPost('txt_name'),
				'description'		=> $this->request->getPost('txt_description'),
				'about'				=> $this->request->getPost('txt_about'),
				'street'			=> $this->request->getPost('txt_streetName'),
				'city'				=> $this->request->getPost('txt_city'),
				'barangay'			=> $this->request->getPost('txt_barangay'),
				'province'			=> $this->request->getPost('txt_province'),
				'country'			=> $this->request->getPost('txt_country'),
				'location'			=> $location,
				'bedroom'			=> $this->request->getPost('txt_bedroom'),
				'bathroom'			=> $this->request->getPost('txt_bathroom'),
				'floor_area'		=> $this->request->getPost('txt_size'),
				'floor_level'		=> $this->request->getPost('txt_floor_level'),
				'furnish_type'		=> $this->request->getPost('txt_furnished') + 0,
				'offer_type'		=> $this->request->getPost('txt_propertyStatus'),
				'parking'			=> $this->request->getPost('txt_parking') + 0,
				'property_type'		=> $this->request->getPost('txt_propertyType'),
				'subcategory'		=> $this->request->getPost('txt_subCategory'),
				'price'				=> $this->request->getPost('txt_price'),
				'property_vid_url'	=> $this->request->getPost('txt_video'),
				'img_session'		=> $img_data
				//'created_at'		=> date('Y-m-d h:i:s')
			));// insert property record

			$this->amenitiesmod->where('property_id', $property_id)->delete(); // delete amenities

			$amenities = $this->request->getPost('amenities');

			if (!empty($amenities)) {

				foreach ($amenities as $row) {
				
					$this->amenitiesmod->insert(array(
						'property_id' 			=> $property_id, 
						'amenity_property_id'	=> $row
					)); // create the amenities of the property

				}

			}


			session()->setFlashdata("user_alert", array(
				"type" 		=> "success",
				"initMsg" 	=> "Well Done",
				"msg"		=> "Record successfully saved changes."
			));

			return redirect()->to(base_url("pages/my_listing"));
		
		}
		else{

			session()->setFlashdata("user_alert", array(
				"type" 		=> "danger",
				"initMsg" 	=> "Oops",
				"msg"		=> $validator->listErrors()
			));
			return_e_inp:

			return redirect()->to(base_url("pages/submit_property"));

		}

	}

	public function delete_property(){

		$this->propertiesmod->delete($this->request->getPost('txt_property_id'));// delete user record

		session()->setFlashdata("user_alert", array(
			"type" 		=> "success",
			"initMsg" 	=> "Well Done",
			"msg"		=> "Record deleted successfully."
		));

		return redirect()->to(base_url("pages/my_listing"));

	}

	//--------------------------------------------------------------------

	private function email_contructor( $user, $Message )
	{
		$page_URL = base_url();
		$user = ucfirst($user);
		$message  = <<<HTML_EMAIL

<html>
<body>

<table cellspacing="0" cellpadding="10" style="margin: auto">
	<thead style="background-color: #2B7DA2">
		<tr>
			<td style="background-color: #fff;"><img src="https://fretrato.com.ph/assets/fretrato/Images/fretrato-logo-transparent.png" alt="logo" style="width: 200px;"/></td>
			<td width="80%">
				<h1 style="color: #fff"><b>Fretrato | Your Real Estate Buddy</b></h1>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2">
				<p>
					{$user},
					<br>
					<br>
					<br>

					&emsp;{$Message}
					
					<br>
					<br>
					Regards,<br>
					Fretrato Team
				</p>
			</td>
		</tr>
	</tbody>
	<tfoot style="background-color: #2B7DA2; color: #fff">
		<tr>
			<td style="text-align: center" colspan="2">
				<p style="width: 100%; min-height: 100px;padding: 10px;">
					<br>
					Cityland 10 Tower II Corner Valero and HV Dela Costa Street Salcedo Village Makati City, Metro Manila, Philippines
					<br>
					+63 917 677 8878
					<br>
					(02) 7906 1307 (02) 8961 9102
					<br>
					© 2018 <a href="https://fretrato.com.ph" style="color: #FFD700">Fretrato Realty</a>
				</p>
			</td>
		</tr>
	</tfoot>
</table>


</body>
</html>
HTML_EMAIL;


		return $message;
	}

}
