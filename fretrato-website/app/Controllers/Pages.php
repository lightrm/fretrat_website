<?php namespace App\Controllers;


class Pages extends BaseController
{
	private $pagesmod;

	public function __construct()
	{
		$this->pagesmod = model('\App\Models\Pagesmod');
		$this->usermod = model('\App\Models\Usermod');
		$this->propertiesmod = model('\App\Models\Propertiesmod');
		$this->blogsmod = model('\App\Models\Blogsmod');
		$this->listingmod = model('\App\Models\Listingmod');
	}

	public function index()
	{

		//$bodyData['cities'] = $this->pagesmod->get_cities();
		$bodyData['propertyDatabd'] = $this->propertiesmod->get_properties(6, 0, false, "true_toogle"); // limit, offset, pagination, featured

		$bodyData['employees'] = $this->usermod->get_user_by_type(2); // type
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['WAYLF_html'] = $this->pagesmod->web_config("web_config", "single", "WAYLF_html");
		$bodyData['WAYLF_C_html'] = $this->pagesmod->web_config("web_config", "single", "WAYLF-C_html");
		$bodyData['WAYLF_H_html'] = $this->pagesmod->web_config("web_config", "single", "WAYLF-H_html");
		$bodyData['WAYLF_AC_html'] = $this->pagesmod->web_config("web_config", "single", "WAYLF-AC_html");
		$bodyData['WAYLF_S247_html'] = $this->pagesmod->web_config("web_config", "single", "WAYLF-S247_html");
		$bodyData['MPP_html'] = $this->pagesmod->web_config("web_config", "single", "MPP_html");
		$bodyData['LBHE_html'] = $this->pagesmod->web_config("web_config", "single", "LBHE_html");
		$bodyData['MOA_html'] = $this->pagesmod->web_config("web_config", "single", "MOA_html");

		$locations = array('makati', 'taguig', 'pasig', 'alabang', 'quezon', 'pasay');

		foreach ($locations as $row) {
			$bodyData[$row] = $this->propertiesmod->get_location_num($row);
		}

		return parent::basic_page("landing_page", $bodyData);

	}

	public function contact_us()
	{

		$bodyData['banner_title'] = "Contact Us";
		$bodyData["breadCrumps"] = array(
			"Contact Us" => base_url("pages/contact_us")
		);

		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['contact_us'] = $this->pagesmod->web_config("web_config", "single", "contact_us");
		$bodyData['address_html'] = $this->pagesmod->web_config("web_config", "single", "address_html");
		$bodyData['email_html'] = $this->pagesmod->web_config("web_config", "single", "email_html");
		$bodyData['contact_html'] = $this->pagesmod->web_config("web_config", "single", "contact_html");
		$bodyData['landline_html'] = $this->pagesmod->web_config("web_config", "single", "landline_html");
		$bodyData['featured_properties'] = $this->propertiesmod->get_property_by_user_id();

		$bodyData['content'] = $this->pagesmod->web_config("contact_us_content", "single");
		$bodyData['social_medias']  = $this->pagesmod->web_config("social_media");
		return parent::basic_page("contact_us", $bodyData);

	}

	public function about_us()
	{
		$bodyData['banner_title'] = "About Us";
		$bodyData["breadCrumps"] = array(
			"About Us" => base_url("pages/about_us")
		);
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$img_session = $this->pagesmod->web_config("webconfig", "single", "About_Us_img_session");
		;
		$bodyData['img_thumbnail'] = $this->propertiesmod->get_property_gallery($img_session['value'], "Thumbnail");
		$bodyData['content'] = $this->pagesmod->web_config("about_us_content", "single");
		return parent::basic_page("about_us", $bodyData);
	}

	public function privacy_policy()
	{
		$bodyData['banner_title'] = "Privacy Policy";
		$bodyData["breadCrumps"] = array(
			"Privacy Policy" => base_url("pages/privacy_policy")
		);
		$img_session = $this->pagesmod->web_config("webconfig", "single", "Privacy_Policy_img_session");

		$bodyData['img_thumbnail'] = $this->propertiesmod->get_property_gallery($img_session['value'], "Thumbnail");
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['landline_html'] = $this->pagesmod->web_config("web_config", "single", "landline_html");
		$bodyData['privacy_policies'] = $this->pagesmod->web_config("web_config", "single", "privacy_policies");

		return parent::basic_page("privacy_policy", $bodyData);
	}

	public function terms_of_use()
	{
		$bodyData['banner_title'] = "Terms of Use";
		$bodyData["breadCrumps"] = array(
			"Terms of Use" => base_url("pages/terms_of_use")
		);
		$img_session = $this->pagesmod->web_config("webconfig", "single", "Terms_and_Condition_img_session");
		;
		$bodyData['img_thumbnail'] = $this->propertiesmod->get_property_gallery($img_session['value'], "Thumbnail");
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['terms_and_condition'] = $this->pagesmod->web_config("web_config", "single", "terms_and_condition");
		
		return parent::basic_page("terms_of_use", $bodyData);
	}

	public function property_list()
	{

		helper('text');

		$bodyData['banner_title'] = "Property List";
		$bodyData["breadCrumps"] = array(
			"Property List" => base_url("property_list")
		);


		//$bodyData['cities'] = $this->pagesmod->get_cities();
		$bodyData['propertyDatabd'] = $this->propertiesmod->get_properties(10, $this->request->getGet('page'), true, "order_by"); // limit, offset, pagination, featured

		$bodyData['main_categories_ctr'] = $this->propertiesmod->get_main_categ_ctr();
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['sub_categories'] = $this->propertiesmod->get_sub_categ($this->request->getGet('type'));
		$bodyData['total_rec'] = $this->propertiesmod->get_properties(0, 0, false, "count"); // limit, offset, returnType, id
        $this->propertiesmod->search_pagination()->paginate(10, 'default', $this->request->getGet('page'));
        $bodyData['pager'] = $this->propertiesmod->pager;

		return parent::basic_page("property_list", $bodyData);
	}

	public function property_view($property_type, $slug)
	{

		//$bodyData['cities'] = $this->pagesmod->get_cities();
		$bodyData['property'] = $this->propertiesmod->get_property($slug);

		if( empty($bodyData['property']) ) {
			$this->bodyData["errorMsg"] = "Sorry, the property you requested may have been moved or deleted.";
			return parent::basic_page("Errors/404", $this->bodyData);
		}
		
		$title = ($bodyData['property']['offer_type'] == "rental" || $bodyData['property']['offer_type'] == "Rental") ? "For Lease" : (($bodyData['property']['offer_type'] == "resale" || $bodyData['property']['offer_type'] == "Resale") ? "For Sale" : "Pre-selling");

		$bodyData['banner_title'] = $title;
		$bodyData["breadCrumps"] = array(
			"Property List" => base_url("property_list"),
			"Property View" => base_url("pages/property_view")
		);

		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['related_properties'] = $this->propertiesmod->get_related_property($bodyData['property']['id'], $bodyData['property']['offer_type'], $bodyData['property']['property_type'], $bodyData['property']['subcategory']);
		$bodyData['property_thumbnail'] = $this->propertiesmod->get_property_gallery($bodyData['property']['img_session'], "Thumbnail");
		$bodyData['property_gallery'] = $this->propertiesmod->get_property_gallery($bodyData['property']['img_session'], "Gallery");
		$bodyData['amenities'] = $this->propertiesmod->get_property_amenities($bodyData['property']['id']);
		$this->headerData['property']  = $bodyData['property'];
		return parent::basic_page("property_view", $bodyData);
	}

	public function blogs_list()
	{
		helper('text');

		//$bodyData['cities'] = $this->pagesmod->get_cities();
		$bodyData['banner_title'] = "Blogs List";
		$bodyData["breadCrumps"] = array(
			"Blogs List" => base_url("pages/blogs_list")
		);
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['blogs_list'] = $this->blogsmod->get_blogs(10, $this->request->getGet('page')); // limit, offset, returnType, id
		$bodyData['blogs_categories'] = $this->blogsmod->get_blogs_categories();
		$bodyData['real_estate_sub_categories'] = $this->blogsmod->get_blogs_sub_categories("plan_your_best_life_now");
		$bodyData['total_rec'] = $this->blogsmod->get_blogs(0, 0, "count"); // limit, offset, returnType, id
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
        $this->blogsmod->search_pagination()->paginate(10, 'default', $this->request->getGet('page'));
        $bodyData['pager'] = $this->blogsmod->pager;

		return parent::basic_page("blogs_list", $bodyData);
	}

	public function blogs_view($id)
	{
		$url_detail = explode("-", $id);
		$id = $url_detail[0];

		helper('text');
		$bodyData['banner_title'] = "Blogs View";
		$bodyData["breadCrumps"] = array(
			"Blogs List" => base_url("pages/blogs_list"),
			"Blogs View" => base_url("pages/blogs_view")
		);
		$bodyData['blogs_categories'] = $this->blogsmod->get_blogs_categories();
		$bodyData['real_estate_sub_categories'] = $this->blogsmod->get_blogs_sub_categories("plan_your_best_life_now");
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['blogs'] = $this->blogsmod->get_blogs(0, 0, "single", $id); // limit, offset, returnType, id
		$bodyData['blogs_categ'] = $this->blogsmod->get_blogs_categ($id); // limit, offset, returnType, id
		$bodyData['blogs_gallery'] = $this->propertiesmod->get_property_gallery($bodyData['blogs']['img_session'], "Gallery");
		$this->headerData['blogs']  = $bodyData['blogs'];

		return parent::basic_page("blogs_view", $bodyData);
	}

	public function agents_list()
	{
		helper('text');

		//$bodyData['cities'] = $this->pagesmod->get_cities();
		$bodyData['banner_title'] = "Agents List";
		$bodyData["breadCrumps"] = array(
			"Agents List" => base_url("pages/agents_list")
		);
		$bodyData['agent_list'] = $this->usermod->get_agents(10, $this->request->getGet('page')); // limit, offset, returnType, id
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['total_rec'] = $this->usermod->get_agents(0, 0, "count"); // limit, offset, returnType, id
        $this->usermod->search_pagination()->paginate(10, 'default', $this->request->getGet('page'));
        $bodyData['pager'] = $this->usermod->pager;
		return parent::basic_page("agents_list", $bodyData);
	}

	public function agents_view($id)
	{
		//$bodyData['cities'] = $this->pagesmod->get_cities();
		$bodyData['banner_title'] = "Agents View";
		$bodyData["breadCrumps"] = array(
			"Agents List" => base_url("pages/agents_list"),
			"Agents View" => base_url("pages/agents_view")
		);
        $agent_detail = explode("-", $id); 
        $id = $agent_detail[0];

		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['featured_properties'] = $this->propertiesmod->get_properties(10, $this->request->getGet('page'), true, "order_by", $id); // limit, offset, pagination, featured

		$bodyData['total_rec'] = $this->propertiesmod->get_properties(0, 0, false, "count", $id); // limit, offset, returnType, id
        $this->propertiesmod->search_pagination()->paginate(10, 'default', $this->request->getGet('page'));
        $bodyData['pager'] = $this->propertiesmod->pager;

		$bodyData['agent'] = $this->usermod->get_agents(0, 0, "single", $id); // limit, offset, returnType, id

		return parent::basic_page("agents_view", $bodyData);
	}

	public function my_listing()
	{

		if( !session()->has("User") ) return redirect()->to("/");

		helper('text');

		$bodyData['banner_title'] = "My Listing";
		$bodyData["breadCrumps"] = array(
			"My Listing" => base_url("pages/my_listing")
		);
		$bodyData['propertyDatabd'] = $this->listingmod->get_my_listing(10, $this->request->getGet('page')); // limit, offset, pagination, featured
		$bodyData['total_rec'] = $this->listingmod->get_my_listing(0, 0, "count"); // limit, offset, returnType, id
        $this->listingmod->search_pagination()->paginate(10, 'default', $this->request->getGet('page'));
        $bodyData['pager'] = $this->listingmod->pager;

		return parent::basic_page("my_listing", $bodyData);

	}

	public function submit_property()
	{

		if( !session()->has("User") ) return redirect()->to("/");

		$bodyData['banner_title'] = "Submit Property";
		$bodyData["breadCrumps"] = array(
			"Submit Property" => base_url("pages/submit_property")
		);
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();

		if (session()->get("inp_propertyType") != null) {
			$bodyData['sub_categories'] = $this->propertiesmod->get_sub_categ(session()->get("inp_propertyType"));
		}

		$bodyData['amenities'] = $this->listingmod->get_property_amenities();

		return parent::basic_page("submit_property", $bodyData);

	}

	public function edit_listing($id)
	{

		if( !session()->has("User") ) return redirect()->to("/");

		$bodyData['banner_title'] = "Edit Listing";
		$bodyData["breadCrumps"] = array(
			"My Listing" => base_url("pages/my_listing")
		);
		$bodyData['property'] = $this->listingmod->get_my_listing(0, 0, "single", $id); // limit, offset, pagination, featured
		$bodyData['thumbnail'] = $this->propertiesmod->get_property_gallery($bodyData['property']['img_session'], "Thumbnail");
		$bodyData['gallery'] = $this->propertiesmod->get_property_gallery($bodyData['property']['img_session'], "Gallery");
		$bodyData['main_categories'] = $this->propertiesmod->get_main_categ();
		$bodyData['sub_categories'] = $this->propertiesmod->get_sub_categ($bodyData['property']['property_type']);
		$bodyData['amenities'] = $this->listingmod->get_property_amenities();
		$bodyData['amenities_property'] = $this->listingmod->get_amenities($id);

		return parent::basic_page("edit_property", $bodyData);

	}

	public function my_profile()
	{

		if( !session()->has("User") ) return redirect()->to("/");

		$bodyData['banner_title'] = "My Profile";
		$bodyData["breadCrumps"] = array(
			"My Profile" => base_url("pages/my_profile")
		);
		return parent::basic_page("my_profile", $bodyData);

	}

	public function change_password()
	{

		if( !session()->has("User") ) return redirect()->to("/");

		$bodyData['banner_title'] = "Change Password";
		$bodyData["breadCrumps"] = array(
			"Change Password" => base_url("pages/change_password")
		);
		return parent::basic_page("change_password", $bodyData);

	}

	public function login()
	{

		if( session()->has("User") ) return redirect()->to("/");

		return parent::basic_page("login");

	}

	public function register()
	{

		if( session()->has("User") ) return redirect()->to("/");

		return parent::basic_page("register");

	}
		
	public function forgot_password()
	{

		if( session()->has("User") ) return redirect()->to("/");

		return parent::basic_page("forgot_password");

	}

	public function reset_password($id)
	{

		if( session()->has("User") ) return redirect()->to("/");

		if(!$this->request->getGet('confirmation_code')) return redirect()->to("/");
		$confirm_code_ctr = $this->usermod->check_credentials_exist("confirmation_code", $this->request->getGet('confimation_code'));

		if ($confirm_code_ctr == 0) {
			return redirect()->to("/");
		}

		$bodyData['id'] = $id;
		$bodyData['confirmation_code'] = $this->request->getGet('confirmation_code');

		return parent::basic_page("reset_password", $bodyData);

	}

	public function verification($id)
	{

		if( session()->has("User") ) return redirect()->to("/");
		
		if(!$this->request->getGet('confirmation_code')) return redirect()->to("/");
		$confirm_code_ctr = $this->usermod->check_credentials_exist("confirmation_code", $this->request->getGet('confirmation_code'), "active", 0);

		if ($confirm_code_ctr == 1) {
			$this->usermod->update($id, array('active' => 1, 'confirmation_code' => null));
		}
		else{
			return redirect()->to("/");
		}

		$bodyData["pluginDir"] = $this->pluginDir;
		$bodyData['hdjs'] = array();
		$bodyData['hdcss'] = array();
		$bodyData['ftjs'] = array();

		return view("verification", $bodyData);

	}

	public function message($type)
	{

		$bodyData["pluginDir"] = $this->pluginDir;
		$bodyData['hdjs'] = array();
		$bodyData['hdcss'] = array();
		$bodyData['ftjs'] = array();

		if ($type == "account_registered") {
			$bodyData['header_msg'] = "Thank You!";
			$bodyData['body_msg'] = "An email has been sent to verify your account.";
			$bodyData['url_txt'] = "Login";
			$bodyData['url_link'] = "/login";
		}
		elseif($type == "reset_password"){
			$bodyData['header_msg'] = "Great!";
			$bodyData['body_msg'] = "An email has been sent to reset the password of your account.";
			$bodyData['url_txt'] = "Home";
			$bodyData['url_link'] = "";
		}
		else{
			$bodyData['header_msg'] = "Well Done!";
			$bodyData['body_msg'] = "Account has been successfully reset password.";
			$bodyData['url_txt'] = "Login";
			$bodyData['url_link'] = "/login";		
		}

		return view("message_page", $bodyData);

	}

	public function sub_category($id, $option = null)
	{

		echo json_encode($this->propertiesmod->get_sub_categ($id, $option)); 

	}

	public function modal_details($slug)
	{
		helper('text');

		$property = $this->propertiesmod->get_property($slug);
		$amenities = $this->propertiesmod->get_property_amenities($property['id']);
		$thumbnail = $this->propertiesmod->get_property_gallery($property['img_session'], "Thumbnail");
		$gallery = $this->propertiesmod->get_property_gallery($property['img_session'], "Gallery");

		echo json_encode(array(
			'description' 	=> word_limiter(strip_tags($property['description']), 20), 
			'amenities' 	=> $amenities, 
			'gallery' 		=> $gallery,
			'thumbnail' 	=> $thumbnail
		)); 

	}

	//--------------------------------------------------------------------

}
