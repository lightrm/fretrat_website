<?php namespace App\Controllers\Errors;

use App\Controllers\BaseController;

class Errorctl extends BaseController
{
	private $errorDir = "errors/";

	public function index()
	{	
		$this->headerData["errorMsg"] = "Sorry, the page you requested may have been moved or deleted.";
		echo view($this->errorDir."404", $this->headerData);
	}
	//--------------------------------------------------------------------

}
