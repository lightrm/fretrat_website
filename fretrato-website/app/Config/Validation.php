<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------

	public $user_form = [
        'txtname' 		=> ['label' => "Name", 'rules' => 'required'],
		'txtusername' 	=> ['label' => "Username", 'rules' => 'required|is_unique[users.username]', "errors" => ['is_unique' => "{field} is already taken"]],
		'txtemail' 		=> ['label' => "Email", 'rules' => 'required|valid_email|is_unique[users.email]', "errors" => ['is_unique' => "{field} is already taken"]],
		'txtpassword' 	=> ['label' => "Password", 'rules' => 'required'],
		'txtpassword2' 	=> ['label' => "Confirmation Password", 'rules' => 'required|matches[txtpassword]',],
		'imgThumb'		=> ['label' => 'Thumbnail', 'rules' => 'is_image[imgThumb]'],
    ];

    public $propety_form = [
        'txtname' 			=> ['label' => "Property Name", 'rules' => 'required|is_unique[properties.name]', "errors" => ['is_unique' => "{field} is already taken"]],
		'txtprice' 			=> ['label' => "Price", 'rules' => 'required|integer'],
		'txtsqrmtr' 		=> ['label' => "Square Meter", 'rules' => 'required|integer'],
		'txtbedroom' 		=> ['label' => "Bedroom", 'rules' => 'required|integer'],
		'slcpropertytype' 	=> ['label' => "Property Type", 'rules' => 'required'],
		'rdpropertytype' 	=> ['label' => "Offer Type", 'rules' => 'required'],
		'txtstreet' 		=> ['label' => "Street", 'rules' => 'required'],
		'txtcity' 			=> ['label' => "City", 'rules' => 'required'],
		'txtcountry' 		=> ['label' => "Country", 'rules' => 'required'],
		'imgThumb'			=> ['label' => 'Thumbnail', 'rules' => 'is_image[imgThumb]'],
		'imgGallery'		=> ['label' => 'Thumbnail', 'rules' => 'is_image[imgThumb]'],
    ];
}
