<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override("App\Controllers\Errors\Errorctl::index");
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('/', 'Pages::index');
$routes->get('property_list', 'Pages::property_list');
$routes->get('(:any)/property_view/(:any)', 'Pages::property_view/$1/$2');
$routes->get('agents_list', 'Pages::agents_list');
$routes->get('agents_view/(:any)', 'Pages::agents_view/$1');

// API NEEDED TO GET IMPORTANT DETAILS
$routes->group('/request', ['hostname' => 'localfretrato.com.ph', 'namespace' => 'App\Controllers\Api'], function($routeReq)
{
	$routeReq->add('(:any)', 'Datarequestctl::$1');
	$routeReq->add('(:any)/(:any)', 'Datarequestctl::$1/$2');
	$routeReq->add('(:any)/(:any)/(:any)', 'Datarequestctl::$1/$2/$3');

});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
