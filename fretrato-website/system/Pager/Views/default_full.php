<?php

/**
 * @var \CodeIgniter\Pager\PagerRenderer $pager
 */

$pager->setSurroundCount(2);
?>

<div class="col-lg-12">
    <div class="pagination-box hidden-mb-45 text-center">
		<nav aria-label="Page navigation example <?= lang('Pager.pageNavigation') ?>">
			<ul class="pagination">
				<?php if ($pager->hasPrevious()) : ?>
					<li class="page-item">
						<a class="page-link" href="<?= $pager->getFirst() ?>" aria-label="<?= lang('Pager.first') ?>">
							<span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
						</a>
					</li>
					<li class="page-item" hidden>
						<a class="page-link" href="<?= $pager->getPrevious() ?>" aria-label="<?= lang('Pager.previous') ?>">
							<span aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
						</a>
					</li>
				<?php endif ?>

				<?php foreach ($pager->links() as $link) : ?>
					<li <?= $link['active'] ? 'class="page-item"' : 'class="page-item"' ?>>
						<a <?= $link['active'] ? 'class="page-link active"' : 'class="page-link"' ?> href="<?= $link['uri'] ?>">
							<?= $link['title'] ?>
						</a>
					</li>
				<?php endforeach ?>

				<?php if ($pager->hasNext()) : ?>
					<li class="page-item" hidden>
						<a class="page-link" href="<?= $pager->getNext() ?>" aria-label="<?= lang('Pager.next') ?>">
							<span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
						</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="<?= $pager->getLast() ?>" aria-label="<?= lang('Pager.last') ?>">
							<span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
						</a>
					</li>
				<?php endif ?>
			</ul>
		</nav>
	</div>
</div>
